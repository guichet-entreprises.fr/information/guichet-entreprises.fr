@ECHO off
REM # -----------------------------------------------------------------------------
REM # 
REM # Le référentiel d'information de Guichet Entreprises est mis à disposition
REM # selon les termes de la licence Creative Commons Attribution - Pas de
REM # Modification 4.0 International.
REM #
REM # Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
REM # suivante :
REM # http://creativecommons.org/licenses/by-nd/4.0/
REM # ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
REM # Mountain View, California, 94041, USA.
REM # 
REM # -----------------------------------------------------------------------------
CALL %*
GOTO EOF
REM -------------------------------------------------------------------------------
:PRINT_LOGO
(
    SET "COMMON=%~dp0/common.bat"
    SETLOCAL EnableDelayedExpansion
    CALL !COMMON! :PRINT_LINE "╔══════════════════════════════════════════════════════════════════════════════════════════════════╗"
    CALL !COMMON! :PRINT_LINE "║                               __      ____      ____      __                                     ║"
    CALL !COMMON! :PRINT_LINE "║                               \ \ /\ / /\ \ /\ / /\ \ /\ / /                                     ║"
    CALL !COMMON! :PRINT_LINE "║                                \ V  V /  \ V  V /  \ V  V /                                      ║"
    CALL !COMMON! :PRINT_LINE "║                                 \_/\_/    \_/\_/    \_/\_/                                       ║"
    CALL !COMMON! :PRINT_LINE "║                                                                                                  ║"
    CALL !COMMON! :PRINT_LINE "╚══════════════════════════════════════════════════════════════════════════════════════════════════╝"
    ENDLOCAL
    exit /b
)
:EOF