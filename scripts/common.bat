@ECHO off
REM # -----------------------------------------------------------------------------
REM # 
REM # Le référentiel d'information de Guichet Entreprises est mis à disposition
REM # selon les termes de la licence Creative Commons Attribution - Pas de
REM # Modification 4.0 International.
REM #
REM # Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
REM # suivante :
REM # http://creativecommons.org/licenses/by-nd/4.0/
REM # ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
REM # Mountain View, California, 94041, USA.
REM # 
REM # -----------------------------------------------------------------------------
CALL %*
GOTO EOF
REM -------------------------------------------------------------------------------
:PRINT_LINE <textVar>
(
    SET "LINE_TO_PRINT=%~1"
    SETLOCAL EnableDelayedExpansion
    @ECHO !LINE_TO_PRINT!
    ENDLOCAL
    exit /b
)
REM -------------------------------------------------------------------------------
:CONFIGURE_DISPLAY
(
    CHCP 65001
    MODE 100,40
    exit /b
)
REM -------------------------------------------------------------------------------
:CLEAR_SCREEN
(
	CLS
    CALL :PRINT_LINE "╔══════════════════════════════════════════════════════════════════════════════════════════════════╗"
    CALL :PRINT_LINE "║                      ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗┌┐┌┌┬┐┬─┐┌─┐┌─┐┬─┐┬┌─┐┌─┐┌─┐                        ║"
    CALL :PRINT_LINE "║                      ║ ╦│ │││  ├─┤├┤  │   ║╣ │││ │ ├┬┘├┤ ├─┘├┬┘│└─┐├┤ └─┐                        ║"
    CALL :PRINT_LINE "║                      ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝┘└┘ ┴ ┴└─└─┘┴  ┴└─┴└─┘└─┘└─┘                        ║"
    CALL :PRINT_LINE "╚══════════════════════════════════════════════════════════════════════════════════════════════════╝"
    IF EXIST "%~dp0/logo.bat" (
        CALL "%~dp0/logo.bat" :PRINT_LOGO
    )
    exit /b
)
REM -------------------------------------------------------------------------------
:LINE_BREAK
(
	CALL :PRINT_LINE "├──────────────────────────────────────────────────────────────────────────────────────────────────┤"
    exit /b
)
REM -------------------------------------------------------------------------------
:UPDATE_PIP
(
    python -V
    pip -V
    python -m pip install --upgrade pip wheel setuptools
    exit /b
)
REM -------------------------------------------------------------------------------
:INSTALL_REQUIREMENTS <requirementsFile>
(
    SET "REQUIRE_FILE=%~1"
    SETLOCAL EnableDelayedExpansion
    CALL :PRINT_LINE "   Install requirements !REQUIRE_FILE!" 
    CALL :UPDATE_PIP
    pip install -r !REQUIRE_FILE!
    exit /b
)
REM -------------------------------------------------------------------------------
:INSTALL_EDITABLE
(
    CALL :PRINT_LINE "   Install editable version" 
    CALL :UPDATE_PIP
    pip install -e .
    exit /b
)
REM -------------------------------------------------------------------------------
:PYTHON_SETUP <setupAction>
(
    SET "SETUP_ACTION=%~1"
    SETLOCAL EnableDelayedExpansion
    CALL :PRINT_LINE "   Launch python setup !SETUP_ACTION!" 
    python setup.py !SETUP_ACTION!
    exit /b
)
REM -------------------------------------------------------------------------------
:PYTHON_LAUNCH <filename>
(
    SET "PY_FILE=%~1"
    SETLOCAL EnableDelayedExpansion
    CALL :PRINT_LINE "   python !PY_FILE!" 
    python !PY_FILE!
    exit /b
)
REM -------------------------------------------------------------------------------
:UPDATE_POT
(
    CALL :PRINT_LINE "   update POT" 
    SET LOCALE_FOLDER_PATH=src\\locale
    SET GETTEXT_PATH=%~dp0\\third_party_software\\gettext\\bin
    SET MSGMERGE_CMD="%GETTEXT_PATH%\\msgmerge.exe"
    SET XGETTEXT_CMD="%GETTEXT_PATH%\\xgettext.exe"
    SET MSGFMT_CMD="%GETTEXT_PATH%\\msgfmt.exe"
    SET XGETTEXT_ARG1=--language=Python --from-code=UTF-8 --add-comments="TRANSLATORS:"
    SET XGETTEXT_ARG2=--copyright-holder="Guichet Entreprises" --package-name="xenon" --package-version="2"
    SET XGETTEXT_ARG3=--keyword=gettext --keyword=ugettext --keyword=dgettext:2 --keyword=ngettext:1,2 --keyword=ungettext:1,2 --keyword=dngettext:2,3 --keyword=_ --keyword=N_
    SET OUTPUT_FILE=--output="%~dp0\\%LOCALE_FOLDER_PATH%\\xenon.pot"

    SET XGETTEXT_INVOKE=%XGETTEXT_CMD% %XGETTEXT_ARG1% %XGETTEXT_ARG2% %XGETTEXT_ARG3% %OUTPUT_FILE%

    ECHO ------------------------------------------
    ECHO Read all new message to translate and add to pot
    ECHO ------------------------------------------
    cd xe2layout
    FOR %%f IN (*.yml) DO (
        @ECHO %%f
        %XGETTEXT_INVOKE% --join-existing --directory="%~dp0\\xe2layout" "%%f"
    )

    ECHO ------------------------------------------
    ECHO Update the PO merge xith POT
    ECHO ------------------------------------------
    CD /D "%~dp0\\%LOCALE_FOLDER_PATH%\\"
    FOR /R %%f IN (*.po) DO (
        @ECHO %%f
        %MSGMERGE_CMD%  "%%f" "%~dp0\\%LOCALE_FOLDER_PATH%\\xenon.pot" --output-file="%%f"    
        %MSGFMT_CMD%  "%%f" --output-file="%%~pf%%~nf.mo"
    )
    exit /b
)
REM -------------------------------------------------------------------------------
:XENON_GENERATE <filename>
(
    SET "XENON_YML=%~dpnx1"
    SETLOCAL EnableDelayedExpansion

    SET "LAUNCHER=xenon2_launcher.bat"
    CALL :PRINT_LINE "   Looking for the Xenon2 launcher !LAUNCHER!"
    SET "FOLDER=C:\Program Files (x86)\ge.fr\xenon2"
    SET "TO_TEST=!FOLDER!\!LAUNCHER!"
    CALL :PRINT_LINE "   Test the path:!TO_TEST!" 
    IF EXIST "!TO_TEST!" (
        SET "XENON2_CMD=!TO_TEST!"        
        CALL :PRINT_LINE "   Found:!XENON2_CMD!" 
    )

    IF NOT DEFINED XENON2_CMD (
        CALL :PRINT_LINE "   Xenon2 is not found" 
        exit /b
    )

    CALL :PRINT_LINE "   !XENON2_CMD! !XENON_YML!" 
    !XENON2_CMD! !XENON_YML!
    exit /b
)
REM -------------------------------------------------------------------------------
:PUSH_REPO <options>
(
    CALL :PRINT_LINE "   push to repo" 
    CALL :PRINT_LINE "   VERSION=%VERSION%" 
    SET "OPTIONS=%~1"
    
    SETLOCAL EnableDelayedExpansion
    SET "LAUNCHER=repotools.exe"
    CALL :PRINT_LINE "   Looking for the repotools: !LAUNCHER!"
    SET "FOLDER=C:\Program Files (x86)\ge.fr\repotools"
    SET "TO_TEST=!FOLDER!\!LAUNCHER!"
    CALL :PRINT_LINE "   Test the path:!TO_TEST!" 
    IF EXIST "!TO_TEST!" (
        SET "REPO_CMD=!TO_TEST!"        
        CALL :PRINT_LINE "   Found:!REPO_CMD!" 
    )

    IF NOT DEFINED REPO_CMD (
        CALL :PRINT_LINE "   Repotools is not found" 
        exit /b
    )

    CALL :PRINT_LINE "   COMMAND:!REPO_CMD!" 

    "!REPO_CMD!" !OPTIONS! --release-version %VERSION% --filename "%MYPATH%build\www" --extra-file=../*.conf --jira --slack

    exit /b
)
:EOF
