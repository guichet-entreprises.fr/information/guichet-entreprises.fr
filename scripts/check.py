﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Le référentiel d'information de Guichet Entreprises est mis à disposition
# selon les termes de la licence Creative Commons Attribution - Pas de
# Modification 4.0 International.

# Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
# suivante :
# http://creativecommons.org/licenses/by-nd/4.0/
# ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
# Mountain View, California, 94041, USA.
# -----------------------------------------------------------------------------

import logging
import sys
import os

from pygefr import core
from pymdtools import translate as trans

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
# -----------------------------------------------------------------------------
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
# -----------------------------------------------------------------------------
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]

# -----------------------------------------------------------------------------
# Set up the logging system
# -----------------------------------------------------------------------------
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

# -----------------------------------------------------------------------------
# Main script call only if this script is runned directly
# -----------------------------------------------------------------------------
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    # langs = ['pt', 'es', 'it', 'de', 'fr', 'ru']
    langs = ['en']
    # langs = ['fr']
    fun_list = []
    # fun_list = [core.var_ge, core.update_md, core.check_newline]
    # fun_list = [core.set_translation_none, core.set_lang_fr, update_md, check_newline]
    # fun_list = [core.check_newline]
    # fun_list.append(core.var_ge)
    # fun_list.append(core.check_key)
    # fun_list.append(core.check_last_update('fr'))
    # fun_list.append(core.set_translation_none)
    # fun_list.append(core.set_lang_fr)
    fun_list.append(core.set_lang("en"))
    fun_list.append(core.set_translation_pro)
    # fun_list.append(core.check_newline)
    # fun_list.append(core.update_md)

    for lang in langs:
        core.process_ge_content(fun_list, lang, save=True, backup=False)

    # core.process_gq_content(core.check_absolute_link, "fr", key_filter="__error__",
    #                         save=True, backup=False)

    # for lang in langs:
    #     core.process_ge_content(core.translation_fun(lang, overwrite=True),
    #                             "fr", key_filter="",
    #                             save=True, backup=False)

    # for lang in langs:
    #     core.process_gq_content(core.update_md, lang,
    #                             key_filter="__error__",
    #                             save=True, backup=False)

    # core.reset_internal_links("es", "fr", save=True, backup=False)
    # core.compare_keys("en", "fr")

    # for lang in langs:
    # core.translate(lang)
    # core.translate('ru')

    # core.refresh_translation('pt')

    logging.info('Finished')
    # ------------------------------------


# -----------------------------------------------------------------------------
# Call main function if the script is main
# Exec only if this script is runned directly
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    __set_logging_system()
    __main()
