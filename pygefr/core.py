#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#   All Rights Reserved.
#   Unauthorized copying of this file, via any medium is strictly prohibited
#   Dissemination of this information or reproduction of this material
#   is strictly forbidden unless prior written permission is obtained
#   from Guichet Entreprises.
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Common functions for the content managment.
# -----------------------------------------------------------------------------
import logging
import sys
import os
import os.path
import copy
import re

from pymdtools import common
from pymdtools import mdcommon
from pymdtools import mdfile
from pymdtools import instruction
import pymdtools.translate as trans
from pymdtools.mdfile import MarkdownContent
from pygereference import check as refcheck


# -----------------------------------------------------------------------------
def short_name(name):
    name = common.str_to_ascii(name)
    name = name.replace("'", " ")
    return common.slugify(common.limit_str(name, 30, ' ', min_last_word=3))

# -----------------------------------------------------------------------------
def url_name(name):
    name = common.str_to_ascii(name)
    name = name.replace("'", " ")
    return common.slugify(common.limit_str(name, 60, ' ', min_last_word=3))

# -----------------------------------------------------------------------------
def create_key(filename, content_path=None):
    if content_path is None:
        content_path = os.path.join(__get_this_folder(), "..", "content")

    local_path = os.path.splitext(os.path.relpath(filename, content_path))[0]

    local_path = local_path.replace(os.sep, " ")
    local_path = common.str_to_ascii(local_path)
    local_path = local_path.replace("'", " ")
    local_path = common.slugify(local_path)
    return local_path

# -----------------------------------------------------------------------------
def translate_frag_filename(fragment, lang_dest, lang_start="fr"):
    fragment = fragment.strip()

    if fragment.startswith("__") and fragment.endswith("__"):
        return fragment

    if fragment in ['index', 'menu']:
        return fragment

    if fragment.isdecimal():
        return fragment

    if fragment == lang_start:
        return lang_dest

    fragment = fragment.replace("_", " ")
    result = trans.translate_txt(fragment, src=lang_start, dest=lang_dest)
    result = url_name(result)
    result = result.replace("-", "_")
    return result

# -----------------------------------------------------------------------------
def translate_filename(filename, lang_dest,
                       content_path=None, lang_start="fr"):
    if content_path is None:
        content_path = os.path.join(__get_this_folder(), "..", "content")

    (local_path, extension) = \
        os.path.splitext(os.path.relpath(filename, content_path))

    destination = ""
    for fragment in local_path.split(os.sep):
        new_frag = translate_frag_filename(fragment, lang_dest)
        destination = os.path.join(destination, new_frag)

    return destination + extension

# -----------------------------------------------------------------------------
def check_disclaimer(md_content):
    content = md_content.content
    references = instruction.refs_in_md_text(content)

    lang = md_content['lang'].lower()
    ref_disclaimer_pro = "disclaimer-eu-" + lang
    ref_disclaimer_auto = "disclaimer-trans-" + lang

    if md_content['translation'].lower() == "none":
        if ref_disclaimer_pro not in references and \
                ref_disclaimer_auto not in references:
            logging.info("    --> everything is OK")
            return md_content
        logging.warning("    --> Bad reference")
        return md_content

    refid = ""
    if md_content['translation'].lower()[0:3] == "pro":
        refid = ref_disclaimer_pro
    else:
        refid = ref_disclaimer_auto

    logging.info("    ref is %s", refid)

    if refid in references:
        logging.info("    --> already done")
        return md_content

    match = instruction.get_title_from_md_text(content, return_match=True)
    match = match.group(0).lstrip()
    content = content.replace(match, match + "\n<!-- begin-include(%s) -->"
                              "\n<!-- end-include -->\n" % refid, 1)
    logging.info("    --> done")
    md_content.content = content
    return md_content


# -----------------------------------------------------------------------------
# Translate a file
#
# @return the MD.
# -----------------------------------------------------------------------------
def translate_files(md_content, lang_dest, lang_start="fr",
                    replace_list=None,
                    overwrite=False):
    filename = md_content.full_filename
    logging.info("Start tranlation of the file %s", filename)
    content_path = os.path.join(__get_this_folder(), "..", "content")

    if lang_dest is None:
        lang_dest = trans.eu_lang_list()
    if not isinstance(lang_dest, list):
        lang_dest = [lang_dest]

    for lang in lang_dest:
        if lang == lang_start:
            continue
        logging.info("lang=%s", lang)

        dest_filename = translate_filename(filename, lang,
                                           content_path=content_path,
                                           lang_start=lang_start)
        dest_filename = os.path.join(content_path, dest_filename)
        dest_filename = common.set_correct_path(dest_filename)

        logging.info("destination=%s", lang)

        if os.path.isfile(dest_filename):
            if not overwrite:
                logging.info(" --> Already done")
                continue

            dest_md = mdfile.MarkdownContent(filename=dest_filename)
            if dest_md['translation'] != "Auto":
                continue

        logging.info("Translate for %s into file %s", lang, dest_filename)
        result_trans = trans.translate_md(md_content.content,
                                          src=lang_start, dest=lang)

        result = mdfile.MarkdownContent(
            filename=dest_filename, content=result_trans)
        result['site:lang'] = lang
        result['lang'] = lang
        result['translation'] = "Auto"
        result['last-update'] = common.timestamp_now()
        if 'key' in md_content:
            result['key'] = md_content['key']

        result = var_ge(result)
        if not result.filename.startswith('__'):
            result = check_disclaimer(result)
        result.set_include_file("generated.txt")

        if replace_list is not None:
            for rplc in replace_list:
                result.content = result.content.replace(
                    rplc[0], rplc[1] % lang)

        common.check_create_folder(result.filename_path)

        result.write()

# -----------------------------------------------------------------------------
# Translate a file
#
# @return the MD.
# -----------------------------------------------------------------------------
def translation_fun(lang_dest, overwrite=False, lang_start="fr"):

    def result(md_content):
        translate_files(md_content, lang_dest,
                        lang_start=lang_start,
                        replace_list=[("/%s/" % lang_start, "/%s/")],
                        overwrite=overwrite)
        return md_content

    return result

# -----------------------------------------------------------------------------
# Translate a file
#
# @return the MD.
# -----------------------------------------------------------------------------
def check_last_update(lang):
    def result(md_content):
        return refcheck.check_last_update(md_content, lang=lang)
    return result

# -----------------------------------------------------------------------------
# List_key
#
# @return the MD.
# -----------------------------------------------------------------------------
def list_key(memory):
    internal_memory = memory
    content_path = os.path.join(__get_this_folder(), "..", "content")

    def result(md_content):
        if 'key' not in md_content:
            return md_content

        key = md_content['key']
        local_path = os.path.relpath(md_content.full_filename, content_path)

        if key in internal_memory:
            logging.error('Two identical key "%s" : %s and %s',
                          key, local_path, internal_memory[key])
            raise Exception('Two identical key "%s" : %s and %s' %
                            (key, local_path, internal_memory[key]))

        internal_memory[key] = local_path
        return md_content

    return result


# -----------------------------------------------------------------------------
# check and change the include file in the header
#
# @return the MD.
# -----------------------------------------------------------------------------
def check_newline(md_content):
    result = md_content.content
    result = result.replace("\r\n", "\n")
    result = result.replace("\n\n\n", "\n\n")
    result = result.replace("\n\n\n\n", "\n\n")
    result = result.replace("\n\n\n\n\n", "\n\n")
    result = result.replace("->\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n\n\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n\n\n\n\n<!-", "->\n<!-")

    md_content.content = result.strip()

    return md_content

# -----------------------------------------------------------------------------
# Process tags in md
# -----------------------------------------------------------------------------
def update_md(md_content):
    md_content.process_tags()
    return md_content

# -----------------------------------------------------------------------------
# Process tags in md
# -----------------------------------------------------------------------------
def check_absolute_link(md_content):
    md_content['page:absolute_link'] = "True"
    return md_content

# -----------------------------------------------------------------------------
# Process tags in md
# -----------------------------------------------------------------------------
def print_filename(md_content):
    print(md_content.full_filename)

# -----------------------------------------------------------------------------
# Process tags in md
# -----------------------------------------------------------------------------
def set_translation_none(md_content):
    if 'translation' not in md_content:
        md_content['translation'] = "None"
    return md_content

# -----------------------------------------------------------------------------
# Process tags in md
# -----------------------------------------------------------------------------
def set_translation_pro(md_content):
    if 'translation' not in md_content:
        md_content['translation'] = "Pro"
    return md_content

# -----------------------------------------------------------------------------
# Process tags in md
# -----------------------------------------------------------------------------
def set_translation_auto(md_content):
    if 'translation' not in md_content:
        md_content['translation'] = "Auto"
    return md_content

# -----------------------------------------------------------------------------
# Process tags in md
# -----------------------------------------------------------------------------
def set_lang_fr(md_content):
    if 'lang' not in md_content:
        md_content['lang'] = "fr"
    return md_content

def set_lang(lang):
    language = lang

    def internal_fun(md_content):
        if 'lang' not in md_content:
            md_content['lang'] = language
        return md_content
    return internal_fun

# -----------------------------------------------------------------------------
# Process tags in md
# -----------------------------------------------------------------------------
def check_key(md_content):
    if 'key' in md_content:
        return md_content

    the_key = create_key(md_content.full_filename)

    if the_key[0:2] == "en":
        the_key = "fr" + the_key[2:]

    if the_key[-2:] == "en":
        the_key = the_key[:-2]

    md_content['key'] = the_key

    logging.info('key "%s" set for %s', the_key, md_content.full_filename)

    return md_content

# -----------------------------------------------------------------------------
# check and change var in md
# -----------------------------------------------------------------------------
def var_ge(md_content):
    md_content.del_include_file("ge.txt")
    md_content.del_include_file("gq.txt")
    md_content.del_include_file("gp.txt")
    md_content.del_include_file("license.txt")
    md_content.del_include_file("license.en.txt")
    md_content.del_include_file("license-short.txt")
    md_content.set_include_file("ge.txt")
    md_content.set_include_file("license-short.txt")

    target_value = {
        'page:description': "",
        'page:keywords': "",
        'page:author': "Guichet Entreprises",
        'page:title': md_content.title,
    }

    for var in target_value:
        if var not in md_content:
            print("Add the key=%s: value=%s  " % (var, target_value[var]))
            md_content[var] = target_value[var]
            continue

        if md_content[var] == target_value[var]:
            continue

        print("Problem with the key=%s: value=%s  " % (var, md_content[var]))
        print("                 --> should be=%s  " % target_value[var])
        md_content[var] = target_value[var]

    deleted_key = ['author', 'title', 'description', 'keywords']

    for key in deleted_key:
        del md_content[key]

    return md_content


# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
# -----------------------------------------------------------------------------
def process_ge_content(process, lang, save=False, backup=True,
                       key_filter=None):
    include_folder = os.path.join(__get_this_folder(), "..", "content_include")
    include_folder = common.check_folder(include_folder)
    content_folder = os.path.join(__get_this_folder(), "..", "content", lang)
    content_folder = common.check_folder(content_folder)
    logging.info("Content folder %s", content_folder)
    logging.info("--------------------------")

    list_processes = []
    if isinstance(process, list):
        list_processes.extend(process)
    else:
        list_processes.append(process)

    if key_filter is None:
        key_filter = ""
    pattern = re.compile(key_filter)

    def joined_processes(filename):
        if pattern.search(filename) is None:
            return
        logging.info("Update %s", filename)
        md_content = MarkdownContent(filename, backup=backup,
                                     search_folders=[include_folder])
        for process in list_processes:
            process(md_content)

        if save:
            md_content.write()

    common.apply_function_in_folder(content_folder, joined_processes)

# -----------------------------------------------------------------------------
def page_keys(lang):
    result = {}
    process_ge_content([list_key(result)], lang, save=False, backup=False)
    return result

# -----------------------------------------------------------------------------
def compare_keys(lang, lang_start="fr"):
    start_key = page_keys(lang_start)
    dest_key = page_keys(lang)
    for key in dest_key:
        if key not in start_key:
            logging.info("Problem with the key %s", key)
        else:
            logging.info("OK with the key %s", key)

# -----------------------------------------------------------------------------
def reset_internal_links(lang, lang_start="fr", save=False, backup=True):
    start_key = page_keys(lang_start)
    dest_key = page_keys(lang)
    content_path = os.path.join(
        __get_this_folder(), "..", "content")
    content_path = common.set_correct_path(content_path)

    transfert = {}
    for key in start_key:
        if key in dest_key:
            transfert[start_key[key]] = dest_key[key]
        else:
            logging.warning('Orfan key: %s', key)

    # -------------------------------------------------------------------------
    def change_links(md_content):
        if 'key' not in md_content:
            logging.warning('Can not handle links (no key defined) in %s',
                            md_content.full_filename)
            return md_content
        key = md_content['key']

        links = mdcommon.search_link_in_md_text(md_content.content)
        links_change = []
        for link in links:
            if mdcommon.is_external_link(link['url']):
                continue
            new_link = copy.deepcopy(link)
            new_url = os.path.join(content_path,
                                   os.path.split(start_key[key])[0],
                                   link['url']).replace('/', os.sep)
            new_url = common.set_correct_path(new_url)
            new_url = new_url[len(content_path) + 1:]
            new_url = new_url.replace(os.sep + lang + os.sep,
                                      os.sep + lang_start + os.sep)
            if new_url not in transfert:
                logging.warning('The link %s is not handle', link)
                continue
            new_url = os.path.relpath(
                os.path.join(content_path, transfert[new_url]),
                md_content.filename_path).replace(os.sep, '/')
            new_link['url'] = new_url
            links_change.append((link, new_link))
            logging.debug("change link '%s' --> '%s'" %
                          (link, new_link))

        md_content.content = \
            mdcommon.update_links_from_old_link(md_content.content,
                                                links_change)
        return md_content
    # -------------------------------------------------------------------------

    process_ge_content(change_links, lang, save=save, backup=backup)

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
# -----------------------------------------------------------------------------
def translate(lang, lang_start="fr"):
    if lang == lang_start:
        return

    process_ge_content(translation_fun(lang, overwrite=True,
                                       lang_start=lang_start),
                       lang_start, save=True, backup=False)

    reset_internal_links(lang, lang_start, save=True, backup=False)

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
# -----------------------------------------------------------------------------
def refresh_translation(lang, lang_start="fr"):
    if lang == lang_start:
        return

    start_key = page_keys(lang_start)
    dest_key = page_keys(lang)
    content_path = os.path.join(__get_this_folder(), "..", "content")

    for key in dest_key:
        dest_key[key] = os.path.join(content_path, dest_key[key])
    for key in start_key:
        start_key[key] = os.path.join(content_path, start_key[key])

    for key in dest_key:
        if key not in start_key:
            continue
        dest_time = os.path.getmtime(dest_key[key])
        start_time = os.path.getmtime(start_key[key])
        logging.info('Refresh translation %s', dest_key[key])
        if start_time < dest_time:
            logging.info('    -> OK')
            continue
        logging.info('    -> Need a refresh')


# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
# -----------------------------------------------------------------------------
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]


# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
# -----------------------------------------------------------------------------
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result
