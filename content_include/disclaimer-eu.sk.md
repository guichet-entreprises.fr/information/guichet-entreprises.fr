﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="sk" -->

Oznámenie o kvalite prekladu
============================

<!-- begin-ref(disclaimer-eu-sk) -->

**Oznámenie o kvalite prekladu**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Táto stránka bola preložená profesionálne, ale môžu obsahovať chyby. Užívateľom sa odporúča overiť správnosť informácií poskytnutých na tejto stránke, ako členský štát podnikne akékoľvek kroky.
Enterprise Bank Service nemôže byť zodpovedný za prevádzku informácií, ktoré budú nepresné kvôli non-verný preklad originálu.<!-- alert-end:warning -->

<!-- end-ref -->