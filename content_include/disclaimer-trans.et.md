﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="et" -->

Teate kvaliteedi kohta masintõlge
=================================

<!-- begin-ref(disclaimer-trans-et) -->

**Teate kvaliteedi kohta masintõlge**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

See lehekülg on tõlgitud kasutades masintõlge vahend ja võib sisaldada vigu. Kasutajad on soovitatav kontrollida andmete õigsust käesoleval lehel pakutava enne tegevuse.

Enterprise Bank Service ei saa pidada vastutavaks toimimise teavet, mis on ebatäpne, kuna mitte-automaatne tõlge ustav originaal.<!-- alert-end:warning -->

<!-- end-ref -->