﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="cs" -->

Oznámení o kvalitě strojového překladu
======================================

<!-- begin-ref(disclaimer-trans-cs) -->

**Oznámení o kvalitě strojového překladu**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Tato stránka byla přeložena pomocí nástroje strojového překladu a může obsahovat chyby. Uživatelům se doporučuje ověřit správnost informací poskytnutých na této stránce, než členský stát podnikne jakékoli kroky.

Enterprise Bank Service nemůže být zodpovědný za provoz informací, které budou nepřesné kvůli non-automatický překlad věrný originálu.<!-- alert-end:warning -->

<!-- end-ref -->