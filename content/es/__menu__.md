﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="[Inicio](index.md)" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="es-__menu__" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

# [Inicio](index.md)
# Creación de empresa
# <!--include(creation_entreprise/__menu__.md)-->
# Trámites online
# <!--include(demarches_en_ligne/__menu__.md)-->
# Actividades reguladas
# <!-- include(../reference/es/directive-services/_list_menu.md) -->
# [Trabajar en Francia](travailler_en_france.md)
# [Preguntas frecuentes](faq.md)
# [Servicios @toggle.env](https://welcome.guichet-entreprises.fr/)
# [Mis carpetas @visibility.authenticated @toggle.env](https://dashboard.guichet-entreprises.fr/)