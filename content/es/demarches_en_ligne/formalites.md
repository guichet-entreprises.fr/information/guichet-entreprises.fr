﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Trámites online" -->
<!-- var(key)="es-formalites" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

# Trámites online

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

Realizar sus trámites relacionados con el registro (cualquier forma legal), la modificación y el cese de actividad de una empresa.

**Los trámites que puede realizar online en guichet-entreprises.fr:**

* **Registrar una empresa**
* **Modificar la actividad o información de una empresa**
* **Cesar la actividad de una empresa**

## Registra una empresa

Guichet-entreprises.fr le permite crear su archivo en línea. Este último será enviado al organismo competente para su procesamiento. En Francia, los centros de trámites comerciales (CFE) se encargan de recoger su expediente. Son los interlocutores de primer nivel de las empresas y desempeñan el papel de interfaz entre las administraciones y ellas.

**Lista de trámites de creación disponibles en guichet-entreprises.fr:**

- Declaración de inicio de actividad para un agente comercial (formulario AC0)
- Declaración de inicio de actividad liberal para una persona física distinta de un microempresario (formulario P0PL)
- Declaración de inicio de actividad liberal para una persona física como microempresario (formulario P0PL *micro-entrepreneur*)
- Declaración de la creación de una empresa agrícola para una persona física (formulario P0 *agricole*)
- Declaración del inicio de una actividad comercial y / o artesanal para una persona física distinta de un microempresario (formulario P0CM)
- Declaración de inicio de actividad comercial y / o artesanal de una persona física como microempresario (formulario P0CM *micro-entrepreneur*)
- Declaración de creación de una SAS, una SA, una SNC, una SELAFA, una SELAS, una sociedad limitada o una sociedad mercantil extranjera (formulario M0)
- Declaración de incorporación de un SARL o SELARL (formulario M0 SARL/SELARL)
- Declaración de registro de una sociedad civil (formulario M0 *société civile*)

\> [Iniciar el trámite de registro comercial](https://welcome.guichet-entreprises.fr/)

**Lea también:** [Cree una empresa en guichet-entreprises.fr: manual de usuario](../articles/comment_creer_une_entreprise.md)

## Modificar una actividad

Puede en cualquier momento realizar un trámite relacionado con la modificación de su empresa en guichet-entreprises.fr con el fin de actualizar la información relativa a su empresa con todos los órganos competentes (impuestos, Urssaf, Insee, registros públicos, etc.). ). Tenga en cuenta que estos cambios deben declararse dentro de un mes a partir de la fecha en que ocurrieron.

\> [Más información sobre trámites de modificación](formalites_modification.md)

**Lista de trámites de modificación disponible en guichet-entreprises.fr:**

- Declaración de modificación de un agente comercial (formulario AC2)
- Declaración de modificación de una empresa liberal para una persona física como microempresario o no microempresario (formulario P2PL)
- Declaración de modificación de una empresa comercial y / o artesanal en microempresario o no microempresario (formulario P2CM)
- Declaración de modificación de empresa agraria o de actividad arrendadora de propiedad rural (formulario P2 *agricole*)
- Declaración de enmienda de una corporación agrícola (formulario M2 *agricole*)
- Declaración de modificación de persona jurídica distinta de la actividad agraria principal (formulario M2)

\> [Inicie el trámite de modificación de la información relativa a su empresa](https://welcome.guichet-entreprises.fr/)

## Detener una actividad

Tiene la posibilidad de dar de baja tu actividad y así dar de baja tu empresa actualizando este cambio a todos los organismos competentes (impuestos, Urssaf, Insee, registros públicos, etc.). Tenga en cuenta que esta formalidad debe declararse en el mes siguiente a la fecha en que se produjo.

**Lista de trámites de modificación disponible en guichet-entreprises.fr:**

- Declaración de baja de un agente comercial (formulario AC4)
- Declaración de cese de actividad liberal para una persona física como microempresario o no microempresario (formulario P4PL)
- Declaración de cese de actividad de una empresa agrícola para una persona física (formulario P4 *agricole*)
- Declaración de baja de una empresa comercial y / o artesanal para una persona física como microempresario o no microempresario (formulario P4CM)
- Declaración de baja de una empresa que tiene una actividad agrícola principal (formulario M4 *agricole*)
- Declaración de baja de una empresa ajena a la principal actividad agraria (formulario M4)

\> [Inicie el trámite para el cese de actividad de su empresa](https://welcome.guichet-entreprises.fr/)

## Más información

\> [Declarar el cese de actividad de una empresa, empresario individual o trabajador autónomo](https://www.service-public.fr/professionnels-entreprises/vosdroits/F23744) (service-public.fr).