<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="empresarial" -->
<!-- var(key)="es-formalites_modification" -->
<!-- var(translation)="None" -->
<!-- var(lang)="es" -->

# Trámites de modificación empresarial

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

## ¿Por qué realizar trámites de modificación comercial?

La información declarada al establecer su negocio está sujeta a cambios con el tiempo.

Tan pronto como se modifique alguno de los datos indicados durante el registro de la empresa, deberá declararse en el plazo de un mes (artículo R. 123-45 del Código de Comercio).

Este es el caso de cambios en la razón social o el objeto social, la forma de la sociedad, el capital social, el administrador, el domicilio del domicilio social, etc.

Entonces tienes la obligación de completar un trámite de modificación comercial.

## Trámites de modificación comercial para empresas

Las modificaciones pueden referirse a:

- información sobre la persona jurídica (nombre, acrónimo, forma jurídica, capital, duración, fecha de cierre del ejercicio, disolución, exclusión de la lista, etc.);
- información relativa a los directores (nombre por nacimiento o uso, nacionalidad, domicilio, nombre, estado del cónyuge (colaborador, socio o cónyuge empleado), nombre, forma jurídica, domicilio social, representante permanente);
- información relativa a un establecimiento (cesión de establecimiento, apertura o cierre de un establecimiento secundario, modificación de la actividad, del nombre comercial o de la marca, puesta en espera, modificación relativa al representante autorizado, etc. ).

La realización de un cambio de empresa puede estar sujeta a determinadas condiciones, como una decisión de los socios o accionistas en una junta general. Para algunas modificaciones, se requiere unanimidad. Para otros, es suficiente un quórum con una mayoría de 2/3 o 3/4 de las acciones de los socios presentes.

La modificación también deberá ser publicada el mes siguiente en un periódico de avisos legales presente en el departamento donde se ubica la sede de la empresa.

La inclusión en un periódico de avisos legales (JAL) es obligatoria para cualquier modificación relacionada con alguno de los datos obligatorios para el registro, en particular el objeto social de la empresa, su denominación social, su capital social, etc.

La ausencia de publicidad en caso de modificación estatutaria podrá ser objeto de una acción de regularización, que podrá ser ejercitada por cualquier interesado durante los 3 años siguientes a la ley de modificación de los estatutos sociales.

[\> Más información sobre los trámites para modificar una empresa](https://www.service-public.fr/professionnels-entreprises/vosdroits/N31143) (service-public.fr)

Por otro lado, no es necesario realizar un trámite de modificación al momento de la presentación de las cuentas, transferencias de acciones (si no hay modificación que impacte a los administradores), modificaciones puramente fiscales (modificación del régimen tributario). 'imposición, por ejemplo: paso del régimen real simplificado al real normal (excluido el cambio de umbral), etc.

## Modificaciones comerciales individuales

El propietario único no es una corporación. Si desea desarrollar su propiedad unipersonal, ya sea que se ejerza en EIRL y/o bajo la condición de microempresario.

Las formalidades de modificación se refieren a:

- información relativa al empresario (nombre de nacimiento o de uso, nacionalidad, domicilio, nombre, estado del cónyuge (cónyuge colaborador o empleado));
- información relativa a un establecimiento (cesión de establecimiento, apertura o cierre de un establecimiento secundario, modificación de la actividad, del nombre comercial o de la marca, puesta en espera, modificación relativa al representante autorizado, etc. );
- asignación de activos (opción para EIRL, fin de EIRL o modificación de la información relacionada con EIRL o el contenido de la declaración de asignación de activos);
- exclusión de la lista (cese permanente de actividad).

Ojo, no olvides declarar el cese de actividad de tu empresa unipersonal, a riesgo de tener que seguir pagando la cotización patrimonial de las empresas y cargas sociales.

## El caso específico de los microempresarios

El microempresario es un empresario individual que se beneficia de un esquema simplificado de propiedad única.

Su empresa está sujeta a los mismos principios que el empresario individual para cualquier modificación, ya se trate de una modificación de su persona (cambio de nombre, dirección, etc.), de su patrimonio, de la actividad o para cualquier otra característica de su actividad, el microempresario deberá realizar los mismos trámites.

Tenga en cuenta que si desea renunciar al régimen de microempresas, debe optar por un régimen fiscal real sobre sus ganancias. Como se trata de un cambio puramente fiscal, deberá ponerse en contacto directamente con el departamento fiscal. No es necesario realizar ningún trámite.