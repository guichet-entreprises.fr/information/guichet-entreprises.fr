﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Preguntas frecuentes" -->
<!-- var(key)="es-faq" -->
<!-- var(translation)="None" -->
<!-- var(lang)="es" -->

Preguntas frecuentes
===================

## 1. ¿Por qué los trámites corporativos? <!-- collapsable:open -->

### ¿Qué es un trámite comercial? <!-- collapsable:close -->

Los trámites societarios son trámites administrativos relacionados con la vida de la empresa.

Hay tres tipos principales de trámites:

- la **creación** (registro o declaración de inicio de actividad) que da existencia legal a una estructura;
- **modificaciones** (cambio de actividad, dirección, nombre, número de socios, etc.) que permitan actualizar la información relativa a la empresa;
- el **cese** de actividad que pone fin a la existencia legal de la estructura.

Los trámites se componen de:

- uno o más **Cerfa**, un formulario administrativo regulado, cuyo modelo se fija mediante una orden;
- **documentos justificativos**: documento de identidad, certificado de no convicción, prueba de cualificación profesional, certificado de residencia, etc. ;
- posiblemente, un **pago**, principalmente relacionado con las tasas de registro (RCS, RM, RSAC, etc.).

### ¿Son obligatorios los trámites de la empresa? <!-- collapsable:close -->

Sí, los trámites corporativos son obligatorios.

Permiten dar existencia jurídica a una empresa e informar a las administraciones competentes (servicios fiscales, Insee, Urssaf, etc.), que interactuarán con ella.

### ¿Cuál es el papel de un centro de trámites comerciales (CFE)? <!-- collapsable:close -->

El Guichet Entreprises no está destinado a gestionar trámites. Esta actividad es competencia de las CFE. El Guichet Entreprises solo permite simplificar el envío de documentos a la CFE competente.

El centro de trámites comerciales (CFE) es la interfaz entre el creador de negocios y las administraciones.

Su CFE centralizará los documentos de su expediente y los remitirá, tras realizar un control formal, a los distintos organismos interesados ​​en información sobre su empresa (servicios fiscales, CPAM, Urssaf, etc.).

La CFE de la que depende su empresa se determina según dos criterios: un criterio categórico y un criterio geográfico.

El criterio categórico se determina según la actividad de su empresa y su forma jurídica:

<table class="table table-striped table-bordered">
<thead style="text-align: center;border-collapse: collapse">
<tr>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;"><i>Chambres de métiers et de l’artisanat</i> (CMA)</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;"><i>Chambres de commerce et d’industrie</i> (CCI)</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;"><i>Chambres d’agricultures</i> (CA)</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;"><i>Greffes du Tribunal de Commerce</i> (GTC)</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">URSSAF (ACOSS)</th>
</tr>
</thead>
<tbody style="text-align: center;border-collapse: collapse">
<tr>
<td style="border: 1px solid #AAA;padding: 4px">Actividades artesanales</td>
<td style="border: 1px solid #AAA;padding: 4px">Comerciantes y empresas comerciales excluidas las actividades artesanales</td>
<td style="border: 1px solid #AAA;padding: 4px">Agricultores</td>
<td style="border: 1px solid #AAA;padding: 4px">Sociedades civiles y agentes comerciales</td>
<td style="border: 1px solid #AAA;padding: 4px">Actividades liberales realizadas en propiedad única (microempresarios o no)</td>
</tr>
</tbody>
</table>

Las CFE están regionalizadas, la ubicación geográfica de la sede de la actividad también determina la CFE competente.

### ¿Qué organizaciones están involucradas en los trámites corporativos (SIE, SSI, Insee, etc.)? <!-- collapsable:close -->

Aparte de las EFC, otras organizaciones intervendrán a lo largo de la vida de su negocio para registrar su declaración de actividad, registrarlo, etc. :

- **Insee** (Instituto Nacional de Estadística y Estudios Económicos):
  - inscribe la empresa en el directorio SIRENE (Sistema Computarizado del Directorio Nacional de Empresas y Establecimientos). Este directorio contiene el estado civil de las empresas que deben registrarse;
  - asigna:
    - un número SIREN (sistema de identificación del directorio de la empresa) que es una serie de 9 dígitos únicos,
    - un número SIRET (Sistema de Identificación del Directorio de Establecimientos) que es una serie de 14 dígitos compuesta por el número SIREN seguido de 5 dígitos que corresponden al código NIC (número de clasificación interna). Los números SIREN y SIRET son enviados directamente por INSEE por correo postal y correo electrónico;
  - emite códigos APE (actividad principal realizada);
- el **servicio fiscal de sociedades** (SIE) es el único punto de contacto para las PYMES (empresas unipersonales, empresas) y cualquiera que sea su actividad (comerciantes, artesanos, agricultores y profesiones liberales) para la presentación de declaraciones profesionales (declaraciones de resultados, IVA , tasación de la propiedad de la empresa, CVAE, etc.) y el pago de los principales impuestos profesionales (impuesto de sociedades, IVA, impuesto sobre la nómina, etc.).
- La **Seguridad social de los autónomos** (SSI) sustituye al Régimen de la Seguridad social de los autónomos (RSI) para la recaudación y gestión de las cotizaciones sociales obligatorias de los autónomos. Se trata de empresas unipersonales: trabajadores independientes, microempresarios, EIRL, pero también administradores mayoritarios de EURL y administradores mayoritarios de SARL. Desde 2020, la ISS se ha integrado en el sistema general de seguridad social.

El registro se realiza ante los registradores, los órganos que llevan los distintos registros a los que deben inscribirse las empresas. Se determinan según tu actividad:

- el secretario del juzgado mercantil para su registro:
  - en el registro mercantil y societario (RCS) si su actividad es comercial, o si crea una sociedad civil,
  - el registro especial de agentes comerciales (RSAC) si es un agente comercial,
  - el registro especial de empresarios individuales con responsabilidad limitada (RSEIRL) para comerciantes y autónomos;
- la cámara de oficios y artesanías (CMA), si su actividad es artesanal, para su registro en el directorio de oficios.

No tiene que informar a estas organizaciones, la CFE se encargará de ello.

## 2. ¿Por qué utilizar el mostrador comercial? <!-- collapsable:open -->

### ¿Qué es el sitio guichet-entreprises.fr? <!-- collapsable:close -->

El sitio guichet-entreprises.fr es un servicio en línea para realizar los trámites administrativos relacionados con:

- cambios en la situación;
- cuando una empresa deja de funcionar.

Su uso es gratuito, sin embargo, como parte de ciertos trámites, se pueden solicitar tarifas (ejemplo: tarifas de registro en el directorio de oficios).

Los pagos realizados en el sitio son seguros.

Tenga en cuenta que el mostrador comercial no acompaña al declarante.

No le ayudará a elegir su estatus legal, por ejemplo. Actores públicos y privados asesoran a creadores y líderes empresariales en esta elección, así como la del sistema tributario y social, etc.

**Bueno saber**

El sistema integrado de Guichet Entreprises incluye reglas comerciales. Gracias a un sencillo y rápido interrogatorio, el declarante es dirigido automáticamente a las EFC y al registrador competentes.

### ¿Qué tipo de negocio puedo crear en el sitio guichet-entreprises.fr? <!-- collapsable:close -->

Se tienen en cuenta todas las formas legales (EI, SARL, SAS, etc.) y todas las etapas de la vida de una empresa (cambio de condición jurídica, aumento de capital, etc.).

En guichet-entreprises.fr, puede seguir los siguientes pasos:

- creación, modificación y cancelación para las profesiones liberales, artesanales, comerciales, agrícolas y agentes comerciales;
- creación y modificación para SARL / SELARL, sociedades comerciales distintas de SARL y sociedades civiles;
- creación y cancelación de empresas agrícolas.

### ¿El uso del sitio guichet-entreprises.fr es gratuito? <!-- collapsable:close -->

Completar su proceso en guichet-entreprises.fr es completamente gratis.

Sin embargo, como parte de ciertas formalidades, se pueden solicitar tarifas (ejemplo: tarifas de registro para el directorio de oficios).

Todos los pagos realizados en guichet-entreprises.fr son seguros.

### ¿El sitio es seguro? <!-- collapsable:close -->

La aprobación de seguridad de las Empresas Guichet se emitió el 1 de junio de 2019 por un período de 30 meses.

Garantiza el cumplimiento del sitio con los requisitos de ANSSI (Agencia Nacional de Seguridad de los Sistemas de Información) en términos de seguridad de acceso, ingreso, almacenamiento, procesamiento e intercambio de datos.

## 3. Necesito información sobre trámites corporativos <!-- collapsable:open -->

### ¿Pueden las Empresas Guichet ayudarme en el desarrollo de mi proyecto? <!-- collapsable:close -->

El Guichet Entreprises **no admite** al declarante. Los actores públicos y privados apoyan a los creadores y líderes empresariales en la elección de estatus legal, régimen fiscal y social, etc.

### ¿Cuál es la diferencia entre una persona física y una persona jurídica? <!-- collapsable:close -->

Una persona física es un individuo con identidad civil.

Una persona jurídica es un grupo de personas, físicas o jurídicas, que trabajan juntas por un objetivo común y tienen existencia jurídica. Sin embargo, puede ser unipersonal.

Si tiene un plan de puesta en marcha de una empresa, puede utilizar diferentes marcos legales para su empresa.

La ley francesa ofrece dos categorías muy distintas:

- El primero es el de las personas físicas: se trata de la propiedad unipersonal (IE), con estas distintas variaciones (propiedad unipersonal con responsabilidad limitada EIRL, régimen microempresarial). La propiedad unipersonal es, por tanto, inseparable de la persona que la creó.
- La otra forma jurídica aplicable es la de persona jurídica, es decir la constitución de una sociedad: por ejemplo, las más utilizadas entre las creaciones empresariales son la sociedad de responsabilidad limitada (SARL) y la sociedad anónima simplificada (SAS). Es una sociedad cuya existencia se disocia de las personas que la integran. De hecho, el liderazgo de una empresa puede cambiar.

Tanto las personas físicas como jurídicas tienen personalidad jurídica, concepto que designa a los titulares de derechos y obligaciones legales después de la creación de una empresa. Es decir, son entidades o personas que pueden acudir a los tribunales o ser objeto de un proceso.

### ¿Qué se entiende por "alguna vez ha trabajado por cuenta propia? »<!-- collapsable:close -->

La actividad por cuenta propia es una actividad que se realiza por cuenta propia, es decir, bajo un régimen diferente al de un trabajador por cuenta ajena.

Incluido en la categoría de autónomos, autónomos:

- microempresarios;
- empresarios individuales (empresas "en su propio nombre");
- administradores de empresas del tipo EURL o SARL;
- socios en una sociedad general (SNC).

### ¿A qué corresponde el número de identificación único? <!-- collapsable:close -->

El número de identificación único corresponde al número de SIREN, se le asigna a cada empresa. Este número de nueve dígitos es único e invariable.

En guichet-entreprises.fr, se le solicita su número SIREN en el contexto de la modificación o terminación de su actividad para poder encontrar información relacionada con su empresa en el directorio SIRENE de Insee.

### Necesito información sobre el código APE <!-- collapsable:close -->

#### Definición del código APE <!-- collapsable:close -->

El código APE (actividad principal que ejerce) identifica la rama principal de actividad de la empresa o del autónomo.

El código está compuesto por 4 dígitos + 1 letra, con referencia a la clasificación francesa de actividades (NAF).

Este código lo asigna el INSEE al registrarse o declarar la actividad de la empresa. Se da según la actividad principal declarada y efectivamente ejercida.

Este código se utiliza principalmente con fines estadísticos y permite al INSEE clasificar las empresas por sector de actividad.

#### Modificación del código APE <!-- collapsable:close -->

Si ha cambiado su campo de actividad, su solicitud de modificación será enviada al INSEE por su [centro de trámites comerciales](https://www.economie.gouv.fr/entreprises/cfe-centre-formalites-entreprises) (CFE) oa través del sitio de la ventana comercial. INSEE modificará o no el código APE en función de la información facilitada.

Si cree que el código APE que se le ha expedido no corresponde a su actividad principal, deberá realizar su solicitud de modificación por correo postal o por correo electrónico a la oficina regional del INSEE responsable del departamento de implementación de su sede social o de su establecimiento. .

Un [formulario de modificación de APE](https://www.insee.fr/fr/information/2015441#titre-bloc-6) para ser impreso y completado por usted está disponible para usted en el sitio web del INSEE.

#### Problema de reconocimiento de código APE <!-- collapsable:close -->

Si no se reconoce su código APE, significa que la actividad a la que corresponde no forma parte de la [directiva “Servicios”](https://eur-lex.europa.eu/legal-content/ES/TXT/?uri=celex%3A32006L0123) del 12 de diciembre de 2006. En este caso, esta actividad no forma parte del alcance de los trámites en línea realizados por el sitio guichet-entreprises.fr.

De hecho, el sitio guichet-entreprises.fr no es competente para los procedimientos relacionados con la creación, modificación o terminación de una actividad pública o parapública.

Este es el caso de las siguientes actividades:

- Fondos de la pensión;
- administración pública en general;
- administración pública (supervisión) de salud, formación, cultura y servicios sociales, distintos de la seguridad;
- administración pública (supervisión) de actividades económicas;
- asuntos Extranjeros ;
- defensa;
- justicia;
- actividades de orden público y seguridad;
- servicios de bomberos y salvamento;
- actividades generales de seguridad social;
- gestión de pensiones complementarias;
- distribución social de la renta;
- actividades de organizaciones de empleadores y consulares;
- reparación de equipos de comunicación;
- actividades de los sindicatos;
- actividades de organizaciones religiosas;
- actividades de organizaciones políticas;
- otras organizaciones que operan por afiliación voluntaria;
- actividades de organismos y organizaciones extraterritoriales.

### Mi actividad es artesanal, ¿por qué la CFE se indica como CCI? <!-- collapsable:close -->

El centro de trámites comerciales (CFE) de una empresa artesanal se determina de acuerdo con el número de empleados en la empresa:

- Si la plantilla es menor o igual a 10 empleados, depende de la CFE de la Cámara de Oficios y Oficios (CMA).
- Si la plantilla supera los diez empleados, depende de la CFE de la Cámara de Comercio e Industria (CCI).

### Soy un empleado, ¿puedo montar una empresa? <!-- collapsable:close -->

Bajo ciertas condiciones, puede crear un negocio mientras tiene un trabajo asalariado.

Debe informar a su empleador y obtener su autorización.

También debe respetar los intereses de la empresa, especialmente si planea crear en el campo de actividades donde trabaja. Comprueba que tu contrato de trabajo no contiene cláusula de exclusividad.

### Estoy jubilado, ¿puedo iniciar un negocio? <!-- collapsable:close -->

Los jubilados pueden iniciar un negocio.

Los ingresos de la actividad profesional pueden combinarse con la totalidad o parte de la pensión de jubilación.

Para obtener más información sobre las consecuencias de establecer una empresa en su pensión, comuníquese con su fondo de pensiones.

### ¿Puedo declarar dos actividades principales? <!-- collapsable:close -->

Una empresa puede tener varias actividades, pero no es posible declarar dos actividades principales.

En este caso, debe declarar una actividad principal y una actividad secundaria. La actividad principal es la actividad que genera más ingresos o en la que más tiempo le dedicas.

Solo determinadas actividades deben ser exclusivas, es el caso de las actividades reguladas de vigilancia, servicios personales, etc.

#### El caso específico de la propiedad unipersonal

Una persona física solo puede tener una propiedad unipersonal, sin embargo, es posible realizar varias actividades allí, incluso si no están relacionadas.

Un empresario individual puede así ejercer todas las actividades artesanales, la mayoría de las actividades comerciales y ciertas actividades liberales.

**ATENCIÓN**

Le es imposible agregar una actividad liberal a una actividad comercial y / o artesanal. En este caso, es necesario crear una actividad liberal indicando en observación que ya ejerce una actividad comercial.

### ¿Mi actividad se corresponde con la de un agente comercial? <!-- collapsable:close -->

Un agente comercial es la persona que, como profesión regular e independiente, negocia y, eventualmente, concluye contratos de compra, venta, alquiler o prestación de servicios, en nombre y por cuenta de otras personas.

No confundir con un prestador de servicios, una persona física o jurídica, incluidos los organismos públicos, que ofrecen servicios (ayuda a domicilio, servicios informáticos o de telecomunicaciones, servicios de consultoría, etc.).

### ¿Qué es una actividad de comerciante itinerante o no sedentario? <!-- collapsable:close -->

Una **actividad itinerante**, o no sedentaria, es una actividad que se lleva a cabo en la vía pública, en salones, mercados, ferias o en lugares privados mediante colportación y que tiene por objeto la venta de un buen mobiliario, la celebración de una contrato de alquiler o servicio.

Esta modalidad de realización de la actividad está regulada y requiere la obtención del carnet de comerciante / artesano itinerante.

### ¿Qué es la solicitud ACRE? <!-- collapsable:close -->

La [ayuda para la creación o adquisición de una empresa](https://www.service-public.fr/particuliers/vosdroits/F11677) (ACRE) consiste en una exención parcial de las cargas sociales, denominada exención del inicio de actividad, y en apoyo durante los primeros años de actividad. También permite a determinados beneficiarios solicitar otras formas de ayuda.

Desde el 1 de enero de 2020, la solicitud de ACRE debe realizarse independientemente del trámite de creación. Ya no es automático y está sujeto a criterios de elegibilidad.

Los microempresarios deben enviar su [solicitud](https://www.service-public.fr/particuliers/vosdroits/R55376) a la Urssaf a más tardar 45 días después de la presentación del archivo de creación o recuperar la empresa.

Los textos establecen que en ausencia de respuesta dentro del mes siguiente a la recepción de la solicitud, se presume aceptada la exención ACRE.

Para las empresas creadas bajo otro estatuto, Urssaf realiza un control a posteriori.

## 4. Administro mi espacio personal <!-- collapsable:open -->

### ¿Por qué es necesario crear un espacio personal? <!-- collapsable:close -->

Debe crear un espacio personal para utilizar los servicios del Guichet Entreprises. Esto le permitirá administrar su(s) archivo(s) y su información personal.

El sitio guichet-entreprises.fr permite:

- cree su archivo en línea;
- adjunte sus documentos justificativos;
- abonar los gastos relacionados con el trámite.

Una vez que su expediente ha sido establecido y validado, se envía automáticamente a la CFE competente para su procesamiento.

De esta forma, construye su expediente de trámites sin tener que desplazarse.

### ¿Cómo funciona el panel de control de mi espacio personal? <!-- collapsable:close -->

Después de crear su espacio personal, accede a su panel de control. Esto mostrará todos sus archivos y le permitirá:

- iniciar un nuevo proceso;
- continuar el iniciado;
- descargue su archivo una vez que se haya enviado a la organización destinataria.

Este tablero es común:

- el sitio guichet-entreprises.fr, que agrupa los procedimientos para la creación, modificación y terminación de la actividad de una empresa;
- en el sitio [guichet-qualifications.fr](https://www.guichet-qualifications.fr/es/), destinado a los nacionales de la Unión Europea y del Espacio Económico Europeo, a excepción de los titulares de diplomas franceses, que deseen que se reconozcan sus cualificaciones profesionales para ejercer en Francia.

Si configura archivos en una de estas dos plataformas, estarán visibles en su panel de control.

### Tengo un problema de contraseña <!-- collapsable:close -->

#### El correo electrónico de validación no funciona <!-- collapsable:close -->

El enlace de validación de un solo uso es válido durante 24 horas. Más allá de eso, debe volver a crear una cuenta con la misma dirección de correo electrónico.

El enlace solo es válido una vez. Si ya ha activado su cuenta, haga clic en [Iniciar sesión](https://account.guichet-entreprises.fr/session/new) para acceder a ella.

#### Contraseña olvidada <!-- collapsable:close -->

Si ha olvidado su contraseña, haga clic en [Renovar mi contraseña](https://account.guichet-entreprises.fr/users/renew) en la página de inicio de sesión.

Ingrese la dirección de correo electrónico que usa para guichet-entreprises.fr y recibirá un enlace por correo electrónico que le permitirá restablecer su contraseña. Este enlace es válido durante 24 horas a partir de la publicación.

#### Renovación de contraseña <!-- collapsable:close -->

Si el enlace de renovación de contraseña no funciona, probablemente significa que no está actualizado. Por lo tanto, será necesario realizar una [nueva solicitud de renovación](https://account.guichet-entreprises.fr/users/renew).

Además, por razones de seguridad, deberá cambiar esta contraseña cada 90 días.

### Quiero cambiar la información de mi cuenta <!-- collapsable:close -->

Si desea cambiar su información personal, inicie sesión en su cuenta y edite su información personal en la pestaña en la parte superior derecha de la página.

Podrá cambiar su nombre, nombre, país, número de teléfono y pregunta / respuesta secreta.

Para cambiar su contraseña, siga el [procedimiento de contraseña olvidada](https://account.guichet-entreprises.fr/users/renew).

Sin embargo, no puede modificar su correo electrónico de inicio de sesión vinculado a su espacio personal. Debe crear una segunda cuenta con un correo electrónico diferente.

Tampoco es posible mover las carpetas creadas en la cuenta anterior para ponerlas en la nueva cuenta.

### ¿Puedo borrar mi espacio personal? <!-- collapsable:close -->

Si desea eliminar su espacio personal en guichet-entreprises.fr, por favor [póngase en contacto con el INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### ¿Puedo conectarme con mi cuenta France Connect? <!-- collapsable:close -->

Puede conectarse a guichet-entreprises.fr con sus credenciales France Connect, creadas previamente en los sitios web [Impots.gouv.fr](https://www.impots.gouv.fr/portail/), [LaPoste.fr](https://lidentitenumerique.laposte.fr/) o [Ameli.fr](https://www.ameli.fr/).

### Perdí mis credenciales de France Connect <!-- collapsable:close -->

El servicio Guichet Entreprises no puede proporcionarle sus credenciales de France Connect.

Debe seguir el procedimiento del sitio ([Impots.gouv.fr](https://www.impots.gouv.fr/portail/), [LaPoste.fr](https://lidentitenumerique.laposte.fr/) o [Ameli.fr](https://www.ameli.fr/)) de donde proceden sus datos de acceso y contraseña de France Connect.

### Creé un archivo en guichet-entreprises.fr identificándome con mi cuenta de France Connect que ha sido eliminada <!-- collapsable:close -->

Si ha creado un archivo en guichet-entreprises.fr utilizando una autenticación [France Connect](https://franceconnect.gouv.fr/) cuya cuenta ha sido eliminada mientras tanto, todavía tiene la posibilidad de "recuperar" su expediente [contactando con el INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

Los archivos de Guichet Entreprises están vinculados a una cuenta de Guichet Entreprises incluso si el usuario utiliza un identificador de France Connect.

### ¿Cuánto tiempo se conservan mis datos? ? <!-- collapsable:close -->

Sus datos proporcionados en guichet-entreprises.fr se conservan durante un año. Si desea eliminarlos antes de este plazo, [póngase en contacto con el INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

Los archivos creados y enviados a las autoridades competentes a través de guichet-entreprises.fr se conservan durante un período de tres meses.

## 5. Entro en mi carpeta <!-- collapsable:open -->

### Antes de crear su carpeta <!-- collapsable:close -->

El uso del sitio guichet-entreprises.fr requiere un navegador configurado para permitir cookies de sesión. Para garantizar una experiencia de navegación óptima, le recomendamos que utilice, como mínimo, las siguientes versiones de navegador:

- Firefox versión 45 y superior;
- Opera versión 11 y superior;
- Safari versión 5.1 para Windows y versión 6.0 y superior para MacOs;
- Internet Explorer versión 9 y superior;
- Microsoft Edge versión 25 y superior;
- Google Chrome versión 48 y superior.

De hecho, es posible que otros navegadores no admitan determinadas funciones del sitio guichet-entreprises.fr.

También se recomienda que utilice la última versión del navegador y la actualice periódicamente para obtener correcciones de seguridad y obtener el mejor rendimiento.

El sitio guichet-entreprises.fr está optimizado para una visualización en 1024 × 768 píxeles.

### ¿Puedo guardar una carpeta que se está creando? <!-- collapsable:close -->

El trámite actual se puede registrar en cualquier momento y completar en el plazo de un mes. Para hacer esto, haga clic en "Guardar y salir".

Puede reanudar sus trámites sin tener que volver a ingresar la información ingresada previamente.

El archivo se transmite definitivamente al organismo competente al hacer clic en "Enviar mi archivo".

### ¿Puedo modificar mi archivo mientras se crea? <!-- collapsable:close -->

Puede modificar su archivo siempre que no se haya enviado a las organizaciones receptoras.

### ¿Puedo modificar mi archivo después de que se haya enviado a las organizaciones receptoras? <!-- collapsable:close -->

De acuerdo con la normativa vigente, no puede modificar o cancelar un archivo en guichet-entreprises.fr, una vez que haya sido validado y puesto a disposición de las organizaciones receptoras.

Por tanto, para cualquier modificación o supresión de estos últimos, es recomendable ponerse en contacto con ellos directamente.

### ¿Puedo eliminar una carpeta? <!-- collapsable:close -->

Es posible eliminar un archivo en curso. En este caso, [póngase en contacto con el INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

Una vez que el archivo se haya validado y enviado a una de las organizaciones asociadas, ya no podrá eliminarlo. Para hacer esto, deberá comunicarse con la CFE a la que se envió.

**Bueno saber**

Cualquier archivo iniciado y no completado se eliminará automáticamente de la plataforma en un plazo de tres meses.

### ¿Por qué me dicen que debo pagar una tarifa cuando pensé que mi archivo era gratuito?

Todos los costos se calculan en función de los datos proporcionados sobre su negocio futuro. Si la(s) regulación(es) no parece preocuparle, le invitamos a revisar los campos ingresados ​​en los pasos 1 y 2.

Por ejemplo, el registro de un microempresario que ejerce una actividad liberal es gratuito, salvo que opte por la condición de EIRL. De hecho, un microempresario autónomo que opte por EIRL debe registrarse en el registro especial de empresarios individuales con responsabilidad limitada (RSEIRL), que incurre en tasas de registro en el registro.

### Tengo un problema para completar el campo del número de seguro social <!-- collapsable:close -->

Si después de varios intentos no se acepta su número de seguro social, marque la entrada de su fecha de nacimiento. Realizamos una verificación entre la fecha de nacimiento y el número de la seguridad social, por lo que los datos deben coincidir.

Si a pesar de esto su número aún no es aceptado, puede completar temporalmente el campo con quince ceros. Luego, en la última página del formulario ("Formalidad"), todo lo que tiene que hacer es ingresar su número de seguro social en el rectángulo "Observaciones" en la parte superior de la página.

### No puedo encontrar mi ciudad de nacimiento en la lista desplegable <!-- collapsable:close -->

En los últimos años, algunos municipios se han agrupado en el marco de reordenamientos territoriales. Como el antiguo municipio ya no existe administrativamente, debe indicar el nombre del nuevo municipio administrativo en el campo "Municipio de nacimiento".

Opcionalmente puede indicar el nombre del antiguo municipio en el apartado "Observaciones" al final del formulario. Independientemente del nombre del municipio, es el código de municipio (repositorio de Insee), que no se modifica, el que prevalece.

Ejemplos en el departamento de Calvados:

- Si tu municipio de nacimiento es "Aunay-sur-Odon", debes elegir ahora el municipio "Les Monts d´Aunay".
- Si su lugar de nacimiento fue "Mézidon-Canon", elija "Mézidon Vallée d´Auge" en la lista desplegable.

### No encuentro mi código postal <!-- collapsable:close -->

El campo "código postal" solo se refiere a direcciones ubicadas en Francia. Para países extranjeros, todo lo que tiene que hacer es ingresar al país y al municipio. El código postal se puede introducir en el campo "dirección adicional".

## 6. Agrego mis documentos de respaldo <!-- collapsable:open -->

### ¿En qué formato deben estar mis archivos adjuntos? <!-- collapsable:close -->

Para evitar cualquier error técnico al enviar su archivo a la organización receptora, le recomendamos que respete ciertos estándares durante el paso de agregar los documentos de respaldo:

- el tamaño de sus documentos justificativos debe ser inferior a 2 MB. De lo contrario, aparecerá un mensaje de error en la pantalla informándole que los archivos adjuntos no se pudieron agregar a la carpeta;
- sus documentos justificativos deben estar en formato PDF;
- el archivo PDF no debe protegerse, de lo contrario, no se puede agregar a la carpeta.

Compruebe que los archivos en formato PDF con campos editables (formularios) no estén dañados y que se muestren correctamente con Acrobat Reader.

Si tiene un problema con un archivo adjunto, [comuníquese con el Inpi](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1) antes de finalizar y enviar su archivo.

### ¿Es posible recuperar los archivos adjuntos descargados? <!-- collapsable:close -->

Es posible eliminar y / o reemplazar por otros documentos los documentos que haya descargado de guichet-entreprises.fr, en cualquier momento, siempre que no haya validado su archivo y no haya sido enviado a la CFE a cargo de su procesamiento.

En cualquier momento, puede descargar su archivo desde su tablero, incluso si este último ha sido enviado a la CFE competente.

### ¿Por qué me piden documentos que no me conciernen? <!-- collapsable:close -->

El servicio en línea determina los documentos de respaldo en función de lo que proporcione durante su declaración.

Si cree que los documentos de respaldo solicitados no le conciernen, lo invitamos a revisar los campos ingresados ​​en este último.

### Me preguntan "la lista de suscriptores", ¿qué es? <!-- collapsable:close -->

La lista de suscriptores se solicita como parte de un trámite para la creación o modificación de una persona jurídica (sociedad), sociedades anónimas (SA) y sociedades anónimas simplificadas (SAS) en particular.

La lista de suscriptores presenta la relación de los distintos accionistas y muestra el número de acciones suscritas y las cantidades pagadas por cada uno de ellos.

Es independiente de los documentos que proporciona el banco depositario para los fondos que integran el capital social y debe estar escrito en el membrete de la sociedad que se crea.

### ¿Qué documentos están autorizados para acreditar mi domicilio? <!-- collapsable:close -->

**Para individuos:**

Debe proporcionar comprobante de domicilio de menos de tres meses a nombre y nombre del gerente.

Los documentos autorizados son:

- facturas (electricidad, gas, agua, teléfono fijo o móvil, impuesto sobre la vivienda);
- si la mudanza es reciente, la escritura de adquisición del inmueble o el arrendamiento.

Si se hospeda en una casa que no está a su nombre, debe proporcionar un certificado de menos de 3 meses a nombre y nombre del anfitrión acompañado de un certificado de alojamiento (carta de él certificando el 'honor que él te aloja).

Si está alquilando, debe proporcionar una copia del contrato de arrendamiento y un certificado de su arrendador que lo autorice a domiciliar su negocio en su hogar.

**Para personas jurídicas:**

Además del comprobante de domicilio de menos de tres meses a la dirección de la casa matriz, el gerente de la empresa deberá redactar un certificado de domiciliación. Esto debe incluir los siguientes elementos:

- la identidad del declarante;
- El nombre de la compañía;
- la dirección exacta del domicilio social tal como aparecerá en la Kbis de la empresa;
- la fecha, la firma del gerente.

### ¿Por qué me piden "una copia del diploma, título o cualquier documento que justifique mi calificación profesional"? <!-- collapsable:close -->

Este documento es obligatorio si la actividad declarada está sujeta a la posesión de una calificación profesional y el empresario no se ha comprometido en su declaración a contratar un empleado calificado en la profesión ejercida.

A continuación, se solicita al declarante que justifique su propia cualificación (diploma) o su experiencia profesional aportando:

- nóminas o certificados de trabajo **para empleados**;
- certificado de registro (SIREN), extracto de Kbis y comprobante de inscripción en el registro de los oficios (RM) **para administradores**.

### Recibí una carta pidiéndome que modifique los documentos de respaldo que he adjuntado a mi expediente: ¿qué debo hacer? <!-- collapsable:close -->

Si ha recibido una carta adicional para modificar los documentos justificativos transmitidos, su expediente ha sido validado y enviado al organismo competente.

En esta etapa, ya no puede volver a abrir el archivo en guichet-entreprises.fr.

Los documentos adicionales y / o información solicitada por el (los) destinatario(s) del trámite formado deberán ser enviados directamente a esta organización, por correo, correo electrónico o depósito físico, según las recomendaciones y con las referencias comunicadas por la organización.

## 7. Pago los costos de mi archivo y lo envío <!-- collapsable:open -->

### ¿Quiénes son los destinatarios de mi pago en guichet-entreprises.fr? <!-- collapsable:close -->

Al armar su archivo en línea, puede pagar los costos asociados con la formalidad en línea. El sitio guichet-entreprises.fr transmite los pagos realizados en el sitio directamente a las organizaciones que reciben los archivos. Para cualquier queja relacionada con el pago, comuníquese con la organización receptora de su archivo.

### Hice el pago en guichet-entreprises.fr, ¿qué pasa si la organización receptora me pide que pague las mismas tarifas nuevamente? <!-- collapsable:close -->

Si la organización receptora de su archivo le solicita tarifas de procesamiento adicionales, puede enviarle una copia de su comprobante de pago a guichet-entreprises.fr. Entonces podrá encontrar su pago. En caso contrario, [póngase en contacto con el INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### ¿Cómo obtengo una factura? <!-- collapsable:close -->

Si ha realizado un pago en guichet-entreprises.fr como parte de sus trámites, tiene la opción de solicitar una factura directamente a la organización receptora de las tasas abonadas. Debe adjuntar a su solicitud el comprobante de pago que se le envió por correo electrónico al finalizar el pago.

### ¿Cuándo se enviará mi archivo a la organización destinataria? <!-- collapsable:close -->

Una vez completado y finalizado, su archivo creado en el sitio guichet-entreprises.fr se envía a las organizaciones receptoras para su procesamiento.

Para saber si su archivo ha sido enviado, vaya a su espacio personal y verifique que la barra de progreso del archivo en cuestión esté al 100% y que se muestre en verde.

Los archivos se transmiten en un plazo de entre 1 y 24 horas.

Si nota que la barra de progreso de su archivo no se muestra 100% en verde más de 48 horas hábiles después de haberlo finalizado, por favor [contacte con el INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### ¿Qué es el centro de trámites comerciales (CFE) a cargo de mi expediente? <!-- collapsable:close -->

El centro de trámites comerciales competente (CFE) se determina de acuerdo con la condición jurídica de la empresa, la naturaleza de la actividad y la ubicación geográfica de la casa matriz.

- **actividad del declarante y su forma jurídica (criterio categórico):**

<table class="table table-striped table-bordered">
<thead style="text-align: center;border-collapse: collapse">
<tr>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Su actividad profesional</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Su CFE</th>
</tr>
</thead>
<tbody style="text-align: center;border-collapse: collapse">
<tr>
<td style="border: 1px solid #AAA;padding: 4px"><a href="https://www.economie.gouv.fr/entreprises/statut-entreprise-individuelle"><i>Entrepreneur individuel</i></a> <i>ou société</i> (<a href="https://www.economie.gouv.fr/entreprises/entreprise-unipersonnelle-responsabilite-limitee-EURL"><i>EURL</i></a>, <a href="https://www.economie.gouv.fr/entreprises/societe-responsabilite-limitee-sarl"><i>SARL</i></a>, <a href="https://www.economie.gouv.fr/entreprises/societe-anonyme-SA"><i>SA</i></a> - <a href="https://www.economie.gouv.fr/entreprises/societe-actions-simplifiee-SAS"><i>SAS</i></a>, <a href="https://www.economie.gouv.fr/entreprises/societe-en-nom-collectif-snc"><i>SNC</i></a>) <i>exerçant une activité commerciale</i>
</td>
<td style="border: 1px solid #AAA;padding: 4px"><i>Chambre de commerce et d'industrie</i> (CCI)</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px"><i>Entrepreneur individuel ou société exerçant une activité artisanale</i></td>
<td style="border: 1px solid #AAA;padding: 4px"><i>Chambre de métiers et de l'artisanat</i> (CMA)</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px"><i>Entreprises de transport de marchandises par voie d'eau ou société coopérative fluviale</i></td>
<td style="border: 1px solid #AAA;padding: 4px"><i>CMA</i></td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px"><i>Entrepreneur individuel ou société exerçant des activités agricoles à titre principal</i></td>
<td style="border: 1px solid #AAA;padding: 4px"><a href="http://www.chambres-agriculture.fr/"><i>Chambre d'agriculture</i></a></td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px"><i>Entrepreneur individuel exerçant une profession libérale</i><br><i>Artiste auteur</i>
</td>
<td style="border: 1px solid #AAA;padding: 4px"><i>Urssaf</i></td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px"><i>Agent commercial (personne physique)</i><br><i>Société civile (SCI, SCM, SCP, etc.)</i><br><i>Société d'exercice libéral (SELARL, SELAFA, SELCA)</i><br><i>Société en participation</i><br><i>Établissement public et industriel (EPIC)</i><br><i>Groupement d'intérêt économique (GIE)</i><br><i>Association assujettie aux impôts commerciaux</i><br><i>Loueur en meublé</i>
</td>
<td style="border: 1px solid #AAA;padding: 4px"><i>Greffe du tribunal de commerce ou du tribunal de grande instance (TGI) statuant commercialement</i></td>
</tr>
</tbody>
</table>

- **la ubicación geográfica de la sede de la actividad**.

Una vez que haya completado su proceso, anote los datos de contacto que se le proporcionaron para contactarlo directamente si es necesario.

## 8. Tengo un problema técnico <!-- collapsable:open -->

### Tengo errores de visualización <!-- collapsable:close -->

El uso del sitio guichet-entreprises.fr requiere un navegador configurado para permitir cookies de sesión. Para garantizar una experiencia de navegación óptima en guichet-entreprises.fr, le recomendamos que utilice, como mínimo, los siguientes navegadores:

- Firefox versión 45 y superior;
- Opera versión 11 y superior;
- Safari versión 5.1 para Windows y versión 6.0 y superior para MacOs;
- Internet Explorer versión 9 y superior;
- Microsoft Edge versión 25 y superior;
- Google Chrome versión 48 y superior.

De hecho, es posible que otros navegadores no admitan determinadas funciones del sitio guichet-entreprises.fr.

También se recomienda que utilice la última versión del navegador y la actualice periódicamente para obtener correcciones de seguridad y obtener el mejor rendimiento.

El sitio guichet-entreprises.fr está optimizado para una visualización en 1024 × 768 píxeles.

### Recibo un mensaje que me dice que se ha producido un error <!-- collapsable:close -->

Si se produjo un error técnico al completar su archivo, [comuníquese con el INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### Tengo un problema con mi cuenta <!-- collapsable:close -->

#### No puedes acceder a tu cuenta <!-- collapsable:close -->

Si no puede acceder a su cuenta, vacíe las cookies e intente iniciar sesión de incógnito en Google Chrome o Mozilla Firefox.

**Bueno saber**

Si eso no funciona, su cuenta puede estar deshabilitada. En este caso, haga clic en [Renovar mi contraseña](https://account.guichet-entreprises.fr/users/renew) y siga el procedimiento de restablecimiento de contraseña para iniciar sesión nuevamente.

#### No has recibido el correo electrónico de activación o renovación de la cuenta <!-- collapsable:close -->

Si no ha recibido el enlace para activar o renovar la cuenta, consulte la carpeta "correo no deseado" en su correo electrónico.

Si el enlace no está allí, vuelva a crear su cuenta con el mismo correo electrónico. En caso de más fallas, lo invitamos a [comunicarse con el INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

#### El correo electrónico de validación no funciona <!-- collapsable:close -->

El enlace de validación recibido por correo electrónico es válido por 24 horas. Después de este período, debe volver a crear una cuenta con la misma dirección de correo electrónico.

**Bueno saber**

El enlace solo es válido una vez. Si ya ha activado su cuenta, haga clic en [Iniciar sesión](https://account.guichet-entreprises.fr/session/new) para acceder a ella.

### No puedo renovar mi contraseña <!-- collapsable:close -->

El enlace de renovación es válido por 24 horas. Si no funciona, probablemente significa que ya no está activo. A continuación, debe realizar una [nueva solicitud de renovación](https://account.guichet-entreprises.fr/users/renew).

Por seguridad, debe cambiar esta contraseña cada 90 días.

### No puedo cargar mis documentos de respaldo <!-- collapsable:close -->

Sus documentos de respaldo deben estar en formato PDF y tener un tamaño inferior a 2 MB.

Si tiene dificultades para descargar, lo invitamos a vaciar sus cachés o usar otro navegador de Internet. También verifique que los archivos PDF no estén protegidos.

Se recomienda utilizar al menos las siguientes versiones de navegador:

- Firefox versión 45 y superior;
- Opera versión 11 y superior;
- Safari versión 5.1 para Windows y versión 6.0 y superior para MacOs;
- Internet Explorer versión 9 y superior;
- Microsoft Edge versión 25 y superior;
- Google Chrome versión 48 y superior.

De lo contrario, aparecerá un mensaje de error en la pantalla informándole que no se pueden agregar archivos adjuntos a la carpeta.

### Se produjo un error técnico al adjuntar un documento <!-- collapsable:close -->

Si ocurre un error técnico al descargar un documento, verifique su formato y tamaño y verifique que los archivos PDF con campos editables (formularios) no estén dañados y que se muestren correctamente con Acrobat Reader.

Sus documentos deben estar en formato PDF y tener un tamaño inferior a 2 MB.

Si el error persiste, [póngase en contacto con el INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

**Bueno saber**

Las modificaciones manuales de extensiones pueden provocar un error técnico al cargar el adjunto (al modificar un documento con extensión .doc a .pdf sin convertir el archivo, por ejemplo).

### No puedo realizar el pago solicitado <!-- collapsable:close -->

Si encuentra un problema técnico durante el pago, [comuníquese con el INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### No puedo firmar el formulario <!-- collapsable:close -->

Es posible que el servicio de firma electrónica no esté disponible temporalmente debido a una gran cantidad de conexiones simultáneas.

En este caso, lo invitamos a volver a intentarlo más tarde.

## 9. Tramitación de trámites <!-- collapsable:open -->

### ¿A quién contactar para dar seguimiento a mi expediente? <!-- collapsable:close -->

Una vez que su expediente ha sido validado y enviado, las Empresas Guichet lo remiten inmediatamente a la CFE competente y, en su caso, al registrador, para su procesamiento.

Son las únicas autoridades autorizadas para estudiar el expediente, por lo que debe contactar con ellos para cualquier información o modificación.

Los datos de contacto de los destinatarios de su archivo se muestran una vez que haya completado su proceso. Anote sus datos de contacto para contactarlos directamente si, por ejemplo, le gustaría conocer el progreso de su caso.

### ¿Cuál es el tiempo de tramitación del expediente por los órganos competentes? <!-- collapsable:close -->

El sitio guichet-entreprises.fr le permite crear su archivo de creación de negocios en línea. Una vez cumplimentado y validado, se envía al centro de trámites empresariales competente (CFE) para su tramitación.

Por lo tanto, el sitio guichet-entreprises.fr no puede comprometerse con los tiempos de procesamiento para las EFC.

Si la organización necesita documentos adicionales para poder procesar su archivo, se comunicará con usted por correo.

**Bueno saber**

Para cualquier consulta relacionada con su expediente (seguimiento, tramitación, etc.) una vez validado y enviado, comuníquese con el destinatario de CFE de su expediente.

### ¿Cuándo y cómo recibiré los números SIREN y SIRET de mi empresa? <!-- collapsable:close -->

Los números SIREN y SIRET son enviados directamente por [Insee](https://www.insee.fr/fr/accueil) (Instituto Nacional de Estadística y Estudios Económicos) por correo y correo electrónico.

Comuníquese con su contacto principal, el Centro de Formalidades Comerciales (CFE) que procesa su caso, para obtener más información. Le indicará la fecha en que se envió la solicitud al INSEE.

### ¿Cuándo recibiré mi extracto de Kbis / mi extracto de K? <!-- collapsable:close -->

Si le preocupa la inscripción en el registro de comercio y sociedades (RCS), recibirá un extracto de Kbis (para todas las personas jurídicas) o K (para las personas físicas que realicen una actividad comercial o que tengan una doble actividad artesanal y comercial), del registro del tribunal mercantil territorialmente competente (GTC).

Los microempresarios y los empresarios individuales que ejercen solo una actividad liberal no se ven afectados por el Kbis y el K.

El tiempo de tramitación depende únicamente del registro y puede variar en función de la admisibilidad del expediente transmitido y la posible necesidad de documentación o información adicional.

### ¿Por qué me dicen que no tengo conocimiento de mi expediente cuando me comunico con el centro de trámites comerciales o el registrador del que dependo?

Si dentro de los quince días hábiles, la organización receptora del trámite no ha podido acusar recibo del trámite, por favor [contacte con el INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1) indicando el número de archivo que comienza con "2020-" o "2021-" o el número de paquete que comienza con "H10000".

### Quiero modificar, cancelar o eliminar mi archivo creado en guichet-entreprises.fr <!-- collapsable:close -->

#### Aún no ha validado su archivo en el sitio guichet-entreprises.fr

- Puede modificar su archivo.

**Advertencia:** Parte de la información no se puede modificar. Estos son los ingresados ​​durante la inicialización del archivo (paso 1). En efecto, la información relativa a la actividad, el lugar de ejercicio de la actividad, la forma jurídica y alguna información adicional (como por ejemplo si ya ha ejercido o no una actividad por cuenta propia, si la actividad de la empresa comenzará o no como una vez creado) permiten completar el trámite y determinar los destinatarios del expediente. En el caso de un error durante la finalización de este paso, se debe inicializar un nuevo archivo.

- Puede eliminar su archivo.

**Nota:** cualquier archivo iniciado y no completado se eliminará automáticamente de la plataforma en 3 meses

#### Ha validado su archivo en el sitio y ha sido enviado a la CFE competente

- No se autorizan más modificaciones en el sitio guichet-entreprises.fr, de acuerdo con la normativa vigente. También será imposible eliminarlo.
- Para cualquier modificación, debe comunicarse con las organizaciones receptoras del archivo:
  - o el archivo aún no ha sido procesado por el cuerpo y, en este caso, se debe ver si la modificación es posible sin tener que rehacer un archivo en línea;
  - o bien el expediente ya ha sido procesado, y en este caso será necesario proceder a un trámite de modificación para subsanar el (los) elemento(s) erróneos.

**Advertencia:** En el primer caso, cualquier modificación que provoque un cambio de forma o modifique la competencia de las organizaciones receptoras requerirá la creación de un nuevo archivo. En el segundo caso, cualquier trámite de modificación deberá realizarse previa atribución del número SIREN, siendo este último necesario para dicho trámite.

**Bueno saber:** De acuerdo con el artículo R. 123-18 del Código de Comercio, el Guichet Entreprises se libera del expediente después de su transmisión a los órganos competentes. En este contexto, los archivos finalizados se eliminarán después de un período de tres meses. Una vez procesada por los órganos competentes, la información relativa a las sociedades civiles y mercantiles se publicará en el Registro Nacional de Comercio y Sociedades. Se pondrán [a disposición del público](https://www.inpi.fr/fr/services-et-prestations-domaine/bases-de-donnees) como datos abiertos y [se distribuirán para su reutilización](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000034924367&cidTexte=LEGITEXT000006069414), bajo licencias de reutilización aprobadas.

Una vez transmitidos los expedientes, los declarantes pueden dirigirse a las autoridades competentes para acceder a sus expedientes o consultarlos en el Guichet Entreprises durante los tres meses siguientes a la transmisión.

El borrado de un fichero finalizado, antes de su borrado automático transcurridos tres meses, debe por otra parte solicitarse al INPI: [formulario de contacto](https://www.inpi.fr/fr/contactez-nous?about=299&under_about=883-702-b1). La modificación de un expediente finalizado está sujeta a una nueva formalidad.