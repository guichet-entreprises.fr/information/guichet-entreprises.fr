﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Qué cambió el 1 de enero de 2018" -->
<!-- var(key)="es-articles-ce_qui_a_change" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Qué cambió el 1 de enero de 2018
==================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

Encuentre una selección de cambios que ocurrieron el 1 de enero de 2018.

## Microempresa: los umbrales de facturación están cambiando

Desde el 1 de enero de 2018, los límites máximos de facturación de una microempresa han aumentado. A partir de ahora, los umbrales se elevan a:

* 170.000 € para la venta de bienes;
* 70.000 € por prestación de servicios.

*Más información por venir, sujeto a la publicación de los decretos de solicitud.*

## Trabajadores por cuenta propia: el RSI se convierte en "Seguridad social - Trabajadores por cuenta propia"

El Régimen de Seguridad Social para Trabajadores Autónomos (RSI) ha cambiado para convertirse en “Seguridad Social - Trabajadores Autónomos”. Durante los años 2018 y 2019, las transformaciones se llevarán a cabo siguiendo un cronograma que oficializará la ley de financiamiento de la Seguridad Social de 2018 y luego de la publicación de los probables decretos de ejecución.

[> Monitorear los cambios al régimen en 2018-2019](http://secu-independants.fr/)

## Afiliación a la pensión complementaria Agirc y Arrco

Desde el 1 de enero de 2018, cualquier empresa de nueva creación está exenta de la obligación de afiliarse a un fondo de pensiones complementario. Solo al contratar a su primer empleado debe completar el proceso de membresía.

Anteriormente, una empresa tenía que unirse a una institución de jubilación complementaria Agirc y Arrco cuando se creó.

El 80% de las estructuras se ven afectadas por esta medida de simplificación, incluidas las empresas muy pequeñas y los microempresarios.

[> Más información](https://www.agirc-arrco.fr/)

*Lista no exhaustiva*