﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Primera visita a guichet-entreprises.fr" -->
<!-- var(key)="es-articles-bienvenue_premiere_visite" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Primera visita a guichet-entreprises.fr
=======================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

** ¡Optimice su visita! ** Para una mejor experiencia de navegación en guichet-entreprises.fr, le recomendamos que utilice los navegadores Mozilla Firefox o Google Chrome. De hecho, es posible que algunos navegadores no admitan determinadas funciones del sitio de Guichet Entreprises.

Sencillo, rápido y seguro: el servicio guichet-entreprises.fr le permite realizar sus trámites online. Guichet-entreprises.fr es la ventanilla única para la creación de empresas. Gratis y accesible para todos, el servicio en línea le permite crear un archivo sin tener que viajar.

## Crea un negocio <!-- collapsable:close -->

¿Tu idea ha sido encontrada, has obtenido la financiación para empezar y has determinado tu situación legal? ¡Entonces su negocio está listo para registrarse!

Los trámites para la constitución de una empresa se centralizan en los centros de trámites empresariales (CFE). Gracias a guichet-entreprises.fr, puede realizar estos trámites [online] (../ formalites.md) sin necesidad de acudir a la CFE competente. Efectivamente, dependiendo de la naturaleza de la actividad profesional (artesanía, comercio, profesión liberal, agricultura, etc.) que ejercerás, el centro de trámites comerciales no es el mismo.

**Lea también**:

[> Creación de una empresa en guichet-entreprises.fr: guía del usuario](./comment_creer_une_entreprise.md)
<br>
[> Relación de trámites comerciales (CFE) según la naturaleza de la actividad profesional](https://www.insee.fr/fr/information/1972060)

## Cambiar la situación y cesar la actividad de una empresa <!-- collapsable:close -->

En guichet-entreprises.fr, también puede actualizar la información relativa a su empresa con todos los organismos competentes (impuestos, Urssaf, Insee, registros públicos, etc.).

Finalmente, es posible que usted dé por terminada su actividad como microempresario y, por lo tanto, cancele su propiedad unipersonal (el trámite de terminación para las empresas aún no es posible en línea). La actualización se hará efectiva con todos los órganos competentes (impuestos, Urssaf, Insee, registros públicos, etc.).

## Crea una cuenta e inicia sesión <!-- collapsable:close -->

Para utilizar el servicio en línea, está invitado a [crear un espacio personal](https://account.guichet-entreprises.fr/user/create) sobre guichet-entreprises.fr. Esto le permitirá crear y administrar posteriormente su(s) archivo(s) y modificar su información personal. Con la autenticación [FranceConnect](https://franceconnect.gouv.fr/), si ya tienes una cuenta en [impots.gouv.fr](https://www.impots.gouv.fr/portail/), [ameli.fr](https://www.ameli.fr/) o [La Poste](https://lidentitenumerique.laposte.fr/), puede conectarse a guichet-entreprises.fr utilizando una de estas tres cuentas.

## Complete y valide su archivo en línea <!-- collapsable:close -->

El sitio guichet-entreprises.fr le permite armar un archivo en línea, adjuntar los documentos de respaldo y pagar las tarifas relacionadas con la formalidad. Una vez constituido y validado el expediente, se envía al organismo competente para su tramitación. Para obtener el seguimiento de su expediente, puede comunicarse con el destinatario de su expediente en cualquier momento.

## Encuentra información sobre una actividad regulada <!-- collapsable:close -->

Determinadas actividades reguladas (peluquería, carnicería, inmobiliaria, abogado, etc.) requieren autorización previa o declaración antes de iniciar su actividad. Guichet-entreprises.fr ofrece fichas técnicas sobre las distintas actividades reguladas con el fin de conocer las condiciones que deben cumplirse y los pasos a seguir para crear una empresa cuya actividad esté regulada.

** ¿Sabías que? ** En Francia, hay 105 actividades reguladas, una lista de las cuales se puede encontrar en la sección [Actividades reguladas](../../reference/es/directive-services/index.md).