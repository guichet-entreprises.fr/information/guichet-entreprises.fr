﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Código APE" -->
<!-- var(key)="es-articles-code_ape" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

# Código APE

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

## ¿Qué es el código APE? <!-- collapsable:open -->

Cada empresa y cada uno de sus establecimientos son asignados por el INSEE (Instituto Nacional de Estadística y Estudios Económicos), cuando se registra en el directorio [Sirene](http://www.insee.fr/fr/methodes/default.asp?page=definition/sys-inf-rep-nat-ent-etab.htm), un código que caracteriza su actividad principal por referencia a la nomenclatura francesa de actividades (NAF).

El código APE (actividad principal que se ejerce) está compuesto por cinco caracteres: cuatro dígitos y una letra. Es un dato fundamental para las estadísticas empresariales porque es la base para clasificar las empresas por sectores de actividad.

## ¿Cómo puedo encontrar mi código APE? <!-- collapsable:open -->

Si ya no conoce su código, puede comunicarse con INSEE proporcionando su número de sirena. También puede encontrarlo [en el sitio web del INSEE](https://avis-situation-sirene.insee.fr/) si no ha solicitado que no se divulgue su información.

Este código se utiliza principalmente con fines estadísticos, permite al INSEE clasificar empresas por sector de actividad.

## Cómo cambiar tu código APE <!-- collapsable:open -->

Si tu actividad ha cambiado, tu solicitud de modificación deberá dirigirse al Centro de Trámites Comerciales (CFE) del que dependes, el cual realizará las gestiones necesarias. Si es un microempresario, este trámite se puede realizar directamente a través del portal guichet-entreprises.fr.

Si no ha cambiado de actividad y cree que el código APE asignado por Insee debe ser corregido, su solicitud de modificación debe realizarse por correo postal a la [oficina regional del INSEE](http://www.insee.fr/fr/service/default.asp?page=entreprises/sirene/sirene_dr.htm) competente para el departamento de implementación de su oficina central, su establecimiento. Un [formulario de modificación de APE](http://www.insee.fr/fr/information/2015441) para ser impreso y completado por usted está disponible en el sitio web de Insee, [www .insee.fr](http://www.insee.fr).

## ¿Por qué no se reconoce mi código APE en el sitio web de Guichet Entreprises? <!-- collapsable:open -->

Es importante respetar la sintaxis exacta del código APE, ya que en caso de error de entrada (falta espacio, letra o número, etc.), el código no será reconocido.

Sin embargo, si resulta que su código APE aún no es reconocido después de verificar la sintaxis, es porque la actividad a la que corresponde no forma parte de la Directiva 2006/123 / EC del Parlamento Europeo y del Consejo de 12 de diciembre de 2006 sobre servicios en el mercado interior. En este caso, esta actividad no entra dentro del alcance de los procedimientos en línea realizados por el sitio guichet-entreprises.fr. De hecho, el sitio guichet-entreprises.fr no es competente para los procedimientos relacionados con la creación, modificación o terminación de una actividad pública o parapública.

Este es el caso de las siguientes actividades:

* Fondos de la pensión;
* administración pública general;
* administración pública (supervisión) de salud, formación, cultura y servicios sociales, distintos de la seguridad;
* actividades de orden público y seguridad;
* servicios de bomberos y salvamento;
* actividades generales de seguridad social;
* gestión de pensiones complementarias;
* distribución social del ingreso;
* actividades de organizaciones de empleadores y consulares;
* reparación de equipos de comunicación;
* actividades de los sindicatos;
* actividades de organizaciones religiosas;
* actividades de organizaciones políticas;
* otras organizaciones que operan por membresía voluntaria;
* actividades de organismos y organizaciones extraterritoriales.