<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="Brexit: iniciar un negocio en Francia siendo británico" -->
<!-- var(key)="es-articles-brexit_creation_entreprise" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

# Brexit: iniciar un negocio en Francia siendo británico

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

Desde el 1 de enero de 2021 y la implementación del Brexit, los ciudadanos británicos que deseen establecer una empresa en Francia deben cumplir con nuevas obligaciones.

## Residías en Francia antes del 1 de enero de 2021

Los británicos que vivan en Francia antes del 1 de enero de 2021 pueden seguir viviendo allí sin un permiso de residencia y trabajar allí sin un permiso de trabajo hasta el **30 de septiembre de 2021**.

Por lo tanto, puede, hasta esa fecha, establecer una empresa en Francia sin tener un permiso de residencia.

A partir del 1 de octubre de 2021, la posesión de dicho título será obligatoria. La [solicitud de un permiso de residencia específico "Acuerdo de retirada del Reino Unido"](https://contacts-demarches.interieur.gouv.fr/brexit/brexit-demande-titre-sejour/) debe realizarse antes del 1 de julio 2021.

## No residías en Francia antes del 1 de enero de 2021

Los británicos que no residían en Francia antes del 1 de enero de 2021 deben obtener un permiso de residencia para establecer una empresa en Francia. Las solicitudes de permiso de residencia deben enviarse a la prefectura.