﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="COVID-19: medidas para ayudar a las empresas" -->
<!-- var(key)="es-articles-covid_19_les_mesures_d_aide_aux_entreprises" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

COVID-19: medidas para ayudar a las empresas
============================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

Ante la epidemia de coronavirus COVID-19, el gobierno ha puesto en marcha medidas de apoyo inmediato para las empresas. Para saber más sobre la implementación de todas estas medidas, acceda a los contactos útiles que le ayudarán en sus esfuerzos, etc.:

https://www.economie.gouv.fr/coronavirus-soutien-entreprises

Para cualquier pregunta relacionada con las consecuencias de la epidemia de COVID-19 en su negocio: [**covid.dge[@]finances.gouv.fr**](mailto:covid.dge[@]finances.gouv.fr)