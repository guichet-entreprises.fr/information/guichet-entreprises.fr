﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Establecer una empresa en guichet-entreprises.fr: guía del usuario" -->
<!-- var(key)="es-articles-comment_creer_une_entreprise" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Establecer una empresa en guichet-entreprises.fr: guía del usuario
========================================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

Cualquiera que sea el sector de actividad y la [forma jurídica](../creation_entreprise/prealables/0-les_formes_juridiques_de_l_entreprise.md) de la empresa que desea crear, guichet-entreprises.fr le permite completar todos los trámites relacionados con la creación: registro, declaración y pago de cualquier costo.

Ahora puede hacer todo sin moverse, en la misma interfaz, con total seguridad, incluso si ejerce una [profesión regulada](https://www.guichet-qualifications.fr/es/dqp/index.html).

## Creación de empresa <!-- collapsable:close -->

Para crear una empresa y darle existencia legal, debe registrarla.

Guichet-entreprises.fr le permite crear su archivo en línea. Este último será enviado al organismo competente para su procesamiento. En Francia, los centros de trámites comerciales (CFE) se encargan de recoger su expediente. Son los interlocutores de primer nivel de las empresas y se encargan de ser la interfaz entre las administraciones y ellas.

El registro de su empresa en guichet-entreprises.fr se realiza en cuatro etapas:

1. creación de su espacio personal (o conexión con sus identificadores de [FranceConnect](https://franceconnect.gouv.fr/));
2. creación de la propia empresa (ya sea un propietario único o una empresa);
3. solicitud de autorización o declaración previa (se puede realizar para determinadas [actividades reguladas](../../reference/es/directive-services/index.md);
4. validación general del archivo.

## Crea un espacio personal <!-- collapsable:close -->

Haga clic en el enlace *Mi cuenta>* [*Registro*](https://account.guichet-entreprises.fr/users/new).

Luego debe completar el formulario que se le propone (los campos seguidos de un asterisco deben ser completados):

- "Dirección de correo electrónico (o dirección de correo electrónico)": esta dirección será su identificador (nombre de usuario) por defecto. Todas las notificaciones se enviarán a la dirección de correo electrónico proporcionada;
- "Nombre": este es su nombre;
- "Nombre": este es su nombre(s) habitual(es);
- "Contraseña": su contraseña debe contener un mínimo de ocho caracteres, incluidos:
  - al menos un número,
  - al menos una letra mayuscula,
  - al menos una letra minúscula,
  - al menos un carácter especial;
- "Confirmar contraseña": vuelva a escribir la contraseña que ha elegido anteriormente.

Se deben completar otros campos: correo electrónico de emergencia, número de teléfono, país, pregunta secreta y respuesta.

Al final de la entrada, haga clic en el botón "Validar" para completar la creación de su espacio personal.

## Registre su empresa: cree su archivo en guichet-entreprises.fr <!-- collapsable:close -->

Una vez que se ha creado la cuenta personal, la creación de la propia empresa puede comenzar haciendo clic en el botón [Conexión].(https://account.guichet-entreprises.fr/session/new).

Deberá completar los campos solicitados relacionados con:

1. su actividad;
2. su ubicación;
3. su estatus legal.

Una vez que haya completado estos campos, no podrá regresar.

Luego comienza la creación de la propia empresa. Deberá completar diversa información en el perfil de la empresa, completar los formularios y agregar los documentos de respaldo requeridos (una copia de un documento de identidad, por ejemplo).

**Es bueno saber**: para descargar los documentos de respaldo, debe tener copias digitales de los documentos (se acepta el formato PDF dentro del límite de 2 MB).

Puede guardar sus datos en cualquier momento y volver para completar su archivo iniciando sesión en su espacio personal. A lo largo de esta etapa se le indicará el centro de trámites empresariales (CFE) del que depende. Puede ponerse en contacto con él una vez validado el archivo y enviado a través de guichet-entreprises.fr para cualquier solicitud relacionada con su seguimiento y procesamiento.

### Caso de actividades reguladas

Algunas [actividades reguladas](../../reference/es/directive-services/index.md) (peluquero, carnicero, etc.) requieren autorización o declaración previa. El sitio guichet-entreprises.fr permite realizar solicitudes de autorización o declaraciones previas para un cierto número de actividades reguladas. Esta posibilidad se extenderá a otras actividades reguladas en una fecha posterior.

Si votre activité est réglementée et que le site ne permet pas pour le moment d'effectuer la demande d'autorisation ou la déclaration préalable, vous ne pourrez procéder qu'à la création de l'entreprise proprement-dite. Vous devrez dans ce cas vous mettre en relation avec l'autorité habilitée pour effectuer la demande d'autorisation ou la déclaration préalable concernée.

### Caso de titulación profesional

Si declaras una actividad artesanal sujeta a la obligación de inscripción en el registro de oficios (RM), deberás acreditar una cualificación profesional. En guichet-entreprises.fr, puede proporcionar una prueba de una calificación de artesanía profesional (JQPA) cuando prepare su archivo de creación de empresa.

## Validación general del archivo y pago de costos <!-- collapsable:close -->

Una vez constituida la empresa, y eventualmente la solicitud de autorización o la declaración previa realizada en el caso de actividades reguladas, dispondrá de un año para proceder a la validación final de su expediente, así como al pago online de costes.

Antes de este último paso, puede leer y / o imprimir los diferentes elementos de este archivo en cualquier momento.

## ¿Qué sucede una vez que haya validado su archivo en guichet-entreprises.fr? <!-- collapsable:close -->

Cuando su archivo se valida en guichet-entreprises.fr, se transmite automáticamente al centro de trámites comerciales (CFE) del que depende.

Esta CFE procesará su expediente. Si la organización necesita documentos adicionales para poder procesar su archivo, se comunicará con usted por correo.

**Bueno saber:** para cualquier pregunta relacionada con el seguimiento y procesamiento de su expediente, debe comunicarse con la CFE que lo recibió.