﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Guichet Entreprises se une al proyecto europeo START EASY" -->
<!-- var(key)="es-articles-guichet_entreprises_s_associe_au_projet_europeen_start_easy" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Guichet Entreprises se une al proyecto europeo START EASY
==========================================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

El servicio Guichet Entreprises es socio del proyecto START EASY para promover la competitividad de las empresas emergentes.

El proyecto START EASY es una iniciativa de la Generalitat de Catalunya realizada en el marco del programa [Interreg Europe](https://www.interregeurope.eu/). Reúne a las autoridades competentes de varios países europeos (Bélgica, España, Francia, Italia, Lituania, Letonia, Polonia) para crear condiciones favorables para el crecimiento.

El proyecto tiene como objetivo crear un entorno favorable para la creación de empresas y la competitividad de las empresas emergentes mediante el desarrollo de herramientas y el desarrollo de políticas específicas.

El desarrollo de ventanillas únicas, la simplificación administrativa y la generalización del principio “Díganos una vez” se encuentran entre las áreas de trabajo de los socios.

## ¿Qué es el programa Interreg Europe?

Interreg Europe ayuda a las autoridades nacionales, regionales y locales de toda Europa a desarrollar e implementar mejores políticas. Al crear un entorno y oportunidades para compartir soluciones, el objetivo de Interreg Europa es garantizar que las inversiones, la innovación y los esfuerzos de implementación tiendan a una integración sostenible para los ciudadanos y las regiones afectadas.

Para lograr este objetivo, Interreg Europe permite que las autoridades públicas regionales y locales compartan sus ideas y experiencias de poner en práctica políticas públicas para mejorar las estrategias para los ciudadanos y las comunidades.

**Vínculos:**

* [Sitio web del proyecto Start Easy](https://www.interregeurope.eu/starteasy/)
* [Cuenta de Twitter](https://twitter.com/StartEasyEU)
* [Página de Facebook](https://www.facebook.com/StartEasyEU/)
* [Sitio web del programa Interreg Europa](https://www.interregeurope.eu/about-us/what-is-interreg-europe/)