﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Trabajar en Francia" -->
<!-- var(key)="es-eugo" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Trabajar en Francia
====================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

## ¿Eres ciudadano de la Unión Europea o del Espacio Económico Europeo y quieres establecerte en Francia? <!-- collapsable:off -->

El servicio INPI Business Counter fomenta la creación de empresas y la movilidad profesional en Francia. El guichet-entreprises.fr y [guichet-qualifications.fr](https://www.guichet-qualifications.fr/es/) juntos constituyen la ventanilla única para la creación de empresas reconocida por la Comisión Europea. Le permiten explorar nuevos puntos de venta y desarrollar una actividad en Francia.

### Actividades reguladas <!-- collapsable:close -->

Infórmate de las condiciones que deben cumplirse para ejercer una actividad regulada en Francia.

\> [Consultar las fichas de información](../reference/es/directive-services/index.md)

### Establecimiento libre <!-- collapsable:close -->

Ahora puedes instalarte en Francia y ejercer tu actividad de forma libre y permanente.

\> [Más información sobre el establecimiento libre](libre_etablissement.md)

### Libertad para brindar servicios <!-- collapsable:close -->

¡Prestar un servicio temporal u ocasional en Francia nunca ha sido tan fácil! Conoce todos los aspectos prácticos antes de prestar tu servicio.

\> [Obtenga más información sobre la libertad de proporcionar servicios](libre_prestation_services.md)

**Bueno saber**

Para obtener el reconocimiento de una titulación profesional, acceda a [guichet-qualifications.fr](https://www.guichet-qualifications.fr/es/)!

### Establecido en otro Estado miembro <!-- collapsable:close -->

¡Todos los países miembros de la Unión Europea tienen una ventanilla única! Las ventanillas únicas son portales de administración en línea para empresarios activos en el sector de servicios. Forman parte de la red "EUGO". Encuentre más información en el sitio web de la Comisión Europea:

<p align="center">[Ventanillas únicas europeas](https://ec.europa.eu/growth/single-market/services/services-directive/in-practice/contact_en) | [Guía práctica para empresas](https://europa.eu/youreurope/business/index_en.htm)

<p align="center">![logo_eugo](./logo_eugo.png)