﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Microempresario comerciante-artesano" -->
<!-- var(key)="es-creation_entreprise-micro_entrepreneur-micro_entrepreneur_commercant_artisan" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Microempresario comerciante-artesano
====================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

Una persona que ejerza una doble actividad comercial y artesanal debe registrarse simultáneamente en el [directorio de oficios](https://www.artisanat.fr/porteur-de-projet/lancer-mon-activite/immatriculation-au-repertoire-des-metiers) (RM) y el registro de comercio y sociedades (RCS) ([ley de 5 de julio de 1996](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000193678&categorieLien=cid)). La CFE de la cámara de oficios y artesanías es la única competente para recibir la declaración. Estos trámites se pueden realizar en guichet-entreprises.fr.

En consecuencia, un empresario puede combinar dentro de una misma empresa una actividad comercial de compra y venta de bienes y mercancías con una actividad artesanal de producción, transformación, reparación o prestación de servicios. Estas actividades pueden estar completamente separadas o vinculadas (tal es el caso, en particular, del procesamiento de productos o materias primas antes de la reventa, que es inherente a determinadas actividades de venta al por menor).

Al registrar una empresa como microempresario con actividad mixta, se determina la actividad principal de acuerdo a la que debe generar más ingresos.

**Los microempresarios del sector de la construcción y la artesanía forman parte de una actividad mixta cuando comercializan las materias primas para las obras que realizan.**

## El régimen del microempresario en actividad mixta

En el caso de una actividad mixta realizada bajo el régimen del microempresario, este último no da lugar a un aumento del volumen de negocios (volumen de negocios), pero las dos actividades distintas caen por debajo de dos umbrales diferentes.

El techo que no debe superarse sigue siendo:

* 176.200 € para las actividades de compra-reventa de bienes, objetos, suministros, alimentos para llevar o consumir in situ o incluso servicios de alojamiento;
* 72.600 € por prestación de servicios.

Si la actividad de compra-reventa constituye la actividad principal, la facturación global no debe superar los 176.200 €. Dentro de este importe, el volumen de negocios relacionado con la prestación de servicios no debe superar los 72.600 €.

Para obtener más información, consulte el [artículo sobre el régimen del microempresario](./le_regime_micro_entrepreneur.md).

Para cualquier pregunta relacionada con los trámites comerciales, debe comunicarse con la cámara de oficios y artesanías en el lugar del establecimiento principal.