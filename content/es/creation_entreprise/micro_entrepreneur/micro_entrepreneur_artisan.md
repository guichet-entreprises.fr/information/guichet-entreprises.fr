﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Artesano microempresario" -->
<!-- var(key)="es-creation_entreprise-micro_entrepreneur-micro_entrepreneur_artisan" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Artesano microempresario
==========================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

El artesano es un emprendedor independiente que se ocupa de todas las etapas de producción y comercialización de sus productos. Esta actividad se puede realizar de forma primaria o secundaria.

La actividad artesanal se define por la producción de servicios o productos. Esta actividad de producción, transformación, reparación o prestación de servicios del tipo [BIC](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32919) (beneficios industriales y comerciales) se realiza gracias a a un conocimiento específico. Los oficios abarcan tres categorías principales: oficios de construcción, comercio de alimentos y oficios de producción y servicios. Dependiendo de si la profesión está regulada o no, los trámites de declaración son diferentes.

La actividad artesanal se vuelve comercial cuando la empresa emplea a más de 10 personas (o, en Alsace Moselle, cuando utiliza procesos puramente industriales).

El microempresario artesanal es un profesional que se rige por el régimen fiscal de la microempresa ([régimen fiscal micro-BIC](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32919) (beneficios industriales y comerciales) y [régimen de exención del IVA](https://www.service-public.fr/professionnels-entreprises/vosdroits/F21746)), a quienes se applica el régimen microsocial simplificado previsto en el [artículo L. 133-6-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000019285616&dateTexte=&categorieLien=cid) del Código de la Seguridad Social.

Para obtener más información, consulte el artículo sobre [el régimen del microempresario](./le_regime_micro_entrepreneur.md).

Algunas actividades artesanales están sujetas a una **cualificación profesional**. La lista de profesiones en cuestión está disponible en el sitio [guichet-qualifications.fr](https://www.guichet-qualifications.fr/es/dqp/artisanat/index.html). actividades artesanales se enumeran en la guía de los comercios ([apéndice de decreto n.° 98-247](http://legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0F6D69D7A7595283840D4B6E1396FF9D.tpdila09v_2?idArticle=LEGIARTI000030665874&cidTexte=LEGITEXT000005625550&dateTexte=20150824) de 2 de abril de 1998 relativo a la calificación artesanal y al directorio de oficios.

## Campos de actividad sujetos a cualificación profesional <!-- collapsable:close -->

Las siguientes actividades solo pueden ser realizadas por persona profesionalmente cualificada o bajo el control efectivo y permanente de esta última:

* mantenimiento y reparación de vehículos y máquinas;
* construcción, mantenimiento y reparación de edificios;
* instalación, mantenimiento y reparación de redes y equipos que utilizan fluidos, así como materiales y equipos destinados al suministro de gas, calefacción de edificios e instalaciones eléctricas;
* barriendo;
* cuidados estéticos a la persona distintos de los médicos y paramédicos y masajes de confort estético sin finalidad médica;
* producción de prótesis dentales;
* para la preparación o fabricación de productos frescos de panadería, pastelería, carnicería, charcutería y pescado, así como para la preparación o elaboración de helados caseros;
* herrador.

Requisitos (diplomas, certificados, etc.) para acceder al ejercicio de una actividad artesanal regulada (lista no exhaustiva, más información en [guichet-qualifications.fr] (https://www.guichet-qualifications.fr /fr/dqp/artisanat/index.html)).

La persona calificada (que puede ser el propio empresario, pero también su cónyuge si participa en la actividad, o incluso un empleado) debe poseer:

* o un certificado de competencia profesional;
* o un certificado de estudios profesionales;
* Diploma o título de nivel equivalente o superior aprobado ([ley n.° 96-603](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000193678&categorieLien=cid) de 5 de julio de 1996 relativa al desarrollo y la promoción del comercio y la artesanía). Para conocer el nivel de su diploma, puede consultar el [Directorio nacional de certificaciones profesionales](http://www.rncp.cncp.gouv.fr/) (RNCP).

A falta de diploma o título, de nivel V en la profesión ejercida, esta persona deberá acreditar una experiencia profesional de tres años efectiva en el territorio de la Unión Europea (UE) o de otro Estado parte para el convenio del Espacio Económico Europeo (EEE) adquirido como consejero de empresa, autónomo o asalariado en el ejercicio de una de las profesiones previstas en el listado de actividades reguladas.

### Casos particulares

#### La obligación de contratar un seguro para actividades en el sector de la construcción

La persona que ejerza su actividad en el sector de la edificación deberá mencionar en sus cotizaciones y facturas el seguro profesional que haya contratado para su actividad, cuando sea obligatorio, así como los datos de contacto de su compañía de seguros y la cobertura. área geográfica de su contrato.

#### Aprovecha la calidad del artesano

Para poder utilizar el término "artesano" o sus derivados, debes tener un diploma de nivel V o equivalente en tu oficio o tener tres años de experiencia profesional. Al registrarse, la CMA le pedirá que justifique estas condiciones para poder mencionar su calidad en el directorio de oficios y en la tarjeta profesional que se le entregará.

#### Aprovecha la calidad del artesano

La calidad de artesano reconoce una calificación profesional para un oficio de arte mencionado en [la lista fijada por decreto](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031941949).La calidad de artesano se otorga en las mismas condiciones que la calidad de artesano: ser titular de un diploma de nivel V, justificar tres años de experiencia profesional y realizar una solicitud específica.

Para cualquier pregunta relacionada con los trámites comerciales, debe comunicarse con la cámara de oficios y artesanías en el lugar del establecimiento principal.

#### Aprovecha la calidad de maestro artesano / maestro artesano en el arte

La calidad de maestro artesano o maestro artesano en arte se otorga en las mismas condiciones a los líderes empresariales individuales que posean una maestría en la profesión ejercida y justifiquen una experiencia profesional de dos años así como a las personas inscritas en el registro de oficios desde hace al menos diez años que acrediten, en ausencia de un título, conocimientos reconocidos para la promoción (o formación) de la artesanía.

Encuentra más información en [el portal de artesanía](https://www.artisanat.fr/artisan/valoriser-mon-activite/obtenir-un-titre-de-maitre-artisan).

#### Peluquería

El ejercicio de la actividad de peluquería debe estar bajo el control efectivo y permanente de una persona profesionalmente cualificada. Asimismo, la actividad de peluquería profesional en domicilios particulares debe ser realizada por persona cualificada.

Esta persona (que puede ser el propio empresario, uno de sus empleados, su cónyuge colaborador) debe tener:

* o el certificado de peluquería profesional (BP);
* o el título de maestría en peluquería (BM);
* ya sea el diploma o título registrado o haber sido registrado en el [Directorio Nacional de Certificaciones Profesionales](http://www.rncp.cncp.gouv.fr/) (RNCP) en el mismo campo que el certificado de peluquería profesional y en un nivel igual o superior.

Para obtener más información, consulte el artículo sobre [el régimen micro-social y micro-fiscal](./le_regime_micro_entrepreneur.md).

## Références <!-- collapsable:close -->

* [Decreto n.° 98-247](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000571009) de 2 de abril de 1998 relativo a la calificación artesanal y al directorio de oficios
* [Decreto n.° 98-246](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=B5391EAD59BB6DBBE4DF181DD395A375.tpdjo08v_2?cidTexte=JORFTEXT000000388449&dateTexte=20090831) de 2 de abril de 1998 relativo a la calificación profesional requerida para el ejercicio de las actividades previstas en el artículo 16 de la [ley n.° 96-603](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000193678&categorieLien=cid) de 5 de julio de 1996 relativo al desarrollo y la promoción del comercio y la artesanía
* Articulos 16 y 19 de la [ley n.° 96-603](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000193678&categorieLien=cid) de 5 de julio de 1996 relativo al desarrollo y la promoción del comercio y la artesanía
* Articulo 1 del [decreto n.° 97-558](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000383103&categorieLien=cid) de 29 de mayo de 1997 relativo a las condiciones de acceso a la profesión de peluquero
* Articulo 3 de la [ley n.° 46-1173](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006068010&dateTexte=20091214) de 23 de mayo de 1946 que regula las condiciones de acceso a la profesión de peluquero