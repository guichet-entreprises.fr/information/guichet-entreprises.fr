﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Agente comercial microempresario" -->
<!-- var(key)="es-creation_entreprise-micro_entrepreneur-micro_entrepreneur_agent_commercial" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Agente comercial microempresario
==================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

El agente comercial es un agente que, como profesión regular e independiente, negocia y eventualmente concluye contratos de compra, venta, alquiler o prestación de servicios, en nombre y por cuenta de otras personas. Es un verdadero emprendedor que no debe estar bajo la subordinación de su principal, lo que lo distingue como tal de un vendedor asalariado (representantes de ventas en particular).

## Requisitos previos

El agente comercial puede optar por ejercer en diferentes formas jurídicas (propiedad única con o sin opción al [régimen del microempresario](./le_regime_micro_entrepreneur.md) o EIRL (empresario individual con responsabilidad limitada), sociedad civil o comercial).

Para obtener más información, consulte el artículo sobre [formas legales de empresa](../prealables/0-les_formes_juridiques_de_l_entreprise.md).

## Su actividad

La actividad del agente comercial, definida por [artículo L. 134-1](http://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=B1D7B5F3637A8F91E5735137A590921D.tpdjo08v_3?idArticle=LEGIARTI000006220397&cidTexte=LEGITEXT000005634379&dateTexte=20101125) del Código de Comercio, es una actividad civil que consiste en la venta de bienes, objetos o alimentos procesados ​​o no, o en la prestación de alojamiento tipo hotel o casa de huéspedes, con fines de lucro.

El agente comercial negocia y concluye, de forma permanente e independiente, contratos de compraventa, alquiler o prestación de servicios, en nombre y por cuenta de otras empresas.

## Declara tu actividad

Debe inscribirse en el Registro Especial de Agentes Comerciales (RSAC) con el secretario del tribunal comercial (o el tribunal superior que dictamina comercialmente en los departamentos de Bas-Rhin, Haut-Rhin y Moselle) de su domicilio ([el registro es de pago](https://www.service-public.fr/professionnels-entreprises/vosdroits/F23282)). Este trámite se puede realizar online en guichet-entreprises.fr.

## Régimen microfiscal y microsocial

Un agente comercial microempresario es una persona natural que depende del régimen microfiscal (régimen especial [beneficios no comerciales](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32105) (BNC) para la tributación de los beneficios y [régimen de exención del IVA](https://www.service-public.fr/professionnels-entreprises/vosdroits/F21746)) a quienes se aplica el régimen microsocial simplificado previsto en el [artículo L. 133-6-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000019285616&dateTexte=&categorieLien=cid) del Código de la Seguridad Social.

Para obtener más información, consulte el artículo sobre [el régimen del microempresario](./le_regime_micro_entrepreneur.md).