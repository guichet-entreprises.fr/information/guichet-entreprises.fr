﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Comerciante microempresario" -->
<!-- var(key)="es-creation_entreprise-micro_entrepreneur-micro_entrepreneur_commercant" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Comerciante microempresario
==============================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

Un comerciante es una persona que realiza actos comerciales y que la hace su profesión habitual ([artículo L. 121-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006219167&cidTexte=LEGITEXT000005634379&dateTexte=20000921) del Código de Comercio).

La ley considera en particular como actos comerciales (artículos [L. 110-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006219125) y [L. 110-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006219126&cidTexte=LEGITEXT000005634379&dateTexte=20000921) del Código de Comercio) :

* cualquier compra de bienes muebles para reventa, ya sea en especie o después de haberlos trabajado y ejecutado;
* cualquier compra de bienes inmuebles con el fin de revenderlos, a menos que el comprador haya actuado con miras a erigir uno o más edificios y venderlos a granel o por locales;
* cualquier transacción de intermediación para la compra, suscripción o venta de edificios, activos comerciales, acciones o participaciones en empresas inmobiliarias;
* cualquier negocio de alquiler de muebles;
* cualquier empresa de fabricación, puesta en servicio, transporte terrestre o acuático;
* cualquier empresa de suministros, agencia, oficinas comerciales, casas de subastas, espectáculos públicos;
* cualquier transacción de divisas, banca, corretaje, emisión y administración de dinero electrónico y cualquier servicio de pago;
* todas las operaciones de los bancos públicos;
* todas las obligaciones entre comerciantes, comerciantes y banqueros;
* billetes de cambio;
* cualquier empresa de construcción, y todas las compras, ventas y reventas de embarcaciones para navegación interior y exterior;
* todos los envíos marítimos;
* cualquier compra y venta de aparejos, artes y suministros;
* cualquier chárter o chárter, préstamo o préstamo a granel;
* todos los seguros y otros contratos relacionados con el comercio marítimo;
* todos los acuerdos y convenciones sobre salarios y alquileres de la tripulación;
* todos los compromisos de la gente de mar para el servicio de buques comerciales.

## Realización de la actividad en una oficina o tienda

El comercio de tiendas no se adapta bien al [régimen de microempresarios](./le_regime_micro_entrepreneur.md), debido a la no deducibilidad de los alquileres y los cargos relacionados del volumen de negocios declarado y se tienen en cuenta en la evaluación del cumplimiento de los límites máximos de la microempresa.

## Régimen microfiscal y microsocial

Un microempresario es una persona que se rige por el régimen fiscal de microempresas ([régimen fiscal micro BIC](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32919) (beneficios industriales y comerciales) y [régimen de exención del IVA](https://www.service-public.fr/professionnels-entreprises/vosdroits/F21746)) a quienes se aplica el régimen microsocial simplificado previsto en el [artículo L. 133-6-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000019285616&dateTexte=&categorieLien=cid) del Código de la Seguridad Social.

Las cotizaciones sociales de un microempresario cuya actividad es comercial ascienden al 12,8% de su facturación recaudada durante el mes o el trimestre, según la tasa de pago elegida.

El microempresario se beneficia del derecho a las dietas en condiciones. La contribución obligatoria a la formación profesional de microempresarios y comerciantes asciende al 0,10% de su facturación.

Para obtener más información, consulte el [artículo sobre el régimen de microempresarios](./le_regime_micro_entrepreneur.md).

## Especificidades

El microempresario es responsable del impuesto por tasas de cámara de comercio e industria, salvo que esté exento del aporte patrimonial empresarial, es decir, cuando su facturación sea menor o igual. hasta 5.000 €.

Para cualquier pregunta relacionada con los trámites comerciales, debe comunicarse con la cámara de comercio e industria del lugar de establecimiento principal.