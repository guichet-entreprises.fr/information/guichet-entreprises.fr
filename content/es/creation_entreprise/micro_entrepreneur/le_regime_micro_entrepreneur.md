﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="El régimen del microempresario" -->
<!-- var(key)="es-creation_entreprise-micro_entrepreneur-le_regime_micro_entrepreneur" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

El régimen del microempresario <!-- collapsable:close -->
==============================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

**El régimen del microempresario permite una creación empresarial simplificada en cuanto a trámites y declaraciones fiscales y sociales.**

**Este plan es especialmente adecuado para personas que deseen desarrollar una actividad complementaria o experimental.**

**A cambio, impone ciertos límites a la facturación y al ámbito de actividad.**

## Microempresario, un plan con múltiples beneficios

Creado el 4 de agosto de 2008 por la ley de modernización de la economía, el régimen del microempresario, originalmente denominado régimen de autoempresarios, es el régimen más sencillo para crear una actividad independiente.

Un microempresario es una persona que realiza una actividad por cuenta propia en una empresa unipersonal que ha optado por el régimen de microempresa y que se beneficia de un régimen social particular, el micro-social, que no es un estatus legal como que tal.

Es posible optar por este régimen al declarar el inicio de actividad realizado a la CFE.

El registro del microempresario es obligatorio y gratuito:

* el directorio de oficios (RM) para artesanos;
* en el registro de comercio y sociedades (RCS) para comerciantes.

La inscripción es obligatoria y con cargo:

* en el Registro Especial de Agentes Comerciales (RSAC) para agentes comerciales;
* en el Registro Especial de Emprendedores Individuales de Responsabilidad Limitada (RSEIRL) para profesiones liberales que hayan optado por la forma jurídica de EIRL.

Sus ventajas:

* Las cargas sociales corresponden a un porcentaje de la facturación. Si el volumen de negocios es cero, no hay cargos a pagar, a diferencia del régimen de common law, en el que se fijan;
* las cotizaciones sociales se deducen una vez que se ha alcanzado la facturación;
* la empresa no factura el IVA, al menos mientras su facturación se mantenga por debajo de los umbrales deducibles en base al IVA (85.500 € para actividades de compra / venta y 36.500 € para servicios ).

## Microempresario, facturación limitada

Las actividades comerciales, artesanales y liberales se pueden ejercer bajo el régimen del microempresario siempre que no superen una determinada facturación anual:

* 176.200 € para la venta de bienes, restauración o alojamiento;
* 72.600 € para la prestación de servicios y las profesiones liberales.

El volumen de negocios debe declararse, aunque sea cero, en [net-entreprises.fr](https://www.net-entreprises.fr/).

Si se supera la facturación autorizada durante dos años consecutivos, se conmuta automáticamente:

* hacia el sistema social de derecho común;
* hacia el verdadero sistema tributario simplificado.

## Microempresario, un sistema fiscal especial

### Impuesto sobre la renta

Todos los microempresarios están sujetos al impuesto sobre la renta.

El beneficio del microempresario se calcula aplicando una deducción sobre la facturación por gastos profesionales según la naturaleza de la actividad:

* 71% para las actividades de venta de mercancías, restauración y alojamiento;
* 50% para la prestación de servicios;
* 34% para las profesiones liberales;

En todos los casos se aplica una reducción mínima de 305 €.

Para determinar el impuesto sobre la renta adeudado, el beneficio fijo así calculado se integra en la renta imponible global, con los demás ingresos del hogar fiscal, y está sujeto a la escala progresiva por tramos del impuesto sobre la renta.

### El pago liberatorio

El [pago liberatorio](https://www.impots.gouv.fr/portail/professionnel/le-versement-liberatoire) El impuesto sobre la renta es un método de pago de impuestos simplificado destinado exclusivamente a microempresarios.

En 2020, los microempresarios cuyo importe de renta del hogar fiscal del penúltimo año (año N-2) no supere los 27.519 € pueden optar por esta forma de pago.

Este umbral es válido para parte del cociente familiar. Se incrementa en un 50% por media acción o un 25% por cada trimestre adicional.

El monto del pago liberatorio se calcula según el volumen de negocios. El pago del impuesto se realiza entonces al mismo tiempo que el pago de sus cargas sociales cada mes o trimestralmente según la frecuencia elegida, a las siguientes tarifas:

* 1% para actividades de venta de mercancías, catering o alojamiento;
* 1,7% para la prestación de servicios;
* 2,2% para las profesiones liberales.

### La contribución de propiedad comercial (CFE)

Los microempresarios están sujetos a la contribución patrimonial de las empresas según una escala progresiva. Están exentos de ella durante su primer año de actividad.

Además, todos los emprendedores cuya facturación sea menor o igual a 5.000 € están exentos de la CFE mínima.

### Tarifas de habitación para comerciantes y artesanos

Los microempresarios que están exentos del impuesto a la propiedad empresarial (CFE) (facturación inferior o igual a 5.000 €) también están exentos del costo de la habitación.

Para los microempresarios comerciales y los microempresarios artesanales que no están exentos, la estimación del monto a pagar se realiza en base a un porcentaje de la facturación. El impuesto se recauda al mismo tiempo que las demás cotizaciones sociales.

## Microempresario, con una cuenta bancaria dedicada

Desde la ley PACTE de 22 de mayo de 2019, la tenencia de una cuenta bancaria dedicada a la actividad profesional es obligatoria solo si la facturación generada es superior a 10.000 € durante al menos dos años consecutivos.

## Microempresario, cambio de estatus

El microempresario puede modificar la información relativa a su negocio (denominación social, cambio de domicilio o establecimiento personal, cambio de actividad, etc.) o cambio de régimen si permanece como propietario único. 

## Microempresario, cese temporal o permanente de actividades

En cualquier momento, el microempresario puede cesar su actividad de forma temporal o definitiva.

Si el microempresario desea suspender temporalmente su actividad, lo único que tiene que hacer es indicar, en su declaración mensual o trimestral, una facturación igual a cero, por un máximo de veinticuatro meses.