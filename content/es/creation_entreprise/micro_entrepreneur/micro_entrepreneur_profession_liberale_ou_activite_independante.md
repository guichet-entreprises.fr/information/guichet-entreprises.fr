﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur profession libérale ou activité indépendante" -->
<!-- var(key)="es-creation_entreprise-micro_entrepreneur-micro_entrepreneur_profession_liberale_ou_activite_independante" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Microempresario de profesión liberal o actividad independiente
=============================================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

**Actividad liberal** incluye cualquier actividad profesional por cuenta propia, no agrícola, no comercial o no artesanal; esto cubre en particular todos los servicios del tipo [BNC](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32105) (beneficios no comerciales). Lo lleva a cabo un proveedor de servicios que trabaja de forma independiente y que demuestra calificaciones específicas, vendido a un tercero bajo su propia responsabilidad.

**Profesiones liberales** se refieren a personas que habitualmente ejercen, de forma independiente y bajo su responsabilidad, una actividad de carácter generalmente civil cuya finalidad es prestar, en interés del cliente o del público, principalmente servicios intelectuales. , técnicas o tratamientos implementados mediante la titulación profesional adecuada y de conformidad con los principios éticos o deontológicos profesionales.

Se dividen en dos categorías: [profesiones reguladas](https://www.guichet-qualifications.fr/es/dqp/index.html) y profesiones no reguladas.

**Las profesiones liberales reguladas** están clasificadas en el sector liberal por ley. Su ejercicio está sujeto a estrictas normas éticas y están bajo el control de su cuerpo profesional (orden, cámara o sindicato). Entre ellos, los más conocidos son abogados, contables o incluso médicos. Su título está protegido.

**Profesiones liberales no reguladas** incluyen profesiones que no pueden clasificarse en otro lugar, es decir que no son comerciales, ni artesanales, ni agrícolas y no entran en el régimen general de asalariados (periodistas y autónomos, artistas de autor, trabajadores de espectáculos, modelos, árbitros y jueces deportivos, etc.). Se trata generalmente de profesiones de carácter intelectual o artístico. Aunque clasificadas en la categoría de actividades liberales no reguladas por no estar controladas por un organismo profesional, su ejercicio puede, no obstante, estar sujeto a normas específicas.

Las fichas de actividades que rigen el ejercicio de una profesión se pueden consultar en el apartado [Actividades reguladas](../../../reference/es/directive-services/index.md).

Las profesiones sujetas a normativas específicas se pueden consultar en [guichet-qualifications.fr](https://www.guichet-qualifications.fr/es/dqp/index.html).

## Requisitos previos <!-- collapsable:close -->

Un microempresario es una persona sujeta al régimen fiscal de microempresas ([régimen especial para beneficios no comerciales](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32105) (BNC) para la tributación de beneficios y [régimen de IVA de la franquicia básica](https://www.service-public.fr/professionnels-entreprises/vosdroits/F21746)) a quienes se aplica el régimen microsocial simplificado previsto en el [artículo L. 133-6-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000019285616&dateTexte=&categorieLien=cid) del Código de la Seguridad Social.

Para obtener más información, consulte el artículo sobre [el régimen de microempresarios](./le_regime_micro_entrepreneur.md).

## Declara su actividad <!-- collapsable:close -->

Puede declarar su actividad en [guichet-entreprises.fr](https://forms.guichet-entreprises.fr/profilEntreprisesV3). Luego, su declaración se enviará a Urssaf para su procesamiento.

Para beneficiarse de la condición de microempresario, la facturación anual no debe superar el umbral del régimen de micro-impuestos, establecido en 2020 en 72.600 € para la prestación de servicios liberales.

Para obtener más información, consulte el artículo sobre [el régimen del microempresario](./le_regime_micro_entrepreneur.md).

**Notas**

El registro de la actividad liberal se debe hacer con la orden, el sindicato o la cámara profesional de la que depende el microempresario y se hace la declaración de actividad a la [Urssaf](https://www.urssaf.fr/portail/home.html) que luego ocupa el rol de CFE (centro de trámites empresariales).

La [Seguridad social - Autónomos](https://www.secu-independants.fr/) es el régimen de protección social obligatorio que gestiona el seguro de enfermedad para los autónomos.

Actividades en el marco de la [Caisse nationale des barreaux français](https://www.cnbf.fr/fr/accueil-2) (CNBF) y secciones del [Fondo nacional de seguro de pensiones para profesiones liberales](https://www.cnavpl.fr/) (CNAVPL) que no sean Cipav no son elegibles para este estado.

### Enlaces útiles

* Declaración y pago de cargos: [el sitio web oficial de declaraciones sociales](http://www.net-entreprises.fr/)
* Protección social : [Seguridad social - Autónomos](http://www.secu-independants.fr/)
* Jubilación: [Fondo interprofesional para profesiones liberales](https://www.lacipav.fr/)

### Références

* Artículo 43 de la [directiva europea n.° 2005/36/CE](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2005:255:0022:0142:fr:PDF) sobre el reconocimiento de cualificaciones profesionales
* Artículo 29 de la [ley 2012-387](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025553296&categorieLien=id#JORFARTI000025553621) de 22 de marzo de 2012 relativo a la simplificación de la ley y la reducción de procedimientos administrativos