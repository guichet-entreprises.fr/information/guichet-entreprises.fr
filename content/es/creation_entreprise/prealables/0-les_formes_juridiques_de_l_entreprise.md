﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Las formas legales de la empresa" -->
<!-- var(key)="es-creation_entreprise-prealables-0-les_formes_juridiques_de_l_entreprise" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Las formas legales de la empresa
=====================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

**A la hora de montar una empresa, debes elegir la forma jurídica en la que llevarás a cabo tu actividad.**

**Microempresario, EIRL, EURL, SA, SARL, etc., la elección del estatus legal tiene implicaciones fiscales y sociales.**

**El tipo de actividad, la presencia de socios, las responsabilidades que desea ejercer o incluso la protección de sus bienes privados le guiarán en la elección de la forma jurídica de su empresa.**

## Tu creas tu negocio solo

Puede ser un microempresario, un comerciante en su propio nombre (propiedad única), ser el socio único de una sociedad unipersonal con responsabilidad limitada (EURL) o de una sociedad anónima simplificada unipersonal (SASU).

### La empresa unipersonal (microempresario y EI)

El microempresario es un empresario individual que se beneficia de un [régimen simplificado](../micro_entrepreneur/le_regime_micro_entrepreneur.md).

La propiedad unipersonal es propiedad exclusiva de una persona física que forma con él una entidad única y es plenamente responsable de la misma.

La identidad de la empresa corresponde a la del gerente, a lo que podemos agregar un [nombre comercial](4-les_noms_de_l_entreprise.md) : Marion Durand, Taller de las estrellas.

Tenga en cuenta que el administrador es indefinidamente responsable de las deudas de su empresa en su propia propiedad. Sin embargo, puede proteger su residencia principal así como sus bienes inmuebles no asignados a la actividad profesional (residencia secundaria o terrenos) haciendo una declaración de exención de embargo ante su notario. Esta declaración se anuncia.

Los artesanos, comerciantes, industriales y las profesiones liberales pueden establecer su negocio bajo el régimen de propiedad única.

El empresario individual no percibe salario. Las ganancias (facturación menos gastos) constituyen sus ingresos. Están sujetos al impuesto sobre la renta en la categoría correspondiente a la actividad: utilidades industriales y comerciales (BIC), como comerciante o artesano, o utilidades no comerciales (BNC) si se trata de un profesión liberal.

Depende de la [Seguridad Social - Independientes](https://www.secu-independants.fr/) (SSI).

### El propietario único con responsabilidad limitada (EIRL)

La condición de empresario individual con responsabilidad limitada (EIRL) permite al empresario proteger su propiedad personal, mediante la creación de un activo destinado a su actividad profesional distinto de sus activos privados. Legalmente, es un propietario único.

### La sociedad anónima simplificada de un solo miembro (SASU)

La sociedad anónima simplificada unipersonal (SASU) es una empresa con un solo socio.

Como cualquier empresa, la SASU debe poseer un capital social, depositado a su nombre en un banco. Su importe mínimo es de 1 €.

La SASU debe designar un presidente, persona física o persona jurídica.

Para obtener más información sobre las diferentes formas legales, consulte [artículo del sitio service-public.fr](https://www.service-public.fr/professionnels-entreprises/vosdroits/F23844).

### La sociedad de responsabilidad limitada unipersonal (EURL)

La empresa unipersonal con responsabilidad limitada (EURL) tiene un solo socio, pero obedece las reglas de una LLC (ver más abajo).

El único socio de una EURL no puede ser otra EURL.

La EURL genera más obligaciones legales y contables que la Compañía Individual.

El accionista único tiene la obligación de constituir, desde la creación de la EURL, un capital social, cuyo importe se determina libremente en los estatutos.

En el caso de EURL cuyo único socio persona física ejerza personalmente la gestión, se podrá optar por el régimen de microempresario.

Puede liberarse en las mismas condiciones que las de una LLC.

## Están creando su negocio juntos

### Está creando una sociedad de responsabilidad limitada (SARL)

La sociedad de responsabilidad limitada (SARL) es la forma de sociedad más común en Francia.

Es una forma legal simple, que requiere la presencia de al menos dos socios cuya responsabilidad se limita al monto de sus aportes.

#### El capital social de la SARL <!-- collapsable:close -->

No se requiere capital social mínimo. Se puede crear una LLC con 1 euro simbólico.

El monto del capital lo establecen los socios, dependiendo de la naturaleza del proyecto y las necesidades de capital de la empresa.

Quien aporta al capital automáticamente se convierte en socio y recibe una cantidad de acciones proporcional al monto de su aportación.

La elección de la LLC permite a los socios proteger sus activos personales. En caso de dificultades financieras, su responsabilidad se limita estrictamente al monto de sus contribuciones. Los acreedores solo se apoderan de los activos de la empresa y no tienen acceso a los activos personales de los socios.

#### Socios de la SARL <!-- collapsable:close -->

Una LLC debe tener entre 2 y 100 socios.

Los socios de las SARL pueden ser personas físicas (personas físicas) o personas jurídicas (estructuras jurídicas como empresas o asociaciones según la ley de 1901).

No hay condición de edad para ingresar al capital de una LLC.

La LLC puede ser un solo accionista. Esto se denomina SARL unipersonal o Sociedad de responsabilidad limitada unipersonal (EURL) (ver más abajo).

#### Contribuciones de socios de la SARL <!-- collapsable:close -->

Las contribuciones pueden realizarse en efectivo (suma de dinero), en especie (locales, equipos, etc.) o incluso "en la industria", es decir cuando una persona pone sus conocimientos a disposición de la sociedad. técnicas, conocimientos técnicos o incluso servicios.

En caso de aportaciones en especie o en la industria, el procedimiento a seguir es más engorroso (nombramiento de un auditor de aportaciones encargado de evaluarlas, etc.).

Estas aportaciones permiten tener acciones, votar en juntas generales y recibir dividendos.

Los derechos de voto y las ganancias se calculan en proporción al número de acciones que posee cada socio.

Se recomienda la realización de un pacto de accionistas que especifique determinadas reglas de funcionamiento para evitar situaciones de bloqueo.

#### El gerente de la SARL <!-- collapsable:close -->

La gestión de la SARL es ejercida por uno o más administradores designados por los socios.

Este último realiza todos los actos de gestión en interés de la empresa: firma de contrato, contratación de empleados, acciones legales, etc. Es el representante legal de este último y, por tanto, actúa en su nombre y por cuenta del mismo.

También es responsable de la celebración de juntas generales. Sus facultades podrán ser limitadas por los socios, quienes podrán subordinar la realización de determinados actos a la previa autorización de la asamblea.

Desde el punto de vista fiscal y social, su estatus varía si es socio mayoritario o minoritario.

Su responsabilidad se limita al monto de sus aportes salvo en caso de falta de gestión donde, en este caso, compromete su propio patrimonio.

#### La tributación del SARL <!-- collapsable:close -->

Fiscalmente, una LLC está sujeta en principio al impuesto de sociedades (IS).

En determinadas condiciones, los asociados de la LLC pueden decidir, por unanimidad, optar por el régimen fiscal de las sociedades, es decir, una tributación de utilidades contra el impuesto sobre la renta. La totalidad de las ganancias del SARL se paga luego a los socios, en proporción a las contribuciones de cada uno, y sujeto a su impuesto sobre la renta en la categoría BIC/BNC. La opción del impuesto sobre la renta debe ejercerse dentro de los primeros cinco años de existencia de la LLC y tiene una vigencia de cinco años (años contables), después de los cuales la empresa está automáticamente sujeta nuevamente al impuesto sobre la renta. empresas.

#### Cese de acciones de SARL <!-- collapsable:close -->

Cualquier socio que desee vender sus acciones en SARL debe obtener primero el acuerdo de la mayoría de sus socios. A esto se le llama procedimiento de licencia.

Como excepción, las acciones de la empresa pueden transferirse libremente a otro socio oa un miembro de la familia (cónyuge, padres o hijos), a menos que se disponga lo contrario en los estatutos.

### Estás creando una empresa autónoma

Las sociedades de práctica liberal (SEL) permiten a las profesiones liberales ejercer su actividad en forma de sociedad de capital.

La actividad se lleva a cabo a través de las estructuras legales existentes y es posible la asociación con otros profesionales en el mismo campo.

La empresa de práctica liberal en ningún caso puede ser multidisciplinar y debe estar inscrita en el registro mercantil.

#### SELARL, SELAFA, SELAS y SELCA: empresas autónomas <!-- collapsable:close -->

Los SEL se rigen por las regulaciones aplicables a las empresas comerciales.

Hay cuatro tipos de empresas autónomas:

* la empresa de práctica privada con responsabilidad limitada (SELARL) que comprende al menos 2 socios o 1 socio único para una SELARL unipersonal;
* la empresa de práctica privada con formulario anónimo (SELAFA) que comprende al menos 3 socios;
* la empresa de ejercicio liberal por acciones simplificadas (SELAS) que comprenda al menos 1 socio;
* la sociedad limitada por acciones (SELCA) con un mínimo de 4 socios.

#### Las ventajas de las empresas autónomas <!-- collapsable:close -->

Las empresas autónomas ofrecen ciertas ventajas en cuanto a fiscalidad y pago de cotizaciones sociales:

* Los BNC (beneficios no comerciales) se tienen en cuenta para determinar la base imponible neta del trabajador y su base de cotización a la seguridad social;
* la tributación de los cargos no deducibles se realiza en virtud del impuesto de sociedades y no del impuesto sobre la renta de las personas físicas;
* los profesionales pueden beneficiarse de responsabilidad limitada hasta el monto de sus contribuciones mientras mantienen la independencia inherente a su estatus liberal;
* también se benefician de normativas específicas sobre la transferencia de sus derechos sociales.

#### Planes de seguridad social para gerentes y directores de empresas independientes <!-- collapsable:close -->

El régimen social depende del tipo de empresa y del cargo del gerente o director dentro de ella.

Los gerentes minoritarios de SELARL y los gerentes de SELAFA o SELAS dependen del sistema social para empleados asimilados.

Los gerentes mayoritarios de SELARL, gerentes de unipersonal SELARL y SELCA están sujetos al régimen de seguridad social para trabajadores autónomos (TNS).

## Otras formas legales

\> [Más información sobre las otras formas legales de la empresa](https://www.service-public.fr/professionnels-entreprises/vosdroits/F23844) (service-public.fr)