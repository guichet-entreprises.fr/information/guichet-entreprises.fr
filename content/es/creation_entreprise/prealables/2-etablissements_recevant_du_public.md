﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Establecimientos abiertos al público" -->
<!-- var(key)="es-creation_entreprise-prealables-2-etablissements_recevant_du_public" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Establecimientos abiertos al público
==================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

**Los establecimientos abiertos al público (ERP) son edificios abiertos a personas distintas de los empleados.**

**La creación de una empresa destinada a recibir al público, una empresa por ejemplo, implica cumplir con los estándares legales en materia de seguridad y accesibilidad.**

## ¿Qué es un ERP?

Los establecimientos abiertos al público (ERP) son "edificios, locales y recintos en los que se admite a las personas, ya sea de forma gratuita, bien pagada o con cualquier participación, o en las que se realizan reuniones abiertas a todos los asistentes o por invitación" pagado o no ”([artículo R. 123-2 del Código de la Construcción y la Vivienda](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000039041081&cidTexte=LEGITEXT000006074096&dateTexte=20190901)).

Se trata de muchos tipos de establecimientos: tiendas, restaurantes, hoteles, teatros, etc.

Los ERP están [clasificados por tipo](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32351) (simbolizado por una letra), según su actividad o la naturaleza de su funcionamiento y por categorías, según la capacidad del edificio.

La clasificación de un establecimiento es validada por la comisión de seguridad en base a la información transmitida por su operador en el expediente de seguridad archivado en el ayuntamiento.

## ERP, obligaciones de seguridad reforzadas

La apertura de un ERP está sujeta a [obligaciones de seguridad](https://www.service-public.fr/professionnels-entreprises/vosdroits/F31684), en particular con respecto a los incendios, que son necesarios en el momento del diseño y durante el funcionamiento del establecimiento.

La normativa aplicable varía según la clasificación del edificio.

## ERP, un deber de accesibilidad

La accesibilidad de ERP es una [obligación legal](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32873).

Desde la [ley de 11 de febrero de 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000809647&dateTexte=) Para la igualdad de derechos y oportunidades, participación y ciudadanía de las personas con discapacidad, los establecimientos abiertos al público (ERP) deben ser accesibles a todo tipo de discapacidad (motora, visual, auditiva, psíquica, etc.).

Los edificios públicos que no sean accesibles deben presentar solicitudes de autorización de trabajo o permisos de construcción para su pleno cumplimiento.

\> [Las fichas prácticas están disponibles en el sitio del servicio public.fr sobre la definición de un ERP, procedimientos de autorización de trabajo, reglas de seguridad y accesibilidad](https://www.service-public.fr/professionnels-entreprises/vosdroits/N31782)