﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Nombres de empresas" -->
<!-- var(key)="es-creation_entreprise-prealables-4-les_noms_de_l_entreprise" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

# Nombres de empresas

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

**Nombre de la empresa, nombre de dominio, nombre de la marca, etc., identifique su negocio.**

**Verificar su disponibilidad y protegerlos son pasos esenciales al establecer una empresa.**

## Iniciar un negocio, busca el nombre de tu empresa

El nombre comercial, el letrero, el nombre de la empresa, etc., son nombres diferentes para diferentes aspectos legales del negocio.

### El nombre comercial

El nombre comercial es el nombre conocido por el público y los clientes.

Se refiere a la empresa unipersonal o al negocio y puede confundirse con el nombre del gerente/fundador.

### El nombre de la empresa

La denominación social designa empresas comerciales y sociedades civiles profesionales (SCP).

La razón social es elegida libremente por los socios. Puede ser puramente creativo o referirse a la actividad. Se utiliza en todos los demás tipos de empresas (SARL, EURL, SA, SAS, SNC, etc.). Ej: "Perfumería DURAND".

### La razón social

La razón social es el nombre que se le da a una empresa. Se compone exclusivamente de los nombres de todos o de algunos socios.

A diferencia de la razón social, las sociedades civiles pueden tener una razón social que no está formada exclusivamente por el nombre de todos o de ciertos socios.

Al registrar la empresa en el [registro comercial y empresarial](https://www.infogreffe.fr/formalites-entreprise/immatriculation-entreprise.html) (RCS), es obligatorio indicar el nombre de la empresa o su denominación seguida, en su caso, de su sigla ([artículo R123-53 del Código de Comercio](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006256506&cidTexte=LEGITEXT000005634379&dateTexte=20070327)).

La razón social y la razón social de la empresa se pueden utilizar como nombre comercial.

### La marca

La marca permite ubicar localmente uno u otro de sus establecimientos comerciales o activos comerciales, generalmente tomados como punto de venta. En la mayoría de los casos, colocado en la fachada del establecimiento, el letrero es sobre todo un letrero o un emblema de reconocimiento del mismo con los clientes. El letrero se puede mencionar en el RCS.

## Iniciar un negocio, proteger tu marca

La presentación de una marca es un procedimiento que permite a cualquier persona, física o jurídica, adquirir y proteger los derechos de uso de una marca.

Este proceso le permite verificar la disponibilidad de la marca elegida, para protegerse de la falsificación y posibles competidores que la utilicen de manera fraudulenta.

En Francia, el registro de la marca se realiza en el [Instituto Nacional de Propiedad Industrial](https://www.inpi.fr/fr/comprendre-la-propriete-intellectuelle/la-marque) (INPI).

Durante este depósito, es necesario elegir, dentro de la clasificación de Niza, las clases correspondientes a sus productos y/o servicios. Una vez validado y registrado el depósito, se garantiza el uso de la marca en el territorio elegido (a nivel nacional, europeo o internacional).

## Iniciar una empresa, elegir tu nombre de dominio

Si proporciona un sitio web a su empresa, debe elegir y registrar su nombre de dominio.

El nombre de dominio es el equivalente, en la web, de una dirección postal. Está precedido por el prefijo “www." y seguido de una extensión ".fr", ".com", ".eu", ".org", etc.

Fácilmente memorable, representativo de su actividad, disponible, el nombre de dominio debe respetar los derechos previos de terceros sobre el mismo nombre.

Luego de verificar la disponibilidad del nombre de dominio deseado, debe registrarlo en una de las oficinas acreditadas por el [ICANN](https://www.icann.org/) (*Internet Corporation for Assigned Names and Numbers*).

En Francia, diversas organizaciones ofrecen estos servicios: AFNIC, GANDI, OVHcloud, etc.

El nombre de dominio también se puede registrar en el RCS al mismo tiempo que el registro de la empresa o empresa o mediante un formulario de modificación. La forma para declarar el nombre de dominio es la cerfa NDI.