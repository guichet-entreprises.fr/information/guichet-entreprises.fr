﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Disponibilidad del nombre de la empresa" -->
<!-- var(key)="es-creation_entreprise-prealables-5-disponibilite_du_nom_de_lentreprise" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Disponibilidad del nombre de la empresa
==========================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

Una empresa puede identificarse con diferentes nombres. En cualquier caso, antes de utilizarlos, es necesario comprobar que estén disponibles. Esta verificación se lleva a cabo mediante la búsqueda de anterioridad frente a diferentes categorías de nombres agrupados en bases de datos. Esto es para asegurar que el nombre elegido no sea utilizado ya por una empresa registrada competidora, como nombre corporativo o nombre comercial, o como marca, para designar productos o servicios relacionados con el futuro. actividad realizada, o como la dirección (nombre de dominio) de un sitio web activo.

## Verifique la disponibilidad de su nombre en comparación con los nombres comerciales y corporativos existentes

Para comprobar la disponibilidad de su nombre en relación con un nombre de empresa o un nombre comercial, puede consultar de forma gratuita la base de datos [data.inpi.fr](https://data.inpi.fr/) del Instituto Nacional de la Propiedad Industrial (INPI) que contiene datos relacionados con el Registro Nacional de Comercio y Sociedades, así como la base de datos SIRENE del INSEE.

## Verifique la disponibilidad de su nombre frente a marcas existentes

Puede realizar esta investigación usted mismo consultando de forma gratuita la [base de datos de marcas registradas](https://www.inpi.fr/fr/base-marques)en el sitio web del INPI.

## Verifique la disponibilidad de su nombre en relación con los nombres de dominio

Para verificar la disponibilidad del nombre elegido con los nombres de dominio existentes, puede consultar la [Base de datos de nombres de Internet de la Asociación Francesa para la Cooperación](https://www.afnic.fr/fr/votre-nom-de-domaine/comment-choisir-et-creer-mon-nom-de-domaine/recherche-de-disponibilite/) (Afnic) (nombres de dominio en .fr en particular). 

**Ir más lejos**

Para ayudarlo en estos pasos, puede comunicarse con el INPI para obtener ayuda.

Se recomienda realizar una investigación en profundidad de disponibilidad para considerar cualquier similitud (ortográfica, fonética e intelectual). Esta investigación se puede solicitar al INPI, en su [sitio web](https://www.inpi.fr/fr/disponibilite-marque-et-societe-en-france).

También puede solicitar al INPI un [paquete que agrupa una búsqueda de similitudes entre marcas, nombres de empresas y una búsqueda idéntica entre nombres de dominio en .fr, .eu y gTLD (.com, .org, etc.)](https://www.inpi.fr/fr/disponibilite-marque-societe-et-domaine-en-france).


**Enlaces útiles**

\> [Nombre de la empresa, nombre comercial y marca](https://www.inpi.fr/fr/comprendre-la-propriete-intellectuelle/les-autres-modes-de-protection/la-denomination-sociale-le-nom) (inpi.fr)

\> [Servicios del INPI](https://www.inpi.fr/fr/services-et-prestations) (inpi.fr)

\> [Busque la disponibilidad de un nombre de dominio](https://www.afnic.fr/fr/votre-nom-de-domaine/comment-choisir-et-creer-mon-nom-de-domaine/recherche-de-disponibilite/) (afnic.fr)

\> [Base de datos del RNCS](https://data.inpi.fr/) (inpi.fr)

## Otros trámites

Si además de la creación de su negocio desea registrar una marca, toda la información necesaria está disponible en el [sitio web del INPI](https://www.inpi.fr/fr/proteger-vos-creations/proteger-votre-marque/les-etapes-cles-du-depot-de-marque).

Si, además de la creación de su empresa, desea reservar un nombre de dominio, deberá ponerse en contacto con un registrador acreditado por Afnic para que actúe como intermediario obligatorio entre usted y Afnic para cualquier solicitud. registro de un nombre de dominio. Encontrará información útil al respecto en el [sitio web de Afnic](https://www.afnic.fr/fr/votre-nom-de-domaine/comment-choisir-et-creer-mon-nom-de-domaine/).
