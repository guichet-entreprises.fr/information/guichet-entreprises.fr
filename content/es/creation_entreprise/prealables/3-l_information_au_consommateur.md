﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Información al consumidor" -->
<!-- var(key)="es-creation_entreprise-prealables-3-l_information_au_consommateur" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

# Información al consumidor

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

**Como gerente comercial que entrega productos o servicios, tiene el deber de brindar información a sus clientes.**

**El Código del Consumidor y el Código Civil rigen las obligaciones de información para los consumidores.**

## Obligaciones de las empresas hacia sus clientes

Como profesional, tienes la obligación de informar a los consumidores sobre los bienes o servicios que les ofreces.

[Artículo L. 111-1 del Código del Consumidor](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000020625532&cidTexte=LEGITEXT000006069565) define claramente la obligación de información:

“Antes de que el consumidor quede vinculado por un contrato de compraventa de bienes o prestación de servicios, el profesional comunica al consumidor, de manera legible y comprensible, la siguiente información:

1. Las características esenciales del bien o servicio [...];
2. El precio del bien o servicio [...];
3. [...] la fecha o el plazo en el que el comerciante se compromete a entregar las mercancías o prestar el servicio;
4. Información relativa a su identidad, sus datos de contacto postales, telefónicos y electrónicos y sus actividades [...] ”.

Se aplican reglas especiales de información a ciertos tipos de bienes y servicios (alimentos, préstamos hipotecarios, etc.).

En ciertos casos, el vendedor debe proporcionar al cliente documentos de información específicos (presupuesto, características técnicas, fecha de disponibilidad) antes de firmar el contrato.

## La DGCCRF, el servicio estatal que supervisa la información al consumidor

La [Dirección General de Competencia, Consumo y Control del Fraude](https://www.economie.gouv.fr/dgccrf) (DGCCRF) garantiza el correcto funcionamiento de los mercados en beneficio de los consumidores y las empresas. Interviene:

* en todas las áreas de consumo (productos alimenticios y no alimenticios, servicios);
* en todas las etapas de la actividad económica (producción, procesamiento, importación, distribución);
* cualquiera que sea la forma de comercio: tiendas, sitios de comercio electrónico o sitios vinculados a la economía colaborativa, etc.

[Las fichas prácticas de la DGCCRF](https://www.economie.gouv.fr/dgccrf/Publications/Vie-pratique/Fiches-pratiques-de-la-concurrence-et-de-la-consom) proporcionan una respuesta sintética a las cuestiones de competencia y consumo. Informativos y operativos, se actualizan periódicamente de acuerdo con los cambios en la normativa.