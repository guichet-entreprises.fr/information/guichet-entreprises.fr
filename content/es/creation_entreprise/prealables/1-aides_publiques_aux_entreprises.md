﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Ayudas empresariales" -->
<!-- var(key)="es-creation_entreprise-prealables-1-aides_publiques_aux_entreprises" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Ayudas empresariales
==================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

**¿Está iniciando su propio negocio? Su financiación estará en el corazón de su proyecto.**

**Más allá de los préstamos bancarios y personales, existen muchas ayudas y subvenciones financieras. Depende de usted encontrar los que le ayudarán a crear su negocio.**

## Aides-entreprises.fr, un sitio único para financiar su negocio

El sitio [aide-entreprises.fr](https://aides-entreprises.fr) enumere más de 1.800 ayudas financieras disponibles para líderes de proyectos y líderes empresariales.

Aides-entreprises.fr ofrece información completa y actualizada a nivel local, nacional o europeo y dirige al solicitante al punto de contacto para cada dispositivo en cuestión.

También aparece en el sitio un calendario de concursos, algunos de los cuales premian a los ganadores con asignaciones financieras.

## Ayuda para compradores y creadores de empresas (ACRE), ayuda simplificada

Desde el 1 de enero de 2020, la adjudicación de ACRE ya no es automática y nuevamente está sujeta a criterios de elegibilidad.

Para beneficiarse de él, los solicitantes deben encontrarse en una de las siguientes situaciones:

* solicitante de empleo compensado;
* solicitante de empleo desempleado registrado en Pôle Emploi durante más de 6 meses en los últimos 18 meses;
* beneficiario de la asignación solidaria específica (ASS) o renta solidaria activa (RSA);
* tener entre 18 y 26 años;
* tener menos de 30 años y estar reconocido como discapacitado;
* han celebrado un contrato de apoyo a proyectos empresariales (CAPE);
* crear o hacerse cargo de una empresa ubicada en un distrito prioritario de la ciudad (QPV);
* beneficiarse del beneficio de educación infantil compartida (PreParE);
* Ser autónomos que no estén sujetos al régimen microsocial.

En el caso de las corporaciones, debes tener el control, es decir, estar en una de tres situaciones:

* posea, personalmente o con su cónyuge, su socio PACS, su conviviente o sus ascendientes y descendientes, más del 50% del capital, del cual al menos el 35% a título personal;
* usted administra la empresa y mantiene, personalmente o con su esposo / esposa, su socio PACS, su socio o sus ascendientes y descendientes, al menos 1/3 del capital, del cual al menos el 25% a título personal, bajo reserva que otro accionista no posea más del 50% del capital;
* solicitantes que en conjunto posean más del 50% del capital, siempre que uno o más de ellos sean administradores y que cada solicitante tenga una participación de capital equivalente al menos a 1/10 de la participación del accionista principal.

El solicitante no debe haberse beneficiado de ACRE en los últimos 3 años.

La solicitud ACRE debe realizarse directamente a través del sitio [autoentrepreneur.urssaf.fr](https://www.autoentrepreneur.urssaf.fr/portail/accueil.html) al crear una empresa o, a más tardar, dentro de los 45 días siguientes a la presentación de la declaración de creación o toma de control de una empresa.

En la actualidad, solo los microempresarios deben presentar un archivo a la Urssaf. Urssaf procesa directamente a los creadores de empresas que se encuentran en otro estado legal sin necesidad de presentar un archivo.

Sólo la Urssaf sigue siendo competente para examinar las solicitudes y notificar la decisión.

Extendido a todos los creadores y compradores, este sistema les permite beneficiarse de una exención temporal de cotizaciones sociales durante 12 meses.