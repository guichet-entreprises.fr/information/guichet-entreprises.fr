﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="open" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Póngase en contacto con el servicio de asistencia al usuario" -->
<!-- var(key)="es-contacter_assistance_utilisateur" -->
<!-- var(last-update)="2021-01-04" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Póngase en contacto con el servicio de asistencia al usuario
=================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

¿Tiene preguntas o ha encontrado un problema al utilizar guichet-entreprises.fr?

## ¿Tiene alguna pregunta sobre el seguimiento de archivos? <!-- collapsable:open -->

El servicio en línea guichet-entreprises.fr le permite armar un archivo en línea, adjuntar documentos de respaldo y pagar las tarifas asociadas con el trámite. Una vez constituido y validado el expediente, se envía al organismo competente para su tramitación.

En consecuencia, **le informamos que el INPI no podrá contestarle sobre el seguimiento de su expediente una vez que haya sido validado y enviado al organismo autorizado.** Para conocer el estado procesando su archivo, por favor contáctese con él. Sus datos de contacto están en su tablero. Le agradecemos su comprensión.

## Pregunta sobre el uso del sitio o pregunta técnica <!-- collapsable:open -->

El INPI te responde:
* por teléfono al 01 56 65 89 98, de lunes a viernes de 9 a 18 h.
* por correo electrónico a través del [formulario de contacto](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1) (inpi.fr).
