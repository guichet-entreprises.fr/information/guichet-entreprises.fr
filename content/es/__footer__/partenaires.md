<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Partenaires" -->
<!-- var(key)="es-__footer__-partenaires" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

# Socios

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

¡Combine la mejor experiencia creativa!

El sitio guichet-entreprises.fr es una iniciativa del Estado francés en el marco de la aplicación de la [directiva europea sobre servicios](https://eur-lex.europa.eu/legal-content/ES/TXT/PDF/?uri=CELEX:32006L0123&from=ES).

Naturellement proches des enjeux des entrepreneurs dont ils sont les premiers interlocuteurs, les centres de formalités des entreprises (CFE) sont les partenaires privilégiés de guichet-entreprises.fr. Leur travail commun permet aujourd'hui aux entrepreneurs français et européens de créer leur entreprise sur un site simple et complet.

## Nuestros socios <!-- collapsable: off -->

### Cámaras de Agricultura (CA) <!-- collapsable:close -->

Las Cámaras de Agricultura (CA) ofrecen un servicio a los agricultores en diversos campos: asesoramiento técnico en producción vegetal y animal, asesoramiento empresarial en instalación, transmisión, urbanismo, diversificación, etc.

Desde 1997 gestiona los centros de trámites empresariales (EFC) prestando asistencia a las empresas agropecuarias en la realización de sus trámites de creación, modificación o extinción de empresas.

Las EFC agrícolas facilitan y centralizan el acceso de las empresas agrícolas a los múltiples trámites con organismos separados (Insee, servicios tributarios, MSA, criadero departamental, registro de vinos, en caso de asociación: registro del tribunal comercial) para suscribir en el mismo lugar y en el mismo documento, las declaraciones relativas a su constitución, a cambios en su situación o al cese de su actividad a los que sean requeridos por las leyes y reglamentos vigentes. Garantizan el control formal y la transmisión de declaraciones y justificantes a los destinatarios de los trámites. Se garantiza la confidencialidad de la información recopilada.

Desde 2006, las EFC son responsables de recibir declaraciones de fondos agrícolas y llevar el registro de fondos agrícolas.

Desde diciembre de 2009, guiados por la directiva europea sobre servicios n ° 2006/123 / E del 12/12/2006, las EFC de las cámaras de agricultura ofrecen una ventanilla única para los centros ecuestres, realizando todos los trámites previos a la constitución de una empresa e información.

Es posible concertar una cita con su cámara de agricultura donde asesores de la CFE agricole le ayudarán en sus trámites.

Las cámaras de agricultura se agrupan a nivel regional en las cámaras regionales de agricultura. La ** Asamblea Permanente de Cámaras de Agricultura (APCA) ** es el nivel nacional de la red de cámaras de agricultura. Es miembro asociado del Consejo Agrícola Francés.

** Contacto: ** Asamblea Permanente de Cámaras de Agricultura - Empresas de Servicios, 9, avenue George V, 75008 Paris

[> Visite el sitio web de las Cámaras de Agricultura] (https://chambres-agriculture.fr/)

### Cámaras de comercio e industria (CCI) <!-- collapsable:close -->

Los establecimientos públicos dirigidos por líderes empresariales electos, las cámaras de comercio e industria son responsables de:

* **representar los intereses de las empresas ante las autoridades públicas**;
* **apoyar a líderes de proyectos y empresas** en cada etapa de su creación y desarrollo;
* **capacitar a empleados y líderes empresariales** (aprendizaje, escuela de negocios, ingeniería);
* **Gestionar equipos imprescindibles para la vida de los territorios** (incubadoras de empresas, puertos, aeropuertos, centros de exposiciones y convenciones, etc.).

Las cámaras de comercio e industria forman parte de la red CCI de France, cuyo establecimiento nacional federativo es **CCI France**.

Para su proyecto de creación, adquisición o desarrollo empresarial, movilizan habilidades fuertes y diversificadas, le ofrecen un curso adaptado a su proyecto y cursos de formación para potenciar sus habilidades emprendedoras.

No dude en ponerse en contacto con la CCI cercana a la ubicación de su proyecto. Sus asesores te acompañarán en tus estudios y su centro de trámites comerciales - one-stop shop se ocupará de tus trámites administrativos.

[> Visite el sitio web de la red CCI](https://www.cci.fr/)

### Cámaras de oficios y artesanías (CMA) <!-- collapsable:close -->

La primera red de apoyo a las empresas artesanales, las cámaras de artesanía y artesanías son administradas por funcionarios electos que son a su vez jefes de empresas artesanales.

Presentes en Francia continental y en el exterior, las CMA son socios fundamentales de las empresas artesanales y su objetivo es su desarrollo, competitividad y sostenibilidad: desarrollan, en estrecha relación, una gama de servicios adaptados a necesidades específicas. para cada tipo de público, ya sean aprendices, creadores, compradores, empleados, buscadores de empleo, personas que buscan orientación profesional o incluso quienes venden un negocio de artesanía.

Esta oferta cubre todas las etapas de la vida de la empresa, desde la creación / adquisición hasta la transmisión, pasando por todas las fases de desarrollo, así como la formación de sus activos.

Los líderes del proyecto recibidos cada año en más de 372 puntos de recepción de CMA se benefician de un recorrido real marcado que también incluye soporte posterior a la creación. Las CMA saben cómo movilizar su red de socios (bancos, notarios, contables, abogados y sindicatos profesionales) para ayudar a formalizar el proyecto y satisfacer todas las necesidades financieras, legales, fiscales y sociales.

**Los CMA también se cobran:**

* llevar un registro de publicidad legal (el directorio de oficios);
* la gestión de los centros de trámites comerciales (CFE) competentes para las empresas unipersonales y empresas sujetas a inscripción en este directorio y para las personas que ejercen una actividad artesanal exenta de registro;
* reconocer la calidad de artesano, artesano o maestro artesano;
* organizar el aprendizaje en el sector de la artesanía en colaboración con las regiones.

Para encontrar una cámara de oficios y artesanías cerca de usted, [visite el portal de cámaras de oficios y artesanías](https://www.artisanat.fr/).

Número de teléfono común: 0 825 36 36 36 (0,15 € TTC/min)

### Los secretarios de los tribunales comerciales <!-- collapsable:close -->

Secretarios judiciales comerciales, **un modelo original y eficaz al servicio de los negocios y la justicia económica**.

En la encrucijada del mundo jurídico y económico, los secretarios de los tribunales mercantiles ejercen **una eficiente y moderna misión de servicio público**. Intervienen en todos los momentos importantes de la vida de las empresas, desde su nacimiento hasta su desaparición, desde el resolución de sus disputas al tratamiento de sus dificultades.

Agentes reales del estado civil de las empresas, contribuyen eficazmente a la seguridad jurídica y la transparencia en la vida económica.

Nombrados por orden del Guardián de los Sellos, los secretarios de los juzgados comerciales están delegados en el poder público del Estado. Ejercen sus misiones bajo el control del Ministerio Público.

El secretario, miembro del tribunal comercial, cumple con varios poderes jurisdiccionales:

* **facultades jurisdiccionales en beneficio de los litigantes y del tribunal:** asistencia a los jueces, conservación de documentos y archivos, autenticación y entrega de copias de las decisiones;
* **facultades legales de carácter económico en beneficio de las empresas:** los dependientes son profesionales del derecho y especialistas en el mantenimiento de registros legales: control de trámites en el registro mercantil y mercantil, conservación y publicación de valores muebles y difusión de información legal y financiera sobre empresas.

Su experiencia combinada con el dominio de las innovaciones tecnológicas es un activo considerable al servicio de la justicia comercial y las empresas. Los secretarios innovan en beneficio de empresas y litigantes, al tiempo que garantizan, dentro de un marco regulatorio establecido por la ley, un servicio público local.

La condición de los secretarios de los tribunales mercantiles y las misiones que desempeñan cumplen un doble requisito:

* **satisfacción del Estado** del que ejercen, por delegación, determinadas prerrogativas y para el que tienen un deber de competencia, lealtad y ética;
* **satisfacción de los usuarios de la justicia comercial** de los que son interlocutores directos.

**Los secretarios, actores de la justicia comercial, al servicio de la jurisdicción y los litigantes:**

Las misiones judiciales que realiza el secretario, miembro del tribunal, están relacionadas con disputas entre empresas, con la prevención y tratamiento de dificultades para las empresas. Procesan más de un millón de decisiones judiciales cada año

**Los dependientes, actores de la vida económica, al servicio de las empresas:**

Al llevar registros legales, los secretarios ofrecen un observatorio privilegiado del mundo económico. Con la disponibilidad de la información contenida en estos registros, que representan de 60.000 a 80.000 actos por día, los secretarios de los juzgados de lo mercantil permiten que todos obtengan información fidedigna sobre las empresas y sus responsables la situación económica y financiera de un socio comercial.

Los registros de los juzgados de lo mercantil o los tribunales superiores que resuelvan en materia mercantil son los centros de trámites empresariales (CFE) competentes para:
a) sociedades civiles y no comerciales;
b) empresas autónomas;
c) personas jurídicas sujetas a inscripción en el registro mercantil y societario distintas de:
  * los comerciantes,
  * empresas comerciales,
  * personas y empresas sujetas a registro en el directorio de oficios,
  * personas físicas que se beneficien de la exención de registro, relacionada con el desarrollo y la promoción del comercio y la artesanía,
  * personas y empresas sujetas a inscripción en el registro de empresas artesanales de vías navegables interiores;
d) establecimientos públicos industriales y comerciales;
e) agentes comerciales;
f) agrupaciones de interés económico y agrupaciones de interés económico europeo.

[> Visite el sitio web del Consejo Nacional de Secretarios de Tribunales Comerciales](https://www.cngtc.fr/fr/)

[> Navegar por el libro virtual que presenta a los secretarios de los tribunales comerciales](https://www.cngtc.fr/page-flip/les-greffiers-des-tribunaux-de-commerce/)

### Las Urssaf <!-- collapsable:close -->

**La red Urssaf** [[1]](#1) <a id="Retour1"></a> es el motor de nuestro sistema de protección social con la misión principal de recaudar las cotizaciones y cotizaciones sociales, fuentes de financiamiento del sistema general de seguridad social. Más de 800 socios le confían misiones de recogida o control. Por lo tanto, recupera **contribuciones al seguro de desempleo y contribuciones AGS** [[2]](# 2) <a id="Retour2"> </a> en nombre de Unedic y procede al cálculo y la convocatoria de cotizaciones destinada al régimen de la Seguridad Social para autónomos.

La estrategia de la red Urssaf se basa en el desarrollo de la calidad de la relación y del servicio con 9,5 millones de usuarios. Como servicio público moderno y confiable, la red Urssaf ha desarrollado ofertas de servicios específicos, en particular para empleadores individuales (CESU, Pajemploi), asociaciones (CEA) o pequeñas empresas (TESE).

Las Urssaf son también el centro de trámites comerciales (CFE) para las profesiones liberales, asociaciones que contratan personal, artistas-autores, taxis arrendatarios, vendedores de viviendas independientes y autoempresarios (profesiones liberales y comerciantes) que realizan sus trámites a través del sitio [autoentrepreneur.Urssaf.fr](https://www.autoentrepreneur.urssaf.fr/portail/accueil.html).

La **Agencia Central de Organismos de Seguridad Social (Acoss)** es el fondo nacional de la red Urssaf. Además de supervisar su red, Acoss asegura la gestión conjunta del flujo de caja de las distintas ramas del esquema general.

Con el fin de ofrecer servicios homogéneos manteniendo una presencia local muy fuerte, Acoss liderará hasta 2014 una consolidación de su red a través de la creación de 22 Urssaf regionales manteniendo un anclaje y presencia regional.

Desde 2011, Acoss también ha sido el único productor certificado de estadísticas trimestrales sobre empleo asalariado.

[> Visite el sitio de la red Urssaf](https://www.urssaf.fr/portail/home.html)

**<a id="1"></a> [[1]](#Retour1) Urssaf :** Unión para la recaudación de cotizaciones a la seguridad social y asignaciones familiares

**<a id="2"></a> [[2]](#Retour2) AGS :** Asociación para la gestión del régimen de garantía de reclamaciones de empleados (o régimen de garantía salarial)

## Los principales usuarios de guichet-entreprises.fr <!-- collapsable:off -->

Los agentes y las asociaciones de agentes son los principales usuarios del servicio guichet-entreprises.fr.

¿Eres un agente? ¿Utiliza a menudo el sitio guichet-entreprises.fr para sus necesidades? <a href="mailto:slelong@inpi.fr">Contáctenos</a> para optimizar el uso de nuestro sitio.