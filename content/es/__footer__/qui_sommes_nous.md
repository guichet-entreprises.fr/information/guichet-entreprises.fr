﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Quiénes somos ?" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="es-__footer__-qui_sommes_nous" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="es" -->

Quiénes somos ?
================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

Última actualización : <!-- begin-var(last-update) -->2020-09-01<!-- end-var -->

** Guichet-entreprises.fr es el sitio nacional del Instituto Nacional de Propiedad Industrial (INPI) que permite a los creadores y líderes empresariales completar sus trámites. **

** Creación, modificación, cese de actividad, etc. : allí se pueden realizar más de 2.000 trámites. **

## Guichet-entreprises.fr, el sitio del INPI que simplifica la creación de empresas

El sitio guichet-entreprises.fr está diseñado y desarrollado por la división Guichet Entreprises del INPI, en aplicación del decreto n ° 2020-946 del 30 de julio de 2020.

La división Guichet Entreprises gestiona guichet-entreprises.fr y [guichet-qualifications.fr](https://www.guichet-qualifications.fr/es/) que constituyen la ventanilla única electrónica definida por las directivas europeas [2006/123/EC](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32006L0123&from=EN) y [2005/36/CE](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2005:255:0022:0142:fr:PDF).

En guichet-entreprises.fr, los creadores y gerentes de empresas realizan, en línea y con total seguridad, los trámites administrativos relacionados con la vida de su empresa: creación, modificación (cambio de domicilio, condición jurídica, etc. ) o cese de actividad.

Para la creación de empresas, el sitio guichet-entreprises.fr registra los archivos de los declarantes y los envía para su procesamiento a los centros de trámites comerciales (CFE) relacionados con:

* cámaras de comercio e industria (CCI) – [cci.fr](https://www.cci.fr/)
* cámaras de agricultura – [chambres-agriculture.fr](https://chambres-agriculture.fr/)
* cámaras de oficios y artesanías (CMA) – [cma-france.fr](https://cma-france.fr/)
* Urssaf – [urssaf.fr](https://www.urssaf.fr/portail/home.html)
* secretarios de tribunales comerciales – [cngtc.fr](https://www.cngtc.fr/fr/)

## Guichet-entreprises.fr, acceso a profesiones reguladas

La normativa francesa condiciona el ejercicio de determinadas actividades (restaurador, agente inmobiliario, contable, etc.) a la obtención de una autorización o autorización.

Guichet-entreprises.fr ofrece fichas de información para hacer un balance de la legislación relativa a estas actividades.

## Guichet-entreprises.fr, un sitio europeo

Guichet-entreprises.fr también está dirigido a residentes de la Unión Europea o del Espacio Económico Europeo que deseen establecerse en Francia.

Les informa de las posibilidades de establecerse permanentemente (libre establecimiento) o de ejercer temporalmente (libertad de prestación de servicios) en Francia, y les permite realizar todos sus trámites en línea.

## Guichet-qualifications.fr, un sitio para tener reconocidas las cualificaciones profesionales

El sitio [guichet-qualifications.fr](https://www.guichet-qualifications.fr/es/) Fomenta la movilidad profesional proporcionando a los ciudadanos la información más completa posible sobre el acceso y el ejercicio de profesiones reguladas en Francia.

El guichet-entreprises.fr y [guichet-qualifications.fr](https://www.guichet-qualifications.fr/es/) présentent toutes les garanties en matière de sécurité (homologation de l'ANSII).

## Guichet-entreprises.fr en cifras

** Guichet-entreprises.fr es: **

* ** más de 2.000 trámites **
* ** más de 100 fichas informativas sobre actividades reguladas **
* ** más de 250 fichas informativas sobre profesiones reguladas **