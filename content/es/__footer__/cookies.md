﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Uso de cookies en guichet-entreprises.fr" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="es-__footer__-cookies" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="es" -->

Uso de cookies en guichet-entreprises.fr
===============================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

## ¿Qué son las cookies y cómo las utiliza el INPI?

El INPI puede utilizar cookies cuando un usuario navega por sus sitios. Las cookies son archivos enviados al navegador por intermediario de un servidor web con el fin de registrar las actividades del usuario durante su tiempo de navegación. El uso de cookies permite reconocer el navegador web utilizado por el usuario para facilitar su navegación. Las cookies también se utilizan para medir la audiencia del sitio y producir estadísticas de consulta.

Las cookies utilizadas por el INPI no proporcionan referencias que permitan deducir datos personales de los usuarios o información personal que permita identificar a un usuario en particular. Son de carácter temporal, con la única finalidad de hacer más eficaz la transmisión posterior. Ninguna cookie utilizada en el sitio tendrá una vigencia superior a dos años.

Los usuarios tienen la opción de configurar su navegador para ser notificado de la recepción de cookies y rechazar la instalación. Al prohibir las cookies o deshabilitarlas, es posible que el usuario no pueda acceder a ciertas funciones del sitio.

En caso de rechazo o desactivación de cookies, se debe reiniciar la sesión.

## ¿Qué tipos de cookies utiliza guichet-entreprises.fr?

### Cookies técnicas

Permiten al usuario navegar por el sitio y utilizar algunas de sus funciones.

### Cookies de análisis

El INPI utiliza cookies de análisis de audiencia para cuantificar el número de visitantes. Estas cookies permiten medir y analizar la forma en que los usuarios navegan por el sitio.