﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Notas legales" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="es-__footer__-mentions_legales" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="es" -->

Notas legales
================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

Última actualización : <!-- begin-var(last-update) -->2020-09-01<!-- end-var -->

## Identificación del editor

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p>

El diseño editorial, el seguimiento, el mantenimiento técnico y las actualizaciones del sitio web guichet-entreprises.fr están a cargo de la división Guichet Entreprises.

La división Guichet Entreprises es un servicio del INPI, en aplicación del decreto n ° 2020-946 del 30 de julio de 2020.

## Objeto del sitio

El sitio guichet-entreprises.fr permite a cualquier empresario realizar de forma remota los trámites y trámites necesarios para la creación, modificación de la situación y el cese de actividad de una empresa, ya sea mediante el envío de un único fichero como definido en el artículo R. 123-23 del Código de Comercio siempre que cumpla con lo dispuesto en el artículo R. 123-24 del Código de Comercio, o bien mediante la preparación de dicho expediente de forma interactiva y su transmisión.

Los trámites están sujetos a firma electrónica, cuyos términos están disponibles en la política de firma electrónica.

Además, el sitio guichet-entreprises.fr proporciona a los empresarios un conjunto de información sobre las formalidades, procedimientos y requisitos relacionados con las actividades reguladas de acuerdo con el artículo R. 123-2 del Código de Comercio.

## Tratamiento de datos personales

Los datos personales contenidos en el fichero único de acuerdo con la normativa general de protección de datos están sujetos a las disposiciones disponibles [aquí](./protection_donnees.md).

Tiene derecho a acceder y rectificar sus datos personales. Por otro lado, el tratamiento de estos es necesario para el cumplimiento de una obligación legal a la que está sujeto el responsable del tratamiento. Por tanto, el derecho a oponerse a este tratamiento no es posible, en aplicación de lo dispuesto en el artículo 6.1 c) del Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016, y del artículo 56 de la ley n ° 78-17 del 6 de enero de 1978 relativa al tratamiento de datos, archivos y libertades en su versión consolidada.

Puede ejercer este derecho de varias formas:

* poniéndose en contacto con el centro de trámites comerciales (CFE) que recibe el expediente de declaración;
* enviando un correo electrónico a la asistencia al usuario a través del [formulario de contacto](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1) ;
* enviando una carta a la siguiente dirección:

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;À l'attention du délégué à la protection des données<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p>

## Derechos de reproducción

El contenido de este sitio se rige por la legislación francesa e internacional sobre derechos de autor y propiedad intelectual.

Todos los elementos gráficos del sitio son propiedad del INPI. Queda estrictamente prohibida cualquier reproducción o adaptación de las páginas del sitio que incluya elementos gráficos.

También está prohibido cualquier uso del contenido con fines comerciales.

Cualquier cita o uso del contenido del sitio debe haber obtenido la autorización del director de la publicación. Debe indicarse la fuente (guichet-entreprises.fr) y la fecha de la copia, así como la mención del INPI.

## Enlaces a las páginas del sitio

Cualquier sitio público o privado está autorizado a establecer enlaces a las páginas del sitio guichet-entreprises.fr. No es necesario solicitar autorización previa. Sin embargo, el origen de la información debe especificarse, por ejemplo, en el formulario: "Creación de empresas (fuente: guichet-entreprises.fr, un sitio INPI)". Las páginas del sitio guichet-entreprises.fr no deben estar anidadas dentro de las páginas de otro sitio. Deben mostrarse en una nueva ventana o en una nueva pestaña.

## Enlaces a páginas de sitios externos

Los enlaces en el sitio guichet-entreprises.fr pueden dirigir al usuario a sitios externos, cuyo contenido no puede comprometer de ninguna manera la responsabilidad del INPI.

## Entorno técnico

Algunos navegadores pueden bloquear la apertura de ventanas en este sitio de forma predeterminada. Para permitirle ver ciertas páginas, debe autorizar la apertura de ventanas cuando el navegador se lo solicite haciendo clic en el banner de advertencia que se muestra en la parte superior de la página.

Si no hay un mensaje de advertencia de su navegador, debe configurarlo para que autorice la apertura de ventanas para el sitio guichet-entreprises.fr.