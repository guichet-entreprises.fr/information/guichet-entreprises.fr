﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Política de firma electrónica" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="es-__footer__-signature_electronique" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="es" -->

Política de firma electrónica
==================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

<table class="table table-striped table-bordered">
<thead style="text-align: center;border-collapse: collapse">
<tr>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Versión</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Fecha</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Objeto</th>
</tr>
</thead>
<tbody style="text-align: center;border-collapse: collapse">
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V1</td>
<td style="border: 1px solid #AAA;padding: 4px">25/05/2018</td>
<td style="border: 1px solid #AAA;padding: 4px">Versión inicial</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V2</td>
<td style="border: 1px solid #AAA;padding: 4px">01/09/2020</td>
<td style="border: 1px solid #AAA;padding: 4px">Actualización tras la entrada en vigencia del Decreto No. 2020-946 de 30 de julio de 2020 que designa al Instituto Nacional de la Propiedad Industrial como organismo único mencionado en el noveno párrafo del artículo 1 de la Ley Núm. 2019-486 de 22 de mayo de 2019 relativo al crecimiento y transformación de empresas y encargando a este instituto la gestión de los servicios informáticos mencionados en los artículos R. 123-21 y R. 123-30-9 del Código de Comercio</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V3</td>
<td style="border: 1px solid #AAA;padding: 4px">03/06/2021</td>
<td style="border: 1px solid #AAA;padding: 4px">Actualización sobre cómo archivar y eliminar el archivo firmado</td>
</tr>
</tbody>
</table>

## Asunto del documento

La firma electrónica adherida a un conjunto de datos garantiza la integridad de los datos transmitidos, el no repudio de los datos firmados y la autenticidad de su remitente.

Esta política de firma electrónica es un documento que describe las condiciones de admisibilidad por parte de las organizaciones receptoras, es decir, los centros de trámites comerciales y los tenedores de registros públicos, de un fichero en el que se coloca una firma electrónica en el marco de los intercambios electrónicos a que se refiere el artículo R. 123-24 del Código de Comercio francés.

Según este artículo, “cuando se requiera firma, se requerirá el recurso a una firma electrónica segura en las condiciones previstas en el artículo 1316-4 del Código Civil [ahora artículo 1367 del Código Civil] y en el decreto núm. 2017-1416 de 28 de septiembre de 2017 relativo a la firma electrónica. No obstante, para la transmisión electrónica de expedientes de creación de empresas o declaraciones previstas en el artículo L. 526-7, se autoriza, incluso para las solicitudes de inscripción en el registro mercantil y societario, el recurso a una firma electrónica que tenga las características previstas en la primera frase del segundo párrafo del artículo 1316-4 del Código Civil [ahora artículo 1367 del Código Civil] ”.

De hecho, las disposiciones reglamentarias aplicables para la implementación de la firma electrónica del servicio online Guichet Entreprises proceden de los siguientes textos:

* Reglamento nº 910/2014 del Parlamento Europeo y del Consejo de 23 de julio de 2014 (e-IDAS) sobre identificación electrónica y servicios de confianza para transacciones electrónicas en el mercado interior;
* Código Civil ;
* Decreto n.º 2017-1416 de 28 de septiembre de 2017 relativo a la firma electrónica.

En el caso de que la firma electrónica requiera el suministro de datos personales, las disposiciones reglamentarias aplicables se derivan de los siguientes textos:

* Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016, Reglamento sobre la protección de las personas en lo que respecta al tratamiento de datos personales y a la libre circulación de dichos datos, y por el que se deroga Directiva 95/46/CE (reglamento general de protección de datos);
* Ley n.º 78-17 del 6 de enero de 1978 relativa a computadoras, archivos y libertades.

Este documento, "Política de firma electrónica del servicio online Guichet Entreprises", describe todas las normas y disposiciones que definen los requisitos que cumple cada uno de los actores involucrados en estos intercambios desmaterializados para la transmisión y recepción de flujos.

Este documento está destinado a:

* en los centros de trámites comerciales;
* tenedores de registros públicos;
* a cualquier proveedor de servicios que participe en estos intercambios desmaterializados en nombre de estas organizaciones receptoras.

En el resto de este documento:

* las organizaciones receptoras mencionadas anteriormente se designan con el término "destinatarios";
* los intercambios desmaterializados antes mencionados se designan con el término "archivos";
* El Instituto Nacional de Propiedad Industrial se designa con el término "INPI".

## Campo de aplicación

La firma electrónica es necesaria para cualquier trámite relacionado con la creación, cambio de situación y cese de actividad de una empresa, o relacionado con el acceso a una actividad regulada en el sentido de la Directiva 2006/123/CE de Parlamento Europeo y del Consejo de 12 de diciembre de 2006 sobre servicios en el mercado interior (“directiva de servicios”) y su ejercicio, mencionado en el artículo R. 123-1 del Código de Comercio.

La firma electrónica tiene diferentes características según la naturaleza de los archivos transmitidos.

**Cuando el expediente se refiera a un trámite de creación de empresa,** la firma electrónica requerida cumple con lo dispuesto en la primera frase del segundo párrafo del artículo 1316-4 del Código Civil:

“Cuando es electrónico, consiste en el uso de un proceso de identificación confiable que garantiza su vínculo con el acto al que está adscrito. "

**Cuando el expediente se refiera a un trámite de modificación de situación o cese de actividad,** la firma electrónica cumple con lo dispuesto en el artículo 1 del decreto n.º 2017 1416 de 28 de septiembre de 2017 (referencia al reglamento “eIDAS” n.º 910/2014):

“Se presume la confiabilidad de un proceso de firma electrónica, hasta que se demuestre lo contrario, cuando este proceso involucre una firma electrónica calificada.

Una firma electrónica cualificada es una firma electrónica avanzada, de acuerdo con el artículo 26 del citado reglamento y que se crea mediante un dispositivo cualificado de creación de firma electrónica que cumpla los requisitos del artículo 29 de dicho reglamento, que se basa en una certificado cualificado de firma electrónica que cumpla con los requisitos del artículo 28 de este reglamento. "

Artículo 26 del reglamento “eIDAS” n.º 910/2014:

“Una firma electrónica avanzada cumple los siguientes requisitos:

1. Estar vinculado al firmante de manera inequívoca;
2. Identificar al signatario;
3. Han sido creados utilizando datos de creación de firma electrónica que el firmante puede, con un alto nivel de confianza, utilizar bajo su único control; y
4. Estar vinculado a los datos asociados a esta firma para que cualquier modificación posterior de los datos sea detectable. "

A título informativo, el reglamento “eIDAS” n.º 910/2014 prevé tres niveles de garantía:

* Bajo: en este nivel, el objetivo es simplemente reducir el riesgo de uso indebido o alteración de la identidad;
* Sustancial: en este nivel, el objetivo es reducir sustancialmente el riesgo de mal uso o alteración de la identidad;
* Alto: En este nivel, el objetivo es evitar el mal uso o alteración de la identidad.

## Identificación

La identificación de esta política de firma electrónica viene dada por la presencia del certificado INPI utilizado para la firma electrónica del documento.

El número de serie del certificado tiene el valor:

4B-51-5A-35-00-00-00-00-01-EF.

## Publicación del documento

Esta política de firma electrónica se publica tras su aprobación por parte del Responsable de Seguridad de los Sistemas de Información (RSSI) del INPI.

## Proceso de actualización

### Circunstancias que hacen necesaria una actualización

La actualización de esta política de firma electrónica puede tener como origen, en particular, la evolución de la legislación vigente (cf. "Objeto del Documento"), la aparición de nuevas amenazas y nuevas medidas de seguridad, la toma de tener en cuenta las observaciones de los distintos actores.

Esta política de firma electrónica se revisa al menos cada dos años.

### Consideración de comentarios

Todos los comentarios o deseos de cambios sobre esta política de firma electrónica deben enviarse por correo electrónico a la siguiente dirección: [contacto](mailto: guichet-entreprises-support@inpi.fr).

Estas observaciones y deseos de cambios son examinados por el CISO del INPI quien, de ser necesario, inicia el proceso de actualización de esta política de firma electrónica.

### Información de las partes interesadas

La información relacionada con la versión actual de esta política y las versiones anteriores está disponible en la tabla de versiones en la parte superior de este documento.

La publicación de una nueva versión de la política de firmas consta de:

1. cargar la política de firma electrónica en formato HTML;
2. Archivar la versión anterior después de colocar las palabras “obsoleta” en cada página.

## Entrada en vigor de una nueva versión y período de validez

Una nueva versión de la política de firmas no entra en vigor hasta un mes calendario después de su publicación en línea y sigue siendo válida hasta que entre en vigor una nueva versión.

El plazo de un mes es utilizado por los destinatarios para tener en cuenta en sus solicitudes los cambios provocados por la nueva política de firma electrónica.

## Actores

### El firmante del expediente

El firmante del expediente es el declarante o su representante que realiza el trámite de creación, modificación de situación o cese de actividad.

#### El rol del firmante

La función del firmante consiste en colocar su firma electrónica en su expediente, incluida la formalidad y los documentos justificativos.

Para colocar una firma electrónica en su archivo, el firmante se compromete a utilizar una herramienta de firma que cumpla con esta política de firma electrónica.

#### Las obligaciones del signatario

Estas obligaciones se describen en los capítulos siguientes.

### Herramienta de firma utilizada

El firmante debe comprobar los datos que va a firmar antes de colocar su firma electrónica.

### Procedimiento de firma electrónica utilizado

El procedimiento de firma electrónica depende del tipo de trámite (ver "Alcance").

**Para expedientes relacionados con trámites de creación de empresas,** el firmante deberá marcar una casilla al final del trámite indicando que declara en su honor la veracidad de los datos del trámite y firma el esta declaración.

En este caso, se hace mención en el espacio de firma del formulario (Cerfa) del cumplimiento de esta firma con los requisitos del numeral 3 del artículo A. 123-4 del Código de Comercio.

Párrafo 3 del artículo A.123-4 del Código de Comercio:

"3 ° Al marcar la casilla informática prevista a tal efecto, el declarante declara en su honor la veracidad de los elementos declarados de acuerdo con la siguiente fórmula:" Declaro en mi honor la veracidad de los datos de formalidad y firmo el esta declaración n.º…, hecha en…, el…. ".

**En lo que respecta a expedientes relativos a trámites de modificación de situación o cese de actividad,** el firmante deberá adjuntar copia de su documento de identidad declarada copia fiel del original de acuerdo con lo dispuesto en el párrafo 2 del artículo A.123-4 del Código de Comercio francés:

Párrafo 2 del artículo A.123-4 del Código de Comercio:

"2 ° Los documentos que lo componen han sido digitalizados. La copia de la prueba de identidad se escanea después de haber sido previamente pegada con una declaración manuscrita sobre el honor de conformidad con el original, una fecha y la firma manuscrita de la persona que realiza la declaración. "

Se le presenta el documento a firmar electrónicamente (formulario Cerfa y copia del documento de identidad) así como el acta de prueba que enumera las condiciones y consecuencias de la firma electrónica del documento.

El firmante debe marcar una casilla indicando que ha leído el acuerdo de prueba y lo acepta sin reservas.

En su teléfono móvil se le envía un código de validación de la firma electrónica, cuyo número indicó al crear su cuenta.

La introducción de este código desencadena el sellado del documento (formulario Cerfa y copia del documento de identidad), es decir, su firma electrónica basada en un certificado, su hash y el sello de tiempo del conjunto. .

El documento sellado se archiva y se envía a los destinatarios interesados.

Los casos de error más frecuentes son los siguientes:

**Caso 1 = Código incorrecto**

Caso funcional:

* El usuario ingresó un código incorrecto (por ejemplo, letras en lugar de números). *

Ejemplo de mensaje mostrado:

* El código que ingresó es incorrecto. Verifique el código que se le envió por SMS y vuelva a ingresarlo. *

**Caso 2 = Código caducado**

Caso funcional:

* El usuario ingresó el código recibido por SMS demasiado tarde (la duración de una OTP = código de contraseña única es de 20 minutos). *

Ejemplo de mensaje mostrado:

* El período de validez de su código ha expirado. Debe renovar su solicitud de firma electrónica. *

**Caso 3 = Código bloqueado**

Caso funcional:

* El usuario realizó N entradas de código OTP incorrectas. Para evitar intentos de piratería, el número de teléfono del usuario se identifica como bloqueado en el sistema. *

Ejemplo de mensaje mostrado:

* Debido a numerosos errores de entrada de su parte, su número de teléfono +33600000000 ya no le permite firmar electrónicamente su procedimiento. Lo invitamos a ingresar un nuevo número de teléfono en su cuenta de usuario o comunicarse con el INPI para desbloquear su número de teléfono actual. *

#### Protección y uso del archivo firmado

Este capítulo sólo trata de expedientes relativos a trámites de modificación de situación o cese de actividad, expedientes de creación.

El documento firmado electrónicamente se guarda en una caja fuerte electrónica acompañado de un archivo legible con todos los rastros de su sello para fines de auditoría.

La caja fuerte electrónica solo es accesible por su administrador y el RSSI del INPI.

El acceso a la caja fuerte electrónica solo es posible mediante un certificado nominativo y se rastrea cualquier acción sobre él.

Para el RSSI del INPI, solo se autoriza el acceso con fines de auditoría a solicitud justificada (por ejemplo, solicitud judicial).

El documento firmado se envía a las organizaciones receptoras encargadas de investigar el archivo asociado.

Los métodos de protección del fichero transmitido firmado electrónicamente son responsabilidad del destinatario y deben cumplir con lo dispuesto en el Reglamento General de Seguridad y el Reglamento General de Protección de Datos.

El CISO del INPI vela por el cumplimiento de estas disposiciones.

#### Archivar y eliminar el archivo firmado

De acuerdo con el artículo R. 123-18 del Código de Comercio, el Mostrador de Negocios se retira del expediente después de su transmisión a los órganos competentes. En este contexto, los archivos finalizados se eliminarán después de un período de tres meses. Una vez procesada por los órganos competentes, la información relativa a las sociedades civiles y mercantiles se publicará en el Registro Nacional de Comercio y Sociedades. [Se pondrán a disposición del público](https://www.inpi.fr/fr/services-et-prestations-domaine/bases-de-donnees) como datos abiertos y [se distribuirán para su reutilización](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000034924367&cidTexte=LEGITEXT000006069414), bajo licencias de reutilización aprobadas.

Tras la transmisión de los expedientes, los declarantes pueden dirigirse a las autoridades competentes para acceder a sus expedientes o consultarlos en el Guichet Entreprises durante los tres meses siguientes a la transmisión.

### Proveedores de soluciones de firma electrónica

La solución debe incorporar, en la estructura de datos de la firma, los requisitos de esta política de firma electrónica.

### INPI

El CISO del INPI vela por el cumplimiento de lo establecido en esta política de firma electrónica para el servicio Guichet Entreprises puesto a disposición de los usuarios.

Verifica que los sistemas de información que son fuentes y receptores de los flujos no presentan ningún riesgo en cuanto a la integridad y confidencialidad de los archivos almacenados y transmitidos.

### Los recipientes

Los destinatarios verifican la integridad de la firma electrónica del archivo y se aseguran de que los procedimientos de almacenamiento y borrado de los archivos recibidos cumplen con las disposiciones reglamentarias.

### Datos de verificación

Para realizar las comprobaciones, el INPI utiliza la información de sellado del archivo firmado electrónicamente (ver "Protección y uso del archivo firmado").

### Protección de recursos

El INPI asegura la implementación de los medios necesarios para proteger los equipos que brindan servicios de validación.

Las medidas tomadas se refieren a ambos:

* la protección del acceso físico y lógico a los equipos solo a personas autorizadas;
* la disponibilidad del servicio;
* seguimiento y seguimiento del servicio.

### Asistencia a los signatarios

La asistencia en el uso del procedimiento de firma electrónica es proporcionada por [INPI Direct](../contacter_assistance_utilisateur.md).

## Firma y validación electrónica

### Datos firmados

Los datos firmados constan del formulario Cerfa y la copia del documento de identidad en forma de un único archivo PDF con un área de firma electrónica editable.

Todo el archivo PDF está firmado y, por lo tanto, no es posible modificarlo una vez sellado.

### Características de la firma

La firma respeta las expectativas de la especificación "XML Signature Syntax and Processing (XMLDsig)" desarrollada por W3C (World Wide Web Consortium: [https://www.w3.org/](https://www.w3.org/)) así como las extensiones de formato de firma especificadas en el estándar europeo XML Advanced Electronic Signature (XADES) de la ETSI ([https://www.etsi.org](https://www.etsi.org/)).

La firma electrónica desplegada implementa un nivel de identificación "sustancial" (norma ISO/IEC 29115: 2013), es decir, de acuerdo con el reglamento eIDAS, una firma electrónica avanzada.

#### Tipo de firma

La firma electrónica es de tipo sobre.

#### Estándar de firma

La firma electrónica debe cumplir con el estándar [XMLDsig, revisión 1.1 de febrero de 2002](https://www.w3.org/TR/2002/REC-xmldsig-core-20020212).

La firma electrónica debe cumplir con la norma XAdES-EPES (Explicit Policy based Electronic Signature), [ETSI TS 101 903 versión v1.3.2](http://uri.etsi.org/01903/v1.3.2).

Conformément à la norme XadES, les propriétés signées (SignedProperties/SignedSignatureProperties) doivent contenir les éléments suivants :

* le certificat du signataire (SigningCertificate) ;
* la date et l’heure de signature (SigningTime) au format UTC.

### Algorithmes utilisables pour la signature

#### Algoritmo de condensación

El cálculo del resumen mediante la iteración de la función de compresión sobre la secuencia de bloques obtenida al dividir el mensaje (esquema iterativo de Merkle-Damgård).

Se puede acceder a este algoritmo en el siguiente enlace: [Merkle-Damgård](https://fr.wikipedia.org/wiki/Construction_de_Merkle-Damg%C3%A5rd)

#### Algoritmo de firma

El algoritmo de firma se basa en RSA/SHA 256.

#### Algoritmo de canonicalización

El algoritmo de canonicalización es [c14N](https://www.w3.org/TR/xml-exc-c14n/).

### Condiciones para declarar válido el archivo firmado

Las condiciones para declarar válido el archivo firmado son las siguientes:

* el firmante ha cumplido con sus obligaciones:
  * descargar una copia de su documento de identidad con mención de una copia fiel del original y aceptación del acuerdo de prueba,
  * ingresar el código enviado por SMS (método de “contraseña única”);
* el INPI ha cumplido con sus obligaciones:
  * provisión de un certificado generado por una CA que certifica su origen (proveedor del servicio de firma electrónica) y cuyas características son las siguientes: Clave pública = RSA 2048 bits, algoritmo de firma = RSA SHA 256;
* el proveedor del servicio de firma electrónica ha cumplido con sus obligaciones:
  * firma y sellado del archivo PDF según las características descritas en los capítulos anteriores.

La verificación de validez de la firma se puede realizar a través de Adobe Acrobat.

## Provisiones legales

### Información personal

El titular tiene el derecho de acceso y rectificación de los datos que le conciernen, que puede ejercer por correo electrónico a [esta dirección](mailto:guichet-entreprises-support@inpi.fr). El tratamiento de sus datos personales es necesario para el cumplimiento de una obligación legal a la que está sujeto el responsable del tratamiento. Por tanto, el derecho a oponerse a este tratamiento no es posible, en aplicación de lo dispuesto en el artículo 6.1 c) del Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016, y del artículo 56 de la ley n.º 78-17 del 6 de enero de 1978 relativa al tratamiento de datos, archivos y libertades en su versión consolidada.

### Ley aplicable - Resolución de disputas

Estas disposiciones están sujetas a la ley francesa.

Cualquier disputa relacionada con la validez, interpretación o ejecución de estas disposiciones estará sujeta a la jurisdicción del Tribunal Administrativo de París.

Última actualización: septiembre de 2020