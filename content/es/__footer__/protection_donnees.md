﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Política de protección de datos personales" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="es-__footer__-protection_donnees" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="es" -->

Política de protección de datos personales
===============================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

Última actualización : <!-- begin-var(last-update) -->2020-09-01<!-- end-var -->

<table class="table table-striped table-bordered">
<thead style="text-align: center;border-collapse: collapse">
<tr>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Versión</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Fecha</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Objeto</th>
</tr>
</thead>
<tbody style="text-align: center;border-collapse: collapse">
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V1</td>
<td style="border: 1px solid #AAA;padding: 4px">25/05/2018</td>
<td style="border: 1px solid #AAA;padding: 4px">Versión inicial</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V2</td>
<td style="border: 1px solid #AAA;padding: 4px">01/09/2018</td>
<td style="border: 1px solid #AAA;padding: 4px">Actualización de información sobre el delegado de protección de datos</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V3</td>
<td style="border: 1px solid #AAA;padding: 4px">05/08/2019</td>
<td style="border: 1px solid #AAA;padding: 4px">Actualización de las referencias de Cerfas</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V4</td>
<td style="border: 1px solid #AAA;padding: 4px">01/09/2020</td>
<td style="border: 1px solid #AAA;padding: 4px">Actualización tras la entrada en vigencia del Decreto N ° 2020-946 de 30 de julio de 2020 que designa al Instituto Nacional de la Propiedad Industrial como organismo único mencionado en el noveno párrafo del artículo 1 de la Ley Núm. 2019-486 de 22 de mayo de 2019 relativo al crecimiento y transformación de empresas y encargando a este instituto la gestión de los servicios informáticos mencionados en los artículos R. 123-21 y R. 123-30-9 del Código de Comercio.</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V5</td>
<td style="border: 1px solid #AAA;padding: 4px">03/06/2021</td>
<td style="border: 1px solid #AAA;padding: 4px">Actualización sobre el período de conservación de los datos personales y el procesamiento de archivos</td>
</tr>
</tbody>
</table>

La política de protección de datos personales (en adelante la “política”) le informa sobre la forma en que sus datos son recogidos y tratados por el servicio Guichet Entreprises del INPI, sobre las medidas de seguridad implementadas para garantizarlo. integridad y confidencialidad y los derechos que tiene para controlar su uso.

Esta política complementa las condiciones generales de uso (TyC) así como, en su caso, los avisos legales. Como tal, esta política se considera incorporada a dichos documentos.

Como parte del desarrollo de nuestros servicios y la implementación de nuevos estándares regulatorios, es posible que necesitemos modificar esta política. Por lo tanto, lo invitamos a leerlo con regularidad.

## Quiénes somos ?

El INPI implementa un servicio en línea (en adelante, las “Guichet Entreprises”) que permite a cualquier empresario realizar de forma remota los trámites y trámites necesarios para la creación, modificación de la situación y la terminación de actividad de una empresa, ya sea mediante la transmisión de un solo expediente según se define en el artículo R. 123-23 del Código de Comercio siempre que cumpla con lo dispuesto en el artículo R. 123-24 del Código de Comercio, o preparando un archivo de este tipo de forma interactiva y transmitiéndolo.

El servicio Guichet Entreprises forma parte del marco legal:

- de la Directiva [2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32006L0123&from=FR) del Parlamento Europeo y del Consejo de 12 de diciembre de 2006 sobre servicios en el mercado interior; 
- de [reglamento n.°910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32014R0910&from=FR) del Parlamento Europeo y del Consejo de 23 de julio de 2014 (e-IDAS) sobre identificación electrónica y servicios de confianza para transacciones electrónicas en el mercado interior;
- de [reglamento n.°2016/679](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679&from=FR) del Parlamento Europeo y del Consejo de 27 de abril de 2016 sobre el tratamiento de datos personales;
- de la [Ley 2019-486](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038496102&categorieLien=id) de 22 de mayo de 2019 relativo al crecimiento y transformación de empresas, en particular su artículo 1;
- de [decreto n.° 2020-946](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000042182948&categorieLien=id) de 30 de julio de 2020 designando al Instituto Nacional de la Propiedad Industrial como organismo único mencionado en el noveno párrafo del artículo 1 de la Ley N ° 2019-486 de 22 de mayo de 2019 sobre el crecimiento y transformación de empresas y encomendar a este instituto la gestión de los servicios informáticos mencionados en los artículos R. 123-21 y R. 123-30-9 del Código de Comercio francés;
- de la [ordenanza de 8 de diciembre 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) relativo a los intercambios electrónicos entre usuarios y autoridades administrativas y entre autoridades administrativas y el decreto n.°  2010-112 de 2 de febrero de 2010 adoptado para la aplicación de los artículos 9, 10 y 12 de esta ordenanza conocida como el sistema general de referencia de seguridad (RGS);
- de la parte dispositiva de la [ley núm. 78-17 du 6 janvier 1978](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) enmendado en relación con ordenadores, archivos y libertades; 
- del [artículo 441-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) del Código Penal; 
- [artículos R. 123-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006255835&cidTexte=LEGITEXT000005634379&dateTexte=20070327) a [R. 123-30](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032944426&cidTexte=LEGITEXT000005634379&dateTexte=20170101) del Código de Comercio.

## ¿Qué organización implementamos para proteger sus datos?

El responsable de protección de datos del INPI supervisa el cumplimiento y la implementación de las disposiciones aplicables en materia de protección de datos personales.

## Cómo contactarnos ?

Para cualquier consulta sobre el uso de sus datos personales por parte del INPI, le invitamos a leer esta política y a ponerse en contacto con nuestro delegado de protección de datos utilizando los siguientes datos de contacto:

Por mail a la atención de:

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;À l’attention du délégué à la protection des données personnelles<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001 92677 Courbevoie Cedex</p>

Por correo electrónico, especificando el asunto "Protección de datos" a [esta dirección](mailto: guichet-entreprises-support@inpi.fr).

## ¿Qué son los datos personales?

Los datos personales son información que lo identifica, ya sea directa o indirectamente. En particular, puede ser su nombre, su nombre, su dirección, su número de teléfono o su dirección de correo electrónico, pero también la dirección IP de su computadora, por ejemplo, o incluso información relacionada con el uso del servicio Guichet Entreprises.

## ¿Cuándo recopilamos datos personales?

El INPI recopila datos personales cuando usted:

* crea tu espacio personal;
* completar un trámite a través del servicio Guichet Entreprises.

## ¿Qué datos personales procesamos?

Los datos personales que procesamos solo se refieren a trámites comerciales de acuerdo con las disposiciones reglamentarias de los artículos R. 123-1 a R. 123-27 del Código de Comercio francés.

Los datos recopilados permiten constituir el "archivo único" como se prevé en el artículo R. 123-7 del Código de Comercio.

Los datos recogidos permiten, en particular, completar los documentos de Cerfa asociados a los trámites y trámites necesarios para la creación, los cambios de situación y el cese de actividad de una empresa.

La siguiente tabla enumera los documentos de Cerfa en cuestión.

<table class="table table-striped table-bordered">
<thead>
<tr>
<th style="text-align:center;border: 1px solid #AAA;padding: 4px">Tramite</th>
<th style="text-align:center;border: 1px solid #AAA;padding: 4px">Forma jurídica</th>
<th style="text-align:center;border: 1px solid #AAA;padding: 4px">Cerfa : Principal</th>
<th style="text-align:center;border: 1px solid #AAA;padding: 4px">Cerfa : Posibles divisores</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Creación</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Microempresario</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13821<br>(P0 PL microempresario)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14214 (PEIRL Microempresario)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">15253<br>(P0 CMB microempresario)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14215 (PEIRL CMB)<br>11771 (P0’)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13821 (AC0)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14218 (PEIRL PL / AC)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Entreprise individuelle<br>(excepto microempresario)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11768 (P0 PL)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14218 (PEIRL PL / AC)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11676<br>(P0 CMB salvo microempresario)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14215 (PEIRL CMB)<br>11771 (P0’)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11922 (P0 agrícola)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11771 (P0’)<br>14216 (PEIRL agrícola)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13821 (AC0)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14218 (PEIRL PL / AC)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SARL</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Excepto actividad agrícola<br>11680 (M0 SARL, SELARL)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11772 (M0 ’ SARL, SELARL)<br>11686 (TNS)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Actividad agrícola<br>11927 (M0 agrícola)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agrícola)<br>14116 (Agri-com)<br>11925 (NSM agrícola)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SELARL</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11680 (M0 SARL, SELARL)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11772 (M0 ’ SARL, SELARL)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SAS</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Excepto actividad agrícola<br>13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Actividad agrícola<br>11927 (M0 agrícola)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agrícola)<br>14116 (Agri-com)<br>11925 (NSM agrícola)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Excepto actividad agrícola<br>13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Actividad agrícola<br>11927 (M0 agrícola)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agrícola)<br>14116 (Agri-com)<br>11925 (NSM agrícola)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SNC</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Excepto actividad agrícola<br>13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Actividad agrícola<br>11927 (M0 agrícola)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agrícola)<br>14116 (Agri-com)<br>11925 (NSM agrícola)&lt;br&gt;11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCS</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SELAFA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SELAS</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SELCA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Société civile</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13958 (M0 société civile)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14067 (M0’ société civile)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCM</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13958 (M0 société civile)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14067 (M0’ société civile)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCP</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13958 (M0 société civile)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14067 (M0’ société civile)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">GAEC</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Actividad agrícola<br>11927 (M0 agrícola)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agrícola)<br>11925 (NSM agrícola)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCEA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Actividad agrícola<br>11927 (M0 agrícola)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agrícola)<br>11925 (NSM agrícola)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">EARL</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Actividad agrícola<br>11927 (M0 agrícola)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agrícola)<br>Cerfa11925 (NSM agrícola)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">GFA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Actividad agrícola<br>11927 (M0 agrícola)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agrícola)<br>Cerfa11925 (NSM agrícola)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Régularisation</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Microempresario</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">15260<br>(R CMB microempresario)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14077 (JQPA)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Modification</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Microempresario</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13905 (P2P4)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14077 (JQPA)<br>14214 (PEIRL Microempresario)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11678 (P2 CMB)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14077 (JQPA)<br>14215 (PEIRL CMB)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Radiación</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Microempresario</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13905 (P2P4)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14214 (PEIRL Microempresario)<br>11679 (P4 CMB)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Declaración de beneficiarios reales</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Empresas (incluidas las sociedades comerciales civiles y extranjeras)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16062 (M’BE)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16062 (M’BE) insertar un formulario M0 – M2 – M3)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Placements collectifs</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16063 (M’BE)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16063 (M’BE) insertar un formulario M0 – M2 – M3)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">GIE ou Asociación</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16064 (M’BE)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16064 (M’BE) insertar un formulario M0 – M2 –M3)</td>
</tr>
</tbody>
</table>

La siguiente lista resume los datos personales que procesamos.

Para gestionar el acceso al espacio personal del servicio, el usuario comparte la siguiente información:

- el identificador de conexión elegido por el usuario;
- la contraseña elegida por el usuario;
- la dirección de correo electrónico del usuario;
- el número de teléfono móvil del usuario;
- la dirección IP de la conexión.

Para el uso del espacio de almacenamiento personal, el usuario comparte la siguiente información:

1 ° Para una persona física:

- nombre, nombre al nacer;
- nombre, nombre habitual;
- fecha y lugar de nacimiento ;
- nacionalidad;
- dirección, dirección de destino del correo;
- estado civil, régimen matrimonial;
- nombre del cónyuge, nombre de nacimiento del cónyuge;
- nombre del cónyuge, nombre habitual del cónyuge;
- dirección del cónyuge;
- fecha y lugar de nacimiento del cónyuge;
- la nacionalidad del cónyuge;
- situación de feria, situación de ejercicio móvil;
- situación del comerciante en un estado miembro de la Unión Europea que no sea Francia;
- presencia de una declaración de exención de embargo en la residencia principal, lugar de publicación en la oficina de conservación de hipotecas;
- nombres de operadores anteriores, nombres de nacimiento de operadores anteriores;
- nombres de operadores anteriores, nombres habituales de operadores anteriores;
- nombre del arrendador, nombre de nacimiento del arrendador;
- nombre del arrendador de los fondos, nombre habitual del arrendador;
- dirección del prestamista del fondo;
- Número de seguridad social ;
- plan de seguro médico;
- ejercicio de una actividad anterior por cuenta propia;
- número de seguro social del cónyuge o pareja colaboradora;
- nombre del representante autorizado;
- nombre de nacimiento del representante autorizado;
- nombre del representante autorizado;
- nombre habitual del representante autorizado;
- dirección del representante autorizado;
- lugar de nacimiento del abogado;
- fecha de nacimiento del apoderado;
- nacionalidad del representante autorizado;
- nombre del beneficiario;
- nombre de nacimiento del beneficiario;
- nombre del beneficiario;
- nombre habitual del beneficiario;
- número de seguro social del beneficiario;
- lugar de nacimiento del beneficiario;
- fecha de nacimiento del beneficiario;
- nacionalidad del beneficiario;
- relación ;
- nombre del firmante;
- nombre de nacimiento del firmante;
- nombre del firmante;
- nombre habitual del firmante;
- dirección del firmante.

2 ° Para una persona jurídica:

- nombre del proveedor de fondos;
- nombre de nacimiento del proveedor del fondo;
- nombre del proveedor del fondo;
- nombre habitual del propietario;
- dirección del prestamista del fondo;
- nacionalidad del gerente de una empresa asociada;
- nombre del gerente de una empresa asociada;
- nombre de nacimiento del gerente de una empresa asociada;
- nombre del gerente de una empresa asociada;
- nombre habitual del director de una empresa asociada;
- dirección del gerente de una empresa asociada;
- lugar de nacimiento del gerente de una empresa asociada;
- fecha de nacimiento del gerente de una empresa asociada;
- nombre del representante de la persona jurídica;
- nombre de nacimiento del representante de la persona jurídica;
- nombre y nombre habitual del representante de la persona jurídica;
- dirección del representante de la persona jurídica;
- fecha y lugar de nacimiento del representante de la persona jurídica;
- nacionalidad del representante de la persona jurídica;
- Nombre de la esposa ;
- nombre de nacimiento del cónyuge;
- nombre del cónyuge;
- nombre habitual del cónyuge;
- dirección del cónyuge;
- lugar de nacimiento del cónyuge;
- fecha de nacimiento de la esposa;
- la nacionalidad del cónyuge;
- nombre de otra persona que puede contratar el establecimiento;
- nombre de nacimiento de otra persona que puede contratar el establecimiento;
- nombre de otra persona que pueda contratar el establecimiento;
- nombre habitual de otra persona que pueda contratar el establecimiento;
- dirección de otra persona que pueda contratar el establecimiento;
- lugar de nacimiento de otra persona que pueda contratar el establecimiento;
- fecha de nacimiento de otra persona que puede contratar el establecimiento;
- nacionalidad de otra persona que pueda contratar el establecimiento;
- nombre del firmante;
- nombre de nacimiento del firmante;
- nombre del firmante;
- nombre habitual del firmante;
- dirección del firmante;
- número de seguro social del gerente;
- plan de seguro médico del gerente;
- número de seguro social del cónyuge o pareja colaboradora;
- nombre del beneficiario;
- nombre de nacimiento del beneficiario;
- nombre del beneficiario;
- nombre habitual del beneficiario;
- número de seguro social del beneficiario;
- lugar de nacimiento del beneficiario;
- fecha de nacimiento del beneficiario;
- nacionalidad del beneficiario;
- relación.

## ¿Qué documentos de respaldo personales procesamos?

Los documentos de respaldo personales que tramitamos sólo se refieren a trámites comerciales de acuerdo con las disposiciones reglamentarias de los artículos R. 123-1 a R. 123-27 del Código de Comercio.

Los documentos justificativos permiten constituir el "expediente único" previsto en el artículo R. 123-7 del Código de Comercio.

La lista de documentos justificativos está sujeta a la aprobación de la autoridad designada en el [artículo 3 del decreto n. 98-1083] (https://www.legifrance.gouv.fr/affichTexteArticle.do? cidTexte = JORFTEXT000000208797 & idArticle = LEGIARTI000006544269 & dateTexte = & categorieLien = cid) de 2 de diciembre de 1998 relativo a simplificaciones administrativas.

Entre los documentos de respaldo personales se encuentran los documentos que prueben la identidad o el domicilio. Estos documentos de respaldo se guardan de la misma manera que los datos personales.

Se almacenan en espacios inaccesibles desde el exterior (Internet) y su descarga o transferencia está asegurada mediante cifrado de acuerdo con lo indicado en el capítulo "¿Cómo se protegen sus datos personales?" "
 
## ¿Cómo gestionamos las cookies?

La gestión de cookies se describe [aquí] (cookies.md).

Todas las cookies administradas utilizan el modo * cookies seguras *.

Las cookies seguras son un tipo de cookie que se transmite exclusivamente a través de conexiones cifradas (https).

Las cookies técnicas contienen los siguientes datos personales: apellidos, nombre, dirección de correo electrónico, teléfono.

Las cookies de análisis contienen la dirección IP de la conexión.

## ¿Cuáles son sus derechos para la protección de sus datos personales?

Tiene derecho a acceder y rectificar sus datos personales. Por otro lado, el tratamiento de estos es necesario para el cumplimiento de una obligación legal a la que está sujeto el responsable del tratamiento. Por tanto, el derecho a oponerse a este tratamiento no es posible, en aplicación de lo dispuesto en el artículo 6.1 c) del Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016, y del artículo 56 de la ley n ° 78-17 del 6 de enero de 1978 relativa al tratamiento de datos, archivos y libertades en su versión consolidada.

Si cree que el uso de sus datos personales contraviene las normas de protección de datos personales, tiene la opción de presentar una reclamación ante la CNIL.

También tiene derecho a definir pautas para el procesamiento de sus datos personales en caso de fallecimiento. Se pueden registrar instrucciones específicas con el controlador. Las pautas generales se pueden registrar con un tercero digital de confianza certificado por la CNIL. Tiene la opción de modificar o eliminar estas pautas en cualquier momento.

Cuando corresponda, también tiene la opción de solicitarnos que le proporcionemos los datos personales que nos ha proporcionado en un formato legible.

Puede enviar sus diversas solicitudes:

- por correo a la atención de:

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;À l’attention du délégué à la protection des données personnelles<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001 92677 Courbevoie Cedex</p>

- por correo electrónico, especificando el asunto "Protección de datos" a [esta dirección] (mailto:guichet-entreprises-support@inpi.fr).

## ¿Cómo se protegen sus datos personales?

El INPI implementa todas las prácticas y procedimientos de seguridad estándar de la industria para evitar cualquier violación de sus datos personales.

Toda la información comunicada se almacena en servidores seguros alojados por nuestros proveedores de servicios técnicos.

Cuando la transmisión de datos es necesaria y autorizada, el INPI asegura que estos terceros presentan garantías suficientes para asegurar un nivel de protección adecuado. Los intercambios se cifran y los servidores se autentican mediante reconocimiento mutuo.

De acuerdo con la normativa de protección de datos, en caso de infracción, el INPI se compromete a comunicar esta infracción a la autoridad de control competente y, en su caso, a los interesados.

## Espacio personal

Todos los datos transmitidos a su espacio personal están encriptados y el acceso está asegurado mediante el uso de una contraseña personal. Se le requiere que mantenga esta contraseña confidencial y no la divulgue.

## Plazo de conservación de datos personales y tratamiento de ficheros

Los datos personales recopilados son necesarios para procesar la solicitud.

Esta información es recolectada por el INPI de acuerdo con el [Decreto 2020-946 de 30 de julio de 2020](https://www.legifrance.gouv.fr/eli/decret/2020/7/30/2020-946/jo/texte) y las disposiciones reglamentarias de los artículos R. 123-1 a R. 123-27 del Código de Comercio. Los datos recopilados permiten constituir el "archivo único" como se prevé en el artículo R. 123-7 del Código de Comercio. Estos datos personales recopilados para la creación de una cuenta, al realizar un trámite en el sitio guichet-entreprises.fr se conservan durante un año (artículo R. 123-18 y R. 123-27 del Código de Comercio).

De acuerdo con el artículo R. 123-18 del Código de Comercio, el Guichet Entreprises se retira del expediente después de su transmisión a los órganos competentes. En este contexto, los archivos finalizados se eliminarán después de un período de tres meses. Una vez procesada por los órganos competentes, la información relativa a las sociedades civiles y mercantiles se publicará en el Registro Nacional de Comercio y Sociedades. [Se pondrán a disposición del público](https://www.inpi.fr/fr/services-et-prestations-domaine/bases-de-donnees) como datos abiertos y [se difundirán para su reutilización](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000034924367&cidTexte=LEGITEXT000006069414), bajo licencias de reutilización aprobadas.

Una vez transmitidos los expedientes, los declarantes pueden dirigirse a las autoridades competentes para acceder a sus expedientes o consultarlos en el Guichet Entreprises durante los tres meses siguientes a la transmisión.

El usuario puede modificar o eliminar su archivo en línea siempre que no esté finalizado. El borrado de un fichero finalizado, antes de su borrado automático transcurridos tres meses, debe por otra parte solicitarse al INPI: [formulario de contacto](https://www.inpi.fr/fr/contactez-nous?about=299&under_about=883-702-b1). La modificación de un expediente finalizado está sujeta a una nueva formalidad.

Cualquier archivo iniciado, iniciado y no finalizado, se eliminará automáticamente de la plataforma en un plazo de 3 meses.

Para cualquier consulta relacionada con la protección de datos personales, puede ponerse en contacto con el [responsable de protección de datos personales del INPI](https://www.inpi.fr/fr/contactez-nous?about=299&under_about=883-702-b1), aportando prueba de su identidad. Tiene un medio de apelación ante la CNIL.

## Respecto a los datos de la tarjeta bancaria

El INPI no almacena ni conserva los datos bancarios de los usuarios sujetos a pago durante su trámite. Nuestro proveedor de servicios gestiona los datos de las transacciones en nombre del INPI de acuerdo con las más estrictas normas de seguridad aplicables en el sector de los pagos online mediante el uso de procedimientos de cifrado.

## ¿A quién se transmiten sus datos?

Los datos recopilados permiten constituir el "archivo único" como se prevé en el artículo R.123-7 del Código de Comercio.

Según el artículo R.123-3 del Código de Comercio, los datos recopilados se envían para su procesamiento a las siguientes organizaciones:

"1 ° Con sujeción a lo dispuesto en 2 ° y 3 °, las cámaras territoriales de comercio e industria crean y gestionan los centros de trámites empresariales encargados de:

1. comerciantes;
2. empresas comerciales.

2 ° Las cámaras regionales de oficios y artesanías crean y administran los centros competentes para personas y empresas sujetas a inscripción en el directorio de oficios

3 ° Los registros de los juzgados de lo mercantil o los tribunales superiores que resuelvan comercialmente crean y gestionan los centros competentes para:

1. sociedades civiles y no comerciales;
2. empresas autónomas;
3. personas jurídicas sujetas a inscripción en el registro mercantil y sociedades distintas de las mencionadas en 1 ° y 2 °;
4. establecimientos públicos industriales y comerciales;
5. agentes comerciales;
6. Grupos de interés económico y grupos de interés económico europeos.

4 ° Los sindicatos de cobro de cotizaciones a la seguridad social y asignaciones familiares (Urssaf) o las cajas generales de la seguridad social crean y gestionan los centros competentes para:

1. Personas que ejerzan, como profesión habitual, una actividad autónoma regulada o no distinta de la comercial, artesanal o agrícola;
2. empresarios cuyas empresas no estén inscritas en el registro mercantil y mercantil, en el directorio de oficios o en el registro de empresas artesanales de navegación interior, y que no pertenezcan a los centros mencionados en el 6 °.

5 ° Las cámaras de agricultura crean y gestionan los centros competentes para las personas físicas y jurídicas que desarrollan principalmente actividades agrícolas […]. "

De conformidad con lo dispuesto en los artículos L. 411-1 y D. 411-1-3 del Código de la Propiedad Intelectual, los datos recopilados están destinados a ser accesibles para su consulta en el portal. [data.inpi](https://data.inpi.fr/) del Instituto Nacional de la Propiedad Industrial y reutilizable a través de sus licencias de datos abiertos aprobadas.