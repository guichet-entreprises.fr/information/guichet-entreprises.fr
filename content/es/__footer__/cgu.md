﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Términos de servicio" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Auto" -->
<!-- var(key)="es-__footer__-cgu" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="es" -->

Términos de servicio
===================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

Última actualización :<!-- begin-var(last-update) -->2020-09-01<!-- end-var -->

## Preámbulo

Este documento presenta las condiciones de contratación para el uso del servicio online Guichet Entreprises del Instituto Nacional de la Propiedad Industrial (INPI) para los usuarios. Encaja en el marco legal:

* de la [Directiva 2006/123 / CE](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32006L0123&from=FR) del Parlamento Europeo y del Consejo de 12 de diciembre de 2006 sobre servicios en el mercado interior;
* del [Reglamento n° 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32014R0910&from=FR) del Parlamento Europeo y del Consejo de 23 de julio de 2014 (e-IDAS) sobre identificación electrónica y servicios de confianza para transacciones electrónicas en el mercado interior;
* del [Reglamento n° 2016/679](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679&from=FR) del Parlamento Europeo y del Consejo de 27 de abril de 2016 sobre el tratamiento de datos personales;
* de [Ley n° 2019-486 de 22 de mayo de 2019](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038496102&categorieLien=id) relativo al crecimiento y transformación de empresas, en particular su artículo 1;
* de [decreto n° 2020-946 de 30 de julio de 2020](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000042182948&categorieLien=id) designando al Instituto Nacional de la Propiedad Industrial como el único órgano mencionado en el noveno párrafo del artículo 1 de la Ley N ° 2019-486 de 22 de mayo de 2019 relativa al crecimiento y transformación de las empresas y encomendando a este instituto la gestión de los servicios informáticos mencionados en los artículos R. 123-21 y R. 123-30-9 del Código de Comercio
* de la [ordenanza de 8 de diciembre de 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) relativo a los intercambios electrónicos entre usuarios y autoridades administrativas y entre autoridades administrativas y el Decreto No. 2010-112 de 2 de febrero de 2010 emitido para la aplicación de los artículos 9, 10 y 12 de esta ordenanza;
* del resolutivo de la [ley n° 78-17 de 6 de enero de 1978 modificada](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) relacionados con ordenadores, archivos y libertades;
* de [artículo 441-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) del Código Penal;
* artículos [R. 123-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006255835&cidTexte=LEGITEXT000005634379&dateTexte=20070327) a [R. 123-30](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032944426&cidTexte=LEGITEXT000005634379&dateTexte=20170101) del Código de Comercio.

## Asunto del documento

Este documento tiene por objeto definir las condiciones generales de uso del servicio online Guichet Entreprises del INPI, en adelante denominado "Servicio", entre el INPI y los usuarios.

Este servicio está bajo la autoridad del Director General del Instituto Nacional de Propiedad Industrial.

## Definición y finalidad del Servicio

El Servicio implementado por el Guichet Entreprises del INPI (en adelante, "el servicio Guichet Entreprises del INPI") ayuda a simplificar los trámites relacionados con la creación, cambios en la situación y la terminación de actividad de una empresa de usuarios franceses y europeos.

Este Servicio ha estado sujeto a aprobación de seguridad de acuerdo con la instrucción interministerial 901 / SGDSN / ANSSI de ANSSI (NOR: PRMD1503279J) del 28 de enero de 2015 y la PGSSI de los ministerios de economía y finanzas (NOR: FCPP1622039A) de 1 de agosto de 2016.

El uso del Servicio es opcional y gratuito. No obstante, como parte de los trámites que se realizan a través de este Servicio, como los de constitución de una empresa, pueden solicitarse pagos online. Estos son seguros.

Todos los destinatarios del Servicio se denominan en lo sucesivo organizaciones asociadas.

Según el artículo R.123-3 del Código de Comercio, los datos recopilados se envían para su procesamiento a las siguientes organizaciones asociadas:

"1 ° Con sujeción a lo dispuesto en 2 ° y 3 °, las cámaras territoriales de comercio e industria crean y gestionan los centros de trámites empresariales encargados de:

a) comerciantes;
b) empresas comerciales.

2 ° Las cámaras regionales de artesanía y artesanía crean y gestionan los centros competentes para las personas naturales y jurídicas sujetas a inscripción en el registro de oficios.

3 ° Los registros de los juzgados mercantiles o juzgados comerciales crean y gestionan los centros competentes para:

a) sociedades civiles y no comerciales;
b) empresas autónomas;
c) personas jurídicas sujetas a inscripción en el registro mercantil y empresas distintas de las mencionadas en 1 ° y 2 °;
d) establecimientos públicos industriales y comerciales;
e) agentes comerciales;
f) agrupaciones de interés económico y agrupaciones de interés económico europeo.

4 ° Los sindicatos de cobro de cotizaciones a la seguridad social y asignaciones familiares (Urssaf) o las cajas generales de la seguridad social crean y gestionan los centros competentes para:

a) las personas que ejerzan, como profesión habitual, una actividad por cuenta propia regulada o no distinta de la comercial, artesanal o agrícola;
b) empresarios cuyas empresas no estén inscritas en el registro mercantil y mercantil, en el directorio de oficios o en el registro de empresas artesanales de navegación interior, y que no pertenezcan a los centros mencionados en el 6 °.

5 ° Las cámaras de agricultura crean y gestionan los centros competentes para las personas físicas y jurídicas que se dediquen principalmente a actividades agrícolas […]. "

De conformidad con lo dispuesto en los artículos L. 411-1 y D. 411-1-3 del Código de la Propiedad Intelectual, los datos recopilados están destinados a ser accesibles para su consulta en el portal [data.inpi.fr] (https: //data.inpi.fr/) del Instituto Nacional de la Propiedad Industrial y reutilizable a través de sus licencias de datos abiertos aprobadas.

## Caracteristicas

El Servicio permite al usuario:

* acceder a la documentación en la que se especifican las obligaciones de los centros de trámites comerciales (CFE) así como los elementos constitutivos del expediente de declaración y del expediente de solicitud de autorización;
* para crear una cuenta de usuario que dé acceso a un espacio de almacenamiento personal. Este espacio permite al usuario gestionar y utilizar sus datos personales, conservar información sobre ellos y los documentos y justificantes que sean necesarios para completar los trámites administrativos.

Desde su espacio, el usuario puede:

* compilar un solo expediente a que se refiere el artículo R. 123-1 del Código de Comercio incluyendo el expediente de declaración y, en su caso, las solicitudes de autorización;
* enviar su expediente de declaración y, en su caso, solicitudes de autorización al centro de trámites comerciales competente;
* acceder a la información para el seguimiento de la tramitación de su expediente de declaración y, en su caso, su expediente de solicitud de autorización;
* Permitir a las EFC implementar los tratamientos necesarios para utilizar la información recibida de la persona jurídica a que se refiere el último párrafo del artículo R. 123-21 del Código de Comercio: recepción del expediente único previsto en el artículo 2 de la ley n ° 94-126 de 11 de febrero de 1994 relativa a la iniciativa y la empresa individual y transmitida por la persona jurídica a que se refiere el artículo R. 123-21 del Código de Comercio; recepción de información de seguimiento para el procesamiento de estos archivos tal como la envían las organizaciones asociadas y las autoridades; transmisión de información para el seguimiento del tratamiento de expedientes únicos a la persona jurídica a que se refiere el artículo R. 121-21 del Código de Comercio.

## Condiciones de registro y uso del Servicio

El acceso al Servicio está abierto a cualquier persona y es gratuito. Es opcional y no excluyente de otros canales de acceso permitir al usuario completar sus trámites.

Para la gestión del acceso al espacio personal del Servicio, el usuario comparte la siguiente información:

* la dirección de correo electrónico del usuario;
* la contraseña elegida por el usuario.

El uso del Servicio requiere el suministro de datos personales para la constitución del archivo único.
El procesamiento de datos personales se describe en la política de protección de datos disponible [aquí](./protection_donnees.md).

El uso del Servicio requiere una conexión y un navegador de Internet. El navegador debe estar configurado para permitir cookies de sesión.

Para garantizar una experiencia de navegación óptima, le recomendamos que utilice las siguientes versiones de navegador:

* Firefox versión 45 y superior;
* Google Chrome versión 48 y superior.

De hecho, es posible que otros navegadores no admitan determinadas funciones del Servicio.

El Servicio está optimizado para una visualización en 1024 × 768 píxeles. Se recomienda que utilice la última versión del navegador y la actualice periódicamente para aprovechar las correcciones de seguridad y el mejor rendimiento.

## Condiciones específicas de uso del servicio de firma

El servicio de firma electrónica es accesible directamente a través del Servicio.

El artículo R. 123-24 del Código de Comercio de conformidad con el Reglamento nº 910/2014 del Parlamento Europeo y del Consejo de 23 de julio de 2014 (e-IDAS) son las referencias aplicables al servicio de firma electrónica del Servicio.

## Roles y compromiso

### Compromiso del servicio Guichet Entreprises del INPI

1. El servicio de Mostrador Comercial del INPI implementa y opera el Servicio de acuerdo con el marco legal vigente definido en el preámbulo.
2. El servicio Contador Comercial del INPI se compromete a tomar todas las medidas necesarias para garantizar la seguridad y confidencialidad de la información facilitada por el usuario.
3. El servicio Guichet Entreprises del INPI se compromete a velar por la protección de los datos recabados en el marco del Servicio, y en particular a evitar que sean falseados, dañados o que terceros no autorizados tengan acceso a los mismos, de acuerdo con las medidas previstas. por ordenanza de 8 de diciembre de 2005 relativa a intercambios electrónicos entre usuarios y autoridades administrativas y entre autoridades administrativas, decreto n ° 2010-112 de 2 de febrero de 2010 tomado para la aplicación de los artículos 9, 10 y 12 de este Ordenanza y Reglamento n. 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016 sobre el tratamiento de datos personales.
4. El servicio Guichet Entreprises del INPI y las organizaciones colaboradoras garantizan a los usuarios del Servicio los derechos de acceso, rectificación y oposición previstos por la ley n ° 78-17 de 6 de enero de 1978 relativa al tratamiento de datos. expedientes y libertades y el Reglamento No. 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016 relativo al tratamiento de datos personales. Este derecho se puede ejercer de varias formas: <br>
	  a) contactando con la central de trámites empresariales (CFE) que recibe el expediente de declaración; <br>
	  b) enviando un correo electrónico al soporte al usuario a través del [formulario de contacto](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1) ;<br>
	  c) enviando una carta a la siguiente dirección:<br>

<br>
<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;Pôle Guichet Entreprises<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p><br>

5. El servicio Guichet Entreprises del INPI y las organizaciones asociadas se comprometen a no comercializar la información y documentos transmitidos por el usuario a través del Servicio, y a no comunicarlos a terceros, salvo casos previstos por la ley.
6. El Guichet Entreprises del INPI se compromete a garantizar la trazabilidad de todas las acciones realizadas por todos los usuarios del Servicio, incluidas las de las organizaciones colaboradoras y el usuario.
7. El servicio Guichet Entreprises del INPI ofrece soporte a los usuarios en caso de incidente o alerta de seguridad.

## Participación del usuario

1. El usuario completa su expediente en línea y lo valida, eventualmente adjuntando los documentos necesarios para su procesamiento.
2. Al finalizar la constitución de su expediente, se despliega en pantalla un resumen de los elementos ingresados ​​por el usuario para que éste pueda comprobarlos y confirmarlos. Después de la confirmación, el archivo se envía a las organizaciones asociadas. La confirmación y transmisión del formulario por parte del usuario constituye su firma.
3. Estas condiciones generales se aplican a todos los usuarios del Servicio.

## Disponibilidad y evolución del Servicio

El Servicio está disponible los 7 días de la semana, las 24 horas del día.

El servicio Guichet Entreprises del INPI se reserva el derecho de desarrollar, modificar o suspender, sin previo aviso, el Servicio por motivos de mantenimiento o por cualquier otro motivo que se considere necesario. La indisponibilidad del Servicio no da derecho a compensación alguna. En caso de indisponibilidad del Servicio, se muestra una página de información al usuario que menciona esta indisponibilidad; luego se le invita a realizar su procedimiento más tarde.

Los términos de estos términos de uso pueden ser modificados en cualquier momento, sin previo aviso, dependiendo de cambios en el Servicio, cambios en leyes o regulaciones, o por cualquier otra razón que se considere necesaria.

## Responsabilidades

1. El servicio Guichet Entreprises del INPI no se hace responsable en caso de robo de identidad o cualquier uso fraudulento del Servicio.
2. Los datos transmitidos a los servicios en línea de las organizaciones asociadas siguen siendo responsabilidad del usuario, incluso si se transmiten por los medios técnicos disponibles en el Servicio. El usuario puede modificarlos o eliminarlos en cualquier momento de las organizaciones asociadas. Puede optar por eliminar toda la información de su cuenta eliminando sus datos del Servicio.
3. Se recuerda al usuario que toda persona que haga una declaración falsa para sí mismo o para terceros es responsable, en particular, de las sanciones previstas en el artículo 441-1 del Código Penal, que prevé penas de hasta 'a tres años de prisión y multa de 45.000 euros.