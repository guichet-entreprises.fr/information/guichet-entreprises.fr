﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(site:package)="https://gitlab.com/|13561843|latest|reference_md|reference/configuration.yml" -->
<!-- var(site:home)="Inicio" -->
<!-- var(site:menu)="__menu__.md" -->
<!-- var(site:footer)="__footer__/__links__.md" -->
<!-- var(site:lang)="es" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="creación; empresa; trámite; cese; modificación; actividad regulada; micro-empresa; micro-empresario; Francia; sociedad;" -->
<!-- var(page:font_sizer_off)="off" -->
<!-- var(page:breadcrumb_off)="off" -->
<!-- var(translation)="Auto" -->
<!-- var(last-update)="2020-05-03 11:31:25" -->
<!-- var(lang)="es" -->
<!-- var(key)="es-index" -->

Guichet Entreprises <!-- section:banner --> <!-- color:dark -->
===================================================

Brexit: montar una empresa en Francia siendo británico

[Leer el artículo <!-- link-model:box-trans -->](articles/brexit_creation_entreprise.md)

Deuxième bloc  <!-- section-information:-160px -->
===================================================

[Optimiza tu visita](articles/bienvenue_premiere_visite.md)
-------------------------------------------------------------
Para una mejor experiencia de navegación en Guichet Entreprises, le recomendamos que utilice los navegadores Mozilla Firefox o Google Chrome.

[Futuro creador](futur_createur.md)
---------------------------------
Formas legales, protección de la razón social, ayudas públicas, etc. ¡Infórmese antes de empezar!

[Trámites online](demarches_en_ligne/formalites.md)
-------------------------------
Complete sus trámites relacionados con el alta (cualquier forma legal), modificación y terminación de un negocio.

[Procesando su archivo](demarches_en_ligne/traitement_dossiers.md)
---------------------------------------------------
Una vez completado y finalizado, su expediente se envía a la CFE competente (CCI, CMA, Urssaf, etc.) para su procesamiento.

Una única dirección para iniciar un negocio  <!-- section-welcome: -->
===================================================

El servicio online guichet-entreprises.fr fomenta la creación de empresas en Francia al permitir a los ciudadanos realizar sus trámites administrativos en torno a la creación de una actividad (registro, solicitudes de autorización, etc.). Es el sitio de los poderes públicos para la creación de una empresa, la modificación y el cese de actividad de una empresa. Este servicio es administrado por el Instituto Nacional de Propiedad Industrial.

¡Facilitar los trámites administrativos fomenta el espíritu empresarial!

148357
------
Los expedientes de creación de empresas se enviaron a los centros de trámites empresariales (CFE) en 2020 a través de guichet-entreprises.fr (un 40,43% más que en 2019)

177436
------
Los expedientes se enviaron a los centros de trámites comerciales (CFE) en 2020 a través de guichet-entreprises.fr (44,23% más que en 2019)

102
------
las fichas de información dedicadas a las actividades reguladas en Francia están disponibles en guichet-entreprises.fr

2000
------
Existen más de 2.000 mecanismos de ayudas públicas para crear o desarrollar tu negocio

Actividades reguladas  <!-- section:courses -->
===================================================

Constructor artesano, restaurador, enfermero, panadero, etc., antes de iniciar la actividad de su negocio, infórmese sobre los pasos a seguir y las condiciones para acceder a una actividad regulada en Francia.

Comprueba si su actividad está regulada consultando [<font style="text-transform: lowercase;">nuestras hojas de información.</font>](../reference/es/directive-services/index.md)

¿Listo para empezar? <!-- section:stories --><!-- color:dark -->
===================================================

El servicio en línea guichet-entreprises.fr simplifica sus trámites ofreciéndole información clara y una ruta personalizada.
El servicio registra y transmite sus trámites al centro de trámites empresariales competente (CCI, CMA, Urssaf, secretarios de juzgados comerciales, cámaras de agricultura o la Cámara Nacional de Patrones Artesanos). Todas las organizaciones de creación de empresas: servicio de impuestos comerciales, Urssaf, Insee, etc. - son informados por la CFE. A continuación, recibirá el número de sirena, el código APE, el número de IVA, etc.

Ya inscrito ? [<font color="#0092BC">Se connecter</font>](https://account.guichet-entreprises.fr/session/new)

[<font color="#0092BC">Modo de empleo</font>](articles/comment_creer_une_entreprise.md) [<font color="#0092BC">Crear una cuenta</font>](https://account.guichet-entreprises.fr/users/new)

¿Por qué elegir guichet-entreprises.fr?  <!-- section:welcome -->
===================================================

El servicio en línea está diseñado para facilitarle la creación de una empresa. Todo lo que tiene que hacer es responder a las preguntas que se le hagan sobre su proyecto profesional, adjuntar los documentos justificativos y pagar los honorarios relacionados con el trámite. Su expediente se envía directamente a la administración competente donde será procesado.

<font color="#0092BC">Espacio personal</font>
------------------------------------------------------------------------------
Administre sus archivos creados en el sitio, suspenda la finalización de su archivo y guárdelo, etc.

<font color="#0092BC">Servicio seguro</font>
------------------------------------------------------------------------------
Realiza sus trámites online, sin necesidad de desplazarse, en una plataforma segura.

<font color="#0092BC">Pago en línea</font>
------------------------------------------------------------------------------
Como parte de un registro para RCS, RM, RSAC o RSEIRL, puede pagar sus tarifas directamente en línea.

<font color="#0092BC">Socios movilizados</font>
------------------------------------------------------------------------------
Centro de trámites empresariales (CFE), Insee, servicio tributario empresarial, Urssaf, etc.

Emprendimiento en Francia y Europa  <!-- section-stories:actu.png -->
===================================================

¿Es ciudadano de la Unión Europea o del Espacio Económico Europeo?

Fomentamos la creación de empresas y la movilidad profesional en Francia y en Europa.
El sitio guichet-entreprises.fr es la ventanilla única francesa para la creación de empresas,
miembro de la red Eugo creada por la Comisión Europea.
Le permite comprender y conocer las condiciones a las que está sujeto para crear una actividad de servicio.
Si necesita que se le reconozca una titulación profesional, vaya a [<font style="text-transform: lowercase;">guichet-qualifications.fr</font><!-- link-model:normal -->](https://www.guichet-qualifications.fr/es/) !

[¿Cómo practicar en Francia de forma temporal o permanente? <!-- link-model:box-trans -->](travailler_en_france.md)