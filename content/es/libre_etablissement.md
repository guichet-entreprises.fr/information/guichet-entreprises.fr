﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Establecimiento libre" -->
<!-- var(key)="es-libre_etablissement" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Establecimiento libre
========================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

## Soy europeo y me gustaría instalarme en Francia (libertad de establecimiento)

**La libertad de establecimiento** (o la libertad de establecimiento) garantiza a los nacionales de los Estados miembros que deseen establecerse en Francia el ejercicio de una actividad permanente, en las mismas condiciones que los nacionales franceses.

La libertad de establecimiento también permite a los nacionales de la Unión Europea [[1]](#1) <a id="Retour1"></a> y el Espacio Económico Europeo [[2]](#2) <a id="Retour2"></a> para crear y gestionar su negocio en Francia. Como tal, los nacionales de estos países pueden establecer su negocio en guichet-entreprises.fr de acuerdo con los mismos procedimientos que los nacionales franceses.

## Caso de actividad regulada y/o profesión (es)

El establecimiento gratuito tiene lugar en dos etapas:

 1. Que se reconozca su cualificación profesional (QP), si está regulada en Francia. Puede consultar las fichas de información sobre profesiones reguladas sobre [guichet-qualifications.fr](https://www.guichet-qualifications.fr/es/dqp/index.html) ;
 2. Solicite una licencia para practicar. Puede consultar las fichas de información sobre actividades reguladas en el [apartado del mismo nombre](../reference/es/directive-services/index.md).

En ambos casos, es la autoridad competente con la que se trata de acercarse para que se reconozca su actividad y profesión. El contacto de las autoridades competentes se puede encontrar en las fichas de información sobre [profesiones reguladas](https://www.guichet-qualifications.fr/es/dqp/index.html) y [actividades reguladas](../referencees/directive-services/index.md).

**Es bueno saber**: si la profesión y la actividad no están reguladas, entonces el establecimiento gratuito se realiza sin requisitos previos.

## Actividades y profesiones reguladas en Francia

En determinadas actividades, y para la creación de un negocio, es necesario respetar determinadas normas y trámites específicos (declaración, autorización, etc.).

[> Comprueba si tu actividad está regulada](../reference/es/directive-services/index.md)

En Francia, 105 actividades, agrupadas por familias de actividades, están reguladas por la [directiva 2006/123/CE](https://eur-lex.europa.eu/legal-content/ES/TXT/PDF/?uri=CELEX:32006L0123&from=ES) del Parlamento Europeo y del Consejo de 12 de diciembre de 2006 sobre servicios en el mercado interior.

**Es bueno saber**: ejercer de forma permanente en Francia puede requerir, según la profesión ejercida, un reconocimiento de cualificación profesional que se puede realizar en el sitio[guichet-qualifications.fr](https://www.guichet-qualifications.fr/es/).

<a id="1"></a> [[1]](#Retour1) **Estados miembros de la Unión Europea (UE)** - [27 países]: Alemania, Austria, Bélgica, Bulgaria, Chipre, Croacia, Dinamarca, España, Estonia, Finlandia, Francia, Grecia, Hungría, Irlanda, Italia, Letonia, Lituania, Luxemburgo, Malta, Países Bajos, Polonia, Portugal, Rumania, Eslovaquia, Eslovenia, Suecia, República Checa.

<a id="2"></a> [[2]](#Retour2) **Estados miembros del Espacio Económico Europeo (EEE)** - [30 países]: estos son los 27 países miembros de la Unión Europea (UE) enumerados anteriormente, y Noruega, de Islandia y Liechtenstein.

**Lea también**: [Soy europeo y deseo practicar ocasionalmente en Francia (libertad de prestación de servicios)](libre_prestation_services.md)