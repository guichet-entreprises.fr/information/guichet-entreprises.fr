﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Libertad para prestar servicios" -->
<!-- var(key)="es-libre_prestation_services" -->
<!-- var(translation)="Auto" -->
<!-- var(lang)="es" -->

Libertad para prestar servicios
===================================

<!-- begin-ref(disclaimer-trans-es) -->

**Advertencia sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida con una herramienta de traducción automática y puede contener errores. Invitamos a los usuarios a verificar la exactitud de la información proporcionada en esta página antes de empezar cualquier trámite.

El servicio Guichet Entreprises no se hace responsable del uso de información que pueda resultar inexacta debido a una traducción automática que no sea fiel a la original.<!-- alert-end:warning -->

<!-- end-ref -->

## Soy europeo y deseo trabajar ocasionalmente en Francia

La **libertad de prestación de servicios** (o la libertad de prestación de servicios) se refiere a los servicios temporales y ocasionales prestados por nacionales de países miembros de la Unión Europea. [[1]](#1) <a id="Retour1"></a> o el Espacio Económico Europeo <a id="Retour2"></a> [[2]](#2). En este contexto, no es necesario que se reconozca una calificación profesional.

Sin embargo, se requieren requisitos previos para realizar los actos de la propia profesión en Francia. El proveedor de servicios debe cumplir una serie de condiciones:

* poseer diplomas, certificados y otros títulos obtenidos en uno de estos Estados;
* estar establecido y ejercer legalmente la profesión en cuestión en un Estado miembro distinto de Francia.

Este es el caso, por ejemplo, cuando un arquitecto establecido en Francia presta un servicio a un beneficiario ubicado en Alemania. La prestación transfronteriza de servicios puede implicar el traslado temporal del proveedor al país de destino o la prestación de un servicio a distancia.

La **libertad de prestación de servicios es temporal**: un establecimiento duradero unido a una actividad permanente implica el establecimiento en el país bajo el régimen de [libre establecimiento](libre_etablissement.md).

## Condiciones

¿Quiere prestar un servicio en un país distinto de aquel en el que está establecido, un país miembro de la UE, el EEE o en Suiza? ¿O quiere probar otro mercado con el fin de establecerse allí o cumplir con un pedido fuera de su estado? A continuación, realiza una prestación temporal de servicios cuya autorización debe obtenerse de las autoridades competentes.

No es necesario recurrir a un reconocimiento de cualificación profesional para un servicio temporal, sin embargo debe estar establecido en su país de origen. En determinados casos, deberá solicitar autorización a las autoridades competentes interesadas antes de ejercer su servicio. Consulte las fichas de información de la profesión en cuestión sobre [guichet-qualifications.fr](https://www.guichet-qualifications.fr/es/dqp/index.html).

## Procedimientos

El contacto con la autoridad competente suele realizarse en la región donde decide establecerse.

Se debe proporcionar una cierta cantidad de documentos de respaldo, algunos de los cuales a veces requieren una traducción jurada.

Profesiones reconocidas por el procedimiento [tarjeta profesional europea](https://www.guichet-qualifications.fr/es/comprendre/carte_professionnelle_europeenne.html) (*European Professional Card*) Por lo tanto, puede derogar el procedimiento clásico.

**Es bueno saberlo**: si su profesión está regulada en su país de origen pero no en su país de acogida (Francia), puede ejercer como ciudadano del país de acogida. La libertad de prestación de servicios está reservada únicamente a las personas físicas o jurídicas que estén establecidas en un Estado miembro de la UE o del EEE.

Respecto a determinadas actividades, antes de la primera prestación de servicios, se debe enviar una declaración previa a la autoridad autorizada, acompañada de los documentos justificativos. Consulte con la autoridad responsable de su profesión.

Además, el prestador del servicio deberá acreditar, por todos los medios, que posee el conocimiento suficiente del idioma francés necesario para la prestación del servicio.

**<a id="1"></a> [[1]](#Retour1) Lista de los 27 Estados miembros de la Unión Europea:** Alemania, Austria, Bélgica, Bulgaria, Chipre, Croacia, Dinamarca, España, Estonia, Finlandia, Francia, Grecia, Hungría, Irlanda, Italia, Letonia, Lituania, Luxemburgo, Malta , Países Bajos, Polonia, Portugal, Rumania, Eslovaquia, Eslovenia, Suecia, República Checa.

**<a id="2"></a> [[2]](#Retour2) Lista de los 30 Estados del Espacio Económico Europeo:** estos son los 27 países miembros de la Unión Europea (UE) enumerados anteriormente, y Noruega, Islandia y Liechtenstein.

**Lea también:** [Soy europeo y quiero instalarme en Francia (libertad de establecimiento)](libre_etablissement.md)