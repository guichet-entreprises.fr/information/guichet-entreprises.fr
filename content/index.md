﻿<!-- include-file(gq.txt)
+-----------------------------------------------------------------------------+
|     ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗ ┬ ┬┌─┐┬  ┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌┌─┐
|     ║ ╦│ │││  ├─┤├┤  │   ║═╬╗│ │├─┤│  │├┤ ││  ├─┤ │ ││ ││││└─┐
|     ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝╚└─┘┴ ┴┴─┘┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘└─┘
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(site:package)="https://gitlab.com/|13561843|latest|reference_md|reference/configuration.yml" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Guichet Entreprises" -->
<!-- var(title)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:font_sizer_off)="off" -->
<!-- var(page:breadcrumb_off)="off" -->
<!-- var(translation)="None" -->
<!-- var(last-update)="2020-05-03 11:31:25" -->
<!-- var(page:redirect_i18n)="on" -->
<!-- var(page:i18n)="on" -->
<!-- var(page:footer_off)="off" -->
<!-- var(page:nav_menu_off)="off" -->
<!-- var(page:nav_default_off)="off" -->
<!-- var(site:home)="" -->
<!-- var(page:absolute_link)="True" -->

Guichet Entreprises
=====================

<div class="row" style="margin-top:10%">
<div class="col-md-4"><a class="flag-md" href="/fr/{{target_page}}"><span class="flag flag-md fr"></span>Français</a></div>
<div class="col-md-4"><a class="flag-md" href="/en/{{target_page}}"><span class="flag flag-md en"></span>English</a></div>
<div class="col-md-4"><a class="flag-md" href="/es/{{target_page}}"><span class="flag flag-md es"></span>Español</a></div>
<!--<div class="col-md-4"><a class="flag-md" href="/de/{{target_page}}"><span class="flag flag-md de"></span>Deutsch</a></div>-->
<!--<div class="col-md-4"><a class="flag-md" href="/it/{{target_page}}"><span class="flag flag-md it"></span>Italiano</a></div>-->
<!--<div class="col-md-4"><a class="flag-md" href="/pt/{{target_page}}"><span class="flag flag-md pt"></span>Português</a></div>-->
<!--<div class="col-md-4"><a class="flag-md" href="/ru/{{target_page}}"><span class="flag flag-md ru"></span>Русский</a></div>-->
</div>
