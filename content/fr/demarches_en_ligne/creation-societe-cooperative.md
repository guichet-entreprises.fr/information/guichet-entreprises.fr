﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Création d'une société coopérative sous forme de société par actions simplifiée ou d'une société coopérative d'intérêt collectif sous forme de SA, SAS ou SARL" -->
<!-- var(key)="fr-creation-societe-cooperative" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Création d'une société coopérative sous forme de société par actions simplifiée (M0 SAS) ou d'une société coopérative d'intérêt collectif (SCIC) sous forme de SA, SAS ou SARL (M0 SAS, M0 SARL, M0 Agricole)

Vous souhaitez déclarer la création d'une société coopérative sous forme de société par actions simplifiée ou d'une société coopérative d'intérêt collectif (SCIC) sous forme de SA, SAS ou SARL ? Deux possibilités de déclaration vous sont offertes :

- soit effectuer votre déclaration sur le [Guichet unique](https://formalites.entreprises.gouv.fr/) ;
