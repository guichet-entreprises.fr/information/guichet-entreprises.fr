﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Déclaration de radiation d'une association immatriculée au registre du commerce et des sociétés" -->
<!-- var(key)="fr-radiation-association-immatriculee-au-rcs" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de radiation d'une association immatriculée au registre du commerce et des sociétés

Vous souhaitez déclarer la radiation de la situation d'une association immatriculée au registre du commerce et des sociétés ? Vous pouvez :

- soit avoir recours au téléservice [Infogreffe](https://www.infogreffe.fr/) ;
- soit remplir le formulaire M4 téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11685-03.pdf" download>Formulaire M4</a></u></b></p>

## Pièces justificatives

Les pièces justificatives à joindre au dossier sont les suivantes :

- copie des actes relatifs à la clôture de la liquidation ;
- l'attestation de parution dans un journal d'annonces légales ou copie de celui-ci.

## Coût

Le coût est de 13,93 € si la clôture des opérations de liquidation est constatée plus d'un mois après la décision de dissolution et/ou si la décision de dissolution a déjà été déclarée au registre du commerce et des sociétés (RCS).

## Autorité compétente

Le formulaire M4 accompagné des pièces justificatives et des frais doit être déposé ou envoyé au greffe du tribunal de commerce ou du tribunal judiciaire statuant commercialement dans le ressort duquel se situe l'adresse de l'établissement public industriel et commercial.