﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Extension d'une entreprise étrangère" -->
<!-- var(key)="fr-entreprises-etrangeres-creation" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration d'une entreprise étrangère en France <!-- collapsable:open -->

Vous êtes dirigeant d'une entreprise basée à l'étranger (personne physique ou morale) sans établissement en France et employez des salariés en France ou êtes redevable de l'impôt en France ?

Vous devez remplir le formulaire Cerfa EE0 si vous êtes concerné par l'une des situations suivantes :

<ul>
	<li>vous employez au moins une personne affiliée à un régime social en France</li>
	<li>vous êtes une entreprise collectrice du prélèvement à la source (PAS) : c'est le cas si votre entreprise, même sans activité en France, emploie au moins une personne redevable de l'impôt sur le revenu en France</li>
	<li>vous êtes redevable de la TVA</li>
	<li>vous êtes redevable d'un autre impôt ou d'une autre obligation déclarative fiscale en France</li>
</ul>

Si l'une des situations suivantes s'applique à votre entreprise, veuillez remplir le formulaire Cerfa EE0 téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_15928-04.pdf" download>Formulaire EE0</a></u></b></p>

Pour vous aider à remplir le formulaire EE0, vous avez la possibilité de télécharger la notice d'aide en cliquant sur le lien ci-dessous :

<p align="center"><b><u><a href="../../static/images/notice_52276-03.pdf" download>Notice du formulaire EE0</a></u></b></p>

## Pièces justificatives <!-- collapsable:open -->

Les pièces justificatives à joindre au formulaire EE0 sont les suivantes :

- un mandat original signé par les deux parties, **si la formalité est effectuée par un mandataire ou un représentant fiscal** ;
- pour les personnes morales :
  - copie des statuts,
  - traduction des statuts libre, en français, des éléments principaux des statuts (forme juridique, associés, gérant, capital social, objet social). La traduction libre est admise dès lors que les statuts originaux sont rédigés dans la langue d'un des pays membres de l'Union européenne. À défaut, la traduction assermentée en français est exigée ;
- pour les personnes physiques :
  - copie de la pièce d'identité ;
- copie du certificat d'inscription au registre du commerce ou assimilé dans votre pays ;
- attestation d'assujettissement à la TVA de votre pays ou certificat d'inscription au registre du commerce ou assimilé dans votre pays, le cas échéant.

## Autorité compétente <!-- collapsable:open -->

Le formulaire EE0 accompagné des pièces justificatives doit être envoyé à l'une des deux autorités suivantes, selon votre situation :

- le Service Firmes Étrangères (SFE) de l'Urssaf Alsace en cas d'emploi d'au moins une personne affiliée à un régime social en France, par courriel à l'adresse [sfe@urssaf.fr](mailto:sfe@urssaf.fr) ou par courrier à l'adresse suivante :

<p align="center"><b>Service Firmes Étrangères</b><br>
Urssaf Alsace<br>
TSA 60003<br>
38046 Grenoble Cedex 9<br><br></p>

- la Direction générale des finances publiques (DGFIP) dans les autres cas, par courriel à l'adresse suivante : [guichet-formalites@dgfip.finances.gouv.fr](mailto:guichet-formalites@dgfip.finances.gouv.fr). La messagerie de la DGFiP permet uniquement le dépôt de formalités, elle ne peut pas être utilisée pour dialoguer avec l'administration fiscale. Si vous souhaitez obtenir des informations fiscales, consultez la rubrique « Professionnel » du site [impots.gouv.fr](https://www.impots.gouv.fr/accueil).