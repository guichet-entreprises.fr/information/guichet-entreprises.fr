﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Démarches en ligne" -->
<!-- var(key)="fr-formalites" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Démarches en ligne

Toutes les formalités d'immatriculation, de modification et de cessation d'entreprise sont réalisables sur le [Guichet unique](https://formalites.entreprises.gouv.fr/), seul site de déclaration des formalités d'entreprise, conformément à la loi*.

Par exception, les formalités suivantes peuvent être réalisées par une voie dérogatoire :

- [Déclaration de création d'une association immatriculée au registre du commerce et des sociétés](creation-association-immatriculee-au-rcs.md) (formulaire M0 ASSO)
- [Déclaration de modification ou de cessation d'une entreprise étrangère employant du personnel affilié à un régime social en France](./entreprises-etrangeres-modification.md) (formulaire EE2-EE4)

<a id="loi-pacte">*</a> Article 1er de la loi PACTE n° 2019-486 du 22 mai 2019