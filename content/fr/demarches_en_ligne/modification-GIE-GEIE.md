﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Déclaration de modification d'un GIE-GIEE" -->
<!-- var(key)="fr-modification-GIE-GEIE" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de modification d'un GIE ou d'un GEIE <!-- collapsable:open -->

Vous souhaitez déclarer la modification d'un groupement d'intérêt économique (GIE) ou groupement européen d'intérêt économique (GEIE) ? Trois possibilités de déclaration en ligne vous sont offertes :

- soit effectuer votre déclaration sur le [Guichet unique](https://formalites.entreprises.gouv.fr/) ;
- soit recourir au téléservice [Infogreffe](https://www.infogreffe.fr/) ;
- soit envoyer votre déclaration par courrier à l'autorité compétente.

Si vous optez pour l'envoi de votre déclaration par courrier, vous devez remplir, selon les cas exposés plus bas, le formulaire M2 et/ou le formulaire G3 téléchargeables ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11682-07.pdf" download>Formulaire M2</a></u></b></p>

<p align="center"><b><u><a href="../../static/images/cerfa_11930-03.pdf" download>Formulaire G3</a></u></b></p>

Si la modification porte sur un ou plusieurs bénéficiaire(s) effectif(s) du GIE ou du GEIE, vous devez également compléter le formulaire M'BE (GIE ou association) téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_16064-02.pdf" download>Formulaire M'BE (GIE ou association)</a></u></b></p>

### Changement de la dénomination sociale d'un commissaire aux comptes <!-- collapsable:close -->

####  Pièces justificatives

Les pièces justificatives à joindre au dossier sont les suivantes :

- un formulaire G3 dûment rempli et signé en deux exemplaires ;
- un pouvoir en original de l'administrateur s'il n'a pas signé lui-même le formulaire G3.

#### Coût

Joindre à la formalité un chèque 178,08 € (TTC).

### Désignation d'une personne ayant le pouvoir d'engager à titre habituel le groupement <!-- collapsable:close -->

#### Pièces justificatives

Les pièces justificatives à joindre au dossier sont les suivantes :

- un exemplaire en copie de l'acte portant désignation ou de l'acte de délégation de pouvoir ou de signature, certifié conforme par le représentant légal du groupement ou par toute personne habilitée par les textes à opérer cette certification, le cas échéant ;
- un formulaire G3 dûment rempli et signé en deux exemplaires ;
- un pouvoir de l'administrateur s'il n'a pas signé lui-même le formulaire G3 ;
- une copie de la pièce d'identité de la personne désignée. Si celle-ci est de nationalité étrangère et réside en France, fournir une copie recto-verso du titre de séjour ;
- une déclaration sur l'honneur de non-condamnation datée et signée en original par l'intéressé, qui fera l'objet d'une vérification par le juge-commis au registre du commerce et des sociétés auprès du casier judiciaire ;
- une attestation de filiation (nom et prénoms des parents), sauf si la filiation figure dans un document déjà produit ;
- si l'activité déclarée est réglementée, une copie de l'autorisation délivrée par l'autorité de tutelle, du diplôme ou du titre.

#### Coût

Joindre à la formalité un chèque de 192,01 € (comprenant 13,93 € de coût de dépôt d'actes).

### Dissolution <!-- collapsable:close -->

#### Pièces justificatives

Les pièces justificatives à joindre au dossier sont les suivantes :

- un exemplaire du procès-verbal décidant la dissolution et nommant le liquidateur, certifié conforme par le représentant légal ;
- un formulaire M2 dûment rempli et signé en deux exemplaires ;
- un pouvoir en original du liquidateur s'il n'a pas signé lui-même le formulaire M2 ;
- si le liquidateur n'est pas sur le Kbis du groupement concerné, fournir les pièces suivantes :
  - une photocopie de la pièce d'identité du liquidateur. Si le liquidateur est de nationalité étrangère et réside en France, fournir une copie recto-verso du titre de séjour,
  - pour les non-résidents, joindre une copie de la carte de commerçant étranger et une copie du passeport, selon la nationalité,
  - une déclaration sur l'honneur de non-condamnation du liquidateur,
  - une attestation de filiation (nom et prénoms des parents) du liquidateur, sauf si la filiation figure dans un document déjà produit.

#### Coût

Joindre à la formalité un chèque de 192,01 € (comprenant 14.35€ de coût de dépôt d'actes).

### Modification de la dénomination sociale <!-- collapsable:close -->

#### Pièces justificatives

Les pièces justificatives à joindre au dossier sont les suivantes :

- un exemplaire de l'acte du groupement décidant du changement de dénomination, certifié conforme par le représentant légal ;
- un exemplaire du contrat de groupement mis à jour daté et certifié conforme par le représentant légal ;
- un formulaire M2 dûment rempli et signé. Les deux exemplaires du formulaire sont à fournir au greffe qui est centre de formalités des entreprises (CFE) pour les groupements d'intérêt économique ;
- un pouvoir en original du représentant légal s'il n'a pas signé lui-même le formulaire M2.

#### Coût

Joindre à la formalité un chèque de 192,01 € (comprenant 13,93 € de coût de dépôt d'actes).

### Prorogation de la durée de la personne morale <!-- collapsable:close -->

#### Pièces justificatives

Les pièces justificatives à joindre au dossier sont les suivantes :

- un exemplaire de l'acte du groupement constatant la décision de prorogation de la durée du groupement certifié conforme par le représentant légal ;
- un exemplaire du contrat mis à jour, certifié conforme par le représentant légal ;
- un formulaire M2 dûment rempli et signé ;
- un pouvoir en original du représentant légal s'il n'a pas signé lui-même le formulaire M2.

#### Coût

Joindre à la formalité un chèque de 62,08 € (comprenant 0 € de coût de dépôt d'actes).

### Prorogation de l'immatriculation pour les besoins de la liquidation dans un GIE en dissolution <!-- collapsable:close -->

#### Pièces justificatives

Les pièces justificatives à joindre au dossier sont les suivantes :

- un formulaire M2 dûment rempli et signé ;
- un pouvoir en original du représentant légal s'il n'a pas signé lui-même le formulaire M2.

#### Coût

Joindre à la formalité un chèque de 62,08 €.

### Transfert de siège depuis un autre ressort <!-- collapsable:close -->

#### Pièces justificatives

Les pièces justificatives à joindre au dossier sont les suivantes :

- un exemplaire de l'acte décidant du transfert du siège social certifié conforme par le représentant légal ;
- un exemplaire du contrat de groupement mis à jour daté et certifié conforme par le représentant légal ;
- un exemplaire de la liste des sièges sociaux antérieurs certifié conforme par le représentant légal ;
- un formulaire M2 dûment rempli et signé ;
- un pouvoir en original du représentant légal s'il n'a pas signé lui-même le formulaire M2 ;
- un justificatif du droit d'occupation du local du nouveau siège social (par tout moyen : quittance EDF, téléphone, bail, contrat de domiciliation, etc.).

#### Coût

Joindre à la formalité un chèque de 226,49 € (comprenant 13,93 € de coût de dépôt d'actes).

### Transfert de siège dans le même ressort <!-- collapsable:close -->

#### Pièces justificatives

Les pièces justificatives à joindre au dossier sont les suivantes :

- un exemplaire de l'acte décidant du transfert du siège social certifié conforme par le représentant légal ;
- un exemplaire du contrat de groupement mis à jour daté et certifié conforme par le représentant légal ;
- un formulaire M2 dûment rempli et signé ;
- un pouvoir en original du représentant légal s'il n'a pas signé lui-même le formulaire M2 ;
- un justificatif du droit d'occupation du local du nouveau siège social (par tout moyen : quittance EDF, téléphone, bail, contrat de domiciliation, etc.).

#### Coût

Joindre à la formalité un chèque de 192,01 € (comprenant 13,93 € de coût de dépôt d'actes). Si la société possède un ou des établissement(s) secondaire(s) en dehors du ressort du greffe compétent, ajouter 44,17 € par établissement supplémentaire situé dans des greffes différents.

### Adjonction, modification ou suppression d'une enseigne <!-- collapsable:close -->

#### Pièces justificatives

Les pièces justificatives à joindre au dossier sont les suivantes :

- un formulaire M2 dûment rempli et signé ;
- un pouvoir en original du représentant légal s'il n'a pas signé lui-même le formulaire M2.

#### Coût

Joindre à la formalité un chèque de 62,08 €.

### Adjonction, modification ou suppression d'un nom commercial dans un établissement principal <!-- collapsable:close -->

#### Pièces justificatives

Les pièces justificatives à joindre au dossier sont les suivantes :

- un formulaire M2 dûment rempli et signé ;
- un pouvoir en original du représentant légal s'il n'a pas signé lui-même le formulaire M2.

#### Coût

Joindre à la formalité un chèque de 178,08 €.

## Autorité compétente <!-- collapsable:open -->

Le formulaire M2 ou G3 accompagné des pièces justificatives doit être envoyé au greffe du tribunal de commerce ou au greffe du tribunal judiciaire statuant commercialement dans le ressort duquel se situe le siège social.