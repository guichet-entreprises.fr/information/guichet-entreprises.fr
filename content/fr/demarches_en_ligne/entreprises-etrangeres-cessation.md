﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Cessation d'activité d'une entreprise étrangère" -->
<!-- var(key)="fr-entreprises-etrangeres-cessation" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Cessation d'activité d'une entreprise étrangère <!-- collapsable:open -->

Vous êtes dirigeant d'une entreprise basée à l'étranger (personne physique ou morale) sans établissement en France et souhaitez déclarer sa cessation ? Pour ce faire, vous devez remplir le formulaire Cerfa EE2-EE4 téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_16207-01.pdf" download>Formulaire EE2-EE4</a></u></b></p>

Pour vous aider à remplir le formulaire EE2-EE4, vous avez la possibilité de télécharger la notice d'aide en cliquant sur le lien ci-dessous :

<p align="center"><b><u><a href="../../static/images/notice_52996-01.pdf" download>Notice du formulaire EE2-EE4</a></u></b></p>

## Autorité compétente <!-- collapsable:open -->

Le formulaire EE2-EE4 accompagné des pièces justificatives doit être envoyé à l'une des deux autorités suivantes, selon votre situation :

- en cas d'emploi d'au moins une personne affiliée à un régime social en France, au Service Firmes Étrangères (SFE) de l'Urssaf Alsace :
  - soit par courriel à l'adresse [sfe@urssaf.fr](mailto:sfe@urssaf.fr)
  - à défaut, par courrier à l'adresse suivante :

<p align="center"><b>Service Firmes Étrangères</b><br>
Urssaf Alsace<br>
TSA 60003<br>
38046 Grenoble Cedex 9<br><br></p>

           Pour plus d'informations, vous pouvez consulter le site du Service Firmes Étrangères de l'Urssaf : [foreign-companies.urssaf.eu](https://www.foreign-companies.urssaf.eu/index.php/fr/).

- dans les autres cas, à la Direction générale des finances publiques (DGFIP), par courriel à l'adresse suivante : [guichet-formalites@dgfip.finances.gouv.fr](mailto:guichet-formalites@dgfip.finances.gouv.fr). La messagerie de la DGFiP permet uniquement le dépôt de formalités, elle ne peut pas être utilisée pour dialoguer avec l'administration fiscale. Si vous souhaitez obtenir des informations fiscales, consultez la rubrique « Professionnel » du site [impots.gouv.fr](https://www.impots.gouv.fr/accueil).
