﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Déclaration de création d'un établissement public industriel et commercial" -->
<!-- var(key)="fr-modification-epic" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de modification d'un établissement public industriel et commercial

Vous souhaitez déclarer la modification de la situation d'un établissement public industriel et commercial ? Trois possibilités de déclaration vous sont offertes :

- soit effectuer votre déclaration sur le [Guichet unique](https://formalites.entreprises.gouv.fr/) ;
- soit recourir au téléservice [Infogreffe](https://www.infogreffe.fr/) ;
- soit envoyer votre déclaration par courrier à l'autorité compétente.

Si vous optez pour l'envoi de votre déclaration par courrier, vous devez remplir le formulaire M2 ou M3 téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11682-07.pdf" download>Formulaire M2</a></u></b></p>

<p align="center"><b><u><a href="../../static/images/cerfa_11683-04.pdf" download>Formulaire M3</a></u></b></p>

## Modification du représentant légal <!-- collapsable:open -->

### Pièces justificatives <!-- collapsable:open -->

Les pièces justificatives à joindre au dossier sont les suivantes :

- copie de la décision nommant les personnes chargées de représenter l'établissement ou de l'administrer ;
- copie de la carte nationale d'identité ou du passeport en cours de validité des personnes chargées de représenter l'établissement ou de l'administrer.

### Coût <!-- collapsable:open -->

Joindre à la formalité un chèque de 178,08 €.

## Modification de la dénomination sociale <!-- collapsable:open -->

### Pièces justificatives <!-- collapsable:open -->

Les pièces justificatives à joindre au dossier sont les suivantes :

- copie du Journal officiel mentionnant l'acte qui a autorisé la modification de la nomination sociale ou copie de l'acte ayant procédé à la modification.

### Coût <!-- collapsable:open -->

Joindre à la formalité un chèque de 178,08 €.

## Autorité compétente <!-- collapsable:open -->

Le formulaire M2 ou M3 accompagné des pièces justificatives et des frais doit être déposé ou envoyé au greffe du tribunal de commerce ou du tribunal judiciaire statuant commercialement dans le ressort duquel se situe l'adresse de l’établissement public industriel et commercial.