﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Modification d'une entreprise étrangère" -->
<!-- var(key)="fr-entreprises-etrangeres-modification" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de modification ou de cessation d'une entreprise étrangère employant du personnel affilié à un régime social en France <!-- collapsable:open -->

Vous êtes dirigeant d'une entreprise basée à l'étranger (personne physique ou morale) sans établissement en France, avec du personnel affilié à un régime social français, et souhaitez déclarer une ou plusieurs modification(s) relative(s) à votre entreprise, ou sa cessation ?

Pour ce faire, rendez-vous sur l’onglet « modification, cessation » du [Guichet unique](https://formalites.entreprises.gouv.fr/) et laissez-vous guider.

En cas de difficulté, et uniquement pour les entreprises étrangères employant du personnel affilié à un régime social en France, vous avez également la possibilité de remplir le formulaire Cerfa EE2-EE4 téléchargeable ci-dessous et l'envoyer, accompagné des pièces justificatives, au Service Firmes Étrangères (SFE) de l'Urssaf Alsace à l'adresse électronique [sfe@urssaf.fr](mailto:sfe@urssaf.fr) ou par courrier au Service Firmes Etrangères, Urssaf Alsace, TSA 60003, 38046 Grenoble Cedex 9 :

<p align="center"><b><u><a href="../../static/images/cerfa_16207-01.pdf" download>Formulaire Cerfa EE2-EE4</a></u></b></p>

Pour vous aider à remplir ce formulaire EE2-EE4, vous pouvez télécharger la notice d'aide en cliquant sur le lien ci-dessous :

<p align="center"><b><u><a href="../../static/images/notice_52996-01.pdf" download>Notice d'aide à la complétion du formulaire</a></u></b></p>

Pour plus d'informations, vous pouvez consulter le site du Service Firmes Étrangères de l'Urssaf : [foreign-companies.urssaf.eu](https://www.foreign-companies.urssaf.eu/index.php/fr/).