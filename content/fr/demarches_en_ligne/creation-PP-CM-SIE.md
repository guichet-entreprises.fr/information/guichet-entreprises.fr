﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Déclaration de début d'une activité commerciale entrant dans le champ de compétence de la DGFIP pour les personnes physiques" -->
<!-- var(key)="fr-creation-PP-CM-SIE" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de début d'une activité commerciale entrant dans le champ de compétence de la DGFIP pour les personnes physiques <!-- collapsable:open -->

Vous souhaitez déclarer la création d'une activité de location de locaux nus commerciaux ou professionnels, opter pour la TVA et faire entrer les loyers perçus dans la catégorie des revenus fonciers ? Vous devez remplir le formulaire Cerfa P0 CM ou P0 CM ME pour les micro-entrepreneurs. Ces deux formulaires sont téléchargeables ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11676-13.pdf" download>Formulaire P0 CM</a></u></b></p>

<p align="center"><b><u><a href="../../static/images/cerfa_15253-08.pdf" download>Formulaire P0 CM ME</a></u></b></p>

Le cas échéant, si le local ou les locaux est ou sont détenu(s) par 2 propriétaires indivis ou plus, ou si 2 personnes ou plus ont le pouvoir d'engager l'établissement, vous devez également remplir l'intercalaire P0' téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11771-05.pdf" download>Intercalaire P0'</a></u></b></p>

En cas de reprise d'un patrimoine affecté, vous devez également compléter l'intercalaire PEIRL CM Impôt :

<p align="center"><b><u><a href="../../static/images/cerfa_14215-06.pdf" download>Intercalaire PEIRL CM Impôt</a></u></b></p>

## Pièces justificatives <!-- collapsable:open -->

Joindre une copie de la carte d'identité revêtant une mention manuscrite datée et signée certifiant que la copie est conforme à l'original.

## Autorité compétente <!-- collapsable:open -->

Le formulaire P0 CM ou PO CM ME accompagné de la copie de la carte d'identité, et des intercalaires P0' et/ou PEIRL CM Impôt le cas échéant, doit être envoyé par courriel à la Direction générale des finances publiques (DGFIP) à l'adresse suivante : [guichet-formalites@dgfip.finances.gouv.fr](mailto:guichet-formalites@dgfip.finances.gouv.fr). La messagerie de la DGFiP permet uniquement le dépôt de formalités, elle ne peut pas être utilisée pour dialoguer avec l'administration fiscale. Si vous souhaitez obtenir des informations fiscales, consultez la rubrique « Professionnel » du site [impots.gouv.fr](https://www.impots.gouv.fr/accueil).