﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Déclaration de modification d'une association immatriculée au registre du commerce et des sociétés" -->
<!-- var(key)="fr-modification-association-immatriculee-au-rcs" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de modification d'une association immatriculée au registre du commerce et des sociétés

Vous souhaitez déclarer la modification de la situation d'une association immatriculée au registre du commerce et des sociétés ? Vous devez remplir le formulaire M2 ou M3 téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11682-07.pdf" download>Formulaire M2</a></u></b></p>

<p align="center"><b><u><a href="../../static/images/cerfa_11683-04.pdf" download>Formulaire M3</a></u></b></p>

## Pièces justificatives

En fonction de la modification envisagée, des justificatifs relatifs à l'identité des dirigeants et la transmission des statuts mis à jour peuvent être exigés.

## Coût

Joindre à la formalité un chèque de 178,08 € ou de 192,01 € si des actes modificatifs doivent être déposés en annexe.

## Autorité compétente

Le formulaire M2 ou M3 accompagné des pièces justificatives et des frais doit être déposé ou envoyé au greffe du tribunal de commerce ou du tribunal judiciaire statuant commercialement dans le ressort duquel se situe l'adresse de l’association immatriculée au registre du commerce et des sociétés.