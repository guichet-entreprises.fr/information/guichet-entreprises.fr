<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Les formalités de modification d’entreprise" -->
<!-- var(key)="fr-formalites_modification" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Les formalités de modification d’entreprise

## Pourquoi réaliser des formalités de modification d’entreprise ?

Les informations déclarées lors de la création de votre entreprise sont susceptibles d’évoluer au fil du temps.

Dès lors qu’une des informations indiquées lors de l’immatriculation de l’entreprise est modifiée, celle-ci doit être déclarée dans le délai d’un mois (article R. 123-45 du Code de commerce).

C’est le cas des changements de dénomination sociale ou de l'objet social, de la forme de la société, du capital social, du gérant, de la domiciliation du siège social, etc.

Vous avez alors l’obligation de réaliser une formalité de modification d’entreprise.

## Les formalités de modification de société

Les modifications peuvent concerner :

- les informations de la personne morale (dénomination, sigle, forme juridique, capital, durée, date de clôture de l'exercice social, dissolution, radiation, etc.) ;
- les informations relatives aux dirigeants (nom de naissance ou d’usage, nationalité, domicile, prénom, statut du conjoint (conjoint collaborateur, associé ou salarié), dénomination, forme juridique, siège social, représentant permanent) ;
- les informations relatives à un établissement (transfert d’établissement, ouverture ou fermeture d’un établissement secondaire, modification de l’activité, du nom commercial ou de l’enseigne, mise en sommeil, modification relative au fondé de pouvoir, etc.).

La réalisation d’une modification de société peut être soumise à certaines conditions, comme une prise de décision des associés ou actionnaires en assemblée générale. Pour certaines modifications, l'unanimité est requise. Pour d’autres, un quorum à la majorité des 2/3 ou 3/4 des parts sociales des associés présents suffit.

La modification doit également faire l’objet d’une publication dans le mois suivant dans un journal d’annonces légales présent dans le département où est situé le siège social de la société.

L'insertion dans un journal d’annonces légales (JAL) est obligatoire pour toute modification portant sur l'une des mentions obligatoires pour l'immatriculation, notamment l'objet de la société, sa dénomination sociale, son capital social, etc.

L’absence de publicité en cas de changement dans les statuts peut faire l'objet d'une action en régularisation, pouvant être exercée par tout intéressé pendant 3 ans à compter de l'acte modifiant les statuts.

[\> Plus d'informations sur les formalités de modification d'une société](https://www.service-public.fr/professionnels-entreprises/vosdroits/N31143) (service-public.fr)

En revanche, il n’est pas nécessaire de réaliser une formalité de modification lors de dépôt des comptes, de cessions de parts (s’il n’y a pas de modification impactant les dirigeants), de modifications purement fiscales (modification du régime d’imposition, ex : passage du régime réel simplifié au réel normal hors changement de seuil), etc. 

## Les modifications d’entreprise individuelle

L'entreprise individuelle n'est pas une société. Si vous souhaitez faire évoluer votre entreprise individuelle, qu’elle soit exercée en EIRL et/ou sous le statut de micro-entrepreneur, les formalités de modification concernent :

- les informations relatives à l’entrepreneur (nom de naissance ou d’usage, nationalité, domicile, prénom, statut du conjoint (conjoint collaborateur ou salarié)) ;
- les informations relatives à un établissement (transfert d’établissement, ouverture ou fermeture d’un établissement secondaire, modification de l’activité, du nom commercial ou de l’enseigne, mise en sommeil, modification relative au fondé de pouvoir, etc.) ;
- l’affectation de patrimoine (option pour l’EIRL, fin de l’EIRL ou modification des informations relatives à l’EIRL ou au contenu de la déclaration d’affectation de patrimoine) ;
- la radiation (cessation définitive d’activité).

Attention, n'oubliez pas de déclarer la cessation d'activité de votre entreprise individuelle, au risque de devoir continuer à vous acquitter de la cotisation foncière des entreprises et des charges sociales.

### Bon à savoir

Depuis le 15 février 2022, suite à l’adoption de la loi n° 2022-172 en faveur de l’activité professionnelle indépendante et de l’entrée en vigueur du statut unique de l’entrepreneur individuel à compter du 15 mai 2022, il n’est plus possible de choisir le statut de l’EIRL.

S’il n’est plus possible de créer sous ce statut, les EIRL déjà immatriculées continuent d'exercer leurs activités dans les mêmes conditions.

## Le cas particulier des micro-entrepreneurs

Le micro-entrepreneur est un entrepreneur individuel qui bénéficie d’un régime simplifié de l’entreprise individuelle.

Son entreprise est soumise aux mêmes principes que l’entrepreneur individuel pour toute modification. Qu’il s’agisse d’une modification de sa personne (changement de nom, de domicile, etc.), de son patrimoine, de l’activité ou de toute autre caractéristique de son activité, le micro-entrepreneur devra effectuer les mêmes formalités.

Attention, si vous souhaitez renoncer au régime de la micro-entreprise il conviendra d’opter pour un régime réel d’imposition de vos bénéfices. Comme il s’agit d’une modification purement fiscale, vous devrez contacter directement le service des impôts. Il n’est pas nécessaire de réaliser une formalité.