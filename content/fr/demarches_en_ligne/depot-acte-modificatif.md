﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Dépôt en annexe du RCS d'un acte modificatif n'entraînant pas de modification des informations déclarées au RCS" -->
<!-- var(key)="fr-depot-acte-modificatif" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Dépôt en annexe du RCS d'un acte modificatif n'entraînant pas de modification des informations déclarées au RCS

Vous souhaitez déposer en annexe du registre du commerce et des sociétés (RCS) un acte, une délibération ou une décision modifiant les pièces déposées lors de la constitution ou tout autre acte dont le dépôt est légalement prévu ? Deux possibilités de déclaration en ligne vous sont offertes :

- soit effectuer votre déclaration sur le [Guichet unique](https://formalites.entreprises.gouv.fr/) ;
- soit recourir au téléservice [Infogreffe](https://www.infogreffe.fr/).
