﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Déclaration de modification d'une société (dont dissolution)" -->
<!-- var(key)="fr-modification-societe" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de modification d'une société (dont dissolution)

Vous souhaitez procéder à la modification de la situation d’une société ? Deux possibilités de déclaration en ligne vous sont offertes :

- soit effectuer votre déclaration sur le [Guichet unique](https://formalites.entreprises.gouv.fr/) ;
- soit recourir au téléservice [Infogreffe](https://www.infogreffe.fr/).

Le recours au téléservice Infogreffe peut amener l’organisme en charge du traitement de votre formalité à qui le dossier sera transmis (chambre de métiers et de l’artisanat, chambre de l’agriculture) à solliciter auprès de vous la transmission d’informations ou de paiement de frais complémentaires.

Dans le cas d’une personne morale agricole, vous avez la possibilité de [déclarer une modification par le recours à un formulaire papier à transmettre à la chambre d’agriculture](modification-societe-agricole.md).

Vous avez en outre la possibilité de [déclarer une modification par le recours à un formulaire papier dans les cas de  transfert d'établissement, d'ouverture d'un nouvel établissement, de modification de la forme juridique, de modification relative aux dirigeants et aux associés, de dissolution ou de cessation d'activité (sans disparition de la personne morale)](modification-transfert-societe.md).