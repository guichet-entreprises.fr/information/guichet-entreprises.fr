﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Déclaration de transfert d’établissement, d'ouverture d'un nouvel établissement, de modification de la forme juridique, de modification relative aux dirigeants et aux associés, de dissolution ou de cessation d'activité (sans disparition de la personne morale) d'une personne morale" -->
<!-- var(key)="fr-modification-transfert-societe" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de transfert d'établissement, d'ouverture d'un nouvel établissement, de modification de la forme juridique, de modification relative aux dirigeants et aux associés, de dissolution ou de cessation d'activité (sans disparition de la personne morale) d'une personne morale

Vous souhaitez déclarer le transfert d'établissement, l'ouverture d'un nouvel établissement, la modification de la forme juridique, une modification relative aux dirigeants et aux associés, la dissolution ou la cessation d'activité (sans disparition de la personne morale) d'une personne morale ?

**<u>Si vous n'avez pas déjà déposé votre demande auprès du Guichet Entreprises</u>**, deux possibilités de déclaration vous sont offertes :

- soit effectuer votre déclaration sur le [Guichet unique](https://formalites.entreprises.gouv.fr/) ;
- soit envoyer votre déclaration par courrier à l'autorité compétente.

Si vous optez pour l'envoi de votre déclaration par courrier, vous pouvez remplir le formulaire M2 ou M3 téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11682-07.pdf" download>Formulaire M2</a></u></b></p>

<p align="center"><b><u><a href="../../static/images/cerfa_11683-04.pdf" download>Formulaire M3</a></u></b></p>

**<u>ATTENTION</u>** : Une formalité ne doit pas être réalisée à la fois par utilisation de ce formulaire **<u>et</u>** par le Guichet Entreprises : cela rendrait son traitement impossible.

## Autorité compétente

Le formulaire M2 ou M3 accompagné des pièces justificatives et des frais doit être déposé ou envoyé :

- pour les sociétés commerciales, à la chambre de commerce et d'industrie compétente dans le ressort de laquelle se situe le siège social de l'entreprise ou son établissement secondaire ;
- pour les sociétés du secteur des métiers et de l'artisanat, à la chambre de métiers et de l'artisanat compétente dans le ressort de laquelle se situe le siège social de l'entreprise ou son établissement secondaire ;
- pour les sociétés civiles et autres que commerciales, les sociétés d'exercice libéral, personnes morales assujetties à l'immatriculation au registre du commerce et des sociétés ne relevant pas des chambres de commerce et d'industrie ou des chambres de métiers et de l'artisanat, auprès du greffe du tribunal de commerce ou du tribunal judiciaire statuant commercialement dans le ressort duquel se situe le siège social de l'entreprise ou son établissement secondaire. La formalité peut également être réalisée par l'intermédiaire du site [www.infogreffe.fr](https://www.infogreffe.fr/).