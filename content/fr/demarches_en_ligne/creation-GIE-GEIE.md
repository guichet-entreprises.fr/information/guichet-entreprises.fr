﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Déclaration de création d'un GIE-GIEE" -->
<!-- var(key)="fr-creation-GIE-GEIE" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de création d'un GIE ou d'un GEIE <!-- collapsable:open -->

Vous souhaitez déclarer la création d'un groupement d'intérêt économique (GIE) ou d'un groupement européen d'intérêt économique (GEIE) ? Vous devez remplir le formulaire Cerfa G0 téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11770-03.pdf" download>Formulaire G0</a></u></b></p>

Si le GIE/GEIE comporte plus de trois dirigeants, vous devez également compléter l'intercalaire G0' téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11773-03.pdf" download>Intercalaire G0'</a></u></b></p>

Vous devez également compléter le formulaire M'BE (GIE ou association) relatif au(x) bénéficiaire(s) effectif(s) du GIE ou du GEIE et téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_16064-02.pdf" download>Formulaire M'BE (GIE ou association)</a></u></b></p>

## Pièces justificatives <!-- collapsable:open -->

### Pour la création d'un GIE <!-- collapsable:close -->

Les pièces à joindre au dossier sont les suivantes :

- une expédition du contrat de groupement s'il est établi par acte authentique, ou un original du contrat timbré et enregistré par la recette des impôts, paraphé, daté et signé par tous les membres ou leurs mandataires, si le contrat est établi par acte sous seing privé. Le(s) pouvoir(s) pour la signature du contrat est ou sont déposé(s) en un exemplaire original ;
- une copie certifiée conforme par le représentant légal des actes de nomination des organes d'administration et de contrôle, si l'administrateur, le contrôleur de gestion, et le contrôleur des comptes ne sont pas nommés dans le contrat constitutif ;
- une copie certifiée conforme de l'acte conférant la qualité de représentant permanent d'une personne morale nommée administrateur, le cas échéant ;
- les imprimés G0 dûment remplis et signés (il doit notamment être y déclaré l'existence ou non d'une clause d'exonération des dettes sociales nées antérieurement à l'entrée des membres dans le groupement, ainsi que la nature civile ou commerciale de l'activité) ;
- un pouvoir en original du représentant légal s'il n'a pas signé lui-même le formulaire G0 ;
- une pièce justifiant de l'occupation régulière des locaux du siège (bail, contrat de domiciliation, quittance EDF ou facture de téléphone). Il est rappelé ici l'importance capitale pour votre groupement d'identifier clairement l'adresse de son siège social, permettant ainsi au greffe de vous envoyer les extraits du registre du commerce et des sociétés lors d'une formalité, ou à vos partenaires d'entrer en contact avec vous. Si le siège social est fixé au domicile personnel du représentant légal, il convient d'identifier sa boîte aux lettres au nom du GIE et d'accomplir les démarches nécessaires auprès de la Poste pour le suivi des courriers ;
- si l'activité déclarée est réglementée, une copie de l'autorisation délivrée par l'autorité de tutelle, du diplôme ou du titre ;
- s'il s'agit d'un achat de fonds de commerce, une copie de l'acte de vente du fonds de commerce ;
- s'il s'agit d'une prise en location-gérance de fonds de commerce :
  - une copie de l'attestation de parution dans un journal d'annonces légales de l'avis relatif à la prise en location-gérance,
  - une copie du contrat de location-gérance ;
- s'il s'agit d'une gérance-mandat de fonds de commerce :
  - une copie de l'attestation de parution dans un journal d'annonces légales de l'avis relatif à la prise en gérance-mandat,
  - une copie du contrat de gérance-mandat ;
- s'il s'agit d'un apport de fonds de commerce, une copie de l'acte d'apport du fonds de commerce.

#### Pour le contrôleur de gestion, le contrôleur aux comptes, les administrateurs personnes physiques <!-- collapsable:close -->

- une copie de la pièce d'identité (copie du passeport ou de la carte nationale d'identité en cours de validité, ou copie recto-verso du titre de séjour en cours de validité, le cas échéant. Le statut porté sur le titre de séjour de son titulaire doit lui permettre de s'inscrire au RCS) ;
- une déclaration sur l'honneur de non-condamnation datée et signée en original par l'intéressé, qui fera l'objet d'une vérification auprès du casier judiciaire par le juge commis au registre du commerce et des sociétés ;
- une attestation de filiation (nom et prénoms des parents), sauf si la filiation figure dans un document déjà produit.

#### Pour les membres personnes physiques <!-- collapsable:close -->

- une copie de la pièce d'identité (copie du passeport ou de la carte nationale d'identité en cours de validité, ou copie recto-verso du titre de séjour en cours de validité, le cas échéant. Le statut porté sur le titre de séjour de son titulaire doit lui permettre de s'inscrire au RCS).

#### Pour les administrateurs, les membres, le contrôleur aux comptes des personnes morales <!-- collapsable:close -->

- un extrait du registre du commerce et des sociétés en original datant de moins de trois mois si la personne est immatriculée, ou tout document officiel justifiant de son existence légale si elle n'est pas immatriculée au RCS ;
- pour le représentant permanent de l'administrateur, produire les mêmes pièces que celles prévues pour les administrateurs personnes physiques.

#### Pour les commissaires aux comptes <!-- collapsable:close -->

- un justificatif d'inscription sur la liste officielle des commissaires aux comptes si celle-ci n'est pas encore publiée ;
- la lettre d'acceptation de la désignation.

### Pour la création d'un GEIE <!-- collapsable:close -->

Les pièces à joindre au dossier sont les suivantes :

- une expédition du contrat de groupement s'il est établi par acte authentique, ou un original du contrat timbré et enregistré par la recette des impôts, paraphé, daté et signé par tous les membres en personne ou par leurs mandataires, si le contrat est établi par acte sous seing privé. Le(s) pouvoir(s) pour la signature du contrat est ou sont déposé(s) en un exemplaire original ;
- une copie certifiée conforme par le représentant légal des actes de nomination du ou des gérant(s) (indiquant s'ils peuvent agir seuls ou conjointement) et des organes de contrôle, s'ils ne sont pas nommés dans le contrat constitutif ;
- si le gérant du GEIE est une personne morale, une copie certifiée conforme de l'acte conférant au représentant permanent cette qualité ;
- les imprimés G0 dûment remplis et signés (il doit notamment être y déclaré l'existence ou non d'une clause d'exonération des dettes sociales nées antérieurement à l'entrée des membres dans le groupement, ainsi que la nature civile ou commerciale de l'activité) ;
- un pouvoir en original du représentant légal s'il n'a pas signé lui-même le formulaire G0 ;
- une pièce justifiant de l'occupation régulière des locaux du siège (bail, contrat de domiciliation, quittance EDF ou facture de téléphone) ;
- si l'activité déclarée est réglementée, une copie de l'autorisation délivrée par l'autorité de tutelle, du diplôme ou du titre ;
- s'il s'agit d'un achat de fonds de commerce, une copie de l'acte de vente du fonds de commerce ;
- s'il s'agit d'une prise en location-gérance de fonds de commerce :
  - une copie de l'attestation de parution dans un journal d'annonces légales de l'avis relatif à la prise en location-gérance,
  - une copie du contrat de location-gérance ;
- s'il s'agit d'une gérance-mandat de fonds de commerce :
  - une copie de l'attestation de parution dans un journal d'annonces légales de l'avis relatif à la prise en gérance-mandat,
  - une copie du contrat de gérance-mandat ;
- s'il s'agit d'un apport de fonds de commerce, une copie de l'acte d'apport du fonds de commerce.

#### Pour le gérant, le contrôleur de gestion, le contrôleur des comptes, les membres personnes physiques <!-- collapsable:close -->

- une copie de la pièce d'identité (copie du passeport ou de la carte nationale d'identité en cours de validité, ou copie recto-verso du titre de séjour en cours de validité, le cas échéant. Le statut porté sur le titre de séjour de son titulaire doit lui permettre de s'inscrire au RCS) ;
- une déclaration sur l'honneur de non-condamnation datée et signée en original par l'intéressé, qui fera l'objet d'une vérification auprès du casier judiciaire par le juge commis au registre du commerce et des sociétés ;
- une attestation de filiation (nom et prénoms des parents), sauf si la filiation figure dans un document déjà produit.

#### Pour les membres, le gérant, le contrôleur des comptes personnes morales <!-- collapsable:close -->

- un extrait du registre du commerce et des sociétés en original datant de moins de trois mois si la personne est immatriculée, ou tout document officiel justifiant de son existence légale si elle n'est pas immatriculée au RCS ;
- pour le représentant permanent du gérant personne morale, les mêmes pièces que celles prévues pour les gérants personnes physiques.

#### Pour les commissaires aux comptes <!-- collapsable:close -->

- un justificatif d'inscription sur la liste officielle des commissaires aux comptes si celle-ci n'est pas encore publiée ;
- la lettre d'acceptation de la désignation.

## Coût <!-- collapsable:open -->

Joindre un chèque de 66,88 € pour les créations ou de 69,56 € pour les achats, les apports, les prises en location-gérance ou les prises en gérance-mandat.

Joindre un chèque de 21,41 € correspondant aux frais de déclaration des bénéficiaires effectifs faite concomitamment à la formalité d’immatriculation.

Les chèques doivent être libellés à l'ordre du greffe de tribunal de commerce compétent et joints à la demande.

## Autorité compétente <!-- collapsable:open -->

Le formulaire G0 accompagné du chèque, des pièces justificatives, et de l'intercalaire G0' le cas échéant, doit être envoyé au greffe du tribunal de commerce ou au greffe du tribunal judiciaire statuant commercialement dans le ressort duquel se situe le siège social.