﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Déclaration de modification de mes bénéficiaires effectifs, sans modification des informations déclarées au RCS" -->
<!-- var(key)="fr-modification-beneficiaires-effectifs" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de modification de mes bénéficiaires effectifs, sans modification des informations déclarées au RCS

Vous souhaitez modifier les informations relatives à vos bénéficiaires effectifs (identification, adresse, modalités du contrôle exercé sur la société), sans que cela ne nécessite de modification des informations inscrites au registre du commerce et des sociétés (RCS) ?

Trois possibilités de déclaration vous sont offertes :

- soit effectuer votre déclaration sur le [Guichet unique](https://formalites.entreprises.gouv.fr/) ;
- soit recourir au téléservice [Infogreffe](https://www.infogreffe.fr/) ;
- soit envoyer votre déclaration par courrier à l'autorité compétente.

Si vous optez pour l'envoi de votre déclaration par courrier, vous devez remplir le formulaire Cerfa M'BE téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_16062-02.pdf" download>Formulaire M'BE</a></u></b></p>

**<u>Attention :</u>** si la modification envisagée entraîne également la modification des informations déclarées au sein du RCS (ex. : montant du capital social, identification du gérant ou du président, siège ou adresse d'un établissement, etc.), vous devez réaliser une formalité de modification des données de la société et la compléter avec les modifications liées aux bénéficiaires effectifs.

Le formulaire peut être déposé directement auprès du guichet du greffe dans le ressort duquel se situe le siège social ou transmis par voie postale.

La formalité peut également être réalisée en ligne par l'intermédiaire du site [infogreffe.fr](https://www.infogreffe.fr/).

Il convient de joindre au dépôt un paiement de 43,35 € TTC en cas de dépôt au guichet ou de 44,84 € TTC en cas de dépôt par voie postale.