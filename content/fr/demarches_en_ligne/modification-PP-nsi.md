﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Déclaration de modification d'une activité exercée par un particulier producteur d'énergie photovoltaïque, loueur de meublé non professionnel (LMNP) non inscrit au RCS, loueur de biens meubles non inscrit au RCS, loueur de garages, quirataire de navires" -->
<!-- var(key)="fr-modification-PP-nsi" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de modification d'une activité exercée par un particulier producteur d'énergie photovoltaïque, loueur de meublé non professionnel (LMNP) non inscrit au RCS, loueur de biens meubles non inscrit au RCS, loueur de garages, quirataire de navires <!-- collapsable:open -->

Vous souhaitez déclarer la modification d'une activité exercée par un particulier producteur d'énergie photovoltaïque, loueur de meublé non professionnel (LMNP) non inscrit au RCS, loueur de biens meubles non inscrit au RCS, loueur de garages, quirataire de navires ? Vous pouvez effectuer votre déclaration sur le [Guichet unique](https://formalites.entreprises.gouv.fr/) ou remplir le formulaire Cerfa P2P4i téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11996-05.pdf" download>Formulaire P2P4i</a></u></b></p>

En cas de reprise d'un patrimoine affecté, vous devez également compléter l'intercalaire PEIRL Impôt téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_14217-05.pdf" download>Intercalaire PEIRL Impôt</a></u></b></p>

## Pièces justificatives <!-- collapsable:open -->

Joindre une copie de la carte d'identité revêtant une mention manuscrite datée et signée certifiant que la copie est conforme à l'original.

## Autorité compétente <!-- collapsable:open -->

Le formulaire P2P4i accompagné de la copie de la carte d'identité doit être envoyé par courriel à la Direction générale des finances publiques (DGFIP) à l'adresse suivante : [guichet-formalites@dgfip.finances.gouv.fr](mailto:guichet-formalites@dgfip.finances.gouv.fr). La messagerie de la DGFiP permet uniquement le dépôt de formalités, elle ne peut pas être utilisée pour dialoguer avec l'administration fiscale. Si vous souhaitez obtenir des informations fiscales, consultez la rubrique « Professionnel » du site [impots.gouv.fr](https://www.impots.gouv.fr/accueil).