﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Déclaration de création d'une association immatriculée au registre du commerce et des sociétés" -->
<!-- var(key)="fr-creation-association-immatriculee-au-rcs" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de création d'une association immatriculée au registre du commerce et des sociétés

Vous souhaitez déclarer la création d'une association immatriculée au registre du commerce et des sociétés ? Vous devez remplir le formulaire M0 ASSO téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_15909-02.pdf" download>Formulaire M0 ASSO</a></u></b></p>

## Pièces justificatives

Les pièces justificatives à joindre au dossier sont les suivantes :

- copie des statuts ;
- copie de la déclaration ou de l'inscription pour les associations d'Alsace-Moselle ou du Journal officiel qui a rendu publique l'association ;
- copie d'extraits des procès-verbaux de délibération des instances ayant désigné les organes de direction et de contrôle ou le conseil d'administration ;
- copie du procès-verbal des délibérations de l'assemblée générale constatant la décision d'émettre des obligations ; 
- copie de la carte nationale d'identité ou du passeport en cours de validité accompagnée d'une déclaration de l'intéressé faisant connaître sa filiation si celle-ci ne figure pas sur le document fourni pour les personnes ayant le pouvoir de les engager à titre habituel (ou document équivalent pour les étrangers, s'il y a lieu tout document justifiant la nationalité et sa traduction en langue française) ;
- justificatif de la jouissance des locaux où est installé le siège par tout document établi au nom de la société permettant de justifier la réalité de l'adresse déclarée.

## Coût

Le coût de la formalité est de 66,88 € TTC.

## Autorité compétente

Le formulaire M0 ASSO accompagné des pièces justificatives et des frais doit être déposé ou envoyé au greffe du tribunal de commerce ou du tribunal judiciaire statuant commercialement dans le ressort duquel se situe le siège de l'association.