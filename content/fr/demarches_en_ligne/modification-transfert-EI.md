﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Déclaration de transfert de l'entreprise, de transfert d'un établissement, d'ouverture d'un nouvel établissement ou de décès de l'exploitant avec poursuite d'exploitation ou demande de maintien provisoire au sein du registre d'immatriculation pour une entreprise individuelle" -->
<!-- var(key)="fr-modification-transfert-EI" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de transfert de l'entreprise, de transfert d'un établissement, d'ouverture d'un nouvel établissement ou de décès de l'exploitant avec poursuite d'exploitation ou demande de maintien provisoire au sein du registre d'immatriculation pour une entreprise individuelle

Vous souhaitez déclarer le transfert de l'entreprise, le transfert d'un établissement, l'ouverture d'un nouvel établissement ou le décès de l'exploitant avec poursuite d'exploitation ou demande de maintien provisoire au sein du registre d'immatriculation pour une entreprise individuelle ?

**<u>Si vous n'avez pas déjà déposé votre demande auprès du Guichet Entreprises</u>**, deux possibilités de déclaration vous sont offertes :

- soit effectuer votre déclaration sur le [Guichet unique](https://formalites.entreprises.gouv.fr/) ;
- soit envoyer votre déclaration par courrier à l'autorité compétente.

Si vous optez pour l'envoi de votre déclaration par courrier, vous devez remplir, selon la nature de votre activité, le formulaire P2 CM, P2 Agricole, P2 PL ou AC2, téléchargeables ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11678-08.pdf" download>Formulaire P2 CM</a></u></b></p>

<p align="center"><b><u><a href="../../static/images/cerfa_11935-10.pdf" download>Formulaire P2 Agricole</a></u></b></p>

<p align="center"><b><u><a href="../../static/images/cerfa-14213-04.pdf" download>Formulaire AC2</a></u></b></p>

**<u>Pour les personnes exerçant, à titre de profession habituelle, une activité indépendante et les artistes auteurs, la formalité doit être réalisée par l'intermédiaire du site</u> [www.cfe-urssaf.fr](https://www.cfe.urssaf.fr/saisiepl/).**

**<u>ATTENTION</u>** : Une formalité ne doit pas être réalisée à la fois par utilisation de ce formulaire **<u>et</u>** par le Guichet Entreprises : cela rendrait son traitement impossible.

## Autorité compétente

Le formulaire P2 CM, P2 Agricole, P2 PL ou AC2, accompagné des pièces justificatives et des éventuels frais doit être déposé ou envoyé :

- pour les commerçants, à la chambre de commerce et d'industrie compétente dans le ressort de laquelle se situe l'adresse de l'entreprise ou son établissement secondaire ;
- pour les entreprises individuelles du secteur des métiers et de l'artisanat, à la chambre de métiers et de l'artisanat compétente dans le ressort de laquelle se situe l'adresse de l'entreprise ou son établissement secondaire ;
- pour les exploitations et entreprises agricoles, à la chambre d'agriculture compétente dans le ressort de laquelle se situe l'adresse de l'entreprise ou son établissement secondaire ou son établissement secondaire ;
- pour les agents commerciaux, au greffe du tribunal de commerce ou du tribunal judiciaire à compétence commerciale dans le ressort duquel se situe l'adresse de l'entreprise ou son établissement secondaire.

Rappel : Pour les personnes exerçant, à titre de profession habituelle, une activité indépendante et les artistes auteurs, la formalité doit être réalisée par l'intermédiaire du site [www.cfe-urssaf.fr](https://www.cfe.urssaf.fr/saisiepl/).