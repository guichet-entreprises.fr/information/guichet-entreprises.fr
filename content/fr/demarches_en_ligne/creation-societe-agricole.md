﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Déclaration de création d'une entreprise agricole (personne morale)" -->
<!-- var(key)="fr-creation-societe-agricole" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Déclaration de création d'une entreprise agricole (personne morale) <!-- collapsable:open -->

Vous souhaitez déclarer la création d'une entreprise agricole (personne morale) ? Vous devez remplir le formulaire Cerfa M0 agricole téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11927-07.pdf" download>Formulaire M0 agricole</a></u></b></p>

Si l'entreprise comporte plus de deux dirigeants, vous devez également compléter l'intercalaire M0' agricole téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_14117-03.pdf" download>Intercalaire M0' agricole</a></u></b></p>

Pour les membres de la personne morale, vous devez compléter le formulaire NSm agricole (volet social) téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11925-07.pdf" download>Formulaire NSm agricole</a></u></b></p>

Si des personnes physiques non salariées participent aux travaux de l'entreprise, vous devez compléter le formulaire NSp agricole (volet social) téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_11926-07.pdf" download>Formulaire NSp agricole</a></u></b></p>

Vous devez également compléter le formulaire M'BE sociétés relatif au(x) bénéficiaire(s) effectif(s) de la société et téléchargeable ci-dessous :

<p align="center"><b><u><a href="../../static/images/cerfa_16062-02.pdf" download>Formulaire M'BE sociétés</a></u></b></p>

## Pièces justificatives <!-- collapsable:open -->

Les pièces à joindre au dossier sont les suivantes :

- statuts constitutifs enregistrés en 2 exemplaires ;
- annonce légale ;
- autorisation de domiciliation + justificatif de jouissance des locaux ;
- pour chacun des gérants :
  - justificatif d’identité (photocopie de la carte d'identité ou du passeport en cours de validité),
  - attestation de non condamnation ;
- pour chaque associé exploitant, le volet social NSm ;
- pour chaque associé indéfiniment responsable, un justificatif d'identité ;
- un chèque de 37,45 € s'il s'agit d'une société commerciale (SARL, SAS, etc.) ou de 66,88 € dans les autres cas ;
- un chèque de 21,41 € correspondant aux frais de déclaration des bénéficiaires effectifs faite concomitamment à la formalité d’immatriculation.

## Autorité compétente <!-- collapsable:open -->

Le formulaire M0 agricole, accompagné du formulaire M'BE, de l'intercalaire M0' agricole et/ou du formulaire NSm agricole et/ou du formulaire NSp agricole le cas échéant, doit être envoyé au greffe du tribunal de commerce ou au greffe du tribunal judiciaire statuant commercialement dans le ressort duquel se situe le siège social.