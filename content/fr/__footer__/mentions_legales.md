﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Mentions légales" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="None" -->
<!-- var(key)="fr-__footer__-mentions_legales" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="fr" -->

Mentions légales
================

Dernière mise à jour : <!-- begin-var(last-update) -->2020-09-01<!-- end-var -->

## Identification de l’éditeur

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p>

La conception éditoriale, le suivi, la maintenance technique et les mises à jour du site internet guichet-entreprises.fr sont assurés par le pôle Guichet Entreprises.

Le pôle Guichet Entreprises est un service de l'INPI, en application du décret n° 2020-946 du 30 juillet 2020.

## Objet du site

Le site guichet-entreprises.fr permet à tout entrepreneur d’accomplir à distance les formalités et procédures nécessaires à la création, aux modifications de la situation et à la cessation d’activité d’une entreprise, soit en transmettant un dossier unique tel que défini à l’article R. 123-23 du Code de commerce dès lors qu’il respecte les dispositions de l’article R. 123-24 du Code de commerce, soit en préparant un tel dossier de manière interactive et le transmettre.

Les formalités font l’objet d’une signature électronique dont les modalités sont disponibles dans le politique de signature électronique.

En outre le site guichet-entreprises.fr met à disposition des entrepreneurs un ensemble d’informations concernant les formalités, procédures et exigences liées aux activités réglementées conformément à l’article R. 123-2 du Code de commerce.

## Traitement des données personnelles

Les données personnelles contenues dans le dossier unique conformément au règlement général sur la protection des données font l’objet de dispositions disponibles [ici](./protection_donnees.md).

Vous disposez d’un droit d’accès et de rectification de vos données personnelles. En revanche, le traitement de celles-ci est nécessaire au respect d’une obligation légale à laquelle le responsable de traitement est soumis. Dès lors, le droit d’opposition à ce traitement n’est pas possible, en application des dispositions de l’article 6.1 c) du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016, et de l’article 56 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés dans sa version consolidée.

Vous pouvez exercer ce droit de plusieurs façons :

* en vous adressant au centre de formalités des entreprises (CFE) destinataire du dossier de déclaration ;
* en envoyant un courriel à l'assistance utilisateur via le [formulaire de contact](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1) ;
* en envoyant un courrier à l’adresse suivante :

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;À l'attention du délégué à la protection des données<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p>

## Droits de reproduction

Le contenu de ce site relève de la législation française et internationale sur le droit d’auteur et la propriété intellectuelle.

L’ensemble des éléments graphiques du site est la propriété de l'INPI. Toute reproduction ou adaptation des pages du site qui en reprendrait les éléments graphiques est strictement interdite.

Toute utilisation des contenus à des fins commerciales est également interdite.

Toute citation ou reprise de contenus du site doit avoir obtenu l’autorisation du directeur de la publication. La source (guichet-entreprises.fr) et la date de la copie devront être indiquées ainsi que la mention de l'INPI.

## Liens vers les pages du site

Tout site public ou privé est autorisé à établir des liens vers les pages du site guichet-entreprises.fr. Il n’y a pas à demander d’autorisation préalable. Cependant, l’origine des informations devra être précisée, par exemple sous la forme : « Création d'entreprise (source : guichet-entreprises.fr, un site de l'INPI) ». Les pages du site guichet-entreprises.fr ne devront pas être imbriquées à l’intérieur des pages d’un autre site. Elles devront être affichées dans une nouvelle fenêtre ou un nouvel onglet.

## Liens vers les pages de sites extérieurs

Les liens présents sur le site guichet-entreprises.fr peuvent orienter l’utilisateur sur des sites extérieurs dont le contenu ne peut en aucune manière engager la responsabilité de l'INPI.

## Environnement technique

Certains navigateurs peuvent bloquer par défaut l’ouverture de fenêtres sur ce site. Afin de vous permettre d’afficher certaines pages, vous devez autoriser l’ouverture des fenêtres lorsque le navigateur vous le propose en cliquant sur le bandeau d’avertissement alors affiché en haut de la page.

En cas d’absence de message d’avertissement de la part de votre navigateur, vous devez configurer celui-ci afin qu’il autorise l’ouverture des fenêtres pour le site guichet-entreprises.fr.