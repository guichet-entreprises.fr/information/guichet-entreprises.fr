<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Guichet-entreprises.fr, un site sécurisé" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="None" -->
<!-- var(key)="" -->
<!-- var(last-update)="" -->
<!-- var(lang)="fr" -->

Guichet-entreprises.fr, un site sécurisé
=====================================

L’homologation sécurité du Guichet Entreprises a été délivrée au Guichet Entreprises le 1er juin 2019 pour une durée de 30 mois.

Cette homologation garantit sa conformité aux exigences de l’[ANSSI](https://www.ssi.gouv.fr/) (Agence nationale de sécurité des systèmes d’informations) en matière de sécurisation d’accès, de saisie, de conservation, de traitement et d’échanges des données.

La démarche d’analyse des risques de sécurité est basée sur les principes énoncés dans la méthode EBIOS (RGS) et la norme ISO 27005.
