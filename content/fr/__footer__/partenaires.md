<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Partenaires" -->
<!-- var(key)="fr-__footer__-partenaires" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Partenaires

Associer les meilleures expertises de la création !

Le site guichet-entreprises.fr est une initiative de l'État français dans le cadre de l'application de la [directive européenne sur les services](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32006L0123&from=FR).

Naturellement proches des enjeux des entrepreneurs dont ils sont les premiers interlocuteurs, les centres de formalités des entreprises (CFE) sont les partenaires privilégiés de guichet-entreprises.fr. Leur travail commun permet aujourd'hui aux entrepreneurs français et européens de créer leur entreprise sur un site simple et complet.

## Nos partenaires <!-- collapsable:off -->

### Les chambres d'agriculture (CA) <!-- collapsable:close -->

Les chambres d'agriculture (CA) proposent un service aux exploitants agricoles dans différents domaines : conseil technique sur les productions végétales et animales, conseil d'entreprise sur l'installation, transmission, urbanisme, diversification, etc.

Depuis 1997, elles gèrent les centres de formalités des entreprises (CFE) en mettant à la disposition des entreprises agricoles une assistance à la réalisation de leurs formalités de création, de modification ou de cessation d'entreprise.

Les CFE agricoles facilitent et centralisent l'accès des entreprises agricoles aux multiples démarches auprès d'organismes distincts (Insee, services fiscaux, MSA, établissement départemental d'élevage, Casier viticole, en cas de société : greffe du tribunal de commerce) pour souscrire en un même lieu et sur un même document les déclarations relatives à leur création, aux modifications de leur situation ou à la cessation de leur activité auxquelles elles sont tenues par les lois et règlements en vigueur. Ils assurent le contrôle formel et la transmission des déclarations et pièces justificatives aux destinataires des formalités. La confidentialité des informations recueillies est assurée.

Depuis 2006, les CFE sont chargés de recevoir les déclarations de fonds agricoles et de tenir le registre des fonds agricoles.

Depuis décembre 2009, guidés par la directive européenne sur les services n° 2006/123/E du 12/12/2006, les CFE des chambres d'agriculture proposent le guichet unique pour les centres équestres, en réalisant pour ces entreprises de service toutes les formalités préalables à la création d'entreprise et d'information.

Il est possible de prendre rendez-vous avec votre chambre d'agriculture où des conseillers du CFE agricole vous aideront dans vos démarches.

Les chambres d'agriculture sont regroupées au niveau régional dans les chambres régionales d'agriculture. L’**Assemblée permanente des chambres d'agriculture (APCA)** est l'échelon national du réseau des chambres d'agriculture. Elle est membre associé du Conseil de l'agriculture française.

**Contact :** Assemblée permanente des Chambres d'Agriculture - Service Entreprises, 9, avenue George V, 75008 Paris

[> Visitez le site des chambres d'agriculture](https://chambres-agriculture.fr/)

### Les chambres de commerce et d'industrie (CCI) <!-- collapsable:close -->

Établissements publics animés par des chefs d'entreprises élus, les chambres de commerce et d'industrie ont pour mission de :

* **représenter les intérêts des entreprises auprès des pouvoirs publics** ;
* **accompagner les porteurs de projet et les entreprises** à chaque étape de leur création et de leur développement ;
* **former les collaborateurs et chefs d'entreprises** (apprentissage, école de commerce, d'ingénieur) ;
* **gérer des équipements indispensables à la vie des territoires** (pépinières d'entreprises, ports, aéroports, centres d'expositions, de congrès, etc.).

Les chambres de commerce et d'industrie font partie du réseau CCI de France dont l'établissement national fédérateur est **CCI France**.

Pour votre projet de création, reprise ou développement d'entreprises, elles mobilisent des compétences fortes et diversifiées, vous proposent un parcours adapté à votre projet et des formations pour doper votre compétence d'entrepreneur.

N'hésitez pas à contacter la CCI proche du lieu de votre projet. Ses conseillers vous accompagneront dans vos études et son centre de formalités des entreprises – guichet unique prendra en charge vos démarches administratives.

[> Visitez le site du réseau des CCI](https://www.cci.fr/)

### Les chambres de métiers et de l'artisanat (CMA) <!-- collapsable:close -->

Premier réseau d'appui aux entreprises artisanales, les chambres de métiers et de l'artisanat sont administrées par des élus qui sont eux-mêmes chefs d'entreprises artisanales.

Présentes en France métropolitaine et d'outre-mer, les CMA sont les partenaires incontournables des entreprises artisanales et ont pour objectif leur développement, leur compétitivité et leur pérennité : elles développent, dans une relation de proximité, une offre de services adaptée aux besoins spécifiques de chaque type de publics, qu'il soit apprenti, créateur, repreneur, salarié, demandeur d'emploi, personne à la recherche d'une orientation professionnelle ou encore cédant d'entreprise artisanale.

Cette offre couvre toutes les étapes de la vie de l'entreprise, de la création/reprise à la transmission, en passant par toutes les phases de développement, ainsi que par la formation de ses actifs.

Les porteurs de projets accueillis chaque année dans plus de 372 points d'accueil des CMA bénéficient d'un véritable parcours balisé comprenant également l'accompagnement post création. Les CMA savent mobiliser leur réseau de partenaires (banques, notaires, experts-comptables, avocats et syndicats professionnels) pour aider à la formalisation de projet et répondre à l'ensemble des besoins financiers, juridiques, fiscaux, sociaux.

**Les CMA sont également chargées :**

* de la tenue d'un registre de publicité légale (le répertoire des métiers) ;
* de la gestion des centres de formalités des entreprises (CFE) compétents pour les entreprises individuelles et les sociétés assujetties à l'immatriculation à ce répertoire et pour les personnes qui exercent une activité artisanale en dispense d'immatriculation ;
* de reconnaître la qualité d'artisan, d'artisan d'art ou de maître artisan ;
* d'organiser l'apprentissage dans le secteur artisanal en partenariat avec les régions.

Pour trouver une chambre de métiers et de l'artisanat proche de chez vous, [visitez le portail des chambres de métiers et de l'artisanat](https://www.artisanat.fr/).

Numéro d'appel commun : 0 825 36 36 36 (0,15 € TTC/min)

### Les greffiers des tribunaux de commerce <!-- collapsable:close -->

Les greffiers des tribunaux de commerce, **un modèle original et efficace au service des entreprises et de la justice économique.**

A la croisée des mondes judiciaire et économique, les greffiers des tribunaux de commerce exercent **une mission de service public efficace et moderne.** Ils interviennent à chaque moment important de la vie des entreprises, de leur naissance à leur disparition, de la résolution de leurs litiges au traitement de leurs difficultés.

Véritables officiers d'état-civil des entreprises, ils contribuent efficacement à la sécurité juridique et à la transparence de la vie économique.

Nommés par arrêté du garde des Sceaux, les greffiers des tribunaux de commerce sont délégataires de la puissance publique de l'État. Ils exercent leurs missions sous le contrôle du ministère public.

Le greffier, membre du tribunal de commerce, remplit des attributions juridictionnelles de plusieurs ordres :

* **des attributions juridictionnelles au profit des justiciables et du tribunal :** assistance des juges, conservation des actes et des archives, authentification et délivrance des copies des décisions ;
* **des attributions juridictionnelles à caractère économique au profit des entreprises :** les greffiers sont des professionnels du droit et des spécialistes de la tenue des registres légaux : contrôle des formalités au registre du commerce et des sociétés, conservation et publicité des sûretés mobilières et diffusion de l'information juridique et financière sur les entreprises.

Leur expérience alliée à la maîtrise des innovations technologiques est un atout considérable au service de la justice commerciale et des entreprises. Les greffiers innovent au profit des entreprises et des justiciables, tout en assurant dans un cadre réglementaire fixé par la loi, un service public de proximité.

Le statut des greffiers des tribunaux de commerce et les missions qu'ils exercent répondent à une double exigence :

* **satisfaction de l'État** dont ils exercent, par délégation, certaines prérogatives et pour lequel ils ont un devoir de compétence, de loyauté et d'éthique ;
* **satisfaction des usagers de la justice commerciale** dont ils sont les interlocuteurs directs.

**Les greffiers, acteurs de la justice commerciale, au service de la juridiction et des justiciables :**

Les missions judiciaires exercées par le greffier, membre du tribunal sont liées au contentieux entre les entreprises, à la prévention et au traitement des difficultés des entreprises. Ils traitent chaque année plus d'un million de décisions de justice

**Les greffiers, acteurs de la vie économique, au service des entreprises :**

Par la tenue des registres légaux, les greffiers offrent un observatoire privilégié du monde économique. Avec la mise à disposition des informations contenues dans ces registres, qui représentent de 60 000 à 80 000 actes par jour, les greffiers des tribunaux de commerce permettent à chacun d'obtenir des informations fiables sur les entreprises et leurs dirigeants, de s'assurer de la situation économique et financière d'un partenaire commercial.

Les greffes des tribunaux de commerce ou des tribunaux de grande instance statuant commercialement sont les centres de formalités des entreprises (CFE) compétents pour :

a) les sociétés civiles et autres que commerciales ;
b) les sociétés d'exercice libéral ;
c) les personnes morales assujetties à l'immatriculation au registre du commerce et des sociétés autres que :
  * les commerçants,
  * les sociétés commerciales,
  * les personnes physiques et les sociétés assujetties à l'immatriculation au répertoire des métiers,
  * les personnes physiques bénéficiant de la dispense d'immatriculation, relative au développement et à la promotion du commerce et de l'artisanat,
  * les personnes physiques et les sociétés assujetties à l'immatriculation au registre des entreprises de la batellerie artisanale ;
d) les établissements publics industriels et commerciaux ;
e) les agents commerciaux ;
f) les groupements d'intérêt économique et les groupements européens d'intérêt économique.

[> Visitez le site du Conseil national des greffiers des tribunaux de commerce](https://www.cngtc.fr/fr/)

[> Feuilletez le livre virtuel présentant les greffiers des tribunaux de commerce](https://www.cngtc.fr/page-flip/les-greffiers-des-tribunaux-de-commerce/)

### Les Urssaf <!-- collapsable:close -->

**Le réseau des Urssaf** [[1]](#1) <a id="Retour1"></a> est le moteur de notre système de protection sociale avec pour mission principale la collecte des cotisations et contributions sociales, sources du financement du régime général de la Sécurité sociale. Plus de 800 partenaires lui confient des missions de recouvrement ou de contrôle. Il recouvre ainsi **les contributions d'assurance chômage et les cotisations AGS** [[2]](#2) <a id="Retour2"></a> pour le compte de l'Unedic et procède au calcul et à l'appel des cotisations destinées au Régime social des indépendants.

La stratégie du réseau des Urssaf est fondée sur le développement de la qualité de la relation et du service auprès de 9,5 millions d'usagers. En tant que service public moderne et fiable, le réseau des Urssaf a développé des offres de services spécifiques, notamment pour les particuliers employeurs (CESU, Pajemploi), les associations (CEA), ou les petites entreprises (TESE).

Les Urssaf sont également le centre de formalités des entreprises (CFE) pour les professions libérales, les associations embauchant du personnel, les artistes-auteurs, les taxis locataires, les vendeurs à domicile indépendants et les auto-entrepreneurs (professions libérales et commerçants) réalisant leurs formalités via le site [autoentrepreneur.Urssaf.fr](https://www.autoentrepreneur.urssaf.fr/portail/accueil.html).

L’**Agence centrale des organismes de sécurité sociale (Acoss)** est la caisse nationale du réseau des Urssaf. Outre le pilotage de son réseau, l'Acoss assure la gestion commune de la trésorerie des différentes branches du régime général.

Afin de proposer des services homogènes tout en conservant une très forte présence locale, l'Acoss mènera jusqu'en 2014 une consolidation de son réseau via la création de 22 Urssaf régionales tout en maintenant un ancrage et une présence départementale.

L'Acoss est également depuis 2011 l'unique producteur labellisé de statistiques trimestrielles de l'emploi salarié.

[> Visitez le site du réseau des Urssaf](https://www.urssaf.fr/portail/home.html)

**<a id="1"></a> [[1]](#Retour1) Urssaf :** Union de recouvrement des cotisations de sécurité sociale et d'allocations familiales

**<a id="2"></a> [[2]](#Retour2) AGS :** Association pour la gestion du régime de garantie des créances des salariés (ou Régime de garantie des salaires)

## Les principaux utilisateurs de guichet-entreprises.fr <!-- collapsable:off -->

Les mandataires et les associations de mandataires sont les principaux utilisateurs du service guichet-entreprises.fr.

Vous êtes un mandataire ? Vous utilisez souvent le site guichet-entreprises.fr pour vos besoins ? <a href="mailto:gu-support-mandataire@inpi.fr">Contactez-nous</a> pour optimiser l'utilisation de notre site.