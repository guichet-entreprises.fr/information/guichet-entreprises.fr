﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Conditions générales d’utilisation" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="None" -->
<!-- var(key)="fr-__footer__-cgu" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="fr" -->

Conditions générales d’utilisation
==================================

Dernière mise à jour : <!-- begin-var(last-update) -->2020-09-01<!-- end-var -->

## Préambule

Ce document présente les modalités d’engagement à l’utilisation du service en ligne Guichet Entreprises de l'Institut national de la propriété industrielle (INPI) pour les usagers. Il s’inscrit dans le cadre juridique :

* de la [directive 2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32006L0123&from=FR) du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur ;
* du [règlement n° 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32014R0910&from=FR) du Parlement européen et du Conseil du 23 juillet 2014 (e-IDAS) sur l’identification électronique et les services de confiance pour les transactions électroniques au sein du marché intérieur ;
* du [règlement n° 2016/679](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679&from=FR) du Parlement européen et du Conseil du 27 avril 2016 relatif au traitement des données à caractère personnel ;
* de la [loi n° 2019-486 du 22 mai 2019](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038496102&categorieLien=id) relative à la croissance et la transformation des entreprises, notamment son article 1er ;
* du [décret n° 2020-946 du 30 juillet 2020](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000042182948&categorieLien=id) désignant l’Institut national de la propriété industrielle en tant qu’organisme unique mentionné au neuvième alinéa de l’article 1er de la loi n° 2019-486 du 22 mai 2019 relative à la croissance et la transformation des entreprises et confiant à cet institut la gestion des services informatiques mentionnés aux articles R. 123-21 et R. 123-30-9 du code de commerce
* de l'[ordonnance du 8 décembre 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives et le décret n° 2010-112 du 2 février 2010 pris pour l’application des articles 9, 10 et 12 de cette ordonnance ;
* du dispositif de la [loi n° 78-17 du 6 janvier 1978 modifiée](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) relative à l’informatique, aux fichiers et aux libertés ;
* de l’[article 441-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) du Code Pénal ;
* des articles [R. 123-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006255835&cidTexte=LEGITEXT000005634379&dateTexte=20070327) à  [R. 123-30](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032944426&cidTexte=LEGITEXT000005634379&dateTexte=20170101) du Code de commerce ;

## Objet du document

Le présent document a pour objet de définir les conditions générales d’utilisation du service en ligne Guichet Entreprises de l'INPI, dénommé « Service » ci-après, entre l'INPI et les usagers.

Ce service est placé sous l’autorité du directeur général de l’Institut national de la propriété industrielle.

## Définition et objet du Service

Le Service mis en œuvre par le Guichet Entreprises de l’INPI (ci-après dénommé « le service Guichet Entreprises de l’INPI ») contribue à simplifier les démarches liées à la création, aux modifications de la situation et à la cessation d’activité d’une entreprise des usagers français et européens.

Le présent Service a fait l’objet d’une homologation sécurité conformément à l’instruction interministérielle 901/SGDSN/ANSSI de l’ANSSI (NOR : PRMD1503279J) du 28 janvier 2015 et la PGSSI des ministères économique et financier (NOR : FCPP1622039A) du 1er août 2016.

L’utilisation du Service est facultative et gratuite. Toutefois, dans le cadre des formalités effectuées via ce Service, comme celles de la création d’entreprise, des paiements en ligne peuvent être demandés. Ceux-ci sont sécurisés.

L’ensemble des destinataires du Service sont dénommés ci-après les organismes partenaires.

En vertu de l’article R.123-3 du Code de commerce, les données recueillies sont transmises aux fins de traitement aux organismes partenaires suivants :

« 1° Sous réserve des dispositions des 2° et 3°, les chambres de commerce et d’industrie territoriales créent et gèrent les centres de formalités des entreprises compétents pour :

a) les commerçants ;
b) les sociétés commerciales.

2° Les chambres de métiers et de l’artisanat de région créent et gèrent les centres compétents pour les personnes physiques et morales assujetties à l’immatriculation au répertoire des métiers.

3° Les greffes des tribunaux de commerce ou des tribunaux judiciaires statuant commercialement créent et gèrent les centres compétents pour :

a)	les sociétés civiles et autres que commerciales ;
b)	les sociétés d’exercice libéral ;
c)	les personnes morales assujetties à l’immatriculation au registre du commerce et des sociétés autres que celles mentionnées aux 1° et 2° ;
d)	les établissements publics industriels et commerciaux ;
e)	les agents commerciaux ;
f)	les groupements d’intérêt économique et les groupements européens d’intérêt économique.

4° Les unions de recouvrement des cotisations de sécurité sociale et d’allocations familiales (Urssaf) ou les caisses générales de sécurité sociale créent et gèrent les centres compétents pour :

a)   les personnes exerçant, à titre de profession habituelle, une activité indépendante réglementée ou non autre que commerciale, artisanale ou agricole ;
b)   les employeurs dont les entreprises ne sont pas immatriculées au registre du commerce et des sociétés, au répertoire des métiers ou au registre des entreprises de la batellerie artisanale, et qui ne relèvent pas des centres mentionnés au 6°.

5° Les chambres d’agriculture créent et gèrent les centres compétents pour les personnes physiques et morales exerçant à titre principal des activités agricoles […]. »

En application des dispositions des articles L. 411-1 et D. 411-1-3 du Code de la propriété intellectuelle, les données recueillies sont destinées à être rendues accessibles en consultation sur le portail [data.inpi.fr](https://data.inpi.fr/) de l’Institut national de la propriété industrielle et réutilisables via ses licences open data homologuées.

## Fonctionnalités

Le Service permet à l’usager :

* d’accéder à la documentation précisant les obligations des centres de formalités des entreprises (CFE) ainsi que les éléments constitutifs du dossier de déclaration et du dossier de demandes d’autorisation ;
* de créer un compte utilisateur donnant accès à un espace de stockage personnel. Cet espace permet à l’usager de gérer et utiliser ses données à caractère personnel, de conserver les informations le concernant et les documents et pièces justificatives qui lui sont nécessaires pour l’accomplissement des démarches administratives.

Depuis son espace, l’usager peut :

* constituer son dossier unique visé à l’article R. 123-1 du Code de commerce comprenant le dossier de déclaration et, le cas échéant, de demandes d’autorisation ;
* transmettre son dossier de déclaration et, le cas échéant, de demandes d’autorisation au centre de formalités des entreprises compétent ;
* accéder aux informations de suivi du traitement de son dossier de déclaration et, le cas échéant, de son dossier de demandes d’autorisation ;
* permettre aux CFE de mettre en œuvre les traitements nécessaires à l’exploitation des informations reçues de la personne morale visées au dernier alinéa de l’article R. 123-21 du Code de commerce : réception du dossier unique prévu à l’article 2 de la loi n° 94-126 du 11 février 1994 relative à l’initiative et à l’entreprises individuelle et transmis par la personne morale visées à l’article R. 123-21 du Code de commerce ; réception des informations de suivi du traitement de ces dossiers telles que transmises par les organismes et autorités partenaires ; transmission des informations de suivi du traitement des dossiers uniques à la personne morale visée à l’article R. 121-21 du Code de commerce.

## Modalités d’inscription et d’utilisation du Service

L’accès au Service est ouvert à toute personne et gratuit. Il est facultatif et n’est pas exclusif d’autres canaux d’accès pour permettre à l’usager d’accomplir ses formalités.

Pour la gestion de l’accès à l’espace personnel du Service, l’usager partage les informations suivantes :

* l’adresse électronique de l’usager ;
* le mot de passe choisi par l’usager.

L’utilisation du Service requiert la fourniture de données personnelles pour la constitution du dossier unique.
Le traitement des données personnelles est décrit dans la politique de protection de données disponible [ici](./protection_donnees.md).

L’utilisation du Service requiert une connexion et un navigateur internet. Le navigateur doit être configuré pour autoriser les cookies de session.

Afin de garantir une expérience de navigation optimale, nous vous recommandons d’utiliser les versions de navigateurs suivantes :

* Firefox version 45 et plus ;
* Google Chrome version 48 et plus.

En effet, d’autres navigateurs sont susceptibles de ne pas supporter certaines fonctionnalités du Service.

Le Service est optimisé pour un affichage en 1024×768 pixels. Il est recommandé d’utiliser la dernière version du navigateur et de le mettre à jour régulièrement pour bénéficier des correctifs de sécurité et des meilleures performances.

## Conditions spécifiques d’utilisation du service de signature

Le service de signature électronique est accessible directement via le Service.

L’article R. 123-24 du Code de commerce en application du règlement n° 910/2014 du Parlement européen et du Conseil du 23 juillet 2014 (e-IDAS) sont les références applicables au service de signature électronique du Service.

## Rôles et engagement

### Engagement du service Guichet Entreprises de l'INPI

1.   Le service Guichet Entreprises de l'INPI met en œuvre et opère le Service conformément au cadre juridique en vigueur défini en préambule.
2.   Le service Guichet Entreprises de l'INPI s’engage à prendre toutes les mesures nécessaires permettant de garantir la sécurité et la confidentialité des informations fournies par l’usager.
3.   Le service Guichet Entreprises de l'INPI s’engage à assurer la protection des données collectées dans le cadre du Service, et notamment empêcher qu’elles soient déformées, endommagées ou que des tiers non autorisés y aient accès, conformément aux mesures prévues par l’ordonnance du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives, le décret n° 2010-112 du 2 février 2010 pris pour l’application des articles 9, 10 et 12 de cette ordonnance et le règlement n° 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif au traitement des données à caractère personnel.
4.   Le service Guichet Entreprises de l'INPI et les organismes partenaires garantissent aux usagers du Service les droits d’accès, de rectification et d’opposition prévus par la loi n° 78-17 du 6 janvier 1978 relative à l’informatique aux fichiers et aux libertés et le réglement n° 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif au traitement des données à caractère personnel. Ce droit peut s’exercer de plusieurs façons :<br>
	  a) en s’adressant au centre de formalités des entreprises (CFE) destinataire du dossier de déclaration ;<br>
	  b) en envoyant un courriel à l'assistance utilisateur via le [formulaire de contact](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1) ;<br>
	  c) en envoyant un courrier à l’adresse suivante :<br>

<br>
<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;Pôle Guichet Entreprises<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p><br>

5.   Le service Guichet Entreprises de l'INPI et les organismes partenaires s’engagent à n’opérer aucune commercialisation des informations et documents transmis par l’usager au moyen du Service, et à ne pas les communiquer à des tiers, en dehors des cas prévus par la loi.
6.   Le service Guichet Entreprises de l'INPI s’engage à assurer la traçabilité de toutes les actions réalisées par l’ensemble des utilisateurs du Service, y compris celles des organismes partenaires et de l’usager.
7.   Le service Guichet Entreprises de l'INPI offre aux usagers un support en cas d’incident ou d’alerte sécurité.

## Engagement de l’usager

1.   L’usager remplit en ligne son dossier et le valide en y joignant éventuellement les pièces nécessaires au traitement de ce dernier.
2.   À l’issue de la constitution de son dossier, s’affiche à l’écran un récapitulatif des éléments renseignés par l’usager afin que celui-ci puisse les vérifier et les confirmer. Après confirmation, le dossier est transmis aux organismes partenaires. La confirmation et la transmission du formulaire par l’usager vaut signature de celui-ci.
3.   Les présentes conditions générales s’imposent à tout utilisateur usager du Service.

## Disponibilité et évolution du Service

Le Service est disponible 7 jours sur 7, 24 heures sur 24.

Le service Guichet Entreprises de l'INPI se réserve toutefois la faculté de faire évoluer, de modifier ou de suspendre, sans préavis, le Service pour des raisons de maintenance ou pour tout autre motif jugé nécessaire. L’indisponibilité du Service ne donne droit à aucune indemnité. En cas d’indisponibilité du Service, une page d’information est alors affichée à l’usager lui mentionnant cette indisponibilité ; il est alors invité à effectuer sa démarche ultérieurement.

Les termes des présentes conditions d’utilisation peuvent être amendés à tout moment, sans préavis, en fonction des modifications apportées au Service, de l’évolution de la législation ou de la réglementation, ou pour tout autre motif jugé nécessaire.

## Responsabilités

1.   La responsabilité du service Guichet Entreprises de l'INPI ne peut être engagée en cas d’usurpation d’identité ou de toute utilisation frauduleuse du Service.
2.   Les données transmises aux services en ligne des organismes partenaires restent de la responsabilité de l’usager, même si celles-ci sont transmises par les moyens techniques mis à disposition dans le Service. L’usager peut à tout moment les modifier ou les supprimer auprès des organismes partenaires. Il peut choisir de supprimer toutes les informations de son compte en supprimant ses données auprès du Service.
3.   Il est rappelé à l’usager que toute personne procédant à une fausse déclaration pour elle-même ou pour autrui s’expose, notamment, aux sanctions prévues à l’article 441-1 du Code Pénal, prévoyant des peines pouvant aller jusqu’à trois ans d’emprisonnement et 45 000 euros d’amende.