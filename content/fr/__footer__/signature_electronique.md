﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de signature électronique" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="None" -->
<!-- var(key)="fr-__footer__-signature_electronique" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="fr" -->

Politique de signature électronique
===================================

<table class="table table-striped table-bordered">
<thead style="text-align: center;border-collapse: collapse">
<tr>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Version</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Date</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Objet</th>
</tr>
</thead>
<tbody style="text-align: center;border-collapse: collapse">
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V1</td>
<td style="border: 1px solid #AAA;padding: 4px">25/05/2018</td>
<td style="border: 1px solid #AAA;padding: 4px">Version initiale</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V2</td>
<td style="border: 1px solid #AAA;padding: 4px">01/09/2020</td>
<td style="border: 1px solid #AAA;padding: 4px">Mise à jour suite à l’entrée en vigueur du décret n° 2020-946 du 30 juillet 2020 désignant l’Institut national de la propriété industrielle en tant qu’organisme unique mentionné au neuvième alinéa de l’article 1er de la loi n° 2019-486 du 22 mai 2019 relative à la croissance et la transformation des entreprises et confiant à cet institut la gestion des services informatiques mentionnés aux articles R. 123-21 et R. 123-30-9 du Code de commerce</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V3</td>
<td style="border: 1px solid #AAA;padding: 4px">03/06/2021</td>
<td style="border: 1px solid #AAA;padding: 4px">Mise à jour concernant l'archivage et la suppression du dossier signé</td>
</tr>
</tbody>
</table>

## Objet du document

La signature électronique apposée sur un ensemble de données permet de garantir l’intégrité des données transmises, la non-répudiation des données signées et l’authenticité de leur émetteur.

La présente politique de signature électronique est un document décrivant les conditions de recevabilité par les organismes destinataires, à savoir les centres de formalités des entreprises et les teneurs de registres publics, d’un fichier sur lequel est apposé une signature électronique dans le cadre d’échanges électroniques visés à l’article R. 123-24 du Code de commerce.

En vertu de cet article, « lorsqu’une signature est requise, le recours à une signature électronique sécurisée est exigé dans les conditions prévues à l’article 1316-4 du Code civil [désormais article 1367 du Code civil] et au décret n° 2017-1416 du 28 septembre 2017 relatif à la signature électronique. Toutefois, pour la transmission par voie électronique des dossiers de création d’entreprise ou des déclarations prévues à l’article L. 526-7, est autorisé, y compris pour les demandes d’immatriculation au registre du commerce et des sociétés, le recours à une signature électronique présentant les caractéristiques prévues par la première phrase du second alinéa de l’article 1316-4 du Code civil désormais article 1367 du Code civil ».

De fait, les dispositions réglementaires applicables pour la mise en œuvre de la signature électronique du service en ligne Guichet Entreprises sont issues des textes suivants :

* Règlement n° 910/2014 du Parlement européen et du Conseil du 23 juillet 2014 (e-IDAS) sur l’identification électronique et les services de confiance pour les transactions électroniques au sein du marché intérieur ;
* Code civil ;
* Décret n° 2017-1416 du 28 septembre 2017 relatif à la signature électronique.

Dans le cas où la signature électronique requiert la fourniture de données personnelles, les dispositions réglementaires applicables sont issues des textes suivants :

* Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016, règlement relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE (règlement général sur la protection des données) ;
* Loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.

Le présent document, « Politique de signature électronique du service en ligne Guichet Entreprises », décrit l’ensemble des règles et des dispositions définissant les exigences auxquelles chacun des acteurs impliqués dans ces échanges dématérialisés se conforme pour la transmission et la réception des flux.

Ce document est destiné :

* aux centres de formalités des entreprises ;
* aux teneurs de registres publics ;
* aux éventuels prestataires participant à ces échanges dématérialisés pour le compte de ces organismes destinataires.

Dans la suite de ce document :

* les organismes destinataires susvisés sont désignés par le terme « destinataires » ;
* les échanges dématérialisés susvisés sont désignés par le terme « dossiers » ;
* l’Institut national de la propriété industrielle est désigné par le terme « INPI ».

## Champ d’application

La signature électronique est requise pour toute formalité liée à la création, aux modifications de situation et à la cessation d’activité d’une entreprise, ou liée à l’accès à une activité réglementée au sens de la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur (« directive services ») et à son exercice, mentionnées à l’article R. 123-1 du Code de commerce.

La signature électronique revêt des caractéristiques différenciées selon la nature des dossiers transmis.

**Lorsque le dossier concerne une formalité de création d’entreprise,** la signature électronique requise répond aux dispositions de la première phrase du second alinéa de l’article 1316-4 du Code civil :

« Lorsqu’elle est électronique, elle consiste en l’usage d’un procédé fiable d’identification garantissant son lien avec l’acte auquel elle s’attache. »

**Lorsque le dossier concerne une formalité de modification de situation ou de cessation d’activité,** la signature électronique répond aux dispositions de l’article 1 du décret n° 2017 1416 du 28 septembre 2017 (référence au règlement « eIDAS » n° 910/2014) :

« La fiabilité d’un procédé de signature électronique est présumée, jusqu’à preuve du contraire, lorsque ce procédé met en œuvre une signature électronique qualifiée. 

Est une signature électronique qualifiée une signature électronique avancée, conforme à l’article 26 du règlement susvisé et créée à l’aide d’un dispositif de création de signature électronique qualifié répondant aux exigences de l’article 29 dudit règlement, qui repose sur un certificat qualifié de signature électronique répondant aux exigences de l’article 28 de ce règlement. »

Article 26 du règlement « eIDAS » n° 910/2014 :

« Une signature électronique avancée satisfait aux exigences suivantes : 

1.	Être liée au signataire de manière univoque ; 
2.	Permettre d’identifier le signataire ; 
3.	Avoir été créée à l’aide de données de création de signature électronique que le signataire peut, avec un niveau de confiance élevé, utiliser sous son contrôle exclusif ; et 
4.	Être liée aux données associées à cette signature de telle sorte que toute modification ultérieure des données soit détectable. »

Pour information, trois niveaux de garantie sont prévus par le règlement « eIDAS » n° 910/2014 :

* Faible : à ce niveau, l’objectif est simplement de réduire le risque d’utilisation abusive ou d’altération d’identité ;
* Substantiel : à ce niveau, l’objectif est de réduire substantiellement le risque d’utilisation abusive ou d’altération d’identité ;
* Élevé : à ce niveau, l’objectif est d’empêcher l’utilisation abusive ou l’altération de l’identité.

## Identification

L’identification de cette politique de signature électronique est signifiée par la présence du certificat de l’INPI utilisé pour la signature électronique du document.

Le numéro de série du certificat a pour valeur :

4B-51-5A-35-00-00-00-00-01-EF.

## Publication du document

La présente politique de signature électronique est publiée suite à son approbation par le responsable sécurité des systèmes d’information (RSSI) de l’INPI.

## Processus de mise à jour

### Circonstances rendant une mise à jour nécessaire

La mise à jour de la présente politique de signature électronique peut avoir pour origines notamment, l’évolution du droit en vigueur (cf. « Objet du Document »), l’apparition de nouvelles menaces et de nouvelles mesures de sécurité, la prise en compte des observations des différents acteurs.

La présente politique de signature électronique est réexaminée au moins tous les deux ans.

### Prise en compte des remarques

Toutes les remarques, ou souhaits d’évolution, sur la présente politique de signature électronique sont à adresser par messagerie électronique à l’adresse suivante : [contact](mailto:guichet-entreprises-support@inpi.fr).

Ces remarques et souhaits d’évolution sont examinés par le RSSI de l’INPI qui engage si nécessaire le processus de mise à jour de la présente politique de signature électronique.

### Information des acteurs

Les informations relatives à la version courante de cette politique et aux versions antérieures sont disponibles dans le tableau des versions situé en tête du présent document.

La publication d’une nouvelle version de la politique de signature consiste à :

1.	mettre en ligne la politique de signature électronique au format HTML ;
2.	archiver la version précédente après apposition de la mention « obsolète » sur chaque page.

## Entrée en vigueur d’une nouvelle version et période de validité

Une nouvelle version de la politique de signature n’entre en vigueur qu’un mois calendaire après sa mise en ligne et reste valide jusqu’à l’entrée en vigueur d’une nouvelle version.

Le délai d’un mois est mis à profit par les destinataires pour prendre en compte dans leurs applications, les changements apportés par la nouvelle politique de signature électronique.

## Acteurs

### Le signataire du dossier

Le signataire du dossier est le déclarant ou son mandataire effectuant la formalité de création, modification de situation ou cessation d’activité.

#### Le rôle du signataire

Le signataire a pour rôle d’apposer sa signature électronique sur son dossier intégrant la formalité et les pièces justificatives afférentes.

Pour apposer une signature électronique sur son dossier, le signataire s’engage à utiliser un outil de signature respectant la présente politique de signature électronique.

#### Les obligations du signataire

Ces obligations sont décrites dans les chapitres ci-dessous.

### Outil de signature utilisé

Le signataire doit contrôler les données qu’il va signer avant d’y apposer sa signature électronique.

### Procédure de signature électronique utilisée

La procédure de signature électronique est fonction du type de formalité (cf. « Champ d’application »).

**Pour ce qui concerne les dossiers portant sur des formalités de création d’activité,** le signataire doit cocher une case en fin de formalité indiquant qu’il déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.

Il est alors fait mention dans l’espace de signature du formulaire (Cerfa) de la conformité de cette signature aux attendus de l’alinéa 3 de l’article A. 123-4 du Code de commerce.

Alinéa 3 de l’article A. 123-4 du Code de commerce :

« 3° En cochant la case informatique prévue à cet effet, le déclarant déclare sur l’honneur l’exactitude des éléments déclarés conformément à la formule suivante : « Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration n° …, faite à…, le…. ». 

**Pour ce qui concerne les dossiers portant sur des formalités de modification de situation ou cessation d’activité,** le signataire doit joindre une copie de sa pièce d’identité déclarée copie conforme à l’original conformément aux dispositions de l’alinéa 2 de l’article A. 123-4 du Code de commerce :

Alinéa 2 de l’article A. 123-4 du Code de commerce :

« 2° Les documents qui la composent ont fait l’objet d’une numérisation. La copie du justificatif d’identité est numérisée après avoir été préalablement revêtue d’une mention manuscrite d’attestation sur l’honneur de conformité à l’original, d’une date et de la signature manuscrite de la personne qui effectue la déclaration. »

Le document à signer électroniquement (formulaire Cerfa et copie de la pièce d’identité) lui est présenté ainsi que la convention de preuve listant les conditions et les conséquences de la signature électronique du document.

Le signataire doit cocher une case indiquant qu’il a pris connaissance de la convention de preuve et l’accepte sans réserve.

Un code de validation de la signature électronique lui est transmis sur son téléphone portable dont il a indiqué le numéro lors de la création de son compte.

La saisie de ce code permet de déclencher le scellement du document (formulaire Cerfa et copie de la pièce d’identité), c’est-à-dire sa signature électronique basée sur un certificat, son hachage et l’horodatage de l’ensemble.

Le document scellé est archivé et transmis au(x) destinataire(s) concerné(s).

Les cas d’erreur les plus fréquents sont les suivants :

**Cas 1 = Code incorrect** 

Cas fonctionnel :

*L’utilisateur a saisi un code incorrect (par exemple des lettres au lieu de chiffres).*

Exemple de message affiché :

*Le code que vous avez renseigné est incorrect. Veuillez vérifier le code qui vous a été transmis par SMS et ressaisissez-le.*

**Cas 2 = Code expiré**

Cas fonctionnel :

*L’utilisateur a saisi trop tardivement le code reçu par SMS (la durée de vie d’un code OTP = One Time Password est de 20 minutes).*

Exemple de message affiché :

*Le délai de validité de votre code a expiré. Vous devez renouveler votre demande de signature électronique.*

**Cas 3 = Code bloqué**

Cas fonctionnel :

*L’utilisateur a effectué N saisies infructueuses de codes OTP. Afin d’éviter des tentatives de piratage, le numéro de téléphone de l’utilisateur est identifié comme bloqué dans le système.*

Exemple de message affiché :

*Suite à de nombreuses erreurs de saisies de votre part, votre numéro de téléphone +33600000000 ne vous permet plus de signer électroniquement votre démarche. Nous vous invitons à renseigner un nouveau numéro de téléphone dans votre compte utilisateur ou à contacter l’INPI pour débloquer votre numéro de téléphone actuel.*

#### Protection et usage du dossier signé

Ce chapitre ne traite que des dossiers portant sur des formalités de modification de situation ou cessation d’activité, les dossiers de création.

Le document signé électroniquement est conservé dans un coffre-fort électronique accompagné d’un fichier lisible portant toutes les traces de son scellement à des fins d’audit.

Le coffre-fort électronique n’est accessible que par son administrateur et le RSSI de l’INPI.

L’accès au coffre-fort électronique n’est possible qu’à l’aide d’un certificat nominatif et toute action sur celui-ci est tracée.

Pour le RSSI de l’INPI, seuls les accès à des fins d’audit sur demande justifiée (par exemple requête judiciaire) ne sont autorisés.

Le document signée est transmis à ou aux organisme(s) destinataire(s) en charge d’instruire le dossier associé.

Les modalités de protection du dossier transmis signé électroniquement sont à la charge du destinataire et doivent être conformes aux dispositions du Règlement général de sécurité et du Règlement général de protection des données.

Le RSSI de l’INPI s’assure du respect de ces dispositions.

#### Archivage et suppression du dossier signé

Conformément à l’article R. 123-18 du Code de commerce, le Guichet Entreprises est dessaisi du dossier après transmission aux organismes compétents. Dans ce cadre, les dossiers finalisés seront supprimés après un délai de trois mois. Après traitement par les organismes compétents, les informations concernant les sociétés civiles et commerciales seront publiées au Registre national du commerce et des sociétés. Elles seront [mises à disposition du public](https://www.inpi.fr/fr/services-et-prestations-domaine/bases-de-donnees) en open data et feront l’objet d'une [diffusion à des fins de réutilisation](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041593491/), dans le cadre de licences de réutilisation homologuées.

Dès transmission du ou des dossier(s), les déclarants peuvent se rapprocher des autorités compétentes pour accéder à leur(s) dossiers ou le(s) consulter sur le Guichet Entreprises pendant les trois mois suivant la transmission.

### Les fournisseurs de solutions de signature électronique

La solution doit incorporer, dans la structure des données de signature, les attendus de la présente politique de signature électronique.

### L’INPI

Le RSSI de l’INPI s’assure du respect des dispositions de la présente politique de signature électronique pour le service Guichet Entreprises mis à disposition des usagers.

Il vérifie que les systèmes d’information sources et destinataires des flux ne présentent pas de risques quant à l’intégrité et la confidentialité des dossiers stockés et transmis.

### Les destinataires

Les destinataires contrôlent l’intégrité de la signature électronique du dossier et s’assure que les modalités de conservation et de suppression des dossiers reçus sont conformes aux dispositions réglementaires.

### Données de vérification

Pour effectuer les vérifications, l’INPI utilise les informations de scellement du dossier signé électroniquement (cf. « Protection et usage du dossier signé »).

### Protection des moyens

L’INPI s’assure de la mise en œuvre des moyens nécessaires à la protection des équipements fournissant les services de validation.

Les mesures prises concernent à la fois :

* la protection des accès physiques et logiques aux équipements aux seules personnes habilitées ;
* la disponibilité du service ;
* la surveillance et le suivi du service.

### Assistance aux signataires

L’assistance à l’utilisation de la procédure de signature électronique est assurée par [INPI Direct](../contacter_assistance_utilisateur.md).

## Signature électronique et validation

### Données signées

Les données signées sont composées du formulaire Cerfa et de la copie de la pièce d’identité sous forme d’un seul fichier PDF avec une zone de signature électronique modifiable.

L’ensemble du fichier PDF est signé et de fait aucune modification de celui-ci n’est possible après son scellement.

### Caractéristiques des signatures

La signature respecte les attendus de la spécification « XML Signature Syntax and Processing (XMLDsig) » élaborée par W3C (World Wide Web Consortium : [https://www.w3.org/](https://www.w3.org/)) ainsi que les extensions de format de signatures spécifiées dans le standard européen XML Advanced Electronic Signature (XADES) de l’ETSI ([https://www.etsi.org](https://www.etsi.org/)).

La signature électronique déployée implémente un niveau d’identification « substantiel » (norme ISO/IEC 29115:2013), soit conformément au règlement eIDAS une signature électronique avancée.

#### Type de signature

La signature électronique est de type enveloppée.

#### Norme de signature

La signature électronique doit respecter la norme [XMLDsig, révision 1.1 de février 2002](https://www.w3.org/TR/2002/REC-xmldsig-core-20020212).

La signature électronique doit respecter la norme XAdES-EPES (Explicit Policy based Electronic Signature), [ETSI TS 101 903 version v1.3.2](http://uri.etsi.org/01903/v1.3.2).

Conformément à la norme XadES, les propriétés signées (SignedProperties/SignedSignatureProperties) doivent contenir les éléments suivants :

* le certificat du signataire (SigningCertificate) ;
* la date et l’heure de signature (SigningTime) au format UTC.

### Algorithmes utilisables pour la signature

#### Algorithme de condensation

Le calcul du condensé par itération de la fonction de compression sur la suite des blocs obtenus en découpant le message (schéma itératif de Merkle-Damgård).

Cet algorithme est accessible au lien suivant : [Merkle-Damgård](https://fr.wikipedia.org/wiki/Construction_de_Merkle-Damg%C3%A5rd)

#### Algorithme de signature

L’algorithme de signature est basé sur RSA/SHA 256.

#### Algorithme de canonicalisation

L’algorithme de canonicalisation est [c14N](https://www.w3.org/TR/xml-exc-c14n/).

### Conditions pour déclarer valide le fichier signé

Les conditions pour déclarer valide le fichier signé sont les suivantes :

* le signataire a rempli ses obligations : 
  * téléchargement d’une copie de sa pièce d’identité avec mention de copie conforme à l’original et acceptation de la convention de preuve,
  * saisie du code envoyé par SMS (méthode « One Time Password ») ;
* l’INPI a rempli ses obligations : 
  * mise à disposition d’un certificat généré par une AC certifiant son origine (fournisseur du service de signature électronique) et dont les caractéristiques sont les suivantes : Clef publique = RSA 2048 bits, algorithme de signature = RSA SHA 256 ;
* le fournisseur du service de signature électronique a rempli ses obligations : 
  * signature et scellement du fichier PDF selon les caractéristiques décrites dans les chapitres précédents.

La vérification de validité de la signature peut être effectuée via Adobe Acrobat.

## Dispositions juridiques

### Données nominatives

Le porteur dispose d’un droit d’accès et de rectification des données le concernant qu’il peut exercer par courriel à [cette adresse](contact@guichet-entreprises.fr). Le traitement de ses données nominatives est nécessaire au respect d’une obligation légale à laquelle le responsable de traitement est soumis. Dès lors, le droit d’opposition à ce traitement n’est pas possible, en application des dispositions de l’article 6.1 c) du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016, et de l’article 56 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés dans sa version consolidée.

### Droit applicable – Résolution des litiges

Les présentes dispositions sont soumises au droit français.

Tout litige relatif à la validité, l’interprétation ou l’exécution des présentes dispositions sera soumis à la juridiction du tribunal administratif de Paris.

Dernière mise à jour : Septembre 2020