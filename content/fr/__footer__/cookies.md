﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Utilisation des cookies sur guichet-entreprises.fr" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="None" -->
<!-- var(key)="fr-__footer__-cookies" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="fr" -->

Utilisation des cookies sur guichet-entreprises.fr
================================================

## Que sont les cookies et comment les utilise l'INPI ?

L'INPI peut utiliser des cookies quand un utilisateur navigue sur ses sites. Les cookies sont des fichiers envoyés au navigateur par l'intermédiaire d'un serveur web dans le but d'enregistrer les activités de l'utilisateur durant son temps de navigation. L'emploi de cookies permet de reconnaître le navigateur web utilisé par l'utilisateur afin de faciliter sa navigation. Les cookies sont également utilisés pour mesurer l'audience du site et produire des statistiques de consultation.

Les cookies utilisés par l'INPI ne fournissent pas de références permettant de déduire des données personnelles des utilisateurs ou des informations personnelles permettant d'identifier un utilisateur en particulier. Ils ont un caractère temporaire, ayant le seul but de rendre plus efficace la transmission ultérieure. Aucun cookie utilisé sur le site n'aura une période de vigueur de plus de deux ans.

Les utilisateurs ont la possibilité de configurer leur navigateur pour être prévenus de la réception de cookies et pour refuser l'installation. En interdisant les cookies ou en les désactivant, l'utilisateur risque de ne pas pouvoir accéder à certaines fonctionnalités du site.

Dans le cas d'un refus ou d'une désactivation des cookies, il conviendra de redémarrer la session.

## Quel types de cookies sont employés par guichet-entreprises.fr ?

### Cookies techniques

Ils permettent à l'utilisateur de naviguer sur le site et d'utiliser certaines de ses fonctionnalités.

### Cookies d'analyse

L'INPI utilise des cookies d'analyse d'audience pour quantifier le nombre de visiteurs. Ces cookies permettent de mesurer et d'analyser la façon dont les utilisateurs naviguent sur le site.