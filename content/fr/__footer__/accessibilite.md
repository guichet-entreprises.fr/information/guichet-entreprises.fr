<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Qui sommes-nous ?" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="accessibilité" -->
<!-- var(translation)="None" -->
<!-- var(key)="fr-__footer__-accessibilite" -->
<!-- var(last-update)="2021-09-24" -->
<!-- var(lang)="fr" -->

Accessibilité : non conforme
========================

Le Ministère de l'Économie, des Finances et de la Relance s’engage à rendre son service accessible, conformément à l’article 47 de la loi n° 2005-102 du 11 février 2005.

Cette déclaration d’accessibilité s’applique à Guichet Entreprises.

## État de conformité

Guichet Entreprises est **non conforme avec le RGAA 4.1**. Le site n'a encore pas été audité : contenus non accessibles.

## Établissement de cette déclaration d'accessibilité

Cette déclaration a été établie le **20 septembre 2021**.

## Amélioration et contact

Si vous n’arrivez pas à accéder à un contenu ou à un service, vous pouvez contacter le responsable de Guichet Entreprises pour être orienté vers une alternative accessible ou obtenir le contenu sous une autre forme.

- via le [formulaire de contact](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1)
- par courrier à l'adresse suivante :

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;Pôle Guichet Entreprises<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p>

## Voie de recours

Cette procédure est à utiliser dans le cas suivant : vous avez signalé au responsable du site internet un défaut d’accessibilité qui vous empêche d’accéder à un contenu ou à un des services du portail et vous n’avez pas obtenu de réponse satisfaisante.

Vous pouvez :

- écrire un message au [Défenseur des droits](https://formulaire.defenseurdesdroits.fr/code/afficher.php?ETAPE=accueil_2016)
- contacter [le délégué du Défenseur des droits dans votre région](https://www.defenseurdesdroits.fr/saisir/delegues)
- envoyer un courrier par la poste (gratuit, ne pas mettre de timbre) :

<p>&nbsp;&nbsp;&nbsp;Défenseur des droits<br>
&nbsp;&nbsp;&nbsp;Libre réponse<br>
&nbsp;&nbsp;&nbsp;71120 75342 Paris CEDEX 07</p>

__________________________________________________________________

Cette déclaration d'accessibilité a été créé le 20 septembre 2021 grâce au [Générateur de Déclaration d'Accessibilité de BetaGouv](https://betagouv.github.io/a11y-generateur-declaration/#create).