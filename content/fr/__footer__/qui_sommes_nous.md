﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Qui sommes-nous ?" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="None" -->
<!-- var(key)="fr-__footer__-qui_sommes_nous" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="fr" -->

Qui sommes-nous ?
================

Dernière mise à jour : <!-- begin-var(last-update) -->2020-09-01<!-- end-var -->

**Guichet-entreprises.fr est le site national de l'Institut national de la propriété industrielle (INPI) permettant aux créateurs et aux chefs d'entreprises de réaliser leurs formalités.**

**Création, modification, cessation d'activité, etc. : plus de 2 000 formalités peuvent y être réalisées.**

## Guichet-entreprises.fr, le site de l’INPI qui simplifie la création d'entreprise

Le site guichet-entreprises.fr est conçu et développé par le pôle Guichet Entreprises de l'INPI, en application du décret n° 2020-946 du 30 juillet 2020.

Le pôle Guichet Entreprises gère les sites guichet-entreprises.fr et [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/) qui constituent le guichet unique électronique défini par les directives européennes [2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32006L0123&from=EN) et [2005/36/CE](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2005:255:0022:0142:fr:PDF).

Sur guichet-entreprises.fr, les créateurs et les dirigeants d'entreprise réalisent, en ligne et en toute sécurité, les formalités administratives liées à la vie de leur entreprise : création, modification (changement d'adresse, de statut juridique, etc.) ou cessation d'activité.

Pour la création d'entreprise, le site guichet-entreprises.fr enregistre les dossiers des déclarants et les transmet pour traitement aux centres de formalités des entreprises (CFE) concernés des :

* chambres de commerce et d'industrie (CCI) – [cci.fr](https://www.cci.fr/)
* chambres d'agriculture – [chambres-agriculture.fr](https://chambres-agriculture.fr/)
* chambres de métiers et de l'artisanat (CMA) – [cma-france.fr](https://cma-france.fr/)
* Urssaf – [urssaf.fr](https://www.urssaf.fr/portail/home.html)
* greffiers des tribunaux de commerce – [cngtc.fr](https://www.cngtc.fr/fr/)

## Guichet-entreprises.fr, un accès aux professions réglementées

La réglementation française conditionne l'exercice de certaines activités (restaurateur, agent immobilier, expert-comptable, etc.) à l'obtention d'un agrément ou d'une autorisation. 

Guichet-entreprises.fr propose des fiches d'information pour faire le point sur la législation concernant ces activités.

## Guichet-entreprises.fr, un site européen

Guichet-entreprises.fr s'adresse également aux résidents de l'Union européenne ou de l'Espace économique européen qui souhaitent s'implanter en France.

Il les informe sur les possibilités de s'implanter durablement (libre établissement) ou d'exercer temporairement (libre prestation de services) en France, et leur permet d'effectuer toutes leurs formalités en ligne.

## Guichet-qualifications.fr, un site pour faire reconnaître ses qualifications professionnelles

Le site [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/) encourage la mobilité professionnelle en donnant au citoyen l'information la plus complète possible sur l'accès et l'exercice des professions réglementées en France.

Les sites guichet-entreprises.fr et [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/) présentent toutes les garanties en matière de sécurité (homologation de l'ANSII).

## Guichet-entreprises.fr en chiffres

**Guichet-entreprises.fr c'est :**

* **+ de 2 000 formalités**
* **+ de 100 fiches d'information sur les activités réglementées**
* **+ de 250 fiches d'information sur les professions réglementées**