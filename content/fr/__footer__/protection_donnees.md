﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Politique de protection des données personnelles" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="None" -->
<!-- var(key)="fr-__footer__-protection_donnees" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="fr" -->

Politique de protection des données personnelles
===============================================

Dernière mise à jour : <!-- begin-var(last-update) -->2020-09-01<!-- end-var -->

<table class="table table-striped table-bordered">
<thead style="text-align: center;border-collapse: collapse">
<tr>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Version</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Date</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Objet</th>
</tr>
</thead>
<tbody style="text-align: center;border-collapse: collapse">
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V1</td>
<td style="border: 1px solid #AAA;padding: 4px">25/05/2018</td>
<td style="border: 1px solid #AAA;padding: 4px">Version initiale</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V2</td>
<td style="border: 1px solid #AAA;padding: 4px">01/09/2018</td>
<td style="border: 1px solid #AAA;padding: 4px">Mise à jour des informations concernant le délégué à la protection des données</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V3</td>
<td style="border: 1px solid #AAA;padding: 4px">05/08/2019</td>
<td style="border: 1px solid #AAA;padding: 4px">Mise à jour des références des Cerfas</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V4</td>
<td style="border: 1px solid #AAA;padding: 4px">01/09/2020</td>
<td style="border: 1px solid #AAA;padding: 4px">Mise à jour suite à l’entrée en vigueur du décret n° 2020-946 du 30 juillet 2020 désignant l’Institut national de la propriété industrielle en tant qu’organisme unique mentionné au neuvième alinéa de l’article 1er de la loi n° 2019-486 du 22 mai 2019 relative à la croissance et la transformation des entreprises et confiant à cet institut la gestion des services informatiques mentionnés aux articles R. 123-21 et R. 123-30-9 du Code de commerce</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V5</td>
<td style="border: 1px solid #AAA;padding: 4px">03/06/2021</td>
<td style="border: 1px solid #AAA;padding: 4px">Mise à jour concernant la durée de conservation des données personnelles et le traitement des dossiers</td>
</tr>
</tbody>
</table>

La politique de protection des données personnelles (ci-après la « politique ») vous informe sur la manière dont vos données sont collectées et traitées par le service Guichet Entreprises de l’INPI, sur les mesures de sécurité mises en œuvre pour en garantir l’intégrité et la confidentialité et sur les droits dont vous disposez pour en contrôler l’usage.

Cette politique vient compléter les conditions générales d’utilisation (CGU) ainsi que, le cas échéant, les mentions légales. À ce titre, la présente politique est réputée être intégrée auxdits documents.

Dans le cadre du développement de nos services et de la mise en œuvre de nouvelles normes réglementaires, nous pouvons être amenés à modifier la présente politique. Nous vous invitons donc à en prendre connaissance régulièrement.

## Qui sommes-nous ?

L’INPI met en œuvre un service en ligne (ci-après le « service Guichet Entreprises ») permettant à tout entrepreneur d’accomplir à distance les formalités et procédures nécessaires à la création, aux modifications de la situation et à la cessation d’activité d’une entreprise, soit en transmettant un dossier unique tel que défini à l’article R. 123-23 du Code de commerce dès lors qu’il respecte les dispositions de l’article R. 123-24 du Code de commerce, soit en préparant un tel dossier de manière interactive et en le transmettant.

Le service Guichet Entreprises s’inscrit dans le cadre juridique :

- de la directive [2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32006L0123&from=FR) du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur ; 
- du [règlement n° 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32014R0910&from=FR) du Parlement européen et du Conseil du 23 juillet 2014 (e-IDAS) sur l’identification électronique et les services de confiance pour les transactions électroniques au sein du marché intérieur ;
- du [règlement n° 2016/679](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679&from=FR) du Parlement européen et du Conseil du 27 avril 2016 relatif au traitement des données à caractère personnel ;
- de la [loi 2019-486](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038496102&categorieLien=id) du 22 mai 2019 relative à la croissance et la transformation des entreprises, notamment son article 1er ;
- du [décret n° 2020-946](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000042182948&categorieLien=id) du 30 juillet 2020 désignant l’Institut national de la propriété industrielle en tant qu’organisme unique mentionné au neuvième alinéa de l’article 1er de la loi n° 2019-486 du 22 mai 2019 relative à la croissance et la transformation des entreprises et confiant à cet institut la gestion des services informatiques mentionnés aux articles R. 123-21 et R. 123-30-9 du Code de commerce ;
- de l’[ordonnance du 8 décembre 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives et le décret n° 2010-112 du 2 février 2010 pris pour l’application des articles 9, 10 et 12 de cette ordonnance dit référentiel général de sécurité (RGS) ;
- du dispositif de la [loi n° 78-17 du 6 janvier 1978](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) modifiée relative à l’informatique, aux fichiers et aux libertés ; 
- de l’[article 441-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) du Code Pénal ; 
- des [articles R. 123-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006255835&cidTexte=LEGITEXT000005634379&dateTexte=20070327) à [R. 123-30](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032944426&cidTexte=LEGITEXT000005634379&dateTexte=20170101) du Code de commerce.

## Quelle organisation mettons-nous en œuvre pour protéger vos données ?

Le respect et la mise en œuvre des dispositions applicables en matière de protection des données personnelles sont suivis par le délégué à la protection des données de l’INPI.

## Comment nous contacter ? 

Pour toute question concernant l’utilisation de vos données personnelles par l'INPI, nous vous invitons à lire la présente politique et à contacter notre délégué à la protection des données en utilisant les coordonnées suivantes :

Par courrier à l’attention de :

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;À l’attention du délégué à la protection des données personnelles<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001 92677 Courbevoie Cedex</p>

Par courriel, en précisant l’objet « Protection des données » à [cette adresse](mailto:guichet-entreprises-support@inpi.fr).

## Qu’est-ce qu’une donnée personnelle ?

Les données personnelles sont des informations qui permettent de vous identifier, que ce soit directement ou indirectement. Il peut notamment s’agir de votre nom, votre prénom, votre adresse, votre numéro de téléphone ou votre adresse électronique, mais également l’adresse IP de votre ordinateur, par exemple, ou encore les informations liées à l’utilisation du service Guichet Entreprises.

## Quand collectons-nous des données personnelles ?

L'INPI collecte des données personnelles lorsque vous :

- créez votre espace personnel ; 
- complétez une formalité via le service Guichet Entreprises.

## Quelles sont les données personnelles que nous traitons ?

Les données personnelles que nous traitons ne concernent que les formalités d’entreprises conformément aux dispositions réglementaires des articles R. 123-1 à R. 123-27 du Code de commerce.

Les données recueillies permettent de constituer le « dossier unique » tel qu’attendu au titre de l’article R. 123-7 du Code de commerce.

Les données recueillies permettent notamment de compléter les documents Cerfa associés aux formalités et procédures nécessaires à la création, aux modifications de la situation et à la cessation d’activité d’une entreprise.

Le tableau ci-dessous liste les documents Cerfa concernés.

<table class="table table-striped table-bordered">
<thead>
<tr>
<th style="text-align:center;border: 1px solid #AAA;padding: 4px">Formalité</th>
<th style="text-align:center;border: 1px solid #AAA;padding: 4px">Forme juridique</th>
<th style="text-align:center;border: 1px solid #AAA;padding: 4px">Cerfa : Principal</th>
<th style="text-align:center;border: 1px solid #AAA;padding: 4px">Cerfa : Intercalaires possibles</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Création</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Micro-entrepreneur</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13821<br>(P0 PL micro-entrepreneur)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14214 (PEIRL Micro-entrepreneur)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">15253<br>(P0 CMB micro-entrepreneur)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14215 (PEIRL CMB)<br>11771 (P0’)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13821 (AC0)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14218 (PEIRL PL / AC)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Entreprise individuelle<br>(hors micro-entrepreneur)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11768 (P0 PL)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14218 (PEIRL PL / AC)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11676<br>(P0 CMB sauf micro-entrepreneur)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14215 (PEIRL CMB)<br>11771 (P0’)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11922 (P0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11771 (P0’)<br>14216 (PEIRL agricole)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13821 (AC0)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14218 (PEIRL PL / AC)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SARL</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Hors activité agricole<br>11680 (M0 SARL, SELARL)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11772 (M0 ’ SARL, SELARL)<br>11686 (TNS)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Activité agricole<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>14116 (Agri-com)<br>11925 (NSM agricole)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SELARL</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11680 (M0 SARL, SELARL)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11772 (M0 ’ SARL, SELARL)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SAS</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Hors activité agricole<br>13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Activité agricole<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>14116 (Agri-com)<br>11925 (NSM agricole)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Hors activité agricole<br>13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Activité agricole<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>14116 (Agri-com)<br>11925 (NSM agricole)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SNC</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Hors activité agricole<br>13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Activité agricole<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>14116 (Agri-com)<br>11925 (NSM agricole)&lt;br&gt;11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCS</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SELAFA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SELAS</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SELCA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Société civile</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13958 (M0 société civile)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14067 (M0’ société civile)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCM</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13958 (M0 société civile)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14067 (M0’ société civile)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCP</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13958 (M0 société civile)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14067 (M0’ société civile)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">GAEC</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Activité agricole<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>11925 (NSM agricole)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCEA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Activité agricole<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>11925 (NSM agricole)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">EARL</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Activité agricole<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>Cerfa11925 (NSM agricole)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">GFA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Activité agricole<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>Cerfa11925 (NSM agricole)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Régularisation</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Micro-entrepreneur</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">15260<br>(R CMB micro-entrepreneur)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14077 (JQPA)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Modification</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Micro-entrepreneur</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13905 (P2P4)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14077 (JQPA)<br>14214 (PEIRL Micro-entrepreneur)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11678 (P2 CMB)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14077 (JQPA)<br>14215 (PEIRL CMB)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Radiation</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Micro-entrepreneur</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13905 (P2P4)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14214 (PEIRL Micro-entrepreneur)<br>11679 (P4 CMB)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Déclaration des bénéficiaires effectifs</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Sociétés (y compris sociétés civiles et sociétés commerciales étrangères)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16062 (M’BE)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16062 (M’BE) intercalaire d’un formulaire M0 – M2 – M3)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Placements collectifs</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16063 (M’BE)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16063 (M’BE) intercalaire d’un formulaire M0 – M2 – M3)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">GIE ou Association</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16064 (M’BE)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16064 (M’BE) intercalaire d’un formulaire M0 – M2 –M3)</td>
</tr>
</tbody>
</table>

La liste suivante résume les données à caractère personnel que nous traitons.

Pour la gestion de l’accès à l’espace personnel du service, l’usager partage les informations suivantes :

- l’identifiant de connexion choisi par l’usager ;
- le mot de passe choisi par l’usager ;
- l’adresse électronique de l’usager ;
- le numéro de téléphone portable de l’usager ;
- l’adresse IP de connexion.

Pour l’utilisation de l’espace de stockage personnel, l’usager partage les informations suivantes :

1° Pour une personne physique :

- nom, nom de naissance ;
- prénom, prénom usuel ;
- date et lieu de naissance ;
- nationalité ;
- adresse, adresse de destination des courriers ;
- situation maritale, régime matrimonial ;
- nom du conjoint, nom de naissance du conjoint ;
- prénom du conjoint, prénom usuel du conjoint ;
- adresse du conjoint ;
- date et lieu de naissance du conjoint ;
- nationalité du conjoint ;
- situation de forain, situation d’exercice ambulant ;
- situation de commerçant au sein d’un état membre de l’Union européenne autre que la France ;
- présence d’une déclaration d’insaisissabilité sur la résidence principale, lieu de la publication au bureau de la conservation des hypothèques ;
- noms des précédents exploitants, noms de naissance des précédents exploitants ;
- prénoms des précédents exploitants, prénoms usuels des précédents exploitants ;
- nom du loueur de fonds, nom de naissance du loueur de fonds ;
- prénom du loueur de fonds, prénom usuel du loueur de fonds ;
- adresse du loueur de fonds ;
- numéro de sécurité sociale ;
- régime d’assurance maladie ;
- exercice d’une activité non salariée antérieure ;
- numéro de sécurité sociale du conjoint ou pacsé collaborateur ;
- nom du fondé de pouvoir ;
- nom de naissance du fondé de pouvoir ;
- prénom du fondé de pouvoir ;
- prénom usuel du fondé de pouvoir ;
- adresse du fondé de pouvoir ;
- lieu de naissance du fondé de pouvoir ;
- date de naissance du fondé de pouvoir ;
- nationalité du fondé de pouvoir ;
- nom de l’ayant droit ;
- nom de naissance de l’ayant droit ;
- prénom de l’ayant droit ;
- prénom usuel de l’ayant droit ;
- numéro de sécurité sociale de l’ayant droit ;
- lieu de naissance de l’ayant droit ;
- date de naissance de l’ayant droit ;
- nationalité de l’ayant droit ;
- lien de parenté ;
- nom du signataire ;
- nom de naissance du signataire ;
- prénom du signataire ;
- prénom usuel du signataire ;
- adresse du signataire.

2° Pour une personne morale :

- nom du loueur de fonds ;
- nom de naissance du loueur de fonds ;
- prénom du loueur de fonds ;
- prénom usuel du loueur de fonds ;
- adresse du loueur de fonds ;
- nationalité du dirigeant d’une société associée ;
- nom du dirigeant d’une société associée ;
- nom de naissance du dirigeant d’une société associée ;
- prénom du dirigeant d’une société associée ;
- prénom usuel du dirigeant d’une société associée ;
- adresse du dirigeant d’une société associée ;
- lieu de naissance du dirigeant d’une société associée ;
- date de naissance du dirigeant d’une société associée ;
- nom du représentant de la personne morale ;
- nom de naissance du représentant de la personne morale ;
- prénom et prénom usuel du représentant de la personne morale ;
- adresse du représentant de la personne morale ;
- date et lieu de naissance du représentant de la personne morale ;
- nationalité du représentant de la personne morale ;
- nom du conjoint ;
- nom de naissance du conjoint ;
- prénom du conjoint ;
- prénom usuel du conjoint ;
- adresse du conjoint ;
- lieu de naissance du conjoint ;
- date de naissance du conjoint ;
- nationalité du conjoint ;
- nom d’une autre personne pouvant engager l’établissement ;
- nom de naissance d’une autre personne pouvant engager l’établissement ;
- prénom d’une autre personne pouvant engager l’établissement ;
- prénom usuel d’une autre personne pouvant engager l’établissement ;
- adresse d’une autre personne pouvant engager l’établissement ;
- lieu de naissance d’une autre personne pouvant engager l’établissement ;
- date de naissance d’une autre personne pouvant engager l’établissement ;
- nationalité d’une autre personne pouvant engager l’établissement ;
- nom du signataire ;
- nom de naissance du signataire ;
- prénom du signataire ;
- prénom usuel du signataire ;
- adresse du signataire ;
- numéro de sécurité sociale du dirigeant ;
- régime d’assurance maladie du dirigeant ;
- numéro de sécurité sociale du conjoint ou pacsé collaborateur ;
- nom de l’ayant droit ;
- nom de naissance de l’ayant droit ;
- prénom de l’ayant droit ;
- prénom usuel de l’ayant droit ;
- numéro de sécurité sociale de l’ayant droit ;
- lieu de naissance de l’ayant droit ;
- date de naissance de l’ayant droit ;
- nationalité de l’ayant droit ;
- lien de parenté.

## Quelles sont les pièces justificatives personnelles que nous traitons ?

Les pièces justificatives personnelles que nous traitons ne concernent que les formalités d’entreprises conformément aux dispositions réglementaires des articles R. 123-1 à R. 123-27 du Code de commerce.

Les pièces justificatives permettent de constituer le « dossier unique » tel qu’attendu au titre de l’article R. 123-7 du Code de commerce.

La liste des pièces justificatives fait l’objet d’une homologation par l’autorité désignée à l’[article 3 du décret n° 98-1083](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000208797&idArticle=LEGIARTI000006544269&dateTexte=&categorieLien=cid) du 2 décembre 1998 relatif aux simplifications administratives.

Parmi les pièces justificatives personnelles figurent les pièces justifiant l’identité ou le domicile. Ces pièces justificatives sont conservées selon les mêmes modalités que les données personnelles (cf. infra).

Leur stockage est assuré sur des espaces inaccessibles depuis l’extérieur (Internet) et leur téléchargement ou transfert est sécurisé par chiffrement conformément aux dispositions indiquées dans le chapitre « Comment sont protégées vos données personnelles ? » 

## Comment gérons-nous les cookies ?

La gestion des cookies est décrite [ici](cookies.md).

Tous les cookies gérés utilisent le mode *secure cookies* (cookies sécurisés).

Les cookies sécurisés sont un type de cookie transmis exclusivement via des connexions chiffrées (https).

Les cookies techniques contiennent les données personnelles suivantes : nom, prénom, courriel, téléphone.

Les cookies d’analyse contiennent l’adresse IP de connexion.

## Quels sont vos droits pour la protection de vos données personnelles ?

Vous disposez d’un droit d’accès et de rectification de vos données personnelles. En revanche, le traitement de celles-ci est nécessaire au respect d’une obligation légale à laquelle le responsable de traitement est soumis. Dès lors, le droit d’opposition à ce traitement n’est pas possible, en application des dispositions de l’article 6.1 c) du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016, et de l’article 56 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés dans sa version consolidée.

Si vous estimez que l’utilisation de vos données personnelles contrevient aux règles de protection des données personnelles, vous avez la possibilité d’adresser une réclamation à la CNIL.

Vous disposez également du droit de définir des directives relatives au traitement de vos données à caractère personnel en cas de décès. Les directives particulières peuvent être enregistrées auprès du responsable du traitement. Les directives générales peuvent être enregistrées auprès d’un tiers de confiance numérique certifié par la CNIL. Vous avez la possibilité de modifier ou supprimer ces directives à tout moment.

Le cas échéant, vous avez également la possibilité de nous demander de vous communiquer les données personnelles que vous nous avez fournies dans un format lisible.

Vous pouvez adresser vos différentes demandes :

* par courrier à l’attention de :

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;À l’attention du délégué à la protection des données personnelles<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001 92677 Courbevoie Cedex</p>

* par courriel, en précisant l’objet « Protection des données » à [cette adresse](mailto:guichet-entreprises-support@inpi.fr).

## Comment sont protégées vos données personnelles ?

L'INPI met en œuvre l’ensemble des pratiques standard de l’industrie et des procédures de sécurité afin de prévenir toute violation de vos données personnelles.

Toutes les informations communiquées sont stockées sur des serveurs sécurisés hébergés par nos prestataires techniques.

Lorsque la transmission de données est nécessaire et autorisée, l'INPI s’assure que ces tiers présentent les garanties suffisantes pour assurer un niveau de protection approprié. Les échanges sont chiffrés et les serveurs authentifiés par reconnaissance mutuelle.

Conformément à la règlementation sur la protection des données, en cas de violation, l'INPI s’engage à communiquer cette violation à l’autorité de contrôle compétente, et lorsque cela est exigé, aux personnes concernées.

## Espace Personnel

Toutes les données transmises sur votre espace personnel sont chiffrées et leur accès est sécurisé par l’utilisation d’un mot de passe personnel. Vous êtes tenu de garder ce mot de passe confidentiel et de ne pas le communiquer.

## Durée de conservation des données personnelles et traitement des dossiers

Les données à caractère personnel recueillies sont nécessaires au traitement de la demande.

Ces informations sont recueillies par l’INPI conformément au [décret 2020-946 du 30 juillet 2020](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042182948?r=EJdJkGOj3Y) et aux dispositions réglementaires des articles R. 123-1 à R. 123-27 du Code de commerce. Les données recueillies permettent de constituer le « dossier unique » tel qu’attendu au titre de l’article R. 123-7 du Code de commerce. Ces données à caractère personnel collectées pour la création d’un compte, lors de la réalisation d’une formalité sur le site guichet-entreprises.fr sont conservées un an (article R. 123-18 et R. 123-27 du Code de commerce).

Conformément à l’article R. 123-18 du Code de commerce, le Guichet Entreprises est dessaisi du dossier après transmission aux organismes compétents. Dans ce cadre, les dossiers finalisés seront supprimés après un délai de trois mois. Après traitement par les organismes compétents, les informations concernant les sociétés civiles et commerciales seront publiées au Registre national du commerce et des sociétés. Elles seront [mises à disposition du public](https://www.inpi.fr/fr/services-et-prestations-domaine/bases-de-donnees) en open data et feront l’objet d'une [diffusion à des fins de réutilisation](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041593491/), dans le cadre de licences de réutilisation homologuées.

Dès transmission du ou des dossier(s) les déclarants peuvent se rapprocher des autorités compétentes pour accéder à leur(s) dossier(s) ou le(s) consulter sur le Guichet Entreprises pendant les trois mois suivant la transmission.

L'utilisateur peut modifier ou supprimer en ligne son dossier tant qu’il n’est pas finalisé. La suppression d’un dossier finalisé, avant sa suppression automatique au bout de trois mois, doit en revanche être demandée à l’INPI via le [formulaire de contact](https://www.inpi.fr/fr/contactez-nous?about=299&under_about=883-702-b1). La modification d’un dossier finalisé fait l’objet d’une nouvelle formalité.

Tout dossier démarré et non finalisé sera automatiquement supprimé de la plateforme dans un délai de 3 mois. 

Pour toute question relative à la protection des données personnelles, vous pouvez [contacter le délégué à la protection des données personnelles de l'INPI](https://www.inpi.fr/fr/contactez-nous?about=299&under_about=883-702-b1), en justifiant de votre identité. Vous disposez d’un moyen de recours auprès de la CNIL.

## Concernant les données de cartes bancaires 

L'INPI ne stocke ni ne conserve les données bancaires des usagers soumis à paiement lors de leur formalité. Notre prestataire gère pour le compte de l'INPI les données de transactions conformément aux règles de sécurité les plus strictes applicables dans le secteur du paiement en ligne via l’utilisation de procédés de chiffrement.

## A qui sont transmises vos données ?

Les données recueillies permettent de constituer le « dossier unique » tel qu’attendu au titre de l’article R.123-7 du Code de commerce.

En vertu de l’article R.123-3 du Code de commerce, les données recueillies sont transmises aux fins de traitement aux organismes suivants :

« 1° Sous réserve des dispositions des 2° et 3°, les chambres de commerce et d’industrie territoriales créent et gèrent les centres de formalités des entreprises compétents pour :

1. les commerçants ;
2. les sociétés commerciales.

2° Les chambres de métiers et de l’artisanat de région créent et gèrent les centres compétents pour les personnes physiques et les sociétés assujetties à l’immatriculation au répertoire des métiers 

3° Les greffes des tribunaux de commerce ou des tribunaux de grande instance statuant commercialement créent et gèrent les centres compétents pour :

1. les sociétés civiles et autres que commerciales ;
2. les sociétés d’exercice libéral ;
3. les personnes morales assujetties à l’immatriculation au registre du commerce et des sociétés autres que celles mentionnées aux 1° et 2°;
4. les établissements publics industriels et commerciaux ;
5. les agents commerciaux ;
6. les groupements d’intérêt économique et les groupements européens d’intérêt économique.

4° Les unions de recouvrement des cotisations de sécurité sociale et d’allocations familiales (Urssaf) ou les caisses générales de sécurité sociale créent et gèrent les centres compétents pour :

1. les personnes exerçant, à titre de profession habituelle, une activité indépendante réglementée ou non autre que commerciale, artisanale ou agricole ;
2. les employeurs dont les entreprises ne sont pas immatriculées au registre du commerce et des sociétés, au répertoire des métiers ou au registre des entreprises de la batellerie artisanale, et qui ne relèvent pas des centres mentionnés au 6°.

5° Les chambres d’agriculture créent et gèrent les centres compétents pour les personnes physiques et morales exerçant à titre principal des activités agricoles […]. »

En application des dispositions des articles L. 411-1 et D. 411-1-3 du Code de la propriété intellectuelle, les données recueillies sont destinées à être rendues accessibles en consultation sur le portail [data.inpi](https://data.inpi.fr/) de l’Institut national de la propriété industrielle et réutilisables via ses licences open data homologuées.