﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Établissements recevant du public" -->
<!-- var(key)="fr-creation_entreprise-prealables-2-etablissements_recevant_du_public" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Établissements recevant du public
=================================

**Les établissements recevant du public (ERP) sont des bâtiments ouverts à des personnes autres que les salariés.**

**La création d'une entreprise destinée à recevoir du public, un commerce par exemple, implique de répondre à des normes légales en matière de sécurité et d'accessibilité.**

## Qu'est-ce qu'un ERP ?

Les établissements recevant du public (ERP) sont des « bâtiments, locaux et enceintes dans lesquels des personnes sont admises, soit librement, soit moyennant une rétribution ou une participation quelconque, ou dans lesquels sont tenues des réunions ouvertes à tout venant ou sur invitation, payantes ou non » ([article R. 123-2 du Code de la construction et de l'habitation](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000039041081&cidTexte=LEGITEXT000006074096&dateTexte=20190901)).

De nombreux types d'établissements sont concernés : commerce, restaurants, hôtels, théâtres, etc.

Les ERP sont [classés par type](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32351) (symbolisé par une lettre), en fonction de leur activité ou la nature de leur exploitation et par catégories, en fonction de la capacité d'accueil du bâtiment. 

Le classement d'un établissement est validé par la commission de sécurité à partir des informations transmises par son exploitant dans le dossier de sécurité déposé en mairie.

## ERP, des obligations de sécurité renforcées

L'ouverture d'un ERP est soumise à des [obligations de sécurité](https://www.service-public.fr/professionnels-entreprises/vosdroits/F31684), en particulier en matière d'incendies, qui s'imposent au moment de la conception et au cours de l'exploitation de l'établissement.

La réglementation applicable varie en fonction du classement du bâtiment.

## ERP, un devoir d'accessibilité

L'accessibilités des ERP est une [obligation légale](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32873).

Depuis la [loi du 11 février 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000809647&dateTexte=) pour l'égalité des droits et des chances, la participation et la citoyenneté des personnes handicapées, les établissements recevant du public (ERP) doivent être accessibles à tous les types de handicap (moteur, visuel, auditif, mental, etc.).

Les ERP qui ne sont pas accessibles doivent déposer des demandes d'autorisation de travaux ou de permis de construire de mise en conformité totale.

\> [Des fiches pratiques sont disponibles sur le site service public.fr sur la définition d'un ERP, les procédures d'autorisation de travaux, les règles de sécurité et l'accessibilité](https://www.service-public.fr/professionnels-entreprises/vosdroits/N31782)