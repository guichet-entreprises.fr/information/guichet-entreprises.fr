﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Disponibilité du nom de l’entreprise" -->
<!-- var(key)="fr-creation_entreprise-prealables-5-disponibilite_du_nom_de_lentreprise" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Disponibilité du nom de l’entreprise
===========================================

Une entreprise peut être identifiée par différents noms. Dans tous les cas, avant de les utiliser, **il est nécessaire de vérifier qu’ils sont disponibles**. Cette vérification s’effectue par une recherche d’antériorité par rapport à différentes catégories de noms regroupées dans des bases de données. Il s’agit de s’assurer que le nom choisi n’est pas déjà utilisé par une société immatriculée concurrente, en tant que dénomination sociale ou nom commercial, ou en tant que marque, pour désigner des produits ou services en lien avec la future activité exercée, ou encore en tant qu’adresse (nom de domaine) d’un site internet actif.

## Vérifier la disponibilité de votre nom par rapport aux dénominations sociales et noms commerciaux existants

Pour vérifier la disponibilité de votre nom par rapport à une dénomination sociale ou un nom commercial, vous pouvez consulter gratuitement, pour une recherche **de 1er niveau**, la base de données [data.inpi.fr](https://data.inpi.fr/) de l’Institut national de la propriété industrielle (INPI) contenant les données relatives au Registre national du commerce et des sociétés ainsi que la base SIRENE de l’INSEE.

Il s’agit d’une première étape. **Il vous est recommandé de compléter cette recherche par une recherche approfondie** (recherche de similarités orthographiques, phonétiques et intellectuelles) sur les noms de marques et de sociétés. Cette prestation peut être commandée en ligne sur le [site de l'INPI](https://www.inpi.fr/fr/disponibilite-marque-et-societe-en-france).

## Vérifier la disponibilité de votre nom par rapport aux marques existantes

Vous pourrez faire une recherche **de 1er niveau** en consultant gratuitement [data.inpi.fr](https://data.inpi.fr/) sur le site de l’INPI.

Il s’agit d’une première étape. **Il vous est recommandé de compléter cette recherche par une recherche approfondie** (recherche de similarités orthographiques, phonétiques et intellectuelles) sur les noms de marques et de sociétés. Cette prestation peut être commandée en ligne sur le [site de l'INPI](https://www.inpi.fr/fr/disponibilite-marque-et-societe-en-france).

## Vérifier la disponibilité de votre nom par rapport aux noms de domaine

Pour vérifier la disponibilité du nom choisi par rapport aux noms de domaine existants, vous pouvez consulter la [base de données de l’Association française pour le nommage internet en coopération](https://www.afnic.fr/noms-de-domaine/tout-savoir/whois-trouver-un-nom-de-domaine/) (Afnic) (noms de domaine en .fr).

Il s’agit d’une première étape. **Il vous est recommandé** de compléter cette recherche par une **recherche approfondie**, que vous pouvez commander sur le site de l’INPI. Elle consiste en [un forfait](https://www.inpi.fr/fr/disponibilite-marque-societe-et-domaine-en-france) regroupant une recherche de similitudes parmi les marques, les noms de société et une recherche à l’identique parmi les noms de domaine en .fr, .eu et les gTLDs (.com, .org, etc.).

Vous pouvez également commander auprès de l’INPI des [services et prestations complémentaires](https://www.inpi.fr/fr/services-et-prestations/2704).

## Autres démarches

Si en plus de la création de votre entreprise vous souhaitez déposer une marque, tous les renseignements nécessaires sont disponibles sur [inpi.fr](https://www.inpi.fr/fr/proteger-vos-creations/proteger-votre-marque/les-etapes-cles-du-depot-de-marque).

Si en plus de la création de votre entreprise vous souhaitez réserver un nom de domaine, il vous faudra vous rapprocher d’un bureau d’enregistrement accrédité par l’Afnic pour vous servir d’intermédiaire obligatoire entre vous et l’Afnic pour toute demande d’enregistrement d’un nom de domaine. Vous trouverez des renseignements utiles à cet effet sur le [site de l’Afnic](https://www.afnic.fr/fr/votre-nom-de-domaine/comment-choisir-et-creer-mon-nom-de-domaine/).
