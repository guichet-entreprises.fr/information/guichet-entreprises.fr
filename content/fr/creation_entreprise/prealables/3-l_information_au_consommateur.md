﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="L'information au consommateur" -->
<!-- var(key)="fr-creation_entreprise-prealables-3-l_information_au_consommateur" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# L'information au consommateur

**En tant que chef d'entreprise délivrant des produits ou des services, vous avez un devoir d'information vis-à-vis de vos clients.**

**Le Code de la consommation et le Code civil régissent les obligations d'information aux consommateurs.**

## Obligations des entreprises vis-à-vis de leurs clients

En tant que professionnel, vous avez l'obligation d'informer les consommateurs sur les biens ou services que vous leur proposez.

[L'article L. 111-1 du Code de la consommation](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000020625532&cidTexte=LEGITEXT000006069565) définit clairement l'obligation d'information :

« Avant que le consommateur ne soit lié par un contrat de vente de biens ou de fourniture de services, le professionnel communique au consommateur, de manière lisible et compréhensible, les informations suivantes :

1. Les caractéristiques essentielles du bien ou du service [...] ;
2. Le prix du bien ou du service [...] ;
3. [...] la date ou le délai auquel le professionnel s'engage à livrer le bien ou à exécuter le service ; 
4. Les informations relatives à son identité, à ses coordonnées postales, téléphoniques et électroniques et à ses activités [...] ».

Des règles particulières d'information s'appliquent à certains types de biens et de services (denrées alimentaires, crédit immobilier, etc.).

Dans certains cas, le vendeur doit remettre au client des documents d'information spécifiques (devis, caractéristiques techniques, date de disponibilité) avant la signature du contrat.

## La DGCCRF, le service de l'État qui veille à l'information des consommateurs

La [Direction générale de la concurrence, de la consommation et de la répression des fraudes](https://www.economie.gouv.fr/dgccrf) (DGCCRF) veille au bon fonctionnement des marchés, au bénéfice des consommateurs et des entreprises. Elle intervient :

* sur tous les champs de la consommation (produits alimentaires et non-alimentaires, services) ;
* à tous les stades de l'activité économique (production, transformation, importation, distribution) ;
* quelle que soit la forme de commerce : magasins, sites de commerce électronique ou liés à l'économie collaborative, etc.

[Les fiches pratiques de la DGCCRF](https://www.economie.gouv.fr/dgccrf/Publications/Vie-pratique/Fiches-pratiques-de-la-concurrence-et-de-la-consom) répondent de manière synthétique aux questions de concurrence et de consommation. Informatives et opérationnelles, elles sont régulièrement actualisées en fonction des évolutions de la réglementation.