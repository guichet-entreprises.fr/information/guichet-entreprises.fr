﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Les aides aux entreprises" -->
<!-- var(key)="fr-creation_entreprise-prealables-1-aides_publiques_aux_entreprises" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Les aides aux entreprises
===================================

**Vous créez votre entreprise ? Son financement sera au cœur de votre projet.**

**Au-delà des prêts bancaires et personnels, de nombreuses aides financières et subventions existent. À vous de trouver celles qui vont contribuer à la création de votre entreprise.**

## Aides-entreprises.fr, un site unique pour financer votre entreprise

Le site [aides-entreprises.fr](https://aides-entreprises.fr) recensent plus de 1 800 aides financières disponibles pour les porteurs de projet et les chefs d'entreprise. 

Aides-entreprises.fr offre une information complète et actualisée à l'échelle locale, nationale ou européenne et oriente le demandeur vers l'interlocuteur de référence sur chaque dispositif visé.

Un agenda des concours, certains récompensant les lauréats par des dotations financières, figure également sur le site.

## Aide aux créateurs et repreneurs d'entreprises (ACRE), une aide simplifiée

Depuis le 1er janvier 2020, l'attribution de l'ACRE n'est plus automatique et est de nouveau soumise à des critères d'éligibilité.

Pour en bénéficier, les demandeurs doivent être dans l'une des situations suivantes :

* demandeur d'emploi indemnisé ;
* demandeur d'emploi non indemnisé inscrit à Pôle emploi depuis plus de 6 mois au cours des 18 derniers mois ;
* bénéficiaire de l'allocation de solidarité spécifique (ASS) ou du revenu de solidarité active (RSA) ;
* avoir entre 18 et 26 ans ;
* avoir moins de 30 ans et être reconnu handicapé ;
* avoir conclu un contrat d'appui au projet d'entreprise (CAPE) ;
* créer ou reprendre une entreprise implantée au sein d'un quartier prioritaire de la ville (QPV) ;
* bénéficier de la prestation partagée d'éducation de l'enfant (PreParE) ;
* être travailleur indépendant ne relevant pas du régime micro-social.

Dans le cas des sociétés, vous devez en avoir le contrôle, c'est à dire être dans l'une de trois situations suivantes :

* vous détenez, personnellement ou avec votre époux/épouse, votre partenaire de Pacs, votre concubin(e) ou vos ascendants et descendants, plus de 50 % du capital, dont au moins 35 % à titre personnel ;
* vous dirigez la société et détenez, personnellement ou avec votre époux/épouse, votre partenaire de Pacs, votre concubin(e) ou vos ascendants et descendants, au moins 1/3 du capital, dont au moins 25 % à titre personnel, sous réserve qu'un autre actionnaire ne détienne pas plus de 50 % du capital ;
* les demandeurs qui détiennent ensemble plus de 50 % du capital, à condition qu'un ou plusieurs d'entre eux soient dirigeant et que chaque demandeur ait une part de capital égale à au moins à 1/10e de la part du principal actionnaire.

Le demandeur ne doit pas avoir bénéficié de l'ACRE au cours des 3 dernières années.

La demande d’ACRE doit être effectuée directement via le site [autoentrepreneur.urssaf.fr](https://www.autoentrepreneur.urssaf.fr/portail/accueil.html) lors de la création d’activité ou, au plus tard, dans un délai de 45 jours à compter du dépôt de la déclaration de création ou de reprise d'entreprise.

À l'heure actuelle, seuls les micro-entrepreneurs doivent déposer un dossier à l'Urssaf. Les créateurs d'entreprise sous un autre statut juridique sont traités directement par l'Urssaf sans dépôt de dossier.

L’Urssaf reste seule compétente pour instruire les demandes et notifier la décision. 

Étendu à tous les créateurs et repreneurs, ce dispositif permet de bénéficier d’une exonération temporaire de cotisations sociales sur 12 mois.