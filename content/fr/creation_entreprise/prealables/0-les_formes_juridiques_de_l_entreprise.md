﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Les formes juridiques de l'entreprise" -->
<!-- var(key)="fr-creation_entreprise-prealables-0-les_formes_juridiques_de_l_entreprise" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Les formes juridiques de l'entreprise
====================================

**Lors de votre création d'entreprise, vous devez choisir sous quelle forme juridique vous allez exercer votre activité.**

**Micro-entrepreneur, EIRL, EURL, SA, SARL, etc., le choix du statut juridique a des incidences fiscales et sociales.**

**Le type d'activité, la présence d'associés, les responsabilités que vous souhaitez exercer ou encore la protection de votre patrimoine privé  vont vous guider dans le choix de la forme juridique de votre entreprise.**

## Vous créez votre entreprise seul

Vous pouvez être micro-entrepreneur, commerçant en nom propre (entreprise individuelle), être l'associé unique d'une entreprise unipersonnelle à responsabilité limitée (EURL) ou d'une société par actions simplifiée unipersonnelle (SASU).

### L'entreprise individuelle (Micro-entrepreneur et EI)

Le micro-entrepreneur est un entrepreneur individuel qui bénéficie d'un [régime simplifié](../micro_entrepreneur/le_regime_micro_entrepreneur.md).

L'entreprise individuelle est la propriété exclusive d'une personne physique qui forme, avec elle, une seule entité et en est entièrement responsable.

L'identité de l'entreprise correspond à celle du dirigeant, à laquelle on peut adjoindre un [nom commercial](4-les_noms_de_l_entreprise.md) : Marion Durand, Atelier des étoiles. 

Attention, le dirigeant est indéfiniment responsable des dettes de son entreprise sur ses biens propres. Il peut toutefois protéger sa résidence principale ainsi que ses biens fonciers non affectés à l'activité professionnelle (résidence secondaire ou terrain) en faisant une déclaration d'insaisissabilité auprès de son notaire. Cette déclaration fait l'objet de publicités.

Les artisans, les commerçants, les industriels et les professions libérales peuvent constituer leur entreprise sous le régime de l'entreprise individuelle. 

L'entrepreneur individuel ne touche pas de salaire. Les bénéfices (chiffre d'affaires moins les charges) constituent son revenu. Ils sont soumis à l'impôt sur le revenu dans la catégorie correspondant à l'activité : bénéfices industriels et commerciaux (BIC), en tant que commerçant ou artisan, ou bénéfices non commerciaux (BNC) s'il s'agit d'une profession libérale.

Il dépend de la [Sécurité Sociale – Indépendants](https://www.secu-independants.fr/) (SSI).

### L'entreprise individuelle à responsabilité limitée (EIRL)

Le statut d'entrepreneur individuel à responsabilité limitée (EIRL) permet à l'entrepreneur de protéger ses biens personnels, en créant un patrimoine affecté à son activité professionnelle distinct de son patrimoine privé. Juridiquement, il s'agit d'une entreprise individuelle. 

#### Bon à savoir

Depuis le 15 février 2022, suite à l’adoption de la loi n° 2022-172 en faveur de l’activité professionnelle indépendante et de l’entrée en vigueur du statut unique de l’entrepreneur individuel à compter du 15 mai 2022, il n’est plus possible de choisir le statut de l’EIRL.

S’il n’est plus possible de créer sous ce statut, les EIRL déjà immatriculées continuent d'exercer leurs activités dans les mêmes conditions.

### La société par actions simplifiées unipersonnelle (SASU)

La société par actions simplifiées unipersonnelle (SASU) est une société avec un seul associé. 

Comme toute société, la SASU doit détenir un capital social, déposé en son nom auprès d'une banque. Son montant minimum est d'1 €.

La SASU doit obligatoirement désigner un Président, personne physique ou personne morale. 

Pour plus d'informations sur les différentes formes juridiques, consultez l'[article du site service-public.fr](https://www.service-public.fr/professionnels-entreprises/vosdroits/F23844).

### L'entreprise unipersonnelle à responsabilité limitée (EURL)

L'entreprise unipersonnelle à responsabilité limitée (EURL) comporte un seul associé mais obéit aux règles d'une SARL (cf. infra).

L'associé unique d'une EURL ne peut être une autre EURL.

L'EURL génère davantage d'obligations juridiques et comptables que l'Entreprise Individuelle.

L'associé unique a l'obligation de constituer, dès la création de l'EURL, un capital social dont le montant est librement déterminé dans les statuts. 

Dans le cas de l'EURL dont l'associé unique personne physique exerce personnellement la gérance, il peut être fait option du régime micro-entrepreneur.

Il peut être libéré dans les mêmes conditions que celui d'une SARL. 

## Vous créez votre entreprise à plusieurs

### Vous créez une société à responsabilité limitée (SARL)

La société à responsabilité limitée (SARL) est la forme de société la plus répandue en France.

Il s'agit d'une forme juridique simple, nécessitant la présence d'au minimum deux associés dont la responsabilité est limitée au montant de leurs apports.

#### Le capital social de la SARL <!-- collapsable:close -->

Aucun capital social minimum n'est requis. Une SARL peut être créée avec 1 euro symbolique.

Le montant du capital est fixé par les associés, en fonction de la nature du projet et des besoins en capitaux de la société. 

Toute personne qui effectue un apport au capital devient automatiquement associé et reçoit un nombre de parts sociales proportionnel au montant de son apport.

Le choix de la SARL permet aux associés de protéger leur patrimoine personnel. En cas de difficultés financières, leur responsabilité est strictement limitée aux montants de leurs apports. Les créanciers saisissent uniquement les biens de la société et n'ont pas accès aux biens personnels des associés.

#### Les associés de la SARL <!-- collapsable:close -->

Une SARL doit comporter entre 2 et 100 associés. 

Les associés de SARL peuvent être des personnes physiques (individus) ou des personnes morales (structures juridiques telles que les sociétés ou associations loi de 1901).

Il n'existe pas de condition d'âge pour entrer au capital d'une SARL.

La SARL peut être à associé unique. On parle alors de SARL unipersonnelle ou d'Entreprise Unipersonnelle à Responsabilité Limitée (EURL) (cf ci-dessous).

#### Les apports des associés de la SARL <!-- collapsable:close -->

Les apports peuvent être réalisés en numéraire (somme d'argent), en nature (local, équipement, etc.) ou encore « en industrie », c'est-à-dire lorsqu'une personne met à disposition de la société ses connaissances techniques, son savoir-faire ou encore ses services. 

En cas d'apport en nature ou en industrie, la procédure à suivre est plus lourde (nomination d'un commissaire aux apports chargé de les évaluer, etc.).

Ces apports permettent de détenir des parts sociales, de voter aux assemblées générales et de toucher des dividendes. 

Les droits de vote et aux bénéfices sont calculés proportionnellement au nombre de parts dont dispose chaque associé.

La réalisation d'un pacte d'associés précisant certaines règles de fonctionnement est conseillée pour éviter les situations de blocage.

#### Le gérant de la SARL <!-- collapsable:close -->

La direction de la SARL est exercée par un ou plusieurs gérant(s), nommé(s) par les associés. 

Celui-ci accompli tous les actes de gestion dans l'intérêt de la société : signature de contrat, recrutement de salariés, action en justice, etc. Il est le représentant légal de celle-ci et agit donc en son nom et pour son compte. 

Il a également la charge de tenir les assemblées générales. Ses pouvoirs peuvent être limités par les associés, qui peuvent subordonner l'accomplissement de certains actes à l'autorisation préalable de l'assemblée.

Sur le plan fiscal et social, son statut varie s'il est associé majoritaire ou minoritaire.

Sa responsabilité est limitée au montant de ses apports sauf en cas de faute de gestion où, dans ce cas, il engage ses biens propres.

#### La fiscalité de la SARL <!-- collapsable:close -->

Fiscalement, une SARL est en principe soumise à l'impôt sur les sociétés (IS). 

Sous certaines conditions, les associés de la SARL peuvent décider, à l'unanimité, d'opter pour le régime fiscal des sociétés de personnes, c'est-à-dire une imposition des bénéfices à l'impôt sur le revenu. L'intégralité du bénéfice de la SARL est alors versée aux associés, proportionnellement aux apports de chacun, et soumis à leur impôt sur le revenu dans la catégorie BIC/BNC. L'option pour l'impôt sur le revenu doit être exercée au cours des cinq premières années d'existence de la SARL et est valable pendant cinq exercices (années comptables), après quoi la société est à nouveau automatiquement soumise à l'impôt sur les sociétés.

#### La cessation de parts de SARL <!-- collapsable:close -->

Tout associé souhaitant céder ses parts de SARL doit, au préalable, obtenir l'accord de la majorité de ses associés. C'est ce que l'on appelle la procédure d'agrément. 

Par exception, les parts sociales peuvent être librement cédées à un autre associé ou à un membre de la famille (conjoint, parents ou enfants), sauf clause contraire dans les statuts.

### Vous créez une société d’exercice libéral

Les sociétés d’exercice libérale (SEL) permettent aux professions libérales de pratiquer leur activité sous la forme d'une société de capitaux.

L’activité est exercée par le biais de structures juridiques existantes et l’association avec d'autres professionnels du même domaine est possible.

La société d'exercice libéral ne peut en aucun cas être pluridisciplinaire et elle doit être inscrite au registre du commerce.

#### SELARL, SELAFA, SELAS et SELCA : les sociétés d'exercice libéral <!-- collapsable:close -->

Les SEL sont régies par les règlements applicables aux sociétés commerciales.

Il existe quatre types de sociétés d'exercice libéral :

* la société d'exercice libéral à responsabilité limitée (SELARL) comprenant 2 associés minimum ou 1 associé unique pour une SELARL unipersonnelle ;
* la société d'exercice libéral à forme anonyme (SELAFA) comprenant 3 associés minimum ;
* la société d'exercice libéral par actions simplifiée (SELAS) comprenant 1 associé minimum ;
* la société d'exercice libéral en commandite par actions (SELCA) comprenant 4 associés minimum.

#### Les avantages des sociétés d'exercice libéral <!-- collapsable:close -->

Les sociétés d'exercice libéral offrent certains avantages en termes d'imposition et de paiement des cotisations sociales :

* les BNC (bénéfices non commerciaux) sont pris en compte pour déterminer le montant net imposable du travailleur et son assiette de cotisations sociales ;
* la taxation des charges non déductibles se fait au titre de l'impôt sur les sociétés plutôt qu’au titre de l’impôt sur le revenu de la personne physique ;
* les professionnels peuvent bénéficier d'une responsabilité limitée à hauteur du montant de leurs apports tout en conservant l'indépendance inhérente à leur statut libéral ;
* ils profitent également d'une réglementation spécifique quant à la cession de leurs droits sociaux.

#### Les régimes sociaux des gérants et dirigeants de sociétés d'exercice libéral <!-- collapsable:close -->

Le régime social dépend du type de société et de la position du gérant ou du dirigeant au sein de celle-ci.

Les gérants minoritaires de SELARL et les dirigeants de SELAFA ou de SELAS dépendent du régime social des assimilés salariés. 

Les gérants majoritaires de SELARL, dirigeants de SELARL unipersonnelle et de SELCA sont soumis au régime social des travailleurs non salariés (TNS).

## Autres formes juridiques

\> [Plus d'informations sur les autres formes juridiques de l'entreprise](https://www.service-public.fr/professionnels-entreprises/vosdroits/F23844) (service-public.fr)