﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Les noms de l'entreprise" -->
<!-- var(key)="fr-creation_entreprise-prealables-4-les_noms_de_l_entreprise" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Les noms de l'entreprise

**Nom de société, nom de domaine, nom de marque, etc., identifient votre entreprise.**

**Vérifier leur disponibilité et les protéger sont des étapes indispensables lors de la création d'une entreprise.**

## Création d'entreprise, trouver le nom de sa société

Nom commercial, enseigne, raison sociale, etc., sont différentes dénominations pour différents aspects juridiques de l'entreprise. 

### Le nom commercial

Le nom commercial correspond au nom connu du public et des clients. 

Il concerne l'entreprise individuelle ou le fonds de commerce et peut être confondu avec le nom du dirigeant/fondateur.

### La dénomination sociale

La dénomination sociale désigne les sociétés commerciales et les sociétés civiles professionnelles (SCP).

La dénomination sociale est librement choisie par les associés. Elle peut être purement créative ou faire référence à l'activité. Elle est utilisée dans tous les autres types de sociétés (SARL, EURL, SA, SAS, SNC, etc.). Ex. : « Parfumerie DURAND ».

### La raison sociale 

La raison sociale est le nom attribué à une entreprise. Elle est exclusivement composée du nom de tous ou de certains associés. 

A la différence de la raison sociale, les sociétés civiles peuvent avoir une dénomination sociale qui n'est pas exclusivement composée du nom de tous ou de certains associés.

Lors de l'immatriculation de la société au [registre du commerce et des sociétés](https://www.infogreffe.fr/formalites-entreprise/immatriculation-entreprise.html) (RCS), il est obligatoire de fournir la raison sociale de l'entreprise ou sa dénomination suivie, le cas échéant, de son sigle ([article R123-53 du Code de commerce](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006256506&cidTexte=LEGITEXT000005634379&dateTexte=20070327)).

La dénomination sociale et la raison sociale de la société peuvent servir de nom commercial.

### L'enseigne

L'enseigne permet de localiser territorialement l'un ou l'autre de ses établissements commerciaux ou fonds de commerce, pris généralement en tant que point de vente. Le plus souvent apposée sur la façade de l'établissement, l'enseigne constitue surtout un signe ou un emblème de reconnaissance de celui-ci auprès de la clientèle. L'enseigne peut être mentionnée au RCS.

<!--
https://www.guichet-entreprises.fr/fr/creation-dentreprise/les-prealables-a-la-creation-dentreprise/les-noms-de-lentreprise-et-leur-protection/disponibilite-et-protection-des-noms-de-societes/
-->

## Création d'entreprise, protéger sa marque

Le dépôt de marque est une procédure qui permet à toute personne, physique ou morale, d'acquérir et protéger les droits d'exploitation sur une marque. 

Cette démarche vous permet de vérifier la disponibilité de la marque choisie, de vous protéger de la contrefaçon et d'éventuels concurrents l'utilisant frauduleusement. 

En France, le dépôt de marque se fait auprès de l'[Institut national de la propriété industrielle](https://www.inpi.fr/fr/comprendre-la-propriete-intellectuelle/la-marque) (INPI).

Lors de ce dépôt, il est nécessaire de choisir, au sein de la classification de Nice, les classes correspondantes à vos produits et/ou services. Une fois le dépôt validé et enregistré, l'utilisation de la marque est garantie sur le territoire choisi (au niveau national, européen ou international).

## Création d'entreprise, choisir son nom de domaine

Si vous dotez votre entreprise d'un site internet, vous devez choisir et déposer son nom de domaine. 

Le nom de domaine est l'équivalent, sur le web, d'une adresse postale. Il est précédé d'un préfixe « www. » et suivi d'une extension « .fr », « .com », « .eu », « .org », etc.

Facilement mémorisable, représentatif de votre activité, disponible, le nom de domaine doit respecter les éventuels droits antérieurs de tiers sur ce même nom. 

Après vérification de la disponibilité du nom de domaine souhaité, vous devez l'enregistrer auprès d'un des bureaux accrédités par l'[ICANN](https://www.icann.org/) (*Internet Corporation for Assigned Names and Numbers*).

En France, différents organismes proposent ces services : AFNIC, GANDI, OVHcloud, etc.

Le nom de domaine peut également être déposé au RCS en même temps que l'immatriculation de l'entreprise ou de la société ou via une formalité de modification. L'imprimé pour déclarer le nom de domaine est le cerfa NDI.