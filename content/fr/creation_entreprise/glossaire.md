﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Glossaire" -->
<!-- var(key)="fr-creation_entreprise-glossaire" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Glossaire
=========

Veuillez sélectionner une entrée dans le menu dépliant :

## A <!-- collapsable:close -->

### ACRE

*Aide à la création ou à la reprise d'une entreprise*. L'aide à la création ou à la reprise d'une entreprise (ACRE) permet de bénéficier d’une exonération temporaire de cotisations sociales sur 12 mois.

La demande d’ACRE doit être effectuée directement via le site [autoentrepreneur.urssaf.fr](https://www.autoentrepreneur.urssaf.fr/portail/accueil.html) lors de la création d’activité ou, au plus tard, dans un délai de 45 jours à compter du dépôt de la déclaration de création ou de reprise d'entreprise.

L’Urssaf est la seule autorité compétente pour instruire les demandes et notifier la décision.

### Acoss

[*Agence centrale des organismes de sécurité sociale*](https://www.Acoss.fr/home.html) (la Caisse nationale des Urssaf). Elle assure la gestion financière du régime général de sécurité sociale (régime des salariés) et coordonne les activités des centres de recouvrement répartis sur le territoire français.

### Acte authentique

Acte rédigé par un notaire.

### Acte sous seing privé (SSP)

Acte établi par les parties, sans l'intervention d'un notaire.

### Actif

Notion comptable se rapportant au bilan d'une entreprise et représentant l'ensemble du patrimoine qu'elle détient. Il comprend deux sortes de biens : les actifs immobilisés (constructions, matériel, etc.) et les actifs circulants (stock, créances clients, etc.).

### Action

Titre de propriété émis par les sociétés de capitaux (SA, SAS et les sociétés en commandite par actions), conférant des droits à son titulaire :

* droit de vote dans les assemblées ;
* droit de rémunération (dividende et boni de liquidation). Une action est dite :
  * « nominative », lorsque son propriétaire est connu de la société émettrice ;
  * « au porteur », lorsqu'il n'est pas connu.

Les actions « au porteur » sont émises par des sociétés cotées en bourse.

### Actionnaire

Personne physique ou morale détentrice d'une ou de plusieurs actions d'une société de capitaux (société anonyme, société par actions simplifiée, société en commandite par actions).

### Activité agricole

Tout acte inhérent à l'exploitation professionnelle d'un cycle végétal ou animal ainsi que toute activité constituant le prolongement de cette exploitation, comme la transformation des produits et leur commercialisation.

### Activité artisanale

Une activité est artisanale quand elle consiste à fabriquer, transformer, réparer un produit ou à fournir une prestation de services nécessitant un travail manuel. La liste des activités artisanales a été fixée par le [décret du 2 avril 1998](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=3BEF27CD0444BF1F02BA62962BEBCD73.tpdjo12v_1?cidTexte=JORFTEXT000000571009&dateTexte=20090202).

Toute entreprise exerçant une activité artisanale (principale ou secondaire) doit être enregistrée auprès du centre de formalités des entreprises des chambres de métiers et de l'artisanat pour être immatriculée au répertoire des métiers.

### Activité civile

Cette activité s'apprécie par opposition à l'activité commerciale. On y recense principalement les activités agricoles et les activités libérales.

### Activité commerciale

L'activité commerciale est définie par le Code de commerce. Pour l'essentiel, il s'agit de l'achat pour la revente dans un but lucratif de biens meubles ou immeubles, ainsi que la vente de certains services : hôtels, restaurants, spectacles, transports, locations, etc. Les entreprises exerçant une activité commerciale doivent être immatriculées au registre du commerce et des sociétés (RCS).

### Adèle

L'Administration française en ligne.

### ADIE

[*Association pour le droit à l'initiative économique*](https://www.adie.org/). L'ADIE finance et accompagne les créateurs d'entreprise qui n'ont pas accès au crédit bancaire et plus particulièrement les demandeurs d'emploi et les bénéficiaires des minima sociaux. Son site propose un accompagnement en ligne et permet aux créateurs d'entreprise de faire directement une demande de financement.

### Administrateur

Personne physique ou morale désignée parmi les actionnaires d'une société anonyme pour exercer une mission de gestion collégiale au sein d'un « conseil d'administration ». Dans les 3 mois de sa nomination, il doit être détenteur d'un nombre minimal d'actions, conformément aux statuts de la société.

### Affiliation

Rattachement d'un assuré social à une caisse déterminée.

### Afnor

[*Association française de normalisation*](https://www.afnor.org/). L’Afnor a pour mission d'animer et coordonner l'élaboration des normes, de les homologuer, de promouvoir et faciliter leur utilisation.

Elle représente et défend les intérêts de la France dans toutes les instances de normalisation et développe la certification des produits et services avec la marque NF.

### Agent commercial

Mandataire indépendant, chargé, de façon permanente, de négocier et éventuellement de conclure des contrats d'achat et de vente, de location ou de prestation de services au nom et pour le compte d'autres entreprises.

Les entreprises n'exercent pas de contrôle direct sur ses activités, car en tant que professionnel indépendant, il dispose d'une liberté totale d'organisation.

### Agent d'affaires

Personne qui, à titre professionnel et moyennant une rémunération, se charge des intérêts des particuliers en les conseillant et parfois en agissant à leur place. Les agents d'affaires exercent une activité commerciale.

### Agessa

[*Association pour la gestion de la sécurité sociale des auteurs*](http://www.secu-artistes-auteurs.fr/).

### Agirc

[*Association générale des institutions de retraite complémentaire des cadres*](http://www.agirc-arrco.fr/). Elle gère le régime de retraite complémentaire des cadres du secteur privé de l’industrie, du commerce, des services et de l’agriculture.

### Allocataire

Personne qui reçoit une allocation ou une aide sociale.

### Année civile

Année du 1er janvier au 31 décembre.

### APCA

[*Assemblée permanente des chambres d'agriculture*](https://chambres-agriculture.fr/).

### APCMA

[*Assemblée permanente des chambres de métiers et de l'artisanat*](https://cma-france.fr/). C'est la tête de réseaux des chambres de métiers et de l'artisanat (CMA).

### APE

*Activité principale exercée (APE)*. Voir l'entrée « Code APE ».

### Apporteur d'affaires

Personne mettant en relation un prospect avec une entreprise sur l'existence d'un marché potentiel moyennant rétribution. Cette activité n'est pas une profession.

### Apports

Biens et/ou ressources financières ou techniques mis à la disposition de la société par les associés en vue de l'exploitation commune et en contrepartie desquels ils reçoivent des droits sociaux (parts ou actions). L'ensemble des apports constitue le capital social de la société.

### Apports en industrie

Mise à disposition par un associé de ses connaissances techniques, de son travail ou de ses services au profit de sa société. Cet apport n'est pas pris en compte dans la formation du capital social initial mais il peut donner lieu à une attribution de titres sociaux inaliénables mais rémunérés (partage des bénéfices et participation aux assemblées) si la société est une SARL ou une SAS.

### Apports en nature

Apport de biens autres que de l'argent au capital d'une société. Les statuts de la société doivent indiquer la valeur de chaque bien à partir du rapport établi par un commissaire aux apports qui sera annexé aux statuts. Toutefois, le [décret 2010-1669](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000023317402&categorieLien=id) autorise les associés de SARL et l'associé d'EURL à ne pas recourir à un commissaire aux apports si les 2 conditions suivantes sont remplies :

* aucun apport en nature n'a une valeur supérieure à 30 000 euros ;
* la valeur totale des apports en nature ne dépasse pas la moitié du capital social.

### Apports en numéraire

Apport d'argent au capital d'une société. Cet apport peut être libéré (versé) en plusieurs fois sans dépasser le délai de 5 ans. Un minimum d'apport doit être libéré à la constitution (1/5 pour les SARL et 50 % pour les SA et SAS).

### Artisan

Titre de qualification pouvant être accordé à un chef d'entreprise artisan par la chambre de métiers et de l'artisanat sous certaines conditions de diplôme ou et de durée d'activité professionnelle (6 années minimum).

### Assiette

Base de calcul.

### Association

Groupement de personnes réunies dans un intérêt commun autre que le partage de bénéfices. Cette structure juridique instituée par la loi de 1901 est très utilisée dans certains domaines (sportif, culturel, artistique, etc.). Au-delà de certains seuils, les activités à caractère lucratif font l'objet d'une assimilation au régime commun des sociétés (TVA et impôts sur les sociétés). Les associations sont en dehors du périmètre du Guichet Entreprises.

### Associé

Personne physique ou morale ayant réalisé des apports en nature ou en espèce au capital d'une société.

Le terme « associé » est principalement utilisé :

* dans les sociétés civiles ;
* dans les SARL (société à responsabilité limitée), SNC (société en nom collectif) et SCS (société en commandite simple).

Le terme « actionnaire » est quant à lui utilisé dans les SA (société anonyme), SAS (société par actions simplifiée) ou SCA (société en commandite par actions).

### Auto-entrepreneur

Voir l'entrée « Micro-entrepreneur ».

### Autorité compétente

Autorité ou instance habilitée spécifiquement par un État membre à délivrer ou à recevoir des titres de formation et autres documents ou informations, ainsi qu'à recevoir des demandes et à prendre des décisions liées aux autorisations d'exercer.

### Ayant droit

Personne qui se substitue à une autre pour l'exercice d'un droit qu'elle tient de cette dernière (exemple : une personne sans activité professionnelle est l'ayant droit de son conjoint salarié en matière d'assurance maladie ; un héritier est l'ayant droit du défunt, etc.).

## B <!-- collapsable:close -->

### Bail

Contrat par lequel une personne laisse à une autre le droit de se servir d'une chose pendant un temps déterminé en échange d'une somme précise. Plus spécialement, contrat que l'on signe quand on loue un logement, un magasin.

### Bail commercial

Contrat de location immobilière consenti aux entreprises qui exercent une activité industrielle, commerciale, artisanale ou libérale. Sa durée minimale est de 9 ans, le locataire pouvant le résilier à l'expiration d'une période triennale (c'est pourquoi on emploie souvent l'expression « bail 3-6-9 ») en respectant impérativement un préavis de 6 mois. Le statut des baux commerciaux présente des avantages considérables lorsque le locataire est immatriculé au registre du commerce et des sociétés ou au répertoire des métiers à la date de renouvellement de son bail. Il bénéficie notamment d'une relative stabilité se caractérisant par :

* le droit au renouvellement de son bail, appelé également la « propriété commerciale » (disposition non applicable aux micro-entrepreneurs) ;
* le plafonnement des augmentations de loyer, qui se calculent en fonction de la variation d'indices légaux, prévus à la signature du bail initial.

### Bail professionnel

Contrat de location consenti aux entreprises exerçant une activité libérale ou artisanale lorsqu'il n'existe pas de fonds artisanal. Sa durée minimale est de 6 ans, le locataire pouvant le résilier à tout moment sous réserve de respecter un délai de préavis de 6 mois.

### Bailleur

Propriétaire du bien qui consent un bail à un locataire.

### Bpifrance (la Banque publique d'investissements)

La [Banque publique d'investissement](https://www.bpifrance.fr/) (BPI) offre l'ensemble des instruments de soutien financier aux petites et moyennes entreprises et aux entreprises de taille intermédiaire, en conformité avec les règles européennes.

Elle propose des services d'accompagnement et de soutien renforcé à l'innovation et à l'export qui seront accessibles pour les entreprises grâce à des guichets uniques dans chaque région.

### Bénéfices agricoles (BA)

Bénéfices réalisés par des personnes physiques ou des sociétés soumises à l'impôt sur le revenu, provenant de l'exploitation d'une activité agricole (culture et/ou élevage).

Les bénéfices agricoles sont imposés en fonction du montant des recettes réalisées, selon le régime du forfait, du bénéfice réel simplifié ou du bénéfice réel normal.

### Bénéfices industriels et commerciaux (BIC)

Bénéfices réalisés par des personnes physiques ou des sociétés soumises à l'impôt sur le revenu qui exercent une activité commerciale, industrielle ou artisanale.

La détermination des BIC peut être effectuée :

* en appliquant au chiffre d'affaires un abattement forfaitaire représentatif des frais professionnels : régime fiscal de la micro-entreprise ;
* en déduisant du chiffre d'affaires les charges réelles supportées par l'entreprise : régime fiscal du bénéfice réel (normal ou simplifié).

### Bénéfices non commerciaux (BNC)

Bénéfices soumis à l'impôt sur le revenu réalisés par des personnes exerçant une activité professionnelle non commerciale (professions libérales) à titre individuel ou comme associés de certaines sociétés. La détermination des BNC peut être effectuée :

* en appliquant aux recettes un abattement forfaitaire représentatif des frais professionnels : régime fiscal de la micro-entreprise ;
* en déduisant des recettes les charges réelles supportées par l'entreprise : régime fiscal de la déclaration contrôlée.

### BIC

*Bénéfices industriels et commerciaux* (voir cette entrée).

### Bilan

Tableau présentant, à un moment donné, la situation de l'entreprise en matière d'emploi des fonds mis en œuvre dans l'entreprise (actif) et de ressources (passif).

L'actif recense ce que l'entreprise possède : immeubles, machines, matériels, brevets, marchandises, créances, argent, etc. Le passif décrit :

* l'origine des ressources : apports, subventions, etc. ;
* les dettes de l'entreprise : emprunts, comptes-courants, etc.

### BNC

*Bénéfices non commerciaux* (voir cette entrée).

### Bodacc

[*Bulletin officiel des annonces civiles et commerciales*](https://www.bodacc.fr/). Il s'agit d'un recueil périodique national et journalier comportant les insertions officielles des créations d'entreprises, des ventes et cessions de fonds de commerce, des ouvertures de procédures collectives.

### Boutique de gestion

Organisme réunissant les compétences nécessaires pour conseiller et accompagner les créateurs et repreneurs d'entreprises, depuis la recherche d'une idée jusqu'à l'entreprise de 2 ans. Les boutiques de gestion sont organisées en réseau (réseau des boutiques de gestion).

### BTP

*Bâtiment et travaux publics* : secteur économique de la construction.

### BTS

*Brevet de technicien supérieur*.

### But lucratif

Une entreprise à but lucratif est une entreprise dont l'objectif principal est la réalisation de bénéfices.

### But non lucratif

Une entreprise « à but non lucratif » est une entreprise regroupant des personnes autour d'un projet dans un but autre que le partage de bénéfices : la promotion d'une activité sportive, la découverte d'une région, la réalisation d'économies, l'insertion de personnes en difficulté, le développement local, le test d'une nouvelle activité, etc. La réalisation de profits est possible, mais cela ne doit pas être l'objectif premier de l'entreprise.

## C <!-- collapsable:close -->

### CADA

[*Commission d'accès aux documents administratifs*](https://www.cada.fr/).

### Caisse des dépôts

Groupe financier public au service du développement économique du pays depuis 1816.

Investisseur de long terme, notamment par l'intermédiaire de sa filiale CDC Entreprises, il facilite l'accès à l'innovation et la croissance durable des entreprises françaises grâce à sa bonne connaissance des enjeux locaux (le groupe est présent en région) et sa capacité à construire des liens avec le secteur privé.

[> Site de la Caisse des dépôts](https://www.caissedesdepots.fr/)

### CAP

*Certificat d'aptitude professionnelle*.

### Capital social

Montant cumulé des apports en numéraire ou en nature réalisés par les associés ou les actionnaires lors de la constitution de l'entreprise (ou par la suite, lors d'une augmentation de capital).

Le montant minimal du capital social est déterminé par la loi pour chaque type de société.

### Capitaux propres

Différence entre la valeur des biens d'une entreprise et celle de ses dettes à l'égard des tiers.

Leur valeur comptable figure au passif du bilan de l'entreprise. Dans un projet de création d'entreprise, les capitaux propres sont les capitaux apportés par les créateurs de l'entreprise (capital et comptes courants d'associés), à la différence des capitaux empruntés qui, eux, sont prêtés à l'entreprise (par des banques, par exemple).

### Carte de résident

Carte pouvant être attribuée à une personne étrangère ayant résidé de façon ininterrompue au moins trois ans en France. Elle est valable 10 ans et renouvelable de plein droit à l'expiration de cette période.

### Carte de séjour

Carte que doit posséder tout étranger qui désire séjourner en France. Sa durée de validité ne peut être supérieure à un an, mais elle est renouvelable.

### Casier judiciaire

État des condamnations pénales prononcées à l'encontre d'une personne physique ou morale, comprenant également les décisions commerciales, civiles et administratives qui privent les personnes physiques de l'exercice de certains droits.

Le « Casier judiciaire national » est un service du ministère de la Justice. Le bulletin n° 3, qui peut être délivré aux intéressés qui en font la demande, comporte notamment les condamnations à un emprisonnement supérieur à 2 ans sans sursis ainsi que certaines interdictions, déchéances, incapacités en cours d'exécution.

### CCI

*Chambre de commerce et d'industrie* (voir cette entrée).

### CCI France

[CCI France](https://www.cci.fr/) est la tête de réseau des chambres de commerce et d'industrie. Elle remplace l'ACFCI depuis septembre 2012.

### CCMSA

[*Caisse centrale de mutualité sociale agricole*](https://www.msa.fr/lfy/accueil).

### CDC

*Caisse des dépôts et consignations*, aujourd'hui dénommée « Caisse des dépôts » (voir cette entrée).

### CDD

*Contrat à durée déterminée*.

### CDI

*Contrat à durée indéterminée*.

### Centre de formalités des entreprises (CFE)

Lieu de passage obligatoire (guichet unique) auprès duquel les créateurs déposent, en une seule fois et avec un seul formulaire (« liasse unique ») les déclarations qu'ils sont tenus d'effectuer lors de la création, de la modification ou de la cessation de leur activité.

### Centre de gestion agréé

Organisme ayant pour objet de fournir aux entreprises commerciales, industrielles, artisanales et agricoles une aide technique en matière de gestion et de fiscalité et de les inciter à développer l'usage de la comptabilité. Les adhérents à un CGA bénéficient de certains avantages fiscaux et notamment d'un abattement sur leurs bénéfices imposables.

### Cerfa

*Centre d'enregistrement et de révision des formulaires administratifs*. Par extension : les formulaires administratifs normalisés.

### Cession

Vente ou donation d'un bien ou d'un droit.

### Chambre de métiers

Voir l'entrée « Chambres de métiers et de l'artisanat » (CMA).

### Chambres d'agriculture

Établissements publics professionnels, sous tutelle du ministère de l'Agriculture, qui ont pour mission de représenter les intérêts de l'agriculture et du monde rural auprès des pouvoirs publics. Elles assurent également un rôle essentiel de service auprès des agriculteurs. L’APCA est l’ Assemblée permanente des chambres d’agriculture. L’APCA est l’[Assemblée permanente des chambres d’agriculture](https://chambres-agriculture.fr/).

### Chambres de commerce et d'industrie (CCI)

Établissements publics administrés par des commerçants et industriels élus par leurs pairs, représentant, à l'échelon local, les métiers du commerce et de l'industrie. Elles sont également chargées de défendre les intérêts généraux du commerce et de l'industrie et assurent le rôle de CFE (centre de formalités des entreprises) pour les commerçants, les sociétés commerciales. Elles organisent des actions de formation, d'accompagnement et de soutien aux entreprises. CCI France est leur tête de réseau. [CCI France](https://www.cci.fr/) est leur tête de réseau.

### Chambres de métiers et de l'artisanat (CMA)

Établissements publics dirigés par des artisans élus. Installées dans chaque région, les chambres de métiers et de l'artisanat représentent les intérêts généraux de l'artisanat. Elles organisent des actions de formation des apprentis, d'accompagnement et de soutien aux entreprises artisanales.

Elles assurent le rôle de CFE (centre de formalités des entreprises) pour les entreprises artisanales et tiennent le répertoire des métiers qui recense l'ensemble des entreprises artisanales.

Elles attribuent aux entrepreneurs la qualification artisanale (artisan, maître artisan) selon des conditions définies par décret. L'[APCMA](https://cma-france.fr/) est leur tête de réseau.

### Chiffre d'affaires

Montant total des factures émises sur des tiers par une entreprise. C'est le total des ventes de biens ou de prestations de services effectuées au cours d'une période donnée.

### CHR

Entreprises et établissements de la branche des *cafés, des hôtels et des restaurants*.

### Cipav

[*Caisse interprofessionnelle de prévoyance et d'assurance vieillesse*](https://www.lacipav.fr/).

### CNIL

[*Commission nationale de l'informatique et des libertés*](https://www.cnil.fr/).

### Code APE

Code de l'activité principale exercée, composé de 4 chiffres et 1 lettre correspondant à la nomenclature d'activités françaises (NAF). Il est parfois appelé « code NAF ».

[> Consultez l'article consacré au code APE](https://www.guichet-entreprises.fr/fr/aide/code-ape/)

### Code NAF

Par abus de langage, le « code NAF » désigne le code APE (voir cette entrée).

### Collectivité territoriale

Communes, départements, régions, départements, territoires d'outre-mer et le regroupement de ces entités (communautés de communes).

### COM

*Collectivités d'outre-mer*.

### Commerçant

Un commerçant est une personne qui effectue des actes de commerce et en fait sa profession habituelle ([article L. 121-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006219167&cidTexte=LEGITEXT000005634379&dateTexte=20000921) du Code du commerce). Le commerçant est immatriculé au registre du commerce et des sociétés (RCS). Il agit en son nom et pour son propre compte.

### Commissaire aux apports

Professionnel chargé d'évaluer les apports en nature au capital d'une société. Il est désigné, selon les structures, par les associés ou par le président du tribunal de commerce.

### Compte de résultat

Le compte de résultat est un des tableaux figurant dans les comptes annuels. Il récapitule et compare les produits (le chiffre d'affaires) et les charges de l'exercice. Il fait apparaître une différence qui constitue le bénéfice ou la perte de l'exercice (que l'on appelle « résultat » de l'exercice). Ce tableau permet de constater que l'entreprise est rentable (elle dégage des profits) ou non.

### Comptes annuels

Composés du bilan, du compte de résultat et de l'annexe, ils doivent être établis à la clôture de chaque exercice par les entreprises soumises à un régime réel d'imposition dans la catégorie des bénéfices industriels et commerciaux (BIC).

Les sociétés commerciales (SARL, SA, SAS, etc.) doivent obligatoirement les déposer au greffe du tribunal de commerce dans le mois qui suit l'assemblée générale ordinaire qui les a approuvés.

### Concubin

Personne qui vit en couple sans être mariée.

### Conformité

Fait de respecter certaines règles.

### Conseil d'administration

Organe collégial de gestion des sociétés anonymes, composé de 3 membres au moins et de 24 membres au plus. Le conseil d'administration est investi des pouvoirs les plus étendus pour agir en toute circonstance au nom de la société dans les limites définies par les statuts.

### Conseil de surveillance

Organe de la société anonyme de type dualiste (directoire/conseil de surveillance), chargé de contrôler la gestion de la société effectuée par le directoire.

Il est composé de 3 à 18 membres au plus, actionnaires de la société. Aucun membre du conseil de surveillance ne peut faire partie du directoire.

### Contrat d'assurances

Les engagements réciproques de l'assureur et de l'assuré sont détaillés dans le contrat d'assurances qui comporte deux parties : les conditions générales et les conditions particulières.

Les conditions générales sont communes à l'ensemble des assurés garantis auprès de la même société, pour un type de contrat déterminé. Elles expliquent le fonctionnement du contrat et détaillent l'ensemble de ses garanties.

Les conditions particulières personnalisent le contrat en l'adaptant à la situation de chaque assuré : coordonnées, garanties choisies, montant de la cotisation, etc. Ce sont les seuls documents signés par l'assureur et par l'assuré.

### Contrat de travail

Contrat par lequel une personne s'engage à travailler sous la supervision d'une autre en contrepartie d'une rémunération.

### Conventionnel

Etabli par un accord entre plusieurs personnes.

### Cotisation foncière des entreprises (CFE)

Remplace la taxe professionnelle.

### Couverture sociale

Protection dont bénéficient les assurés sociaux (remboursement de certains soins, médicaments, appareillage, etc.).

### CPAM

[*Caisse primaire d'assurance maladie*](https://www.ameli.fr/).

### CRDS

*Contribution pour le remboursement de la dette sociale*.

### CSG

*Contribution sociale généralisée* : impôt, proportionnel aux revenus, payé par tous les contribuables et destiné à financer les assurances sociales.

## D <!-- collapsable:close -->

### Dividendes

Part des bénéfices d'une société attribuée à chaque associé.

### DOM

*Département(s) d'Outre-mer*.

### Domicile

Lieu d'habitation officiel et habituel.

### Domiciliation

Établissement du siège social au domicile du représentant légal de la société.

### Droit au bail

Indemnité demandée par un commerçant titulaire d'un bail commercial à celui qui prend sa suite dans la location du local commercial qu'il occupait.

### Droit d'auteur

Droit que possède un auteur sur une œuvre de l'esprit originale, du seul fait de sa création.

Sont considérées comme « œuvres de l'esprit » :

* les œuvres littéraires : thèses, romans, pièces de théâtre, etc. ;
* les œuvres d'art : peintures, sculptures, créations, plans d'architecte, photographies ;
* les œuvres musicales ou audiovisuelles ;
* les logiciels.

Le droit d'auteur est composé d'un « droit moral » (droit de divulgation, droit de paternité, droit au respect de l'œuvre, droit de repentir et droit de retrait) et d'un « droit patrimonial » (rémunération). Ce dernier confère à l'auteur le droit exclusif d'autoriser ou d'interdire toute utilisation de ses œuvres et de percevoir une rémunération notamment au titre des droits de reproduction ou de représentation.

## E <!-- collapsable:close -->

### EARL

*Exploitation agricole à responsabilité limitée* : société civile ayant une activité agricole employant jusqu'à 10 associés, et dont les associés sont responsables dans la limite de leur apport.

### EEE

*Espace économique européen* : espace composé des 28 États membres de l'Union européenne (UE) et de 3 États (la Norvège, l'Islande et le Liechtenstein) appartenant à l'Association européenne de libre-échange (AELE).

### EIRL

*Entrepreneur individuel à responsabilité limitée* (EIRL) : régime proposé aux entrepreneurs individuels qui souhaitent limiter l'étendue de leur responsabilité. Pour cela ils constituent un patrimoine d'affectation dédié à leur activité professionnelle sans constituer de société. Il ne s'agit pas d'une nouvelle forme juridique.

L'EIRL reprend les caractéristiques de l'entreprise individuelle mais s'en distingue sur deux points : l'étendue de la responsabilité et la possibilité d'opter, dans certains cas, pour l'impôt sur les sociétés.

#### Bon à savoir

Depuis le 15 février 2022, suite à l’adoption de la loi n° 2022-172 en faveur de l’activité professionnelle indépendante et de l’entrée en vigueur du statut unique de l’entrepreneur individuel à compter du 15 mai 2022, il n’est plus possible de choisir le statut de l’EIRL.

S’il n’est plus possible de créer sous ce statut, les EIRL déjà immatriculées continuent d'exercer leurs activités dans les mêmes conditions.

### Enseigne

L'enseigne est un panneau lumineux ou non, à caractère informatif, publicitaire ou décoratif (voire les trois), généralement à destination du public.

L'enseigne est également un des éléments incorporels du fonds de commerce. Elle est le prolongement du nom commercial. C'est la dénomination sous laquelle, en dehors du nom, le commerce est exercé et connu du public, que le commerçant appose sur sa vitrine, ses voitures de livraison, etc.

### Entreprise

Personne physique exerçant une activité professionnelle non salariée, ou personne morale constituée pour la production de biens ou la réalisation de prestations de services, autonome financièrement et juridiquement. La plupart des entreprises n'ont qu'un seul établissement, mais les plus importantes en ont plusieurs.

### Entreprise individuelle

Une entreprise individuelle est une entreprise dans laquelle l'entrepreneur (ou entrepreneur individuel) exerce son activité professionnelle en son nom propre. L'entrepreneur est indéfiniment responsable des dettes professionnelles sur l'ensemble de son patrimoine personnel. Le choix du régime matrimonial peut donc s'avérer important. L'entreprise individuelle peut avoir des salariés.

### Établissement

Unité économique dépendante d'une entreprise pouvant constituer une entreprise ou une fraction de l'entreprise. Exemple : bureau, atelier, usine, etc.

### État membre

État membre de l'Union européenne (UE)

### EURL

*Entreprise unipersonnelle à responsabilité limitée* : SARL à associé unique.

### Exercice social

Durée de l'exercice comptable d'une entreprise.

La durée d'un exercice comptable est en principe de 12 mois.

Toutefois, elle peut être différente lorsqu'il s'agit du premier exercice :

* s'il s'agit d'une société imposée à l'IS, le premier exercice peut aller jusqu'au 31 décembre de l'année qui suit la création de l'entreprise ;
* s'il s'agit d'une entreprise individuelle ou une société de personnes imposées à l'IR, le premier exercice ne peut pas en principe dépasser le 31 décembre de l'année de création.

### Exonération

Dispense de paiement.

### Expert-comptable

L'expert-comptable est un professionnel libéral indépendant, qui intervient, à la demande du chef d'entreprise notamment dans les domaines suivants :

* établissement des comptes annuels ;
* gestion de l'entreprise ;
* obligations légales de l'entreprise ;
* audit de l'entreprise ;
* informatisation de l'entreprise, etc.

Après une solide formation théorique et pratique (bac +8), il doit obligatoirement, pour exercer la profession, être inscrit à l'Ordre des experts-comptables, après avoir prêté serment. Il est soumis à une déontologie rigoureuse dans l'intérêt de ses clients.

### Exploitation agricole à responsabilité limitée (EARL)

Société civile ayant une activité agricole employant jusqu'à 10 associés, et dont les associés sont responsables dans la limite de leur apport.

## F <!-- collapsable:close -->

### Fonds de commerce

Ensemble des éléments mobiliers corporels (matériel, outillage, mobilier, agencements..) et incorporels (clientèle, nom commercial, enseigne, droit au bail, brevets, marques, licences…) que possède une entreprise commerciale ou industrielle.

### Fonds propres

Appellation générique évoquant dans une entreprise les ressources financières durables qui « appartiennent » directement ou indirectement à celle-ci : capital, réserves, bénéfice, report à nouveau, subvention d'investissement, provisions réglementées par opposition aux financements externes (provenant de tiers).

En création d'entreprise, les fonds propres évoquent l'apport personnel ou le capital social de l'entreprise.

### Franchise

Système de commercialisation de produits, services ou technologies reposant sur une étroite collaboration entre deux entreprises juridiquement et financièrement indépendantes l'une de l'autre. Moyennant une contribution financière, une entreprise (le franchisé) acquiert auprès d'une autre entreprise (le franchiseur), le droit d'utiliser son enseigne et/ou sa marque, son savoir-faire, de commercialiser ses produits ou services, conformément aux directives prévues dans le contrat, tout en bénéficiant d'une assistance commerciale et technique.

## G <!-- collapsable:close -->

### Gérant

Dirigeant d'une société de personnes ou d'une SARL/EURL.

### Grande entreprise

Selon la Commission européenne, une « grande entreprise » est une entreprise qui occupe au moins 5 000 personnes (salariés, dirigeants, associés, etc.) et qui réalise plus de 1,5 Md € de chiffre d'affaires ou présente plus de 2 Md € de total de bilan.

### Greffe

Bureau qui garde les dossiers des procès, tient les registres et enregistre les déclarations.

### Greffe du tribunal de commerce

Office assurant les services administratifs du tribunal : tenue des registres, mises à jour des dossiers, conservation des minutes, accueil, etc. Le greffe est par ailleurs chargé de la tenue du registre du commerce et des sociétés (RCS), de la gestion des procédures collectives et de la tenue du fichier des sûretés (nantissements et privilèges). Il remplit également le rôle de centre de formalités des entreprises pour les agents commerciaux, les sociétés civiles, les groupements d'intérêt économique (GIE) et les établissements publics industriels et commerciaux (EPIC).

Il s'agit d'une charge (ou « office ministériel ») au même titre que les offices des huissiers de justice ou notaires.

### Groupement d'intérêt économique (GIE)

Regroupement de plusieurs entreprises préexistantes, entreprises individuelles ou sociétés, en vue de faciliter ou de développer leur activité économique, tout en conservant leur indépendance. Le GIE est immatriculé au registre du commerce et des sociétés (RCS).

### Groupement européen d'intérêt économique (GEIE)

Structure juridique européenne de coopération entre plusieurs entreprises de nationalités différentes. Le GEIE est immatriculé auprès du registre compétent de l'État où est situé son siège social.

## I <!-- collapsable:close -->

### Immobilisation

Une immobilisation est un bien ou une valeur qu'une société achète (ou produit pour elle-même) dans le but d'être utilisé pour une durée supérieure à un an.

Elle est inscrite au bilan de l'entreprise. L'ensemble des immobilisations constitue « l'actif immobilisé ».

Il existe plusieurs catégories d'immobilisations :

* les immobilisations corporelles : machines, mobilier, véhicules, ordinateurs, terrains, bâtiments ;
* les immobilisations incorporelles : frais de constitution de l'entreprise, frais de premier établissement, brevets, marques, fonds de commerce, licences ;
* les immobilisations financières : actions, obligations, dépôts et cautionnements.

### Impôt sur le revenu (IR)

Il s'agit de l'un des deux modes d'imposition des bénéfices des entreprises, l'autre étant l'impôt sur les sociétés.

L'IR concerne principalement les entreprises individuelles, les EURL, les sociétés de personnes et les autres sociétés sur option.

### Impôt sur les sociétés (IS)

Cet impôt sur les bénéfices de l'entreprise concerne :

* de plein droit les sociétés de capitaux (SARL, SA, SAS, SCA et SCS) ;
* les associations qui réalisent des bénéfices ;
* et sur option les autres sociétés (EURL, SNC, etc.).

### INPI

L'[*Institut national de la propriété industrielle*](https://www.inpi.fr/fr) (INPI) est un établissement public français chargé de délivrer les brevets, les marques, les dessins et les modèles et de donner accès à l'information sur la propriété industrielle et les entreprises. Il participe aussi à la lutte contre la contrefaçon.

### Insee

[*Institut national de la statistique et des études économiques*](https://www.insee.fr/fr/accueil). L'Insee est une direction générale du ministère de l'Économie et des Finances. Il a pour mission de collecter, analyser et diffuser des informations sur l'économie et la société française sur l'ensemble de son territoire.

### Investissement

Emploi de capitaux pour renforcer le potentiel d'une entreprise. Les investissements se retrouvent au bilan de l'entreprise sous forme d'immobilisations (actif immobilisé).

Par exemple, acheter un ordinateur ou un véhicule utilitaire, faire construire un entrepôt, prendre une participation dans le capital d'un fournisseur constituent des investissements.

### IR

*Impôt sur le revenu* (voir cette entrée).

### IS

*Impôt sur les sociétés* (voir cette entrée).

## J <!-- collapsable:close -->

### Journal d'annonces légales

Journal d'information (quotidien ou hebdomadaire) habilité à publier les annonces des sociétés relatives à leur création, à leurs modifications statutaires et à leur disparition, dans le département de leur siège social. La liste de journaux habilités, ainsi que le tarif applicable par ligne, sont fixés pour chaque département par arrêté préfectoral.

## K <!-- collapsable:close -->

### KBIS (extrait)

Extrait délivré par le greffe du tribunal de commerce à tout intéressé souhaitant obtenir des informations juridiques et financières sur une société immatriculée au registre du commerce et des sociétés (RCS). Pour une entreprise individuelle il s'agit de l'extrait K.

## L <!-- collapsable:close -->

### Légataire

Personne ou institution bénéficiant des biens ou de la fortune d'une personne décédée.

### Libérale (profession)

Voir l'entrée « Profession libérale ».

### Libre prestation de services (LPS)

C'est la libre circulation des services entre États membres offrant aux prestataires la sécurité juridique nécessaire à l'exercice effectif de cette liberté fondamentale garantie par le Traité.

[> Lire l'article sur la libre prestation de services](../libre_prestation_services.md)

### Licence

Autorisation administrative permettant d'exercer un commerce ou une activité réglementée.

Autorisation d'exploiter un brevet d'invention.

## M <!-- collapsable:close -->

### Mandataire

Personne chargée d'agir au nom d'une autre personne.

### Mensualité

Somme payée chaque mois.

### Micro-entrepreneur (ex-autoentrepreneur)

Ce régime s'adresse à toute personne souhaitant exercer une activité commerciale, artisanale ou libérale à titre principal ou complémentaire et relevant du régime fiscal de la micro-entreprise. Le micro-entrepreneur bénéficie d'un calcul simplifié de ses charges sociales.

[> Lire l'article sur le régime du micro-entrepreneur](micro_entrepreneur/le_regime_micro_entrepreneur.md)

### Micro-entreprise

Cette notion est d'abord utilisée à des fins d'analyse statistique ou économique pour identifier une entreprise qui occupe moins de 10 personnes (dirigeants compris) et qui a un chiffre d'affaires annuel ou un total de bilan n'excédant pas 2 millions €.

Elle est également utilisée pour désigner un régime fiscal ultra-simplifié de détermination du bénéfice imposable des entreprises individuelles qui se situent en dessous d'un certain seuil de chiffre d'affaires : le régime fiscal de la « micro-entreprise ».

### MSA

[*Mutualité sociale agricole*](https://www.msa.fr/lfy/accueil).

## N <!-- collapsable:close -->

### NAF

Nomenclature d'activités française (voir l'entrée « code APE »).

[> Consultez l'article sur le code APE](https://www.guichet-entreprises.fr/fr/aide/code-ape/)

### Nom commercial

Appellation ayant pour but d'identifier une entreprise commerciale.

### Nom de domaine

Nom donné à un site internet. L'utilisation, pour un nom de domaine, d'une appellation appartenant à autrui peut entraîner des poursuites judiciaires.

### Non lucratif

Voir l'entrée « But non lucratif ».

### Notification

Document qui informe une personne d'une décision la concernant (et qui ouvre généralement un délai de procédure).

### Numéro Siren

Voir l'entrée « Siren ».

### Numéro Siret

Voir l'entrée « Siret ».

## O <!-- collapsable:close -->

### Obligation solidaire

Obligation pour chacun des débiteurs communs de payer l'intégralité d'une dette.

## P <!-- collapsable:close -->

### Pacs

*Pacte civil de solidarité*.

### Parts sociales

Une part sociale est un titre de propriété sur le capital d'une entreprise.

Une part sociale est détenue par :

* un associé d'une société à statut commercial n'ayant pas celui de société par actions (cas par exemple des SARL en France) ;
* ou un sociétaire d'une coopérative ou mutuelle.

Une part sociale donne notamment à son détenteur :

* un droit de vote à l'assemblée générale (« une part, une voix » pour les sociétés commerciales, « un homme, une voix » pour les coopératives) ;
* une participation pécuniaire aux bénéfices (versement de dividendes pour les sociétés, d'intérêts pour les coopératives).

### Patrimoine

Ensemble des biens, créances et dettes d'une personne.

### Pépinière d'entreprises

Une pépinière d'entreprises est un organisme qui apporte un soutien fort à certains types de créateurs ou de jeunes entreprises en :

* leur proposant une solution d'hébergement dans des locaux modernes, fonctionnels et adaptés à leurs besoins moyennant un loyer à des conditions avantageuses pour une durée pouvant aller jusqu'à 23 mois ;
* leur offrant une palette de services sur place : assistance (conseils juridiques et de gestion, facilitation), formation, animation, secrétariat partagé, salle de réunion, équipements divers (reprographie, vidéoprojecteur, etc.).

Chaque pépinière a ses propres critères pour sélectionner les entreprises hébergées.

Résider en pépinière permet de limiter les frais fixes et de transformer certaines charges fixes en charges variables (exemple : coût des photocopies payées à l'unité au lieu d'acquérir un photocopieur).

### Personne morale

Groupe qui a une existence juridique et donc des droits et des obligations (institutions, organismes, sociétés, associations, établissements publics, etc.).

### Personne physique

Par opposition au terme juridique de personne morale qui désigne une entité (une entreprise, un groupe, etc.), il s'agit d'un individu (d'une personne).

### Plan d'affaires

Dossier écrit de présentation d'un projet de création d'entreprise (appelé aussi "*business plan*"). Il présente tous les aspects du projet : les créateurs, le produit ou le service, le marché (les clients), les moyens techniques qui seront mis en œuvre, les moyens humains, le coût de ces moyens, les prévisions financières, le cadre juridique retenu, le planning prévu et tout autre aspect utile pour que le lecteur comprenne le projet. Ce document est indispensable pour expliquer un projet à un tiers et discuter avec différents partenaires (investisseurs, banquiers, administrations, etc.). Il est aussi très utile pour matérialiser une vision commune du projet entre les associés. Au niveau financier, le plan d'affaires comprend, au minimum :

* le compte de résultat prévisionnel ;
* le plan de financement ;
* le plan de trésorerie ;
* le calcul du seuil de rentabilité.

Un plan d'affaires ne doit pas être un « pavé » rebutant à l'avance le lecteur : il ne doit pas dépasser 30 à 50 pages (les annexes : documents techniques, justificatifs, extraits des sources citées, documents de l'étude de marché, courriers, etc., doivent constituer un dossier à part). Un bon plan d'affaires doit être complet, concis, précis, clair, soigné et vendeur.

### Plus-value

Augmentation de la valeur d'un bien entre son achat et sa revente.

### PME

Sigle désignant une *petite ou moyenne entreprise*. Cette notion est utilisée à des fins d'analyse statistique ou économique pour identifier une entreprise qui occupe moins de 250 personnes, et dont le chiffre d'affaires annuel est inférieur à 50 millions € ou dont le total de bilan n'excède pas 43 millions €.

### PMI

*Petite ou moyenne industrie*.

### Préjudice

Dommage matériel ou moral.

### Prestataire de services

Personne fournissant des services.

### Profession libérale

La terminologie « profession libérale » ne désigne pas un statut particulier mais une activité de « nature intellectuelle » reposant sur la pratique personnelle d'une science ou d'un art.

Il faut différencier les professions libérales dites « réglementées » des « non réglementées ».

Les **professions libérales « réglementées »** sont les plus connues. Elles ont été classées dans le domaine libéral par la loi. Il s'agit des architectes, des avocats, des experts-comptables, des géomètres-experts, des médecins, des huissiers de justice, des notaires, des agents généraux d'assurances, etc.

Elles nécessitent une immatriculation dans un ordre ou organisme particulier et, lorsqu'elles sont exploitées en société, disposent de structures spécifiques : SCP, SEL, etc.

Les **professions libérales « non réglementées »** regroupent toutes les professions qui exercent une activité qui n'est ni commerciale, ni artisanale, ni industrielle, ni agricole et qui n'entrent pas dans le domaine des professions libérales réglementées. Il s'agit des consultants, des formateurs, des experts, des traducteurs, etc.

[> Consultez l'article consacré aux professions libérales exerçant sous le statut du micro-entrepreneur](micro_entrepreneur/micro_entrepreneur_profession_liberale_ou_activite_independante.md)

### Profession réglementée

Activité professionnelle dont l'accès, l'exercice ou une des modalités d'exercice est subordonné, directement ou indirectement, à des conditions particulières.

[> Consulter les fiches « professions réglementées » sur le site guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/professions-reglementees/)

### Propriété commerciale

Droit pour un commerçant d'exiger de son propriétaire le renouvellement du bail commercial.

### Propriété industrielle

Droit qui s'attache aux inventions et créations nouvelles.

### Propriété littéraire et artistique

Ensemble des droits pécuniaires et moraux attachés à l'auteur d'une création littéraire ou artistique.

## Q <!-- collapsable:close -->

### Quote-part

Part que chacun reçoit ou paie.

## R <!-- collapsable:close -->

### Raison sociale

Nom attribué à certaines sociétés de personnes (principalement aux sociétés civiles professionnelles), composé du nom d'un ou de plusieurs associés suivi des mots « et compagnie ».

### RCS

*Registre du commerce et des sociétés* (voir cette entrée).

### Récépissé

Écrit par lequel une personne ou une entité reconnaît avoir reçu d'une autre des sommes, des objets en dépôt, ou encore des dossiers.

### Recouvrement

Action pour se faire payer des sommes dues.

### Régime matrimonial

Règles qui déterminent la répartition des biens entre les époux et la manière dont ils sont gérés.

### Régime social des indépendants (RSI)

Voir l'entrée « Séciroté sociale des indépendants ».

### Registre du commerce et des sociétés (RCS)

Fichier tenu par chaque tribunal de commerce ou de grande instance statuant commercialement, ayant pour fonction l'immatriculation sur déclaration des commerçants, des sociétés, et des GIE (Groupement d'intérêt économique).

### Registre spécial des agents commerciaux (RSAC)

Registre d'inscription des agents commerciaux.

### Répertoire des métiers (RM)

Fichier d'immatriculation des artisans tenu par la chambre de métiers et de l'artisanat.

### Répertoire Sirene

Répertoire national dont la gestion est confiée à l'Institut national de la statistique et des études économiques (Insee), alimenté au quotidien par les déclarations des entreprises transmises par les CFE. Il délivre le numéro d'identification de l'entreprise qui est ensuite officialisé par l'inscription dans les registres juridiques.

[> Consulter le répertoire Sirene](https://avis-situation-sirene.insee.fr/)

### Rescrit

Interprétation officielle d'un texte donnée par l'Administration sur la demande d'un administré.

### Résiliation

Acte ou jugement par lequel on met fin à un contrat.

### Responsabilité civile

Obligation de réparer les dommages causés par soi-même, ou par une personne ou un animal dont on est responsable.

### Résultat d'exploitation

Sous total du « compte de résultat » donnant le gain (ou la perte) réalisé par l'activité courante de l'entreprise, mais en prenant en compte les dotations aux amortissements et aux provisions d'exploitation. Le résultat d'exploitation ne tient compte ni des « charges et produits financiers », ni des « charges et produits exceptionnels ».

### Résultat financier

Sous-rubrique du « compte de résultat », il représente la différence entre les produits financiers et les charges financières de l'exercice.

### Résultat net

Solde final du « compte de résultat ».

## S <!-- collapsable:close -->

### SA

*Société anonyme* (voir cette entrée).

### SARL

*Société à responsabilité limitée* (voir cette entrée).

### SAS

*Société par actions simplifiée* (voir cette entrée).

### SASU

*Société par actions simplifiée à associé unique*.

### SCIC

*Société coopérative d'intérêt collectif* : elle a pour objectif la production ou la fourniture de biens et de services d'intérêt collectif qui présentent un caractère d'utilité sociale. Elle permet d'associer au capital celles et ceux qui, salariés, bénéficiaires, bénévoles, collectivités territoriales ou tous autres partenaires, veulent agir ensemble dans un même projet de développement local. Société à capital variable, la SCIC peut adopter la forme SA (Société anonyme) ou SARL (Société à responsabilité limitée) et repose sur une loi propre publiée en juillet 2001.

### SCM

*Société civile de moyens* (voir cette entrée).

### SCOP

*Société coopérative de production* : société commerciale de type SARL (Société à responsabilité limitée) ou SA (Société anonyme) dont les associés ont souhaité se placer sur un strict pied d'égalité selon le principe « un associé = une voix ». En contrepartie des particularités liées à son mode de fonctionnement, et notamment de l'obligation de se constituer un patrimoine propre (réserves financières impartageables), le statut de SCOP présente certains avantages d'ordre fiscal.

Ce statut peut être une bonne solution pour la transmission d'une entreprise à ses salariés.

### SCP

*Société civile professionnelle*.

### SCS

*Société en commandite simple* (voir cette entrée).

### SEL

*Société d'exercice libéral* (voir cette entrée).

### Sécurité sociale des indépendants (SSI)

La [Sécurité sociale des indépendants](https://www.secu-independants.fr/) (SSI) a succédé au RSI en janvier 2020.

Les travailleurs indépendants sont depuis intégrés au régime général de la Sécurité sociale.

### Siège social

Lieu de gestion effectif d'une société déterminant son domicile juridique, sa nationalité, et le ressort juridique auquel elle sera rattachée.

### Siren

Numéro composé de 9 chiffres, attribué par l'Institut national de la statistique et des études économiques (Insee) lors de l'inscription de l'entreprise au répertoire national des entreprises, servant à identifier cette entreprise.

### Siret

Numéro composé de 14 chiffres, attribué par l'Institut national de la statistique et des études économiques (Insee) et identifiant un établissement de l'entreprise. Il se compose du numéro Siren, suivi du numéro NIC comportant 5 chiffres.

### SNC

*Société en nom collectif* (voir cette entrée).

### Société

Contrat par lequel deux ou plusieurs personnes mettent en commun quelque chose dans le but de faire des bénéfices.

### Société à responsabilité limitée (SARL)

Société commerciale dont le capital est divisé en parts sociales et dont les associés sont responsables à concurrence de leur apport.

### Société anonyme (SA)

Société commerciale dite « de capitaux » dont le capital est constitué par souscription d'actions et dont les associés, appelés actionnaires, sont responsables à concurrence de leurs apports. Le montant de son capital minimum est de 37 000 €, et 50 % des apports en espèces doivent être versés au moment de la constitution.

### Société civile de moyens (SCM)

Société fondée par des membres de professions libérales réglementées ou non, désireux de partager des locaux, du matériel (médical, informatique) et des structures administratives dans le but de réaliser une économie.

Les membres de la SCM conservent une totale indépendance de clientèle et de pratique professionnelle. Ce genre de société est à but non lucratif, il s'agit seulement d'un partage de frais, comme deux associés qui peuvent avoir un compte commun.

### Société de portage salarial

Société constituée pour facturer les services rendus par des personnes qui ne souhaitent pas s'immatriculer en qualité de travailleur indépendant et qui leur reversent les sommes encaissées sous forme de salaire.

**À noter**

La légalité de cette pratique, qui s'est développée ces dernières années, est contestée par certains professionnels du droit social.

### Société d'exercice libéral (SEL)

Société spéciale de capitaux ayant comme associés des personnes exerçant des professions libérales.

### Société en commandite simple (SCS)

Société de personne, caractérisée par la présence de deux catégories d'associés. Les premiers appelés commandités, ont le statut d'associés d'une société en nom collectif, (ils doivent donc être obligatoirement commerçants), et sont indéfiniment responsables du passif. Les autres appelés commanditaires sont responsables à hauteur de leur apport.

### Société en cours d'immatriculation

Société se trouvant dans la période comprise entre le dépôt de sa demande d'immatriculation auprès du centre de formalités des entreprises et l'immatriculation elle-même.

### Société en formation

Société se trouvant dans la période de formation comprise entre le moment où les associés décident de constituer la société et l'immatriculation au registre du commerce et des sociétés. Pendant cette période, les actes nécessaires à la création de la société sont accomplis par les associés « au nom et pour le compte de la société en formation ».

### Société en nom collectif (SNC)

Société commerciale dite « de personnes », dont les associés sont indéfiniment et solidairement responsables avec la société des dettes sociales.

Les décisions importantes sont, en principe, prises à l'unanimité.

### Société en participation

Société sans personnalité morale, non publiée au RCS (registre du commerce et des sociétés), créée entre plusieurs personnes physiques ou morales, dans un esprit de collaboration.

### Société par actions simplifiée (SAS)

Société de capitaux très proche de la SA (Société anonyme) dont les actionnaires sont des personnes physique ou morales. Son fonctionnement est plus souple que celui de la SA. Le montant de son capital minimum est libre.

### SSI

Voir l'entrée « Sécurité sociale des indépendants ».

### SSII

*Sociétés de services en ingénierie informatique*.

### Statutaire

Conforme aux règles, aux actes qui constituent une société ou une association et qui précisent ses objectifs, moyens et règles de fonctionnement.

### Statuts

Acte constitutif du contrat d'une société ou d'une association contenant certaines mentions obligatoires relatives à son fonctionnement et à son objet.

### Stipulations

Éléments énoncés en principe dans un contrat.

### Subvention

Aide financière non remboursable accordée à une entreprise, le plus souvent par l'État ou une collectivité locale. On distingue :

* les « subventions d'investissement », dont l'objet est d'aider l'entreprise à acquérir certains équipements ;
* les « subventions d'exploitation », qui viennent compléter son chiffre d'affaires lorsque l'activité entraîne des surcoûts anormaux ou n'est pas de nature à pouvoir être rentabilisée, mais que cette activité est reconnue d'utilité pour la collectivité ;
* les « subventions d'équilibre », destinées à combler en partie ou totalement la perte globale qu'aurait subie une entreprise sans cette subvention, etc.

### Succession

Transmission aux héritiers des biens et des dettes d'une personne décédée.

### Succursale

Établissement commercial ou financier dépendant d'un autre mais disposant d'une certaine autonomie, notamment au niveau de sa gestion.

## T <!-- collapsable:close -->

### Tacite reconduction

Renouvellement automatique du contrat à son terme en vertu d'une clause de tacite reconduction ou par la poursuite des relations contractuelles.

### Titre de formation

Diplômes, certificats et autres titres délivrés par une autorité d'un État membre sanctionnant une formation professionnelle.

### Titre de séjour

Document assurant la reconnaissance par l'autorité publique du droit à séjourner sur le territoire national pour un ressortissant étranger majeur. Un titre de séjour se définit par sa nature juridique, son motif d'admission et sa durée de validité.

### Très petite entreprise (TPE)

Ce terme non juridique désigne généralement la micro-entreprise (entreprise occupant moins de 10 salariés).

### Tuteur

Personne chargée de protéger et de représenter un mineur ou un majeur qui n'a pas toutes ses capacités.

### TVA

La TVA (*taxe sur la valeur ajoutée*) est un impôt indirect sur la consommation touchant presque tous les biens et services consommés ou utilisés en France. C'est le consommateur final qui supporte la charge de la TVA, et non l'entreprise productrice du bien ou du service. L'entreprise facture au client la TVA et la reverse ultérieurement au Trésor public, déduction faite de la TVA payée sur les achats constitutifs de son prix de revient.

## U <!-- collapsable:close -->

### Ucanss

[*Union des caisses nationales de sécurité sociale*](http://extranet.ucanss.fr/portail).

### UE

L'Union européenne est composée de 27 États membres : Allemagne, Autriche, Belgique, Bulgarie, Chypre, Croatie, Danemark, Espagne, Estonie, Finlande, France, Grèce, Hongrie, Irlande, Italie, Lettonie, Lituanie, Luxembourg, Malte, Pays-Bas, Pologne, Portugal, Roumanie, Suède, République Thèque, Slovaquie, Slovénie.

### Urssaf

[*Union de recouvrement des cotisations de sécurité sociale et d'allocations familiales*](https://www.urssaf.fr/portail/home.html).

### Usufruit

Droit d'une personne d'utiliser une chose et d'en recevoir les revenus, mais non d'en disposer.

## V <!-- collapsable:close -->

### Valeur locative

Montant que peut rapporter un bâtiment ou un local loué.

### Valeur mobilière

Titre émis par une société anonyme (SA) représentant les droits d'un actionnaire ou d'un prêteur à moyen ou long terme (obligation).

### Valeur nette comptable

Valeur résiduelle d'un bien de l'entreprise, après déduction de son prix d'acquisition, des amortissements, ou des provisions pour dépréciation le concernant.

### VRP

Voyageur, représentant et placier.