<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="micro-entrepreneur; micro-entreprise; services; chambres; hôte" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur et chambres d’hôtes" -->
<!-- var(key)="fr-creation_entreprise-micro_entrepreneur-micro_entrepreneur_chambres_dhotes" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Micro-entrepreneur et chambres d’hôtes

Les chambres d’hôtes représentaient, en 2020, 10 % des hébergements touristiques marchands en France et près de 21500 loueurs mettaient à disposition une partie de leur logement à la location (source : [Accueillir Magazine](https://www.accueillir-magazine.com/pour-proprietaires-chambres-hotes/secteur-chambres-hotes.html)).

Cette activité, qui peut s’exercer à titre principal ou complémentaire, est encadrée par les articles [L. 324-3](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006813153/) et [D. 324-13](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006813408) et suivants du Code du tourisme.

Les chambres d’hôtes sont définies comme « des chambres meublées situées chez l'habitant en vue d'accueillir des touristes, à titre onéreux, pour une ou plusieurs nuitées, assorties de prestations ».

Cette activité peut s’exercer sous le statut de micro-entrepreneur mais doit respecter certaines obligations.

## Obligations du loueur de chambre d’hôtes

La location de chambre d’hôtes est soumise à plusieurs obligations :

- son activité doit être déclarée au préalable en mairie ;
- le loueur doit accueillir les hôtes à son domicile ;
- sa capacité maximale ne peut excéder 5 chambres et 15 personnes ;
- la prestation comprend l’accueil en chambre meublée et la fourniture du petit-déjeuner et du linge de maison ainsi que le nettoyage de la chambre qui ne peut être facturé en supplément ;
- un confort minimal doit être proposé (chambre d’une surface minimale de 9m2, salle d’eau et toilettes accessibles) ; 

## Régime applicable

La location de chambre d’hôtes est une activité de nature commerciale, elle relève donc de la chambre de commerce et d’industrie (CCI) et doit être enregistrée au registre du commerce et des sociétés (RCS).

**Montant des prélèvements fiscaux et sociaux :**

- son plafond de chiffre d’affaires annuel est de 176 200 € ;
- le taux de cotisation sur le chiffre d’affaires est de 12,8 % (6,4 % jusqu’à la fin du troisième trimestre civil qui suit celui de la date de début d’activité, pour les bénéficiaires de l’aide à la création ou à la reprise d’entreprise - ACRE) ;
- étant imposable au titre des bénéfices industriels et commerciaux (BIC), elle est soumise à un taux de paiement libératoire de l’impôt sur le revenu (IR) de 1 % ;
- le loueur doit verser une contribution additionnelle de 0,1 % de son chiffre d’affaires annuel, au titre de la formation professionnelle ; 
- le seuil de franchise en base de TVA qui lui est applicable est de 85 800 € la 1ère année (seuil de tolérance à 94 300 € les années suivantes si le seuil de la TVA n’a pas été dépassé l’année précédent le dépassement).