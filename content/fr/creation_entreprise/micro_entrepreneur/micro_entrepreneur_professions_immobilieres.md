<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="micro-entrepreneur; micro-entreprise; services" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur et professions immobilières" -->
<!-- var(key)="fr-creation_entreprise-micro_entrepreneur-micro_entrepreneur_professions_immobilieres" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Micro-entrepreneur et professions immobilières

Le secteur immobilier est à la mode et suscite de nombreuses vocations. Ce secteur étant réglementé, l’exercice de certaines activités est réservé aux titulaires d’une carte professionnelle.

Si les agents immobiliers ne peuvent légalement exercer sous le statut de micro-entrepreneur, les agents commerciaux le peuvent. 

## Agent immobilier et agent commercial immobilier, quelles différences ?

Les professions d’agent immobilier et d’agent commercial immobilier sont proches mais ne sont pas soumises à la même réglementation.

L’agent immobilier doit détenir une carte professionnelle (carte T) qui lui permet de réaliser des actes juridiques relatifs à des biens immobiliers.

De fait, la profession d’agent immobilier est exclue du régime micro BIC et du régime de franchise en base de TVA et ne peut donc s’exercer en tant que micro-entrepreneur. 

L’agent commercial en immobilier est un travailleur indépendant mandaté par une agence immobilière. Ne possédant pas de carte professionnelle, il ne peut réaliser d’actes juridiques mais négocie, conseille et assure le suivi des transactions immobilières jusqu’à leur conclusion.

De ce fait, il n’entre pas dans le cadre des exclusions du régime de la franchise en base de TVA, prévu par l’article 293 C du Code général des impôts. Il peut donc très bien opter pour le régime fiscal de la micro-entreprise.