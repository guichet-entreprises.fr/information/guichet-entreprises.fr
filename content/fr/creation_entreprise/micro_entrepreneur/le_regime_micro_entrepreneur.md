﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Le régime du micro-entrepreneur " -->
<!-- var(key)="fr-creation_entreprise-micro_entrepreneur-le_regime_micro_entrepreneur" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Le régime du micro-entrepreneur <!-- collapsable:close -->
==============================

**Le régime du micro-entrepreneur permet une création d'entreprise simplifiée en ce qui concerne les formalités et les déclarations fiscale et sociale.**

**Ce régime est tout particulièrement adapté aux personnes souhaitant développer une activité à titre complémentaire ou expérimental.**

**Il impose, en contrepartie, certaines limites de chiffre d'affaires et de champ d'activité.**

## Micro-entrepreneur : un régime aux multiples avantages

Créé le 4 août 2008 par la loi de modernisation de l'économie, le régime du micro-entrepreneur, appelé à l'origine régime de l'auto-entrepreneur, est le régime le plus simple pour créer une activité indépendante. 

Un micro-entrepreneur est une personne exerçant une activité non salariée en entreprise individuelle ayant opté pour le régime de la micro-entreprise et qui bénéficie d'un régime social particulier, le micro-social, qui n'est pas un statut juridique en tant que tel.

Il est possible d'opter pour ce régime au moment de la déclaration de début d'activité effectuée auprès du CFE.

L'immatriculation du micro-entrepreneur est obligatoire et gratuite :

* au répertoire des métiers (RM) pour les artisans ;
* au registre du commerce et des sociétés (RCS) pour les commerçants.

L'immatriculation est obligatoire et payante :

* au Registre spécial des agents commerciaux (RSAC) pour les agents commerciaux ;
* au Registre spécial des entrepreneurs individuels à responsabilité limitée (RSEIRL) pour les professions libérales ayant opté pour la forme juridique de l'EIRL.

Ses avantages :

* les charges sociales correspondent à un pourcentage du chiffre d'affaires. Si le chiffre d'affaires est nul, il n'y a pas de charges à payer, contrairement au régime de droit commun, dans lequel elles sont forfaitisées ;
* les contributions sociales sont prélevées une fois le chiffre d'affaires réalisé ;
* l'entreprise ne facture pas la TVA, du moins tant que son chiffre d'affaires demeure inférieur aux seuils de franchise en base de TVA (85 500 € pour les activités d'achat/vente et 36 500 € pour les prestations de services).

## Micro-entrepreneur : un chiffre d'affaires plafonné

Les activités commerciales, artisanales et libérales peuvent être exercées sous le régime de micro-entrepreneur à condition de ne pas dépasser un certain chiffre d'affaires annuel : 

* 176 200 € pour les activités de vente de marchandises, de restauration ou d'hébergement ; 
* 72 600 € pour les prestations de services et les professions libérales.

Le chiffre d'affaires doit être déclaré, même s'il est égal à zéro, sur [net-entreprises.fr](https://www.net-entreprises.fr/).

En cas de dépassement du chiffre d'affaires autorisé pendant deux années consécutives, il est basculé automatiquement :

* vers le régime social de droit commun ;
* vers le régime fiscal réel simplifié.

## Micro-entrepreneur : une fiscalité particulière

### L'impôt sur le revenu

Tous les micro-entrepreneurs sont assujettis à l'impôt sur le revenu. 

Le bénéfice du micro-entrepreneur est calculé en appliquant un abattement sur le chiffre d'affaires pour frais professionnels en fonction de la nature de l'activité : 

* de 71 % pour les activités de vente de marchandises, de restauration et d'hébergement ;
* de 50 % pour les prestations de services ;
* de 34 % pour les professions libérales ;

Dans tous les cas, un abattement minimum de 305 € s'applique.

Pour déterminer l'impôt sur le revenu dû, le bénéfice forfaitaire ainsi calculé est intégré dans le revenu global imposable, avec les autres revenus du foyer fiscal, et soumis au barème progressif par tranches de l'impôt sur le revenu.

### Le versement libératoire

Le [versement libératoire](https://www.impots.gouv.fr/portail/professionnel/le-versement-liberatoire) de l'impôt sur le revenu est un mode de paiement de l'impôt simplifié destiné exclusivement aux micro-entrepreneurs.

En 2020, les micro-entrepreneurs dont le montant des revenus du foyer fiscal de l'avant-dernière année (année N-2) n'excède pas 27 519 € peuvent opter pour ce mode de paiement.

Ce seuil est valable pour une part de quotient familial. Il est majoré de 50 % par demi-part ou de 25 % par quart de part supplémentaire.

Le montant du versement libératoire est calculé selon le chiffres d'affaires. Le paiement de l'impôt s'effectue alors en même temps que le paiement de ses charges sociales tous les mois ou tous les trimestres selon la périodicité choisie, aux taux suivants :

* 1 % pour les activités de ventes de marchandises, de restauration ou d'hébergement ;
* 1,7 % pour les prestations de services ;
* 2,2 % pour les professions libérales.

### La cotisation foncière des entreprises (CFE)

Les micro-entrepreneurs sont assujettis à la cotisation foncière des entreprises selon un barème progressif. Ils en sont exonérés pour leur première année d'activité. 

De plus, tous les entrepreneurs dont le chiffre d'affaires est inférieur ou égal à 5 000 € sont exonérés de CFE minimum.

### Les frais de chambre pour les commerçants et les artisans

Le micro-entrepreneur exonéré de cotisation foncière des entreprises (CFE) (chiffre d'affaires inférieur ou égal à 5 000 €) est également exonéré de frais de chambre. 

Pour les micro-entrepreneurs commerçants et micro-entrepreneurs artisans qui ne sont pas exonérés, l'estimation du montant à payer se fait sur la base d'un pourcentage du chiffre d'affaires. La taxe est recouvrée en même temps que les autres cotisations sociales.

## Micro-entrepreneur : détention d'un compte bancaire dédié

Depuis la loi PACTE du 22 mai 2019, la détention d'un compte bancaire dédié à l'activité professionnelle est obligatoire uniquement si le chiffre d'affaires généré est supérieur à 10 000 € pendant au moins deux années consécutives. 

## Micro-entrepreneur : modification du statut

Le micro-entrepreneur peut modifier les informations relatives à son entreprise (nom commercial, changement d'adresse du domicile personnel ou de l'établissement, changement d'activité, etc.) ou changer de régime s'il reste en entreprise individuelle. 

## Micro-entrepreneur : cessation temporaire ou définitive d'activités

A tout moment, le micro-entrepreneur peut cesser son activité de façon temporaire ou définitive. 

Si le micro entrepreneur souhaite suspendre temporairement son activité, il lui suffit d'indiquer, sur sa déclaration mensuelle ou trimestrielle, un chiffre d'affaires égal à zéro, pendant vingt-quatre mois maximum.