﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur artisan" -->
<!-- var(key)="fr-creation_entreprise-micro_entrepreneur-micro_entrepreneur_artisan" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Micro-entrepreneur artisan
==========================

L'artisan est un chef d'entreprise autonome qui assure lui-même toutes les étapes de la production et de la commercialisation de ses produits. Cette activité peut être exercée à titre principal ou secondaire.

L'activité artisanale se définit par la production de services ou produits. Cette activité de production, de transformation, de réparation ou de prestation de services de type [BIC](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32919) (bénéfices industriels et commerciaux) est réalisée grâce à un savoir-faire spécifique. L'artisanat recouvre trois grandes catégories : les métiers du bâtiment, les métiers d'alimentation et les métiers de production et de services. Selon que la profession est réglementée ou pas, les formalités de déclaration sont différentes.

L'activité artisanale devient commerciale dès lors que l'entreprise emploie plus de 10 salariés (ou, en l'Alsace Moselle, dès lors qu'elle utilise des procédés purement industriels).

Le micro-entrepreneur artisan est un professionnel qui relève du régime fiscal de la micro-entreprise ([régime d'imposition micro-BIC](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32919) (bénéfices industriels et commerciaux) et [régime de la franchise en base de TVA](https://www.service-public.fr/professionnels-entreprises/vosdroits/F21746)), à qui s'applique de droit le régime micro-social simplifié prévu à l'[article L. 133-6-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000019285616&dateTexte=&categorieLien=cid) du Code de la Sécurité sociale.

Pour plus d’informations, consultez l’article sur [le régime du micro-entrepreneur](./le_regime_micro_entrepreneur.md).

Certaines activités artisanales sont soumises à une **qualification professionnelle**. La liste des professions concernées est disponible sur le site [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/dqp/artisanat/index.html). Les activités artisanales sont inscrites au répertoire des métiers ([annexe du décret n° 98-247](http://legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0F6D69D7A7595283840D4B6E1396FF9D.tpdila09v_2?idArticle=LEGIARTI000030665874&cidTexte=LEGITEXT000005625550&dateTexte=20150824)) du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

## Domaines d'activités soumis à une qualification professionnelle <!-- collapsable:close -->

Ne peuvent être exercées que par une personne qualifiée professionnellement ou sous le contrôle effectif et permanent de celle-ci les activités :

* d'entretien et de réparation des véhicules et des machines ;
* de construction, d'entretien et de réparation des bâtiments ;
* de mise en place, d'entretien et de réparation des réseaux et des équipements utilisant les fluides, ainsi que des matériels et équipements destinés à l'alimentation en gaz, au chauffage des immeubles et aux installations électriques ;
* de ramonage ;
* de soins esthétiques à la personne autres que médicaux et paramédicaux et de modelages esthétiques de confort sans finalité médicale ;
* de réalisation de prothèses dentaires ;
* de préparation ou de fabrication de produits frais de boulangerie, pâtisserie, boucherie, charcuterie et poissonnerie, ainsi que de préparation ou de fabrication de glaces alimentaires artisanales ;
* de maréchal-ferrant.

Exigences requises (diplômes, certificats, etc.) pour prétendre à l'exercice d'une activité artisanale réglementée (liste non exhaustive, plus de renseignements sur [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/dqp/artisanat/index.html)).

La personne qualifiée (qui peut être l'entrepreneur lui-même, mais aussi son conjoint s'il participe à l'activité, ou même un salarié) doit être titulaire :

* soit d'un certificat d'aptitude professionnelle ;
* soit d'un brevet d'études professionnelles ;
* soit d'un diplôme ou d'un titre de niveau égal ou supérieur homologué ([loi n° 96-603](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000193678&categorieLien=cid) du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat). Pour connaître le niveau de votre diplôme, vous pouvez consulter le [Répertoire national des certifications professionnelles](http://www.rncp.cncp.gouv.fr/) (RNCP).

À défaut de diplôme ou de titre, d'un niveau V dans le métier exercé, cette personne doit justifier d'une expérience professionnelle de trois années effectives sur le territoire de l'Union européenne (UE) ou d'un autre État partie à l'accord de l'Espace économique européen (EEE) acquise en qualité de dirigeant d'entreprise, de travailleur indépendant ou de salarié dans l'exercice de l'un des métiers prévus dans la liste des activités réglementées.

### Cas particuliers

#### L'obligation de souscrire une assurance pour les activités du secteur du bâtiment

La personne qui exerce son activité dans le secteur du bâtiment doit mentionner sur ses devis et factures l'assurance professionnelle qu'elle a souscrite pour son activité, lorsqu'elle est obligatoire, ainsi que les coordonnées de sa compagnie d'assurance et la couverture géographique de son contrat.

#### Se prévaloir de la qualité d'artisan

Pour pouvoir utiliser le terme « artisan » ou ses dérivés, vous devez être titulaire d'un diplôme de niveau V ou équivalent dans votre métier ou avoir trois ans d'expérience professionnelle. Au moment de votre inscription, la CMA vous demandera de justifier de ces conditions pour pouvoir mentionner votre qualité au répertoire des métiers et sur la carte professionnelle qui vous sera délivrée.

#### Se prévaloir de la qualité d'artisan d'art

La qualité d'artisan d'art reconnaît une qualification professionnelle envers un métier d'art mentionné dans [la liste fixée par arrêté](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031941949). La qualité d'artisan d'art est attribuée dans les mêmes conditions que la qualité d'artisan : être titulaire d'un diplôme de niveau V, justifier de trois ans d'expérience professionnelle et en faire la demande expressément.

Pour toute question relative aux formalités d'entreprises, il convient de se rapprocher de la chambre de métiers et de l'artisanat du lieu de l'établissement principal.

#### Se prévaloir de la qualité de maître artisan/maître artisan en métier d'art

La qualité de maître artisan ou de maître artisan en métier d'art est attribuée sous les mêmes conditions aux chefs d'entreprise individuelle titulaires d'un brevet de maîtrise dans le métier exercé et justifiant d'une expérience professionnelle de deux ans ainsi qu'aux personnes immatriculées au répertoire des métiers depuis au moins dix ans justifiant, à défaut de diplôme, d'un savoir-faire reconnu au titre de la promotion de l'artisanat (ou formations).

Retrouvez plus d'informations sur [le portail de l'artisanat](https://www.artisanat.fr/artisan/valoriser-mon-activite/obtenir-un-titre-de-maitre-artisan).

#### Coiffure

L'exercice de l'activité de coiffure doit être placé sous le contrôle effectif et permanent d'une personne professionnellement qualifiée. De même, l'activité professionnelle de coiffure au domicile des particuliers doit être exercée par une personne qualifiée.

Cette personne (qui peut être l'entrepreneur lui-même, l'un de ses salariés, son conjoint collaborateur) doit être titulaire :

* soit du brevet professionnel de coiffure (BP) ;
* soit du brevet de maîtrise de la coiffure (BM) ;
* soit du diplôme ou titre inscrit ou ayant été inscrit au [Répertoire national des certifications professionnelles](http://www.rncp.cncp.gouv.fr/) (RNCP) dans le même domaine que le brevet professionnel de coiffure et d'un niveau égal ou supérieur.

Pour plus d'informations, veuillez vous reporter à l'article sur [le régime micro-social et micro-fiscal](./le_regime_micro_entrepreneur.md).

## Références <!-- collapsable:close -->

* [Décret n° 98-247](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000571009) du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers
* [Décret n° 98-246](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=B5391EAD59BB6DBBE4DF181DD395A375.tpdjo08v_2?cidTexte=JORFTEXT000000388449&dateTexte=20090831) du 2 avril 1998 relatif à la qualification professionnelle exigée pour l'exercice des activités prévues à l'article 16 de la [loi n° 96-603](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000193678&categorieLien=cid) du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat
* Articles 16 et 19 de la [loi n° 96-603](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000193678&categorieLien=cid) du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat
* Article 1 du [décret n° 97-558](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000383103&categorieLien=cid) du 29 mai 1997 relatif aux conditions d'accès à la profession de coiffeur
* Article 3 de la [loi n° 46-1173](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006068010&dateTexte=20091214) du 23 mai 1946 portant réglementation des conditions d'accès à la profession de coiffeur