<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="micro-entrepreneur; micro-entreprise; services" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur et location de biens et de matériels" -->
<!-- var(key)="fr-creation_entreprise-micro_entrepreneur-micro_entrepreneur_location_biens_materiels" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Micro-entrepreneur et location de biens et de matériels

Vêtements de mariage, perceuses, canoës, œuvres d'art… aujourd'hui tout se loue. L'utilisation ponctuelle de matériels, l'envie de changer ou la logique économique poussent de plus en plus de particuliers à louer de nombreux biens et matériels plutôt qu'à les acheter.

Tous les biens de consommation durable peuvent se louer, il y a donc matière à créer des entreprises dédiées. Le statut de micro-entrepreneur est envisageable pour ceux qui veulent se lancer. 

## Quels biens peut-on proposer à la location ?

Les biens qui peuvent être proposés à la location sont appelés des biens durables, ce qui signifie qu'ils sont utilisables plusieurs fois et qu'ils ont une durée de vie assez longue.

Parmi ceux souvent proposés en location :

- les articles de sport et de loisirs ;
- le mobilier pour événements divers (séminaires, réceptions, mariages, etc.) ;
- le matériel audiovisuel et informatique ;
- le matériel de bricolage et de jardinage ;
- les appareils électroménagers ;
- les véhicules motorisés ou non ;
- les œuvres d'art ;
- les vêtements et accessoires de luxe.

## Conditions d'exercice de la location de biens et de matériels

Aucune formation, certificat ou diplôme n'est requis pour exercer l'activité de location de biens durables.

L'activité de location de biens durables étant considérée comme relevant des prestations de services, elle est soumise, dans le régime micro-social simplifié :

- au plafond de chiffre d'affaires annuel de 72 600 € ;
- au taux d'abattement forfaitaire de 50 % sur le chiffre d'affaires, pour déterminer le bénéfice imposable ;
- au taux de paiement libératoire social de 22 % du chiffre d'affaires ;
- au taux de paiement libératoire de 1,7 % du chiffre d'affaires, au titre de l'impôt sur le revenu ;
- au taux de paiement libératoire de 0,2 % du chiffre d'affaires, au titre de la formation professionnelle.


