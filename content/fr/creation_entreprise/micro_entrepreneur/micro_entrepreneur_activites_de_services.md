<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="micro-entrepreneur; micro-entreprise; services" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur et activités de service" -->
<!-- var(key)="fr-creation_entreprise-micro_entrepreneur-micro_entrepreneur_activites_service" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Micro-entrepreneur et activités de services

Depuis quelques années, les activités de services se sont  développées grâce, en partie, au régime du micro-entrepreneur.

De nombreuses activités sont concernées : focus sur trois d’entre elles.

## Les services à la personne <!-- collapsable:open -->

Les services à la personne regroupent 26 activités exercées au domicile des clients : garde d’enfants, assistance aux personnes vulnérables, aide-ménagère, soutien scolaire, travaux de bricolage, etc.

Parmi elles, seules deux activités ne peuvent s’exercer sous le régime du micro-entrepreneur : l’assistante maternelle à domicile, qui ne peut être exercée qu’à titre salarié, et le jardinage à domicile, qui est obligatoirement rattaché à la Mutualité sociale agricole (MSA).

Avantage important pour les personnes utilisant les services d’un prestataire, la possibilité de bénéficier d’une réduction d’impôt de 50 %. Pour cela, le micro-entrepreneur doit disposer d’un agrément, d’une autorisation ou effectuer une déclaration facultative, selon l’activité de services à la personne exercée.

Les activités de services à la personne faisant l’objet d’une déclaration sont listées au II de l’[article D. 7231-1](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=9F2A07A5DF68DA4DD211F8DF632EC683.tplgfr29s_1?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000018521280&dateTexte=20180803&categorieLien=cid#LEGIARTI000018521280) du Code du travail.

**Les activités faisant l’objet d’une déclaration :**

- entretien de la maison et travaux ménagers ;
- petits travaux de jardinage ;
- travaux de petit bricolage ;
- garde d’enfants de plus de 3 ans à domicile ;
- soutien scolaire ou cours à domicile ; 
- soins d’esthétique à domicile pour les personnes dépendantes ;
- préparation de repas à domicile ;
- livraison de repas à domicile ;
- livraison de courses à domicile ;
- collecte et livraison à domicile de linge repassé ;
- assistance informatique à domicile ;
- soins et promenade d’animaux de compagnie pour les personnes dépendantes ;
- maintenance, entretien et vigilance temporaires à domicile ;
- assistance administrative à domicile ;
- accompagnement des enfants de plus de 3 ans dans leurs déplacements ;
- télé-assistance et visio-assistance ;
- interprète en langue des signes ;
- assistance aux personnes ayant besoin d’une aide temporaire à leur domicile ;
- conduite du véhicule des personnes en cas d’invalidité temporaire ;
- accompagnement des personnes présentant une invalidité temporaire ;
- coordination et délivrance des services à la personne.

**Les activités soumises à agrément :**

- garde d’enfants de moins de 3 ans et de moins de 18 ans handicapés à domicile ;
- accompagnement d’enfants de moins de 3 ans et de moins de 18 ans handicapés.

**Les activités suivantes sont soumises à agrément si elles sont exercées sous mode mandataire et à autorisation en mode prestataire :**

- assistance aux personnes âgées et aux personnes handicapées ; 
- conduite du véhicule des personnes ayant des difficultés de mobilité ;
- accompagnement des personnes en dehors de leur domicile.

Pour obtenir ces agréments, le micro-entrepreneur doit remplir certaines conditions : 

- avoir un casier judiciaire vierge ; 
- avoir des conditions de moyens, c’est-à-dire être en possession de moyens matériels, humains, financiers nécessaires à cette activité ; 
- remplir une condition d’exclusivité, c’est-à-dire se consacrer exclusivement et entièrement à l’activité de services à la personne.

**Bon à savoir**

On parle de mode mandataire quand un particulier confie à un organisme de services à la personne une tâche par contrat de mandat : recherche de candidatures, recrutement et embauche, calcul et établissement des bulletins de paie, voire du prélèvement à la source, etc.

Le mode prestataire s’applique lorsque l’intervenant qui effectue la prestation au domicile du client est employé par l’organisme de services à la personne.

Pour en savoir plus, rendez-vous sur [servicesalapersonne.gouv.fr](https://www.servicesalapersonne.gouv.fr)

## Chauffeur VTC <!-- collapsable:open -->

La profession de chauffeur de VTC est réglementée : le micro-entrepreneur doit entreprendre certaines démarches avant de commencer son activité.

**L’obtention de la carte professionnelle**

Le chauffeur VTC doit posséder une carte professionnelle préalablement à la création de son activité sous le régime du micro-entrepreneur. 

La demande de carte professionnelle de chauffeur VTC s’effectue par courrier auprès de la préfecture. 

Cette carte est délivrée dans un délai de 3 mois maximum et est valable 5 ans. Son prix est fixé à 57,60 € HT, auxquels s'ajoutent la TVA et l'acheminement par voie postale soit, au total, 62,09 €.

**Inscription au registre des VTC**

Après l’obtention de la carte professionnelle et la création de la microentreprise, il reste une dernière obligation à remplir : l’inscription au registre des VTC.

L’inscription au registre des VTC est obligatoire et se fait [en ligne](https://registre-vtc.din.developpement-durable.gouv.fr/public/accueil.action).

Cette inscription doit être accompagnée : 

- d’une attestation d’assurance de responsabilité civile professionnelle ;
- d’un justificatif de l’immatriculation de l’entreprise ;
- d’une copie de la carte professionnelle ;
- de la carte grise du véhicule ;
- d’un extrait d'immatriculation D1 ;
- d’un justificatif de la garantie financière pour chaque véhicule utilisé de façon régulière (hors événement exceptionnel, salon, etc.), dont le montant est de 1 500 € pour chaque véhicule. La garantie financière n'est pas nécessaire si l'exploitant est propriétaire du véhicule utilisé lors de la prestation de VTC ou si le véhicule fait l'objet d'une location de longue durée (supérieure à 6 mois). Dans ce cas, tout justificatif permettant de vérifier la propriété ou le caractère locatif de longue durée du véhicule doit être fourni.

Cette inscription engendre des frais d’immatriculation de 170 €.

## Livreur <!-- collapsable:open -->

La situation sanitaire depuis quelques mois a entrainé une forte augmentation du nombre livraison à domicile, que ce soit pour les colis ou les repas.

Les plateformes spécialisées travaillent avec des livreurs indépendants qu’elles rémunèrent principalement à la livraison.

Le régime du micro-entrepreneur est le plus souvent choisi par les personnes souhaitant exercer cette activité, que ce soit à vélo ou en véhicule motorisé (moto, voiture, etc.).

**Comment devenir livreur ?**

L’activité de livreur est une activité commerciale, il faut donc être majeur pour l’exercer et résider en France.

Les étrangers, hors Union européenne, résidant en France doivent posséder un titre de séjour les autorisant à exercer une activité indépendante en France.

Le cumul d’activité de livreur sous le statut de micro-entrepreneur et d’une autre activité (étudiant, demandeur d’emploi, salarié, etc.) est autorisé.

**Quels sont les prérequis pour devenir livreur ?**

Il existe des prérequis pour devenir livreur, en premier lieu, posséder un moyen de transport adapté au type de colis livrable : vélo, moto, voiture ou camionnette.

Un smartphone est également nécessaire. Les plateformes contactent les livreurs se trouvant dans un secteur donné pour effectuer la livraison d’un colis par le biais d’une application.

**Le livreur à vélo**

Dans le cas du livreur à vélo, il n’y a pas de qualification ou de formation à suivre.

**Le livreur avec véhicule motorisé**

Le livreur avec véhicule motorisé doit s’inscrire au Registre national des transporteurs afin d’obtenir une licence de transport : capacité de transport de marchandises légères (moins de 3,5 T).

Cette attestation est délivrée suite à une formation payante d’une centaine d’heures se concluant par un examen écrit.

La demande d’autorisation et d’inscription au Registre des transporteurs se fait auprès de la Direction régionale de l’environnement, de l’aménagement et du logement (DREAL) (ou de la [Direction régionale et interdépartementale de l’équipement et de l’aménagement et des transports (DRIEAT)](http://www.driea.ile-de-france.developpement-durable.gouv.fr/contactez-nous-a4575.html?lang=fr&forcer_lang=true) si vous êtes en Île-de-France).

Pour en savoir plus sur les démarches à accomplir, rendez-vous sur [service-public.fr](https://www.service-public.fr/professionnels-entreprises/vosdroits/F31849).

**Bon à savoir**

Toutes les plateformes ne demandent pas l’attestation de capacité professionnelle. Sachez qu’en cas de défaut de possession de cette attestation vous risquez des poursuites lors de contrôle ou d’accident.

**La souscription d’une assurance**

En sus de l’assurance obligatoire du véhicule motorisé, obligatoire, il est recommandé de souscrire à une assurance responsabilité civile professionnelle (RC Pro) pour vous couvrir en cas de dommages causés à un tiers (exemple : renverser un piéton).

Cette assurance est facultative pour les livreurs à vélo mais obligatoire pour les livreurs motorisés.