﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur commerçant-artisan" -->
<!-- var(key)="fr-creation_entreprise-micro_entrepreneur-micro_entrepreneur_commercant_artisan" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Micro-entrepreneur commerçant-artisan
=====================================

Une personne exerçant une double activité commerciale et artisanale doit être inscrite simultanément au [répertoire des métiers](https://www.artisanat.fr/porteur-de-projet/lancer-mon-activite/immatriculation-au-repertoire-des-metiers) (RM) et au registre du commerce et des sociétés (RCS) ([loi du 5 juillet 1996](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000193678&categorieLien=cid)). Le CFE de la chambre de métiers et de l'artisanat est seul compétent pour recevoir la déclaration. Ces formalités peuvent être réalisées sur guichet-entreprises.fr.

De ce fait, un entrepreneur pourra cumuler au sein de la même entreprise une activité commerciale d'achat et vente de biens et de marchandises avec une activité artisanale de production, de transformation, de réparation ou de prestation de services. Ces activités peuvent être tout-à-fait distinctes ou liées (tel est le cas notamment de la transformation de produits ou matières premières avant revente, qui est inhérente à certaines activités de vente au détail).

Lors de l'enregistrement d'une entreprise en micro-entrepreneur avec une activité mixte, l'activité principale se détermine selon celle qui doit générer le plus de revenus. 

**Les micro-entrepreneurs du secteur du bâtiment et de l'artisanat relèvent d'une activité mixte dès lors qu'ils vendent les matières premières des ouvrages qu'ils réalisent.**

## Le régime du micro-entrepreneur en activité mixte

En cas d'activité mixte exercée sous le régime du micro-entrepreneur, ce dernier n'a pas pour conséquence d'augmenter le chiffre d'affaires (CA), mais les deux activités distinctes relèvent de deux seuils différents.

Le plafond à ne pas dépasser reste de :

* 176 200 € pour les activités d'achat-revente de marchandises, d'objets, de fournitures, de denrées à emporter ou à consommer sur place ou encore de prestations d'hébergements ;
* 72 600 € pour les prestations de services.

Si l'activité d'achat-revente constitue l'activité principale, le chiffre d'affaires global ne devra pas dépasser 176 200 €. À l'intérieur de ce montant, le chiffre d'affaires relatif aux prestations de services ne devra pas dépasser 72 600 €.

Pour en savoir plus, reportez-vous à l'[article sur le régime du micro-entrepreneur](./le_regime_micro_entrepreneur.md).

Pour toute question relative aux formalités d'entreprises, il convient de se rapprocher de la chambre de métiers et de l'artisanat du lieu de l'établissement principal.