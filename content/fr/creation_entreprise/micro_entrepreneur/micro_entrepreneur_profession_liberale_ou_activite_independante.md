﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur profession libérale ou activité indépendante" -->
<!-- var(key)="fr-creation_entreprise-micro_entrepreneur-micro_entrepreneur_profession_liberale_ou_activite_independante" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Micro-entrepreneur profession libérale ou activité indépendante
==============================================================

**L'activité libérale** englobe toute activité professionnelle non salariée, non agricole, non commerciale ou non artisanale ; cela recouvre notamment toutes les prestations de services de type [BNC](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32105) (bénéfices non commerciaux). Elle est exercée par un prestataire de services travaillant à titre indépendant et faisant preuve de qualifications spécifiques, vendues à un tiers sous sa propre responsabilité.

**Les professions libérales** désignent les personnes exerçant à titre habituel, de manière indépendante et sous leur responsabilité, une activité de nature généralement civile ayant pour objet d'assurer, dans l'intérêt du client ou du public, des prestations principalement intellectuelles, techniques ou de soins mises en œuvre au moyen de qualifications professionnelles appropriées et dans le respect de principes éthiques ou d'une déontologie professionnelle.

Elles sont réparties en deux catégories : les [professions réglementées](https://www.guichet-qualifications.fr/fr/dqp/index.html) et les professions non réglementées.

**Les professions libérales réglementées** sont classées dans le secteur libéral par la loi. Leur exercice est soumis à des règles déontologiques strictes et sont placées sous le contrôle de leur instance professionnelle (ordre, chambre, ou syndicat). Parmi elles, les plus connues sont les avocats, les experts-comptables ou encore les médecins. Leur titre fait l'objet d'une protection.

**Les professions libérales non réglementées** regroupent les professions qui ne peuvent être classées ailleurs, c'est-à-dire qu'elles ne sont ni commerciales, ni artisanales, ni agricoles et ne relèvent pas du régime général des salariés (journalistes et pigistes, artistes auteurs, intermittents du spectacle, mannequins, arbitres et juges sportifs, etc.). Il s'agit généralement de professions à caractère intellectuel ou artistique. Bien que classées dans la catégorie non réglementée des activités libérales car non contrôlées par une instance professionnelle, leur exercice peut néanmoins être soumis à une réglementation spécifique.

Les fiches d'activités qui encadrent l'exercice d'une profession sont consultables dans la rubrique [Activités réglementées](../../../reference/fr/directive-services/index.md).

Les professions soumises à une réglementation spécifique sont consultables sur [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/dqp/index.html).

## Prérequis <!-- collapsable:close -->

Un micro-entrepreneur est une personne relevant du régime fiscal de la micro-entreprise ([régime spécial des bénéfices non commerciaux](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32105) (BNC) pour l'imposition des bénéfices et [régime de TVA de la franchise en base](https://www.service-public.fr/professionnels-entreprises/vosdroits/F21746)) à qui s'applique de droit le régime micro-social simplifié prévu à l'[article L. 133-6-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000019285616&dateTexte=&categorieLien=cid) du Code de la Sécurité sociale.

Pour en savoir plus, consultez l'article sur [le régime du micro-entrepreneur](./le_regime_micro_entrepreneur.md).

## Déclarer son activité <!-- collapsable:close -->

Vous pouvez déclarer votre activité sur [guichet-entreprises.fr](https://forms.guichet-entreprises.fr/profilEntreprisesV3). Votre déclaration sera ensuite transmise à l'Urssaf pour traitement.

Afin de bénéficier du statut de micro-entrepreneur, le chiffre d'affaires annuel ne doit pas dépasser le seuil du régime micro-fiscal, établi en 2020 à 72 600 € pour les prestations de services libérales.

Pour plus d'informations, consultez l'article sur [le régime du micro-entrepreneur](./le_regime_micro_entrepreneur.md).

**Remarques**

L'inscription de l'activité libérale doit s'effectuer auprès de l'ordre, du syndicat ou de la chambre professionnelle dont le micro-entrepreneur dépend et la déclaration d'activité s'effectue auprès de l'[Urssaf](https://www.urssaf.fr/portail/home.html) qui occupe alors le rôle de CFE (centre de formalités des entreprises).

La [Sécurité sociale – Indépendants](https://www.secu-independants.fr/) est le régime de protection sociale obligatoire qui gère l'assurance maladie des indépendants.

Les activités relevant de la [Caisse nationale des barreaux français](https://www.cnbf.fr/fr/accueil-2) (CNBF) et des sections de la [Caisse nationale d'assurance vieillesse des professions libérales](https://www.cnavpl.fr/) (CNAVPL) autres que la Cipav ne sont pas éligibles à ce statut.

### Liens utiles

* Déclaration et paiement des charges : [le site officiel des déclarations sociales](http://www.net-entreprises.fr/)
* Protection sociale : [Sécurité sociale – Indépendants](http://www.secu-independants.fr/)
* Retraite : [Caisse interprofessionnelle des professions libérales](https://www.lacipav.fr/)

### Références

* Article 43 de la [directive européenne n° 2005/36/CE](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2005:255:0022:0142:fr:PDF) relative à la reconnaissance des qualifications professionnelles
* Article 29 de la [loi 2012-387](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025553296&categorieLien=id#JORFARTI000025553621) du 22 mars 2012 relative à la simplification du droit et à l'allégement des démarches administratives