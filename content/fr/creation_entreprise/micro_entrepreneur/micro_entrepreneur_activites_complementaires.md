<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="micro-entrepreneur; micro-entreprise; complémentaire; étudiant; retraité; mineur; demandeur; salarié" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur et activités complémentaires" -->
<!-- var(key)="fr-creation_entreprise-micro_entrepreneur-micro_entrepreneur_activites_complementaires" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Micro-entrepreneur et activités complémentaires

Créer une entreprise est beaucoup plus facile depuis la création du statut d’auto-entrepreneur en 2009, devenu micro-entrepreneur depuis.

Conçu comme une opportunité de revenus supplémentaires ou comme l’occasion de tester une idée de création d’entreprise, le statut de micro-entrepreneur peut être exercé comme une activité complémentaire pour les salariés, les étudiants, les retraités et les demandeurs d’emploi.

## Étudiants <!-- collapsable:open -->

Pour acquérir une expérience, développer leur réseau professionnel ou financer leurs études, les étudiants ont la possibilité de créer une activité sous le statut de micro-entrepreneur.

Certaines activités sont cependant conditionnées à l’âge de l’étudiant micro-entrepreneur.

### Les étudiants mineurs <!-- collapsable:close -->

#### Les mineurs non émancipés <!-- collapsable:close -->

Pour certaines activités agricoles, artisanales ou professions libérales, les mineurs non émancipés peuvent exercer sous forme d’EIRL, EURL ou SASU sous réserve de l’accord des deux parents.

Les activités nécessitant des diplômes ou des qualifications particulières (conducteur VTC, agent de sécurité, etc.), leur sont de fait interdites.

S’agissant des activités commerciales, elles ne sont possibles qu’en créant une EURL ou une SASU (sous réserve de l’autorisation des deux parents).

##### Bon à savoir

Depuis le 15 février 2022, suite à l’adoption de la loi n° 2022-172 en faveur de l’activité professionnelle indépendante et de l’entrée en vigueur du statut unique de l’entrepreneur individuel à compter du 15 mai 2022, il n’est plus possible de choisir le statut de l’EIRL.

S’il n’est plus possible de créer sous ce statut, les EIRL déjà immatriculées continuent d'exercer leurs activités dans les mêmes conditions.

#### Les mineurs émancipés <!-- collapsable:close -->

Étant considérés comme des majeurs, ils peuvent exercer tous types d’activité sous la forme de leur choix (sous réserve de la réglementation en vigueur).

Si un mineur émancipé souhaite être commerçant, diriger, être membre ou associé d'une SNC, être associé commandité d'une société en commandite simple (SCS) ou par actions (SCA), il doit cependant y être autorisé :

- soit par le juge des tutelles au moment de la décision d'émancipation ;
- soit par le président du tribunal, s'il fait cette demande après avoir été émancipé.

### Les étudiants majeurs <!-- collapsable:close -->

Sous réserve de respecter les conditions d’accès à certaines activités, les étudiants majeurs peuvent exercer une activité professionnelle sous le statut de micro-entrepreneur.

Attention à la domiciliation de la micro-entreprise dans le cas où les étudiants bénéficient d’un logement CROUS (centre régional des œuvres universitaires et scolaires) : ils doivent vérifier que cette domiciliation est possible.

### Conséquences fiscales, sociales et financières <!-- collapsable:close -->

Les conséquences de l’exercice d’une activité de micro-entrepreneur seront différentes si l’étudiant est rattaché au régime fiscal de ses parents ou s’il en est détaché :

- si l’étudiant rattaché au foyer fiscal de ses parents a moins de 25 ans, les revenus tirés de l’activité de la micro-entreprise doivent être inclus dans la déclaration fiscale du foyer ;
- si l’étudiant est détaché du foyer fiscal de ses parents, il remplit sa déclaration d’impôt.

L’étudiant micro-entrepreneur, même s’il bénéficie de la sécurité sociale étudiante, doit verser des cotisations sociales.

Les aides accordées par l’État aux étudiants, de type APL ou bourse sur critères sociaux, sont calculées sur les revenus déclarées 2 ans auparavant. Il n’y a donc pas de conséquences immédiates sur l’attribution de ces aides.

## Retraités <!-- collapsable:open -->

Tous les retraités peuvent créer une activité sous le statut de micro-entrepreneur. Ce régime est particulièrement adapté aux retraités qui souhaitent garder une activité professionnelle tout en conservant leur pension, les deux revenus pouvant être cumulés sous certaines conditions.

Le cumul intégral du revenu de la micro-entreprise et de la pension de retraite est possible pour les retraités du régime général de la sécurité sociale, et ceux du régime social des indépendants, de l'assurance vieillesse des professions libérales et du régime agricole, à condition de bénéficier d’une retraite à taux plein et d’avoir liquidé l’ensemble de leurs pensions de retraite.

Le cumul du revenu de la micro-entreprise et de la pension de retraite est plafonné dans d’autres cas.

Le retraité exerçant une activité de micro-entrepreneur doit verser des cotisations au titre de l'assurance vieillesse, comme n’importe quel micro-entrepreneur. Même si, par définition, il touche déjà une pension de retraite.

Pour plus d’informations concernant les conséquences de votre création d’entreprise sur votre pension, rapprochez-vous de votre caisse de retraite.

## Demandeurs d’emploi <!-- collapsable:open -->

Le régime de micro-entrepreneur a été conçu, dès son origine, pour faciliter le retour à l’emploi des personnes au chômage.

Les statuts de micro-entrepreneur et de chercheur d’emploi sont donc compatibles et leurs revenus cumulables en tout ou partie.

Avant toute chose, informez Pôle Emploi de la création de votre micro-entreprise. Vous devrez également déclarer chaque mois votre chiffre d’affaires pour actualiser le montant mensuel de votre allocation :

- si votre micro-entreprise ne dégage aucun chiffre d’affaires, votre allocation d’aide au retour à l’emploi (ARE) est maintenue dans son intégralité ;
- s’il y a un chiffre d’affaire votre ARE est maintenue partiellement.

Pôle Emploi convertit également votre chiffre d’affaire en jours d’ARE supplémentaires, prolongeant ainsi votre période d’indemnisation.

## Salariés <!-- collapsable:open -->

Sous certaines réserves vous pouvez créer une micro-entreprise tout en étant salarié. Ces réserves tiennent principalement à la relation avec votre employeur (clause d’exclusivité ou de non-concurrence).

Tout salarié a envers son employeur un devoir de loyauté. Vous devez donc informer votre employeur de votre souhait de devenir micro-entrepreneur (et le cas échéant obtenir son autorisation).

Au-delà des règles normales de savoir-être, un salarié ne doit pas travailler durant ses heures de travail salarié sur son projet, utiliser les ressources de l’entreprise à son bénéfice, dénigrer l’entreprise et doit respecter ses intérêts, en particulier, si le micro-entrepreneur souhaite exercer son activité dans le même domaine que celui de son activité salariée.

Un manquement à ce devoir de loyauté peut entraîner le licenciement.