﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur agent commercial" -->
<!-- var(key)="fr-creation_entreprise-micro_entrepreneur-micro_entrepreneur_agent_commercial" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Micro-entrepreneur agent commercial
===================================

L'agent commercial est un mandataire qui, à titre de profession habituelle et indépendante, négocie et éventuellement conclut des contrats d'achat, de vente, de location ou de prestation de services, au nom et pour le compte d'autres personnes. Il est un véritable chef d'entreprise qui ne doit pas être sous la subordination de son mandant, ce qui le distingue à ce titre du vendeur salarié (VRP notamment).

## Prérequis

L'agent commercial peut choisir d'exercer sous différentes formes juridiques (entreprise individuelle avec ou sans option pour le [régime du micro-entrepreneur](./le_regime_micro_entrepreneur.md) ou de l'EIRL (entrepreneur individuel à responsabilité limitée), société civile ou commerciale).

Pour plus d’informations, consultez l’article sur [les formes juridiques de l'entreprise](../prealables/0-les_formes_juridiques_de_l_entreprise.md).

### Bon à savoir

Depuis le 15 février 2022, suite à l’adoption de la loi n° 2022-172 en faveur de l’activité professionnelle indépendante et de l’entrée en vigueur du statut unique de l’entrepreneur individuel à compter du 15 mai 2022, il n’est plus possible de choisir le statut de l’EIRL.

S’il n’est plus possible de créer sous ce statut, les EIRL déjà immatriculées continuent d'exercer leurs activités dans les mêmes conditions.

## Son activité

L'activité de l'agent commercial, définie par l'[article L. 134-1](http://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=B1D7B5F3637A8F91E5735137A590921D.tpdjo08v_3?idArticle=LEGIARTI000006220397&cidTexte=LEGITEXT000005634379&dateTexte=20101125) du Code de commerce, est une activité civile consistant à vendre des marchandises, des objets ou des denrées transformées ou non, ou à fournir des logements de type hôtel ou chambre d'hôte, dans un but lucratif.

L'agent commercial négocie et conclut, de façon permanente et indépendante, des contrats de vente, d'achat, de location ou de prestation de services, au nom et pour le compte d'autres entreprises.

## Déclarer son activité

Vous devez vous immatriculer au Registre spécial des agents commerciaux (RSAC) auprès du greffe du tribunal de commerce (ou du tribunal de grande instance statuant commercialement dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle) de votre domicile ([l'immatriculation est payante](https://www.service-public.fr/professionnels-entreprises/vosdroits/F23282)). Cette formalité peut être effectuée en ligne sur guichet-entreprises.fr.

## Régime micro-fiscal et micro-social

Un agent commercial micro-entrepreneur est une personne physique qui dépend du régime micro-fiscal (régime spécial [bénéfices non commerciaux](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32105) (BNC) pour l'imposition des bénéfices et [régime de la franchise en base de TVA](https://www.service-public.fr/professionnels-entreprises/vosdroits/F21746)) à qui s'applique de droit le régime micro-social simplifié prévu à l'[article L. 133-6-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000019285616&dateTexte=&categorieLien=cid) du Code de la Sécurité sociale.

Pour plus d'informations, veuillez vous reporter à l'article sur [le régime du micro-entrepreneur](./le_regime_micro_entrepreneur.md).