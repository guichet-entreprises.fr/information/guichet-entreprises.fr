﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur commerçant" -->
<!-- var(key)="fr-creation_entreprise-micro_entrepreneur-micro_entrepreneur_commercant" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Micro-entrepreneur commerçant
=============================

Un commerçant est une personne qui exerce des actes de commerce et qui en fait sa profession habituelle ([article L. 121-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006219167&cidTexte=LEGITEXT000005634379&dateTexte=20000921) du Code de commerce).

La loi considère notamment comme actes de commerce (articles [L. 110-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006219125) et [L. 110-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006219126&cidTexte=LEGITEXT000005634379&dateTexte=20000921) du Code de commerce) :

* tout achat de biens meubles pour les revendre, soit en nature, soit après les avoir travaillés et mis en œuvre ;
* tout achat de biens immeubles aux fins de les revendre, à moins que l'acquéreur n'ait agi en vue d'édifier un ou plusieurs bâtiments et de les vendre en bloc ou par locaux ;
* toute opération d'intermédiaire pour l'achat, la souscription ou la vente d'immeubles, de fonds de commerce, d'actions ou parts de sociétés immobilières ;
* toute entreprise de location de meubles ;
* toute entreprise de manufacture, de commission, de transport par terre ou par eau ;
* toute entreprise de fournitures, d'agence, bureaux d'affaires, établissements de vente à l'encan, de spectacles publics ;
* toute opération de change, banque, courtage, activité d'émission et de gestion de monnaie électronique et tout service de paiement ;
* toutes les opérations de banques publiques ;
* toutes obligations entre négociants, marchands et banquiers ;
* les lettres de change ;
* toute entreprise de construction, et tous achats, ventes et reventes de bâtiments pour la navigation intérieure et extérieure ;
* toutes expéditions maritimes ;
* tout achat et vente d'agrès, apparaux et avitaillements ;
* tout affrètement ou nolisement, emprunt ou prêt à la grosse ;
* toutes assurances et autres contrats concernant le commerce de mer ;
* tous accords et conventions pour salaires et loyers d'équipages ;
* tous engagements de gens de mer pour le service de bâtiments de commerce.

## Exercice de l'activité dans un bureau ou magasin

Le commerce en boutique est peu adapté au [régime du micro-entrepreneur](./le_regime_micro_entrepreneur.md), en raison de la non-déductibilité des loyers et des charges afférentes du chiffre d'affaires déclaré et pris en compte dans l'appréciation du respect des plafonds de la micro-entreprise.

## Régime micro-fiscal et micro-social

Un micro-entrepreneur est une personne relevant du régime fiscal de la micro-entreprise ([régime d'imposition micro BIC](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32919) (bénéfices industriels et commerciaux) et [régime de la franchise en base de TVA](https://www.service-public.fr/professionnels-entreprises/vosdroits/F21746))à qui s'applique de droit le régime micro-social simplifié prévu à l'[article L. 133-6-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000019285616&dateTexte=&categorieLien=cid) du Code de la Sécurité sociale.

Les cotisations sociales d'un micro-entrepreneur dont l'activité est commerciale s'élèvent à 12,8 % de son chiffre d'affaires encaissé durant le mois ou le trimestre, en fonction du rythme de paiement choisi.

Le micro-entrepreneur bénéficie du droit aux indemnités journalières sous conditions. La contribution obligatoire à la formation professionnelle pour les micro-entrepreneurs commerçants s'élève à 0,10 % de son chiffre d'affaires.

Pour en savoir plus, consultez l'[article sur le régime du micro-entrepreneur](./le_regime_micro_entrepreneur.md).

## Spécificités

Le micro-entrepreneur est redevable de la taxe pour frais de chambre de commerce et d'industrie, sauf s'il est exonéré de la cotisation foncière des entreprises , c'est-à-dire lorsque son chiffre d'affaires est inférieur ou égal à 5 000 €.

Pour toute question relative aux formalités d'entreprise, il convient de se rapprocher de la chambre de commerce et d'industrie du lieu d'établissement principal.