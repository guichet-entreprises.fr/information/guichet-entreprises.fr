﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Libre établissement (LE)" -->
<!-- var(key)="fr-libre_etablissement" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Libre établissement (LE)
========================

## Je suis européen et je souhaite m'implanter en France (liberté d'établissement)

Le **libre établissement** (ou la liberté d'établissement) garantit aux ressortissants des États membres qui souhaitent s'installer en France d'exercer une activité permanente, et ce, dans les mêmes conditions que les ressortissants français.

La liberté d'établissement permet aussi aux ressortissants de l'Union européenne [[1]](#1) <a id="Retour1"></a> et de l'Espace économique européen [[2]](#2) <a id="Retour2"></a> de créer et de gérer leur entreprise en France. À ce titre, les ressortissants de ces pays peuvent créer leur entreprise sur guichet-entreprises.fr selon les mêmes procédures que les ressortissants français.

## Cas d'une activité et/ou d'une profession réglementée(s)

Le libre établissement s'effectue en deux étapes :

 1. Faire reconnaître sa qualification professionnelle (QP), si celle-ci est réglementée en France. Vous pouvez consulter les fiches d'information sur les professions réglementées sur [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/dqp/index.html) ;
 2. Demander une autorisation d'exercer. Vous pouvez consulter les fiches d'information sur les activités réglementées dans la [rubrique du même nom](../reference/fr/directive-services/index.md).

Dans les deux cas, c'est l'autorité compétente auprès de laquelle il s'agit de se rapprocher pour faire reconnaître son activité et sa profession. Le contact des autorités compétentes figure dans les fiches d'information sur les [professions réglementées](https://www.guichet-qualifications.fr/fr/dqp/index.html) et les [activités réglementées](../reference/fr/directive-services/index.md).

**Bon à savoir** : si la profession et l'activité ne sont pas réglementées, alors le libre établissement s'effectue sans prérequis.

## Les activités et professions réglementées en France

Concernant certaines activités, et pour créer une entreprise, il est nécessaire de respecter certaines règles et formalités particulières (déclaration, autorisation, etc.).

[> Vérifiez si votre activité est réglementée](../reference/fr/directive-services/index.md)

En France, 105 activités, regroupées par familles d'activités, sont réglementées au titre de la [directive 2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32006L0123&from=FR) du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur.

**Bon à savoir** : exercer de façon permanente en France peut nécessiter, selon la profession exercée, une reconnaissance de qualification professionnelle que vous pouvez effectuer sur le site [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/).

<a id="1"></a> [[1]](#Retour1) **États membres de l'Union européenne (UE**) – [27 pays] : Allemagne, Autriche, Belgique, Bulgarie, Chypre, Croatie, Danemark, Espagne, Estonie, Finlande, France, Grèce, Hongrie, Irlande, Italie, Lettonie, Lituanie, Luxembourg, Malte, Pays-Bas, Pologne, Portugal, Roumanie, Slovaquie, Slovénie, Suède, Tchéquie.

<a id="2"></a> [[2]](#Retour2) **États membres de l'Espace économique européen (EEE)** – [30 pays] : il s'agit des 27 pays membres de l'Union européenne (UE) dont la liste figure ci-dessus, et de la Norvège, de l'Islande et du Liechtenstein.

**Lire aussi** : [Je suis européen et je souhaite exercer occasionnellement en France (liberté de prestation de services)](libre_prestation_services.md)