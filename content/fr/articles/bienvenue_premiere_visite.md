﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Première visite sur guichet-entreprises.fr" -->
<!-- var(key)="fr-articles-bienvenue_premiere_visite" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Première visite sur guichet-entreprises.fr
======================================

**Optimisez votre visite !** Pour une meilleure expérience de navigation sur guichet-entreprises.fr, nous vous recommandons d'utiliser les navigateurs Mozilla Firefox ou Google Chrome. En effet, certains navigateurs sont susceptibles de ne pas supporter certaines fonctionnalités du site Guichet Entreprises.

Simple, rapide et sécurisé : le service guichet-entreprises.fr vous permet de réaliser vos démarches en ligne. Guichet-entreprises.fr est le guichet unique de la création d'entreprise. Gratuit et accessible à tous, le service en ligne permet de constituer un dossier sans avoir à se déplacer.

## Créer son entreprise <!-- collapsable:close -->

Votre idée est trouvée, vous avez obtenu les financements pour vous lancer et vous avez déterminé votre statut juridique ? Alors votre entreprise est prête à être immatriculée !

Les formalités de création d'entreprise sont centralisées auprès des centres de formalités des entreprises (CFE). Grâce à guichet-entreprises.fr, vous pouvez réaliser ces formalités [en ligne](../formalites.md) sans avoir à vous déplacer dans le CFE compétent. En effet, selon la nature de l'activité professionnelle (artisanale, commerciale, profession libérale, agricole, etc.) que vous exercerez, le centre de formalités des entreprises n'est pas le même.

**Lire aussi** :

[> Créer une entreprise sur guichet-entreprises.fr : mode d'emploi](./comment_creer_une_entreprise.md)
<br>
[> Liste des entres de formalités des entreprises (CFE) selon la nature de l'activité professionnelle](https://www.insee.fr/fr/information/1972060)

## Modifier la situation et cesser l'activité d'une entreprise <!-- collapsable:close -->

Sur guichet-entreprises.fr, vous pouvez également mettre à jour les informations relatives à votre entreprise auprès de tous les organismes compétents (impôts, Urssaf, Insee, registres publics, etc.).

Enfin, il vous est possible de mettre fin à votre activité de micro-entrepreneur et ainsi de radier votre entreprise individuelle (la formalité de cessation d'activité pour les sociétés n'est pas encore possible en ligne). La mise à jour sera effective auprès de tous les organismes compétents (impôts, Urssaf, Insee, registres publics, etc.).

## Créer un compte et s'identifier <!-- collapsable:close -->

Pour utiliser le service en ligne, vous êtes invité à [créer un espace personnel](https://account.guichet-entreprises.fr/user/create) sur guichet-entreprises.fr. Ce dernier vous permettra de créer et gérer par la suite votre/vos dossier(s) et modifier vos informations personnelles. Avec l'authentification [FranceConnect](https://franceconnect.gouv.fr/), si vous disposez déjà d'un compte sur [impots.gouv.fr](https://www.impots.gouv.fr/portail/), [ameli.fr](https://www.ameli.fr/) ou [La Poste](https://lidentitenumerique.laposte.fr/), vous pouvez vous connecter sur guichet-entreprises.fr en utilisant l'un de ces trois comptes.

## Compléter et valider votre dossier en ligne <!-- collapsable:close -->

Le site guichet-entreprises.fr permet de constituer un dossier en ligne, d'y joindre les éventuelles pièces justificatives et de régler les éventuels frais liés à la formalité. Une fois le dossier constitué et validé, ce dernier est envoyé à l'organisme compétent pour traitement. Pour obtenir le suivi de votre dossier, vous pourrez à tout moment contacter le destinataire de votre dossier.

## Rechercher des informations sur une activité réglementée <!-- collapsable:close -->

Certaines activités réglementées ([coiffeur](../../reference/fr/directive-services/autres-services/coiffeur-en-salon.md), [boucher](../../reference/fr/directive-services/alimentation/boucher.md), [agent immobilier](../../reference/fr/directive-services/batiment-immobilier/agent-immobilier-syndic-de-copropriete.md), [avocat](../../reference/fr/directive-services/secteur-financier-et-juridique/avocat.md), etc.), requièrent une autorisation ou une déclaration préalable avant de démarrer son activité. Guichet-entreprises.fr propose des fiches sur les différentes activités réglementées afin de connaître les conditions à remplir et les démarches à accomplir pour créer une entreprise dont l'activité est réglementée.

**Le saviez-vous ?** En France, il existe 105 activités réglementées dont vous trouverez la liste dans la rubrique [Activités réglementées](../../reference/fr/directive-services/index.md).