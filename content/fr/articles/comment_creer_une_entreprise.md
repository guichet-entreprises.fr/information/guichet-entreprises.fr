﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Créer une entreprise sur guichet-entreprises.fr : mode d'emploi" -->
<!-- var(key)="fr-articles-comment_creer_une_entreprise" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Créer une entreprise sur guichet-entreprises.fr : mode d'emploi
=========================================================

Quels que soient le secteur d'activité et la [forme juridique](../creation_entreprise/prealables/0-les_formes_juridiques_de_l_entreprise.md) de l'entreprise que vous souhaitez créer, guichet-entreprises.fr vous permet d'accomplir l'ensemble des formalités liées à la création : immatriculation, déclaration et paiement des frais éventuels.

Vous pouvez désormais tout faire sans vous déplacer, sur une même interface, en toute sécurité, et ce, même si vous exercez une [profession réglementée](https://www.guichet-qualifications.fr/fr/dqp/index.html).

## Création d'entreprise <!-- collapsable:close -->

Pour créer son entreprise et lui donner une existence juridique, vous devez l'immatriculer.

Guichet-entreprises.fr vous permet de constituer votre dossier en ligne. Ce dernier sera ensuite transmis à l'organisme compétent pour le traiter. En France, ce sont les centres de formalités des entreprises (CFE) qui sont en charge de recueillir votre dossier. Ils sont les interlocuteurs de premier niveau des entreprises et sont chargés d'être l'interface entre les administrations et elles.

L'immatriculation de votre entreprise sur guichet-entreprises.fr se déroule en quatre étapes :

1. création de votre espace personnel (ou connexion avec vos identifiants [FranceConnect](https://franceconnect.gouv.fr/)) ;
2. création de l'entreprise elle-même (qu'il s'agisse d'une entreprise individuelle ou d'une société) ;
3. demande d'autorisation ou déclaration préalable (peut être effectuée pour certaines [activités réglementées](../../reference/fr/directive-services/index.md) ;
4. validation générale du dossier.

## Créer un espace personnel <!-- collapsable:close -->

Cliquez sur le lien *Mon compte >* [*Inscription*](https://account.guichet-entreprises.fr/users/new).

Vous devez ensuite remplir le formulaire qui vous est proposé (les champs suivis d'un astérisque doivent être impérativement renseignés) :

- « Adresse de courriel (ou adresse e-mail) » : cette adresse sera par défaut votre identifiant (nom d'utilisateur). Toutes les notifications seront envoyées sur le courriel renseigné ;
- « Nom » : il s'agit de votre nom ;
- « Prénom » : il s'agit de votre ou de vos prénom(s) usuel(s) ;
- « Mot de passe » : votre mot de passe doit comporter un minimum de huit caractères dont :
  - au moins un chiffre,
  - au moins une lettre en majuscule,
  - au moins une lettre en minuscule,
  - au moins un caractère spécial ;
- « Confirmer le mot de passe » : retapez le mot de passe que vous avez choisi ci-dessus.

D'autres champs sont à remplir : courriel de secours, numéro de téléphone, pays, question et réponse secrètes.

En fin de saisie, cliquez sur le bouton « Valider » pour achever la création de votre espace personnel.

## Immatriculer son entreprise : constituer votre dossier sur guichet-entreprises.fr <!-- collapsable:close -->

Une fois le compte personnel créé, la création de l'entreprise proprement dite peut commencer en cliquant sur le bouton [Connexion](https://account.guichet-entreprises.fr/session/new).

Vous devrez remplir les champs demandés concernant :

1. votre activité ;
2. sa localisation ;
3. son statut juridique.

Une fois ces champs complétés, vous ne pourrez plus revenir en arrière.

Démarre ensuite la création de l'entreprise elle-même. Vous devrez renseigner différentes informations sur le profil de l'entreprise, compléter les formulaires et ajouter les pièces justificatives demandées (la copie d'une pièce d'identité, par exemple).

**Bon à savoir** : pour télécharger les pièces justificatives, vous devrez disposer des copies numériques des documents (format PDF accepté dans la limite de 2 Mo).

Vous pourrez à tout moment enregistrer vos données et revenir compléter votre dossier en vous connectant à votre espace personnel. Tout au long de cette étape, le centre de formalités des entreprises (CFE) dont vous dépendez vous sera indiqué. Vous pourrez le contacter une fois le dossier validé et envoyé par le biais de guichet-entreprises.fr pour toute demande relative à son suivi et à son traitement.

### Cas des activités réglementées

Certaines [activités réglementées](../../reference/fr/directive-services/index.md) ([coiffeur](../../reference/fr/directive-services/autres-services/coiffeur-en-salon.md), [boucher](../../reference/fr/directive-services/alimentation/boucher.md), etc.) nécessitent une autorisation ou une déclaration préalable. Le site guichet-entreprises.fr permet d'effectuer des demandes d'autorisation ou des déclarations préalables pour un certain nombre d'activités réglementées. Cette possibilité sera étendue à d'autres activités réglementées ultérieurement.

Si votre activité est réglementée et que le site ne permet pas pour le moment d'effectuer la demande d'autorisation ou la déclaration préalable, vous ne pourrez procéder qu'à la création de l'entreprise proprement-dite. Vous devrez dans ce cas vous mettre en relation avec l'autorité habilitée pour effectuer la demande d'autorisation ou la déclaration préalable concernée.

### Cas de la qualification professionnelle

Si vous déclarez une activité artisanale soumise à l'obligation d'immatriculation au répertoire des métiers (RM), vous devrez attester d'une qualification professionnelle. Sur guichet-entreprises.fr, vous pouvez procéder à la justification d'une qualification professionnelle artisanale (JQPA) lorsque vous constituez votre dossier de création d'entreprise.

## Validation générale du dossier et règlement de frais <!-- collapsable:close -->

Une fois l'entreprise créée, et éventuellement la demande d'autorisation ou la déclaration préalable effectuée dans le cas des activités réglementées, vous aurez un an pour procéder à la validation finale de votre dossier, ainsi qu'au paiement en ligne des frais.

Avant cette étape finale, vous pourrez à tout moment relire et/ou imprimer les différents éléments de ce dossier.

## Que se passe-t-il une fois que vous avez validé votre dossier sur guichet-entreprises.fr ? <!-- collapsable:close -->

Lorsque votre dossier est validé sur guichet-entreprises.fr, ce dernier est transmis automatiquement au centre de formalités des entreprises (CFE) dont vous dépendez.

C'est ce CFE qui traitera votre dossier. Si l'organisme a besoin de pièces complémentaires pour pouvoir traiter votre dossier, ce dernier prendra contact avec vous par courrier.

**Bon à savoir :** pour toute question relative au suivi et au traitement de votre dossier, vous devrez alors contacter le CFE qui en a été destinataire.