<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="Brexit : créer une entreprise en France quand on est britannique" -->
<!-- var(key)="fr-articles-brexit_creation_entreprise" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Brexit : créer une entreprise en France quand on est britannique

Depuis le 1er janvier 2021 et la mise en œuvre du Brexit, les citoyens britanniques souhaitant créer une entreprise en France doivent répondre à de nouvelles obligations.

## Vous étiez résident en France avant le 1er janvier 2021

Les Britanniques installés en France avant le 1er janvier 2021 peuvent continuer d’y vivre sans titre de séjour et y exercer une activité professionnelle sans autorisation de travail jusqu'au **30 septembre 2021**.

Vous pouvez donc, jusqu’à cette date, créer une entreprise en France sans détenir de titre de séjour.

À compter du 1er octobre 2021, la possession d'un tel titre sera obligatoire. La [demande de titre de séjour spécifique « accord de retrait du Royaume Uni »](https://contacts-demarches.interieur.gouv.fr/brexit/brexit-demande-titre-sejour/) doit être effectuée avant le 1er juillet 2021.

## Vous n’étiez pas résident en France avant le 1er janvier 2021

Les britanniques qui ne résidaient pas en France avant le 1er janvier 2021 doivent obtenir un titre de séjour pour créer une entreprise en France. Les demandes de titre de séjour doivent être déposées en préfecture.