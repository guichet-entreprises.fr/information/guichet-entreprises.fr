﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="COVID-19 : les mesures d’aide aux entreprises" -->
<!-- var(key)="fr-articles-covid_19_les_mesures_d_aide_aux_entreprises" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

COVID-19 : les mesures d’aide aux entreprises
=============================================

Face à l’épidémie de Coronavirus COVID-19, le gouvernement a mis en place des mesures de soutien immédiates aux entreprises. Pour en savoir plus sur la mise en œuvre de l’ensemble de ces mesures, accéder aux contacts utiles pour vous accompagner dans vos démarches, etc. :

https://www.economie.gouv.fr/coronavirus-soutien-entreprises

Pour toute question relative aux conséquences de l'épidémie de COVID-19 sur votre entreprise : [**covid.dge[@]finances.gouv.fr**](mailto:covid.dge[@]finances.gouv.fr)