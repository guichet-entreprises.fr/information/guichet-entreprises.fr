﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Guichet Entreprises s'associe au projet européen START EASY" -->
<!-- var(key)="fr-articles-guichet_entreprises_s_associe_au_projet_europeen_start_easy" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Guichet Entreprises s'associe au projet européen START EASY
===========================================================

Le service Guichet Entreprises est partenaire du projet START EASY pour favoriser la compétitivité des start-ups.

Le projet START EASY est une initiative du gouvernement de Catalogne menée dans le cadre du programme [Interreg Europe](https://www.interregeurope.eu/). Il rassemble les autorités compétentes de plusieurs pays européens (Belgique, Espagne, France, Italie, Lituanie, Lettonie, Pologne) pour créer des conditions favorables à la croissance.

Le projet vise à créer un environnement favorable pour la création d'entreprise et la compétitivité des start-ups à travers le développement d'outils et l'élaboration de politiques dédiées.

Développement des guichets uniques, simplification administrative et généralisation du principe du « Dites-le nous une fois » font partie des axes de travail des partenaires.

## Qu'est-ce que le programme Interreg Europe ?

Interreg Europe aide des autorités nationales, régionales et locales à travers toute l'Europe à développer de meilleurs politiques et à les mettre en œuvre. En créant un environnement et des opportunités pour partager des solutions, l'objectif d'Interreg Europe est de s'assurer que les investissements, l'innovation et les efforts de mise en place tendent vers une intégration durable pour les citoyens et les régions concernées.

Pour atteindre cet objectif, Interreg Europe permet aux autorités publiques régionales et locales de partager leurs idées et leur expérience de mise en pratique des politiques publiques afin de d'améliorer les stratégies pour les citoyens et les collectivités.

**Liens :**

* [Site internet du projet Start Easy](https://www.interregeurope.eu/starteasy/)
* [Compte Twitter](https://twitter.com/StartEasyEU)
* [Page Facebook](https://www.facebook.com/StartEasyEU/)
* [Site internet du programme Interreg Europe](https://www.interregeurope.eu/about-us/what-is-interreg-europe/)