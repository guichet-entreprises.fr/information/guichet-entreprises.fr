﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Ce qui a changé au 1er janvier 2018" -->
<!-- var(key)="fr-articles-ce_qui_a_change" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Ce qui a changé au 1er janvier 2018
===================================

Retrouvez une sélection de changements survenus au 1er janvier 2018.

## Micro-entreprise : les seuils du chiffre d'affaires évoluent

Depuis le 1er janvier 2018, les plafonds de chiffre d'affaires d'une micro-entreprise ont augmenté. Dorénavant, les seuils sont portés à :

* 170 000 € pour la vente de marchandises ;
* 70 000 € pour une prestation de services.

*Plus d'informations à venir, sous réserve de la sortie des décrets d'application.*

## Travailleurs indépendants : le RSI devient la « Sécurité sociale – Indépendants »

Le Régime social des indépendants (RSI) s'est transformé pour devenir la « Sécurité sociale – Indépendants ». Durant les années 2018 et 2019, des transformations interviendront au fur et à mesure suivant un calendrier qui sera rendu officiel par la loi de financement de la Sécurité sociale 2018 et après la sortie de probables décrets d'application.

[> Suivre les transformations du régime sur 2018-2019 ](http://secu-independants.fr/)

## Affiliation à la retraite complémentaire Agirc et Arrco

Depuis le 1er janvier 2018, toute entreprise nouvellement créée est exemptée de l'obligation de s'affilier à une caisse de retraite complémentaire. Ce n'est qu'à l'embauche de son premier salarié qu'elle doit effectuer la démarche d'adhésion.  

Auparavant, une entreprise devait adhérer à une institution de retraite complémentaire Agirc et Arrco à sa création.  

80% des structures sont concernées par cette mesure de simplification, parmi lesquelles les TPE et les micro-entrepreneurs.

[> En savoir plus](https://www.agirc-arrco.fr/)

*Liste non exhaustive*