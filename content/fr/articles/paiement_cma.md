<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="Indisponibilité des paiements pour certaines CMA" -->
<!-- var(key)="fr-paiement_cma" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Indisponibilité des paiements pour certaines CMA

De nouvelles modalités de paiement ont été mises en place au sein du réseau des CMA à compter du 1er février 2021. 

Le règlement des frais de façon dématérialisée est provisoirement indisponible pour les CMA suivantes : 

- XXXXXXX ;

Si vous êtes concerné, imprimez votre dossier et effectuez votre règlement en prenant rendez-vous directement auprès de la délégation départementale de votre CMA. 

Les coordonnées de celle-ci seront affichées après validation de votre dossier.
