﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Code APE" -->
<!-- var(key)="fr-articles-code_ape" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# Code APE

## Qu'est-ce que le code APE ? <!-- collapsable:open -->

Toute entreprise et chacun de ses établissements se voit attribuer par l'Insee (Institut national de la statistique et des études économiques), lors de son inscription au répertoire [Sirene](http://www.insee.fr/fr/methodes/default.asp?page=definitions/sys-inf-rep-nat-ent-etab.htm), un code qui caractérise son activité principale par référence à la nomenclature d'activités française (NAF).

Le code APE (activité principale exercée) est composé de cinq caractères : quatre chiffres et une lettre. C'est un renseignement fondamental pour la statistique d'entreprise car il est à la base du classement des entreprises par secteurs d'activité.

## Comment faire pour retrouver son code APE ? <!-- collapsable:open -->

Si vous ne connaissez plus votre code, vous pouvez vous rapprocher de l'Insee en vous munissant de votre numéro Siren. Vous pouvez également le retrouver [sur le site de l'Insee](https://avis-situation-sirene.insee.fr/) si vous n'avez pas demandé la non-divulgation de vos informations.

Ce code est utilisé essentiellement à des fins statistiques, il permet à l'Insee de réaliser des classements d'entreprises par secteur d'activité.

## Comment modifier son code APE <!-- collapsable:open -->

Si votre activité a changé, votre demande de modification doit se faire auprès du centre de formalités des entreprises (CFE) dont vous dépendez qui effectuera les démarches nécessaires. Si vous êtes micro-entrepreneur, cette formalité peut s'effectuer directement via le portail guichet-entreprises.fr.

Si vous n'avez pas changé d'activité et que vous estimez que le code APE attribué par l'Insee doit être corrigé, votre demande de modification doit être formulée par courrier postal à la [direction régionale de l'Insee](http://www.insee.fr/fr/service/default.asp?page=entreprises/sirene/sirene_dr.htm) compétente pour le département d'implantation de votre siège social, de votre établissement. Un [formulaire de modification d'APE](http://www.insee.fr/fr/information/2015441) à imprimer et à compléter par vos soins est mis à votre disposition sur le site internet de l'Insee, [www.insee.fr](http://www.insee.fr).

## Pourquoi mon code APE n'est-il pas reconnu sur le site Guichet Entreprises ? <!-- collapsable:open -->

Il est important de respecter la syntaxe exacte du code APE, car en cas d'erreur de saisie (espace, lettre ou chiffre manquant, etc.), le code ne sera pas reconnu.

S'il s'avérait toutefois que votre code APE n'est toujours pas reconnu après vérification de la syntaxe, c'est que l'activité à laquelle il correspond ne fait pas partie de la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur. Dans ce cas, cette activité ne fait pas partie du périmètre des démarches en ligne porté par le site guichet-entreprises.fr. En effet, le site guichet-entreprises.fr n'est pas compétent pour les démarches relatives à la création, la modification ou la cessation d'une activité publique ou parapublique.

C'est le cas des activités suivantes :

* caisses de retraite ;
* administration publique générale ;
* administration publique (tutelle) de la santé, de la formation, de la culture et des services sociaux, autre que sécurité ;
* activités d'ordre public et de sécurité ;
* services du feu et de secours ;
* activités générales de sécurité sociale ;
* gestion des retraites complémentaires ;
* distribution sociale de revenus ;
* activités des organisations patronales et consulaires ;
* réparation d'équipements de communication ;
* activités des syndicats de salariés ;
* activités des organisations religieuses ;
* activités des organisations politiques ;
* autres organisations fonctionnant par adhésion volontaire ;
* activités des organisations et organismes extraterritoriaux.