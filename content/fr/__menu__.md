﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="[Accueil](index.md)" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(key)="fr-__menu__" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

# [Accueil](index.md)
# Création d'entreprise
# <!--include(creation_entreprise/__menu__.md)-->
# Démarches en ligne
# <!--include(demarches_en_ligne/__menu__.md)-->
# Activités réglementées
# <!-- include(../reference/fr/directive-services/_list_menu.md) -->
# [Travailler en France](travailler_en_france.md)
# [Foire aux questions](faq.md)
# [Mes dossiers @visibility.authenticated @toggle.env](https://dashboard.guichet-entreprises.fr/)