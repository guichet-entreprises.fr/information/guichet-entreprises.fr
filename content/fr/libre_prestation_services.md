﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Libre prestation de services (LPS)" -->
<!-- var(key)="fr-libre_prestation_services" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Libre prestation de services (LPS)
==================================

## Je suis européen et je souhaite exercer occasionnellement en France

La **libre prestation de services** (ou liberté de prestation de services) concerne les services temporaires et occasionnels fournis par des ressortissants membres des pays de l'Union européenne [[1]](#1) <a id="Retour1"></a> ou de l'Espace économique européen <a id="Retour2"></a> [[2]](#2). Dans ce cadre, il n'y a pas à faire reconnaître de qualification professionnelle.

Néanmoins, des prérequis sont exigés pour exécuter en France des actes de sa profession. Le prestataire doit répondre à un certain nombre de conditions :

* être titulaire de diplômes, certificats, autres titres obtenus dans l'un de ces États ;
* être établi et exercer légalement la profession concernée dans un État membre autre que la France.

C'est le cas, par exemple, lorsqu'un architecte établi en France fournit une prestation de services auprès d'un bénéficiaire se situant en Allemagne. La prestation transfrontalière de services peut impliquer le déplacement temporaire du prestataire dans le pays de destination ou bien la prestation d'un service à distance.

La **liberté de prestation de services est temporaire** : une implantation durable couplée à une activité permanente implique l'établissement dans le pays sous le régime du [libre établissement](libre_etablissement.md).

## Conditions

Vous voulez fournir un service dans un autre pays que celui dans lequel vous êtes installé, un pays membre de l'UE, de l'EEE ou en Suisse ? Ou vous voulez tester un autre marché en vue de vous y implanter ou répondre à une commande en dehors de votre État ? Vous délivrez alors une prestation temporaire de services dont l'autorisation est à obtenir auprès des autorités compétentes.

Il n'est pas nécessaire de recourir à une reconnaissance de qualification professionnelle pour une prestation temporaire, cependant vous devez être établi dans votre pays d'origine. Dans certains cas, vous devrez préalablement à l'exercice de votre prestation demander une autorisation auprès des autorités compétentes concernées. Consultez les fiches d'information de la profession concernée sur [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/dqp/index.html).

## Démarches

Le contact avec l'autorité compétente est le plus souvent à prendre dans la région où vous décidez de vous installer.

Un certain nombre de pièces justificatives sont à fournir, dont certaines nécessitent parfois une traduction assermentée.

Les professions reconnues par la procédure de la [carte professionnelle européenne](https://www.guichet-qualifications.fr/fr/comprendre/carte_professionnelle_europeenne.html) (*European Professional Card*) peuvent ainsi déroger à la procédure classique.

**Bon à savoir** : si votre profession est réglementée dans votre pays d'origine mais pas dans votre pays d'accueil (France), vous pouvez exercer comme les ressortissants du pays d'accueil. La libre prestation de services est réservée aux seules personnes physiques ou morales qui sont établies dans un État membre de l'UE ou de l'EEE.

Concernant certaines activités, avant la première prestation de services, il faut adresser une déclaration préalable à l'autorité habilitée, accompagnée de pièces justificatives. Renseignez-vous auprès de l'autorité chargée de votre profession.

En outre, le prestataire doit apporter la preuve, par tous moyens, qu'il possède une connaissance suffisante de la langue française nécessaire à la réalisation de la prestation.

**<a id="1"></a> [[1]](#Retour1) Liste des 27 États membres de l'Union européenne :** Allemagne, Autriche, Belgique, Bulgarie, Chypre, Croatie, Danemark, Espagne, Estonie, Finlande, France, Grèce, Hongrie, Irlande, Italie, Lettonie, Lituanie, Luxembourg, Malte, Pays-Bas, Pologne, Portugal, Roumanie, Slovaquie, Slovénie, Suède, Tchéquie.

**<a id="2"></a> [[2]](#Retour2) Liste des 30 États de l'Espace économique européen :** il s'agit des 27 pays membres de l'Union européenne (UE) dont la liste figure ci-dessus, et de la Norvège, de l'Islande et du Liechtenstein.

**Lire aussi :** [Je suis européen et je souhaite m'implanter en France (liberté d'établissement)](libre_etablissement.md)