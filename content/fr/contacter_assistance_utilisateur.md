﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="open" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Contacter l'assistance utilisateur" -->
<!-- var(key)="fr-contacter_assistance_utilisateur" -->
<!-- var(last-update)="2021-01-04" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Contacter l'assistance utilisateur
================================

Vous avez des questions ou vous avez rencontré un problème lors de l'utilisation de guichet-entreprises.fr ?

## Une question sur le suivi de dossier ? <!-- collapsable:open -->

Le service en ligne guichet-entreprises.fr permet de constituer un dossier en ligne, d'y joindre les pièces justificatives et de régler les éventuels frais liés à la formalité. Une fois le dossier constitué et validé, ce dernier est envoyé à l'organisme compétent pour traitement.

De ce fait, **nous vous informons que l'INPI n'est pas en mesure de vous répondre sur le suivi de votre dossier une fois celui-ci validé et envoyé à l'organisme habilité.** Afin de connaître l'état de traitement de votre dossier, veuillez le contacter. Ses coordonnées figurent sur votre tableau de bord. Nous vous remercions de votre compréhension.

## Question sur l'utilisation du site ou question d'ordre technique <!-- collapsable:open -->

L'INPI vous répond :
* par téléphone au 01 56 65 89 98, du lundi au vendredi de 9 h à 18 h ;
* par courriel via le [formulaire de contact](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1) (inpi.fr).
