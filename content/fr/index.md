﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(site:package)="https://gitlab.com/|13561843|latest|reference_md|reference/configuration.yml" -->
<!-- var(site:home)="Accueil" -->
<!-- var(site:menu)="__menu__.md" -->
<!-- var(site:footer)="__footer__/__links__.md" -->
<!-- var(site:lang)="fr" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Guichet Entreprises  " -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="création; entreprise; formalités; cessation; modification; activités réglementées; micro-entreprise; micro-entrepreneur; société;" -->
<!-- var(page:font_sizer_off)="off" -->
<!-- var(page:breadcrumb_off)="off" -->
<!-- var(translation)="None" -->
<!-- var(last-update)="2021-01" -->
<!-- var(lang)="fr" -->
<!-- var(key)="fr-index" -->

Guichet Entreprises <!-- section:banner --> <!-- color:dark -->
===================================================

Nous vous invitons à réaliser vos formalités de création, modification et cessation d'entreprise sur [formalites.entreprises.gouv.fr](https://formalites.entreprises.gouv.fr/) et vous informons que le site Guichet Entreprises fermera définitivement le 2 septembre 2024

[Lire l'article <!-- link-model:box-trans -->](articles/guichet_unique.md)

Deuxième bloc  <!-- section-information:-160px -->
===================================================

[Optimisez votre visite](articles/bienvenue_premiere_visite.md)
-------------------------------------------------------------
Pour une meilleure expérience de navigation sur Guichet Entreprises, nous vous recommandons d’utiliser les navigateurs Mozilla Firefox ou Google Chrome.

[Formalités de modification](demarches_en_ligne/formalites_modification.md)
---------------------------------
Retrouvez toutes les informations sur les formalités de modification d'entreprise.

[Démarches en ligne](demarches_en_ligne/formalites.md)
-------------------------------
Réalisez vos formalités liées à la modification d'une entreprise.

[Activités réglementées](../reference/fr/directive-services/_list_menu.md)
---------------------------------------------------
Informez-vous sur les conditions à respecter pour exercer une activité réglementée en France.

Une seule adresse pour modifier son entreprise  <!-- section-welcome: -->
===================================================

Le service en ligne guichet-entreprises.fr permet aux entrepreneurs de réaliser les démarches administratives nécessaires à la vie de leur entreprise (modification, demandes d'autorisation, etc.). Il est le site des pouvoirs publics de la modification d'une entreprise. Ce service est géré par l'Institut national de la propriété industrielle.

Faciliter les démarches administratives, c'est encourager l'esprit d'entreprendre !

230568
------
dossiers de création d’entreprises ont été transmis aux centres de formalités des entreprises (CFE) en 2021 via guichet-entreprises.fr (55,41 % de plus qu’en 2020)

275683
------
dossiers ont été transmis aux centres de formalités des entreprises (CFE) en 2021 via guichet-entreprises.fr (55,37 % de plus qu’en 2020)

106
------
fiches d'information dédiées aux activités réglementées en France sont disponibles sur guichet-entreprises.fr

2000
------
Il existe plus de 2 000 dispositifs d'aide publique pour créer ou développer votre entreprise

Activités réglementées  <!-- section:courses -->
===================================================

Artisan du bâtiment, restaurateur, infirmier, boulanger, etc., avant de démarrer l'activité de votre entreprise, informez-vous sur les démarches à accomplir et sur les conditions d'accès à une activité réglementée en France.

Vérifiez si votre activité est réglementée en consultant [<font style="text-transform: lowercase;">nos fiches d'information.</font>](../reference/fr/directive-services/index.md)

Prêt à vous lancer ?  <!-- section:stories --><!-- color:dark -->
===================================================

Le service en ligne guichet-entreprises.fr simplifie vos démarches en vous offrant une information claire et un parcours personnalisé.  
Le service enregistre et transmet vos formalités aux organismes partenaires compétents (CCI, CMA, Urssaf, greffiers des Tribunaux de commerce ou chambres d'agriculture). Tous les organismes intervenant dans la vie de votre entreprise – service des impôts des entreprises, Urssaf, Insee, etc. – sont informés par les organismes partenaires. Vous recevrez par la suite numéro Siren, code APE, numéro de TVA, etc.

Pourquoi choisir guichet-entreprises.fr ?  <!-- section:welcome -->
===================================================

Le service en ligne est conçu pour faciliter les démarches administratives relatives à la vie de votre entreprise. Il vous suffit de répondre aux questions posées sur votre projet professionnel, d'y joindre les pièces justificatives et de régler les éventuels frais liés à la formalité. Votre dossier est directement envoyé à l'administration compétente où il sera traité.

<font color="#0092BC">Espace personnel</font>
------------------------------------------------------------------------------
Gérer votre ou vos dossier(s) créé(s) sur le site, suspendre la complétion de votre dossier et le sauvegarder, etc.

<font color="#0092BC">Service sécurisé</font>
------------------------------------------------------------------------------
Réalisez vos démarches en ligne, sans avoir à vous déplacer, sur une plateforme sécurisée.

<font color="#0092BC">Paiement en ligne</font>
------------------------------------------------------------------------------
Dans le cadre d'une inscription au RCS, RM, RSAC ou RSEIRL, vous pouvez régler vos frais directement en ligne.

<font color="#0092BC">Partenaires mobilisés</font>
------------------------------------------------------------------------------
Greffeiers des tribunaux de commece, Urssaf, Chambres de métiers et de l'artisanats, chambres d'agriculture, Insee, service des impôts des entreprises, etc.

Entreprendre en France et en Europe  <!-- section-stories:actu.png -->
===================================================

Vous êtes ressortissant de l'Union européenne ou de l'Espace économique européen ?

Nous encourageons la mobilité professionnelle en France et en Europe.
Le site guichet-entreprises.fr est membre du réseau Eugo créé par la Commission européenne. 
Il vous permet de comprendre et de connaître les conditions auxquelles vous êtes soumis pour exercer une activité de services.
Si vous devez faire reconnaître une qualification professionnelle, rendez-vous sur [<font style="text-transform: lowercase;">guichet-qualifications.fr</font><!-- link-model:normal -->](https://www.guichet-qualifications.fr/fr/) !

[Comment exercer en France de façon temporaire ou permanente ?<!-- link-model:box-trans -->](travailler_en_france.md)