﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Foire aux questions" -->
<!-- var(key)="fr-faq" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Foire aux questions
===================

## 1. Pourquoi les formalités d'entreprise ? <!-- collapsable:open -->

### Qu'est-ce qu'une formalité d'entreprise ? <!-- collapsable:close -->

Les formalités d’entreprise sont des formalités administratives liées à la vie de l'entreprise.

Il existe trois grands types de formalités :

- la **création** (immatriculation ou déclaration de début d’activité) qui permet de donner une existence légale à une structure ;
- les **modifications** (changement d’activité, d’adresse, de nom, du nombre d’associés, etc.) qui permettent de mettre à jour les informations relatives à l'entreprise ;
- la **cessation** d’activité qui met fin à l’existence légale de la structure.

Les formalités sont composées :

- d’un ou plusieurs **Cerfa**, formulaire administratif réglementé dont le modèle est fixé par un arrêté ;
- de **pièces justificatives** : pièce d'identité, attestation de non-condamnation, justificatif de qualification professionnelle artisanale, attestation de domicile, etc. ;
- éventuellement, d’un **paiement**, lié principalement aux frais d'inscription aux registres (RCS, RM, RSAC, etc.).

### Les formalités d'entreprise sont-elles obligatoires ? <!-- collapsable:close -->

Oui, les formalités d’entreprise sont obligatoires. 

Elles permettent de donner une existence légale à une entreprise et d’informer les administrations compétentes (services fiscaux, Insee, Urssaf, etc.), qui interagiront avec elle.

### Quels sont les organismes intervenant dans les formalités d'entreprise (SIE, SSI, Insee, etc.) ? <!-- collapsable:close -->

Des organismes vont intervenir tout au long de la vie de votre entreprise pour enregistrer votre déclaration d’activité, vous immatriculer, etc. : 

- l’**Insee** (Institut national de la statistique et des études économiques) :
  - inscrit l’entreprise au répertoire SIRENE (Système Informatisé du Répertoire National des Entreprises et des Établissements). Ce répertoire contient l’état civil des entreprises tenues de s’y inscrire ;
  - attribue : 
    - un numéro SIREN (Système d’Identification du Répertoire des Entreprises) qui est une série de 9 chiffres uniques,
    - un numéro SIRET (Système d’Identification du Répertoire des Établissements) qui est une série de 14 chiffres composée du numéro SIREN suivi de 5 chiffres qui correspondent au code NIC (numéro interne de classement). Les numéros SIREN et SIRET sont envoyés directement par l’Insee par courrier et courriel ;
  - délivre  les codes APE (activité principale exercée) ;
- le **service des impôts des entreprises** (SIE) est l’interlocuteur unique des PME (entreprise individuelle, sociétés) et quelle que soit leur activité (commerçants, artisans, agriculteurs et professions libérales) pour le dépôt des déclarations professionnelles (déclarations de résultats, de TVA, de cotisation foncière des entreprises, de CVAE, etc.) et le paiement des principaux impôts professionnels (impôt sur les sociétés, TVA, taxe sur les salaires, etc.).
- la **Sécurité sociale des indépendants** (SSI) remplace le Régime social des indépendants (RSI) pour la collecte et la gestion des cotisations sociales obligatoires des travailleurs non salariés. Il s’agit des entreprises individuelles : travailleurs indépendants, micro-entrepreneurs, EIRL, mais aussi des gérants majoritaires d’EURL et des gérants majoritaires de SARL. Depuis 2020, la SSI est intégrée au régime général de la Sécurité sociale.

L’immatriculation se fait auprès des teneurs de registres, les organismes qui tiennent les différents registres auxquels doivent obligatoirement s’inscrire les sociétés. Ils sont déterminés en fonction de votre activité : 

- le greffe du tribunal de commerce pour votre inscription :
  - au registre du commerce et des sociétés (RCS) si votre activité est commerciale, ou si vous créez une société civile,
  - au registre spécial des agents commerciaux (RSAC) si vous êtes agent commercial,
  - au registre spécial des entrepreneurs individuels à responsabilité limitée (RSEIRL) pour les commerçants et les libéraux ;
- la chambre de métiers et de l'artisanat (CMA), si votre activité est artisanale, pour votre inscription au répertoire des métiers.

## 2. Pourquoi utiliser le Guichet Entreprises ? <!-- collapsable:open -->

### Qu’est-ce que le site guichet-entreprises.fr ? <!-- collapsable:close -->

Le site guichet-entreprises.fr est un service en ligne permettant de réaliser les démarches administratives liées aux modifications de la situation d'une entreprise.

Son utilisation est gratuite, toutefois, dans le cadre de certaines formalités, des frais peuvent être demandés (exemple : des frais d’inscription au répertoire des métiers).

Les paiements effectués sur le site sont sécurisés.

Attention, le Guichet Entreprises n’accompagne pas le déclarant. 

Il ne l’aidera pas à choisir son statut juridique, par exemple. Des acteurs publics et privés conseillent les créateurs et les chefs d’entreprise dans ce choix, ainsi que sur celui du régime fiscal et social, etc.

### L’utilisation du site guichet-entreprises.fr est-elle gratuite ? <!-- collapsable:close -->

La réalisation de votre démarche sur guichet-entreprises.fr est totalement gratuite.

Toutefois, dans le cadre de certaines formalités, des frais peuvent être demandés (exemple : des frais d’inscription au répertoire des métiers).

Tous les paiements effectués sur guichet-entreprises.fr sont sécurisés.

### Le site est-il sécurisé ? <!-- collapsable:close -->

L’homologation sécurité du Guichet Entreprises a été délivrée le 1er juin 2019 pour une durée de 30 mois.

Elle garantit la conformité du site aux exigences de l’ANSSI (Agence nationale de sécurité des systèmes d’information) en matière de sécurisation d’accès, de saisie, de conservation, de traitement et d’échanges des données.

## 3. J'ai besoin d'informations sur les formalités d'entreprise <!-- collapsable:open -->

### Le Guichet Entreprises peut-il m’aider dans l’élaboration de mon projet ? <!-- collapsable:close -->

Le Guichet Entreprises **n’accompagne pas** le déclarant. Des acteurs publics et privés accompagnent les créateurs et les chefs d’entreprise dans le choix du statut juridique, du régime fiscal et social, etc.

### Quelle est la différence entre une personne physique et une personne morale ?  <!-- collapsable:close -->

Une personne physique est un individu ayant une identité civile.

Une personne morale est un regroupement de personnes, physiques ou morales, travaillant ensemble vers un but commun et ayant une existence juridique. Elle peut toutefois être unipersonnelle.

Si vous avez un projet de création d’entreprise, vous pouvez utiliser différents cadres juridiques pour votre activité. 

Le droit français propose deux grandes catégories bien distinctes :

- La première est celle des personnes physiques : il s’agit de l’entreprise individuelle (EI), avec ces différentes déclinaisons (entreprise individuelle à responsabilité limitée EIRL, régime de la micro-entreprise). L’entreprise individuelle est donc indissociable de la personne qui l’a créée. 
- L’autre forme juridique applicable est celle d’une personne morale, c’est-à-dire la constitution d’une société : par exemple, les plus utilisées parmi les créations d’entreprise sont la société à responsabilité limitée (SARL) et la société par actions simplifiée (SAS). C’est une société dont l’existence se dissocie des personnes qui la composent. De fait, les dirigeants d’une société peuvent changer.

Les personnes physiques et morales disposent toutes deux de la personnalité juridique, notion désignant les titulaires de droits et d’obligations juridiques après la création d’entreprise. Autrement dit, il s’agit des entités ou des individus pouvant saisir les juridictions ou faire l’objet d’une procédure.

### Qu’entend-on par « avez-vous déjà exercé une activité non salariée ? » <!-- collapsable:close -->

Une activité non salariée est une activité exercée en tant que travailleur indépendant, c’est-à-dire selon un régime différent de celui de salarié.

Font partie de la catégorie des travailleurs indépendants non salariés :

- les micro-entrepreneurs ;
- les entrepreneurs individuels (entreprises « en nom propre ») ;
- les gérants de sociétés de type EURL ou SARL ;
- les associés de société en nom collectif (SNC).

### A quoi correspond le numéro unique d’identification ? <!-- collapsable:close -->

Le numéro unique d’identification correspond au numéro SIREN, il est attribué à chaque entreprise. Ce numéro qui comporte neuf chiffres est unique et invariable. 

Sur guichet-entreprises.fr, votre numéro de SIREN vous est demandé dans le cadre la modification de votre activité afin de pouvoir retrouver les informations relatives à votre entreprise à partir du répertoire SIRENE de l’Insee. 

### J'ai besoin d'informations concernant le code APE <!-- collapsable:close -->

#### Définition du code APE <!-- collapsable:close -->

Le code APE (activité principale exercée) permet d'identifier la branche d'activité principale de l'entreprise ou du travailleur indépendant.

Le code est composé de 4 chiffres + 1 lettre, en référence à la nomenclature d'activités française (NAF).

Ce code est attribué par l'Insee lors de l'immatriculation ou de la déclaration d'activité de l'entreprise. Il est donné en fonction de l'activité principale déclarée et réellement exercée.

Ce code est essentiellement utilisé à des fins statistiques et permet à l’Insee de réaliser des classements d’entreprises par secteur d’activité.

#### Modification du code APE <!-- collapsable:close -->

Si vous avez changé de domaine d’activité, votre demande de modification sera transmise à l’Insee par votre [centre de formalités des entreprises](https://www.economie.gouv.fr/entreprises/cfe-centre-formalites-entreprises) (CFE) ou par le site guichet entreprise. L’Insee procédera ou non à la modification du code APE en fonction des informations fournies.

Si vous estimez que le code APE qui vous a été délivré ne correspond pas à votre activité principale, votre demande de modification doit être formulée par courrier postal ou par courrier électronique à la direction régionale de l'Insee compétente pour le département d'implantation de votre siège social ou de votre établissement.

Un [formulaire de modification d’APE](https://www.insee.fr/fr/information/2015441#titre-bloc-6) à imprimer et à compléter par vos soins est mis à votre disposition sur le site internet de l’Insee.

#### Problème de reconnaissance du code APE <!-- collapsable:close -->

Si votre code APE n’est pas reconnu, c’est que l’activité à laquelle il correspond ne fait pas partie de la [directive « Services »](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32006L0123) du 12 décembre 2006. Dans ce cas, cette activité ne fait pas partie du périmètre des démarches en ligne porté par le site guichet-entreprises.fr. 

En effet, le site guichet-entreprises.fr n’est pas compétent pour les démarches relatives à la création, la modification ou la cessation d’une activité publique ou parapublique.

C’est le cas des activités suivantes :

- caisses de retraite ;
- administration publique générale ;
- administration publique (tutelle) de la santé, de la formation, de la culture et des services sociaux, autre que sécurité ;
- administration publique (tutelle) des activités économiques ;
- affaires étrangères ;
- défense ;
- justice ;
- activités d’ordre public et de sécurité ;
- services du feu et de secours ;
- activités générales de sécurité sociale ;
- gestion des retraites complémentaires ;
- distribution sociale de revenus ;
- activités des organisations patronales et consulaires ;
- réparation d’équipements de communication ;
- activités des syndicats de salariés ;
- activités des organisations religieuses ;
- activités des organisations politiques ;
- autres organisations fonctionnant par adhésion volontaire ;
- activités des organisations et organismes extraterritoriaux.

### Mon activité est artisanale, pourquoi le CFE indiqué est-il une CCI ? <!-- collapsable:close -->

Le centre de formalités des entreprises (CFE) d’une entreprise artisanale est déterminé en fonction du nombre de salariés dans l'entreprise :

- Si l’effectif est inférieur ou égal à 10 salariés, elle dépend du CFE de la chambre de métiers et de l’artisanat (CMA).
- Si l’effectif est supérieur à dix salariés, elle dépend du CFE de la chambre de commerce et d’industrie (CCI).

### Puis-je déclarer deux activités principales ? <!-- collapsable:close -->

Une entreprise peut avoir plusieurs activités, mais il n’est pas possible de déclarer deux activités principales.

Vous devez, dans ce cas, déclarer une activité principale et une activité secondaire. L’activité principale correspond à l’activité qui génère le plus de chiffre d’affaires ou à laquelle vous consacrez le plus de temps.

Seules certaines activités doivent être exclusives, c’est le cas des activités réglementées de gardiennage, de services à la personne, etc. 

#### Le cas particulier de l’entreprise individuelle

Une personne physique ne peut avoir qu’une seule entreprise individuelle, il est en revanche possible d'y exercer plusieurs activités, même si elles n'ont aucun lien entre elles.

Un entrepreneur individuel peut ainsi exercer toutes les activités artisanales, la plupart des activités commerciales et certaines activités libérales.

**ATTENTION**

Il vous est impossible d'adjoindre une activité libérale à une activité commerciale et/ou artisanale. Dans ce cas, il faut faire une création d’activité libérale en indiquant en observation que vous exercez déjà une activité commerciale.

### Mon activité correspond-elle à celle d’un agent commercial ? <!-- collapsable:close -->

Est agent commercial la personne qui, à titre de profession habituelle et indépendante, négocie et, éventuellement, conclut des contrats d’achat, de vente, de location ou de prestation de services, au nom et pour le compte d’autres personnes.

À ne pas confondre avec un prestataire de services, une personne physique ou morale, y compris les organismes publics, offrant des services (aide à domicile, services informatiques ou de télécommunications, services de conseil, etc.).

### Qu’est-ce qu’une activité de commerçant ambulant ou non sédentaire ? <!-- collapsable:close -->

Une **activité ambulante**, ou non sédentaire, est une activité exercée sur la voie publique, sur les halles, les marchés, les foires ou dans les lieux privés par voie de démarchage et qui a pour objet la vente d'un bien mobilier, la conclusion d'un contrat de location ou de prestation de service.

Ce mode d’exercice de l’activité est réglementé et nécessite l’obtention d’une carte de commerçant/artisan ambulant.

### Qu’est-ce que la demande d’ACRE ? <!-- collapsable:close -->

L’[aide à la création ou à la reprise d’une entreprise](https://www.service-public.fr/particuliers/vosdroits/F11677) (ACRE) consiste en une exonération partielle de charges sociales, dite exonération de début d’activité, et en un accompagnement pendant les premières années d’activité. Elle permet aussi à certains bénéficiaires de prétendre à d’autres formes d’aides.

Depuis le 1er janvier 2020, la demande d’ACRE doit être réalisée indépendamment de la formalité de création. Elle n’est plus automatique et est soumise à des critères d’éligibilité. 

Les micro-entrepreneurs doivent déposer leur [demande](https://www.service-public.fr/particuliers/vosdroits/R55376) auprès de l’Urssaf au plus tard dans les 45 jours suivants le dépôt de dossier de création ou de reprise d’entreprise.

Les textes prévoient qu’à défaut de réponse dans le délai d’un mois à compter de la réception de la demande, l’exonération ACRE est présumée acceptée.

Pour les entreprises créées sous un autre statut, l’Urssaf effectue un contrôle a posteriori.

## 4. Je gère mon espace personnel <!-- collapsable:open -->

### Pourquoi est-il nécessaire de créer un espace personnel ? <!-- collapsable:close -->

Vous devez créer un espace personnel pour utiliser les services du Guichet Entreprises. Cela vous permettra de gérer votre/vos dossier(s) et vos informations personnelles. 

Le site guichet-entreprises.fr permet :

- de constituer votre dossier en ligne ;
- d’y joindre vos pièces justificatives ;
- de régler les éventuels frais liés à la formalité.

Une fois votre dossier constitué et validé, il est automatiquement transmis au CFE compétent pour traitement.

Vous constituez ainsi votre dossier de formalité sans vous déplacer. 

### Comment fonctionne le tableau de bord de mon espace personnel ? <!-- collapsable:close -->

Après avoir créé votre espace personnel, vous accédez à votre tableau de bord. Celui-ci affichera l’ensemble de vos dossiers et vous permettra de :

- commencer une nouvelle démarche ;
- poursuivre celle entamée ;
- télécharger votre dossier une fois que celui-ci a été transmis à l’organisme destinataire. 

Ce tableau de bord est commun :

- au site guichet-entreprises.fr, qui regroupe les démarches de création, modification et cessation d’activité pour une entreprise ;
- au site [guichet-qualifications.fr](https://www.guichet-qualifications.fr/), qui s’adresse aux ressortissants de l’Union européenne et de l’Espace économique européen, à l'exception des titulaires de diplômes français, souhaitant faire reconnaître leurs qualifications professionnelles pour exercer en France.

Si vous constituez des dossiers sur une de ces deux plateformes, ils seront visibles sur votre tableau de bord.

### J'ai un problème de mot de passe <!-- collapsable:close -->

#### Le courriel de validation ne fonctionne pas <!-- collapsable:close -->

Le lien de validation, à usage unique, est valable 24 heures. Au-delà, vous devez recréer un compte avec la même adresse courriel.

Le lien n’est valable qu’une seule fois. Si vous avez déjà activé votre compte, cliquez sur [Connexion](https://account.guichet-entreprises.fr/session/new) pour y accéder.

#### Oubli du mot de passe <!-- collapsable:close -->

Si vous avez oublié votre mot de passe, cliquez sur [Renouveler mon mot de passe](https://account.guichet-entreprises.fr/users/renew) à partir de la page de connexion.

Saisissez le courriel que vous utilisez pour guichet-entreprises.fr et vous recevrez par courriel un lien qui vous permettra de réinitialiser votre mot de passe. Ce lien est valable 24 heures à compter de l’envoi.

#### Renouvellement du mot de passe <!-- collapsable:close -->

Si le lien de renouvellement de mot passe ne fonctionne pas, cela veut probablement dire qu’il est périmé. Il faudra donc effectuer une [nouvelle demande de renouvellement](https://account.guichet-entreprises.fr/users/renew).

De plus, par mesure de sécurité, vous devrez changer ce mot de passe tous les 90 jours.

### Je souhaite modifier les informations figurant dans mon compte <!-- collapsable:close -->

Si vous souhaitez modifier vos informations personnelles, connectez-vous à votre compte et modifiez vos informations personnelles dans l’onglet en haut à droite de la page. 

Vous pourrez changer votre nom, prénom, pays, numéro de téléphone et question/réponse secrète.

Pour modifier votre mot de passe, veuillez suivre la [procédure d’oubli de mot de passe](https://account.guichet-entreprises.fr/users/renew).

En revanche, vous ne pouvez pas modifier votre courriel de connexion lié à votre espace personnel. Vous devez créer un deuxième compte avec un courriel différent. 

Il est également impossible de déplacer vos dossiers créés sur l’ancien compte pour les mettre sur le nouveau compte. 

### Puis-je supprimer mon espace personnel ? <!-- collapsable:close -->

Si vous souhaitez supprimer votre espace personnel sur guichet-entreprises.fr, veuillez [contacter l’INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### Puis-je me connecter avec mon compte France Connect ? <!-- collapsable:close -->

Vous pouvez vous connecter sur guichet-entreprises.fr avec vos identifiants France Connect,  précédemment créés sur les sites internet [Impots.gouv.fr](https://www.impots.gouv.fr/portail/), [LaPoste.fr](https://lidentitenumerique.laposte.fr/) ou [Ameli.fr](https://www.ameli.fr/).

### J’ai perdu mes identifiants France Connect <!-- collapsable:close -->

Le service Guichet Entreprises n’est pas en mesure de vous fournir vos identifiants France Connect. 

Vous devez suivre la procédure du site ([Impots.gouv.fr](https://www.impots.gouv.fr/portail/), [LaPoste.fr](https://lidentitenumerique.laposte.fr/) ou [Ameli.fr](https://www.ameli.fr/)) dont sont issus vos identifiants de connexion et votre mot de passe France Connect.

### J’ai créé un dossier sur guichet-entreprises.fr en m’identifiant avec mon compte France Connect qui a été supprimé <!-- collapsable:close -->

Si vous avez créé un dossier sur guichet-entreprises.fr en utilisant une authentification [France Connect](https://franceconnect.gouv.fr/) dont le compte a été supprimé entre temps, vous avez toujours la possibilité de « récupérer » votre dossier en [contactant l'INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

Les dossiers Guichet Entreprises sont rattachés à un compte Guichet Entreprises même si l’utilisateur utilise un identifiant France Connect. 

### Combien de temps mes données sont-elles conservées ? ? <!-- collapsable:close -->

Vos données fournies sur guichet-entreprises.fr sont conservées un an. Si vous souhaitez les supprimer avant ce délai, [contactez l’INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

Les dossiers créés et envoyés aux autorités compétentes via guichet-entreprises.fr sont conservés pendant une période de trois mois.

## 5. Je saisis mon dossier <!-- collapsable:open -->

### Avant de créer votre dossier <!-- collapsable:close -->

L’utilisation du site guichet-entreprises.fr requiert un navigateur configuré pour autoriser les cookies de session. Afin de garantir une expérience de navigation optimale, nous vous recommandons d’utiliser, au minimum, les versions de navigateurs suivantes :

- Firefox version 45 et plus ;
- Opera version 11 et plus ;
- Safari version 5.1 pour Windows et version 6.0 et plus pour MacOs ;
- Internet Explorer version 9 et plus ;
- Microsoft Edge version 25 et plus ;
- Google Chrome version 48 et plus.

En effet, d’autres navigateurs sont susceptibles de ne pas supporter certaines fonctionnalités du site guichet-entreprises.fr. 

Il est également recommandé d’utiliser la dernière version du navigateur et de le mettre à jour régulièrement pour bénéficier des correctifs de sécurité et des meilleures performances.

Le site guichet-entreprises.fr est optimisé pour un affichage en 1024×768 pixels. 

### Puis-je sauvegarder un dossier en cours de création ? <!-- collapsable:close -->

La formalité en cours peut être enregistrée à tout moment et terminée dans un délai d’un mois. Pour cela, cliquez sur « Enregistrer et quitter ».

Vous pourrez reprendre vos démarches sans avoir à ressaisir les informations précédemment renseignées. 

Le dossier est définitivement transmis à l’organisme compétent lorsque vous cliquez sur « Envoyer mon dossier ».

### Puis-je modifier mon dossier en cours de création ? <!-- collapsable:close -->

Vous pouvez modifier votre dossier tant que celui-ci n’est pas transmis aux organismes destinataires.

### Puis-je modifier mon dossier après sa transmission aux organismes destinataires ? <!-- collapsable:close -->

Conformément à la règlementation en vigueur, il vous est impossible  de modifier ou annuler un dossier sur guichet-entreprises.fr, une fois que celui-ci a été validé et mis à la disposition des organismes destinataires.

Ainsi, pour toute modification ou suppression de ce dernier, il convient alors de les contacter directement.

### Puis-je supprimer un dossier ? <!-- collapsable:close -->

Il est possible de supprimer un dossier en cours de constitution. Dans ce cas, veuillez [contacter l’INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

Une fois le dossier validé et envoyé à l’un des organismes partenaires vous ne pouvez plus le supprimer. Pour cela, il vous faudra contacter le CFE auquel il a été transmis.

**Bon à savoir**

Tout dossier démarré et non complété sera automatiquement supprimé de la plateforme dans un délai de trois mois.

### Pourquoi m’indique-t-on des frais à payer alors que je pensais que mon dossier était sans frais ?

Les frais éventuels sont calculés en fonction des données fournies sur votre future entreprise. Si le ou les règlement(s) ne semble(nt) pas vous concerner, nous vous invitons à revoir les champs saisis lors des étapes 1 et 2.

Par exemple, l’immatriculation d’un micro-entrepreneur exerçant une activité libérale est gratuite, sauf s’il opte pour le statut de l’EIRL. En effet, un micro-entrepreneur exerçant une activité libérale optant pour l’EIRL doit s’inscrire au registre spécial des entrepreneurs individuels à responsabilité limitée (RSEIRL), ce qui entraîne des frais d’inscription au registre.

### J'ai un problème pour remplir le champ concernant le numéro de sécurité sociale<!-- collapsable:close -->

Si, après plusieurs tentatives, votre numéro de sécurité sociale n’est pas accepté, vérifiez la saisie de votre date de naissance. Nous effectuons un contrôle entre la date de naissance et le numéro de sécurité sociale, les données doivent donc correspondre.

Si malgré cela votre numéro n’est toujours pas accepté, vous pouvez temporairement remplir le champ par quinze zéros. Puis, dans la dernière page du formulaire (« Formalité »), il vous suffira d’indiquer votre numéro de sécurité sociale dans le rectangle « Observations » en haut de page.

### Je ne trouve pas ma commune de naissance dans la liste déroulante <!-- collapsable:close -->

Depuis quelques années, certaines communes ont été regroupées dans le cadre de réorganisations territoriales. L’ancienne commune n’existant plus administrativement, vous devez indiquer le nom de la nouvelle commune administrative dans le champ « Commune de naissance ». 

Vous pouvez éventuellement indiquer le nom de l’ancienne commune dans la partie « Observations » qui se trouve à la fin du formulaire. Peu importe le nom de la commune, c’est le code commune (référentiel Insee), qui lui est inchangé, qui fait foi.

Exemples dans le département du Calvados :

- Si votre commune de naissance est « Aunay-sur-Odon », il vous faut dorénavant choisir la commune « Les Monts d’Aunay ».
- Si votre commune de naissance était « Mézidon-Canon », choisissez « Mézidon Vallée d’Auge » dans la liste déroulante.

### Je ne trouve pas mon code postal <!-- collapsable:close -->

Le champ « code postal » concerne uniquement les adresses situées en France. Pour les pays étrangers, il suffit de renseigner le pays et la commune. Le code postal peut être indiqué dans le champ « complément d’adresse ».

## 6. J’ajoute mes pièces justificatives <!-- collapsable:open -->

### Sous quel format doivent être mes pièces jointes ? <!-- collapsable:close -->

Pour éviter toute erreur technique lors de la transmission de votre dossier à l’organisme destinataire, nous vous conseillons de respecter certains standards lors de l’étape d’ajout des pièces justificatives :

- la taille de vos pièces justificatives doit être inférieure à 2Mo. Dans le cas contraire, un message d’erreur s’affichera sur l’écran vous informant que les pièces jointes n’ont pas pu être ajoutées au dossier ;
- vos pièces justificatives doivent obligatoirement être au format PDF ;
- le fichier PDF ne doit pas être protégé sans quoi il pourrait ne pas être ajouté au dossier.

Vérifiez que les fichiers au format PDF avec des champs saisissables (formulaires) ne soient pas corrompus et qu'ils s'affichent correctement avec Acrobat Reader.

Si vous rencontrez un problème avec une pièce jointe, veuillez [contacter l’Inpi](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1) avant de finaliser et d’envoyer votre dossier.

### Est-il possible de récupérer les pièces jointes téléchargées ? <!-- collapsable:close -->

Il est possible de supprimer et/ou remplacer par d’autres pièces les documents que vous avez téléchargés sur guichet-entreprises.fr, à tout moment, tant que vous n’avez pas validé votre dossier et qu’il n’a pas été transmis au CFE en charge de son traitement. 

A tout moment, vous pouvez télécharger votre dossier à partir de votre tableau de bord, même si ce dernier a été transmis au CFE compétent.

### Pourquoi me demande-t-on des pièces qui ne me concernent pas ? <!-- collapsable:close -->

Le service en ligne détermine les pièces justificatives en fonction de ce que vous renseignez lors de votre déclaration.

Si vous estimez que les pièces justificatives demandées ne vous concernent pas, nous vous invitons à revoir les champs saisis sur cette dernière.

### On me demande « la liste des souscripteurs », de quoi s’agit-il ? <!-- collapsable:close -->

La liste des souscripteurs est demandée dans le cadre d’une formalité de création ou de modification d’une personne morale (société), les sociétés anonymes (SA) et les sociétés par actions simplifiées (SAS) plus particulièrement.

La liste des souscripteurs présente la liste des différents actionnaires et fait apparaître le nombre d’actions souscrites et les sommes versées par chacun d’eux.

Elle est distincte des documents fournis par la banque dépositaire des fonds composant le capital de la société et doit être rédigée sur un papier à en-tête de la société en cours de création.

### Quels sont les documents autorisés pour justifier de mon domicile ? <!-- collapsable:close -->

**Pour les personnes physiques :**

Vous devez fournir un  justificatif de domicile de moins de trois mois au nom et prénom du dirigeant. 

Les documents autorisés sont :

- les factures (électricité, gaz, eau, téléphone fixe ou mobile, taxe d’habitation) ;
- si l’emménagement est récent, l’acte d’acquisition du bien ou le bail.

Si vous êtes hébergé à un domicile qui n’est pas à votre nom, vous devez fournir une attestation de moins de 3 mois au nom et prénom de l’hébergeant accompagné d’une attestation d’hébergement (courrier de sa part attestant sur l’honneur qu’il vous héberge).

Si vous êtes en location, vous devez fournir une copie du bail et une attestation de votre propriétaire vous autorisant à domicilier votre entreprise à votre domicile.

**Pour les personnes morales :**

En plus d’un justificatif de domicile de moins de trois mois à l’adresse du siège sociale, le dirigeant de la société doit rédiger une attestation de domiciliation. Celle-ci doit comporter les éléments suivants :

- l’identité du déclarant ;
- la dénomination de l’entreprise ;
- l’adresse précise du siège social telle qu’elle apparaîtra sur le Kbis de l’entreprise ;
- la date, la signature du dirigeant.

### Pourquoi me demande-t-on la « copie du diplôme, du titre ou de toute pièce justifiant ma qualification professionnelle » ? <!-- collapsable:close -->

Cette pièce est demandée si l’activité déclarée est soumise à la détention d’une qualification professionnelle et que l’entrepreneur n’a pas pris l’engagement dans sa déclaration de recruter un salarié qualifié dans le métier exercé. 

Il est alors demandé au déclarant de justifier de sa propre qualification (diplôme) ou de son expérience professionnelle en fournissant :

- bulletins de salaire ou certificats de travail **pour les salariés** ;
- attestation d’immatriculation (SIREN), extrait Kbis et justificatif d’inscription au répertoire des métiers (RM) **pour les dirigeants**.

### J’ai reçu un courrier dans lequel on me demande de modifier les pièces justificatives que j’ai jointes à mon dossier : comment faire ? <!-- collapsable:close -->

Si vous avez reçu un courrier complémentaire pour modifier les pièces justificatives transmises, c’est que votre dossier a été validé puis envoyé à l’organisme compétent. 

À ce stade, vous ne pouvez plus rouvrir le dossier sur guichet-entreprises.fr. 

Les pièces et/ou informations complémentaires sollicitées par le(s) destinataire(s) de la formalité constituée doivent être envoyées directement à cet organisme, par courrier, courriel ou dépôt physique, selon les recommandations et avec les références communiquées par l’organisme.

## 7. Je paie les frais de mon dossier et je le transmets <!-- collapsable:open -->

### Qui sont les destinataires de mon paiement sur guichet-entreprises.fr ? <!-- collapsable:close -->

En constituant votre dossier en ligne, vous avez la possibilité de régler les frais liés à la formalité en ligne. Le site guichet-entreprises.fr transmet les paiements effectués sur le site directement aux organismes destinataires des dossiers. Pour toute réclamation liée au paiement, veuillez vous adresser à l’organisme destinataire de votre dossier.

### J’ai procédé au paiement sur guichet-entreprises.fr, comment faire si l’organisme destinataire me demande de repayer les mêmes frais ? <!-- collapsable:close -->

Si l’organisme destinataire de votre dossier vous redemande des frais de dossier, vous pouvez lui envoyer une copie de votre justificatif de paiement sur guichet-entreprises.fr. Il pourra alors retrouver votre paiement. Si tel n’est pas le cas, veuillez [contacter l’INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### Comment obtenir une facture ? <!-- collapsable:close -->

Si vous avez effectué un paiement sur guichet-entreprises.fr dans le cadre de la réalisation de votre formalité, vous avez la possibilité de demander une facture directement auprès de l’organisme destinataire des frais versés. Vous devrez joindre à votre demande le justificatif de paiement qui vous a été transmis par courriel à l’issue du règlement.

### Quand mon dossier sera-t-il transmis à l’organisme destinataire ? <!-- collapsable:close -->

Une fois complété et finalisé, votre dossier créé sur le site guichet-entreprises.fr est transmis aux organismes destinataires pour être traité.

Pour savoir si votre dossier a bien été envoyé, rendez-vous sur votre espace personnel et vérifiez que la barre de progression du dossier en question est bien à 100 % et qu’elle s’affiche en vert.

Les dossiers sont transmis dans un délai compris entre 1 et 24 heures.

Si vous remarquez que la barre de progression de votre dossier ne n’affiche pas à 100 % en vert plus de 48 heures ouvrées après l’avoir finalisé, veuillez [contacter l’INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### Quel est le centre de formalités des entreprises (CFE) en charge de mon dossier ? <!-- collapsable:close -->

Le centre de formalités des entreprises (CFE) compétent est déterminé en fonction du statut juridique de l’entreprise, de la nature de l’activité et de la localisation géographique du siège. 

- **l’activité du déclarant et sa forme juridique (critère catégoriel) :**

<table class="table table-striped table-bordered">
<thead style="text-align: center;border-collapse: collapse">
<tr>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Votre activité professionnelle</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Votre CFE</th>
</tr>
</thead>
<tbody style="text-align: center;border-collapse: collapse">
<tr>
<td style="border: 1px solid #AAA;padding: 4px"><a href="https://www.economie.gouv.fr/entreprises/statut-entreprise-individuelle">Entrepreneur individuel</a> ou société (<a href="https://www.economie.gouv.fr/entreprises/entreprise-unipersonnelle-responsabilite-limitee-EURL">EURL</a>, <a href="https://www.economie.gouv.fr/entreprises/societe-responsabilite-limitee-sarl">SARL</a>, <a href="https://www.economie.gouv.fr/entreprises/societe-anonyme-SA">SA</a> - <a href="https://www.economie.gouv.fr/entreprises/societe-actions-simplifiee-SAS">SAS</a>, <a href="https://www.economie.gouv.fr/entreprises/societe-en-nom-collectif-snc">SNC</a>) exerçant une activité commerciale
</td>
<td style="border: 1px solid #AAA;padding: 4px">Chambre de commerce et d'industrie (CCI)</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">Entrepreneur individuel ou société exerçant une activité artisanale</td>
<td style="border: 1px solid #AAA;padding: 4px">Chambre de métiers et de l'artisanat (CMA)</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">Entreprises de transport de marchandises par voie d'eau ou société coopérative fluviale</td>
<td style="border: 1px solid #AAA;padding: 4px">CMA</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">Entrepreneur individuel ou société exerçant des activités agricoles à titre principal</td>
<td style="border: 1px solid #AAA;padding: 4px"><a href="http://www.chambres-agriculture.fr/">Chambre d'agriculture</a></td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">Entrepreneur individuel exerçant une profession libérale<br>Artiste auteur
</td>
<td style="border: 1px solid #AAA;padding: 4px">Urssaf </td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">Agent commercial (personne physique)<br>Société civile (SCI, SCM, SCP, etc.)<br>Société d'exercice libéral (SELARL, SELAFA, SELCA)<br>Société en participation<br>Établissement public et industriel (EPIC)<br>Groupement d'intérêt économique (GIE)<br>Association assujettie aux impôts commerciaux<br>Loueur en meublé
</td>
<td style="border: 1px solid #AAA;padding: 4px">Greffe du tribunal de commerce ou du tribunal de grande instance (TGI) statuant commercialement </td>
</tr>
</tbody>
</table>

- **la localisation géographique du siège de l’activité**.

Votre démarche terminée, notez les coordonnées qui vous sont communiquées afin de prendre contact directement avec lui si nécessaire. 

## 8. J'ai un problème technique <!-- collapsable:open -->

### Je rencontre des erreurs d'affichage <!-- collapsable:close -->

L’utilisation du site guichet-entreprises.fr requiert un navigateur configuré pour autoriser les cookies de session. Afin de garantir une expérience de navigation optimale sur guichet-entreprises.fr, nous vous recommandons d’utiliser, au minimum, les navigateurs suivants :

- Firefox version 45 et plus ;
- Opera version 11 et plus ;
- Safari version 5.1 pour Windows et version 6.0 et plus pour MacOs ;
- Internet Explorer version 9 et plus ;
- Microsoft Edge version 25 et plus ;
- Google Chrome version 48 et plus.

En effet, d’autres navigateurs sont susceptibles de ne pas supporter certaines fonctionnalités du site guichet-entreprises.fr.

Il est également recommandé d’utiliser la dernière version du navigateur et de le mettre à jour régulièrement pour bénéficier des correctifs de sécurité et des meilleures performances.

Le site guichet-entreprises.fr est optimisé pour un affichage en 1024×768 pixels. 

### Je reçois un message m’indiquant qu’une erreur est survenue <!-- collapsable:close -->

Si une erreur technique est survenue lors de la complétion de votre dossier, veuillez [contacter l’INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### Je rencontre un problème avec mon compte <!-- collapsable:close -->

#### Vous ne pouvez pas accéder à votre compte <!-- collapsable:close -->

Si vous ne pouvez pas accéder à votre compte, videz les cookies et essayez de vous connecter en navigation privée sur Google Chrome ou Mozilla Firefox.

**Bon à savoir**

Si cela ne fonctionne pas, il est possible que votre compte soit désactivé. Dans ce cas, cliquez sur [Renouveler mon mot de passe](https://account.guichet-entreprises.fr/users/renew) et suivez la procédure de réinitialisation du mot de passe pour vous connecter à nouveau.

#### Vous n’avez pas reçu le courriel d’activation ou de renouvellement du compte <!-- collapsable:close -->

Si vous n’avez pas reçu le lien d'activation ou de renouvellement du compte, vérifiez le dossier « courrier indésirable » de votre messagerie électronique. 

Si le lien ne s’y trouve pas, recréez votre compte avec le même courriel. En cas de nouvel échec, nous vous invitons à [contacter l’INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

#### Le courriel de validation ne fonctionne pas <!-- collapsable:close -->

Le lien de validation reçu par courriel est valable 24 heures. Passé ce délai, vous devez recréer un compte avec la même adresse courriel.

**Bon à savoir**

Le lien n’est valable qu’une seule fois. Si vous avez déjà activé votre compte, veuillez cliquer sur [Connexion](https://account.guichet-entreprises.fr/session/new) pour y accéder.

### Je ne peux pas renouveler  mon mot de passe <!-- collapsable:close -->

Le lien de renouvellement est valable 24 heures. Si celui-ci ne fonctionne pas, cela veut probablement dire qu’il est n’est plus actif. Vous devez  alors effectuer une [nouvelle demande de renouvellement](https://account.guichet-entreprises.fr/users/renew).

Par mesure de sécurité, vous devez changer ce mot de passe tous les 90 jours.

### Je ne parviens pas à télécharger mes pièces justificatives <!-- collapsable:close -->

Vos pièces justificatives doivent obligatoirement être au format PDF et être d’une taille inférieure à 2Mo. 

Si vous rencontrez des difficultés lors du téléchargement, nous vous invitons à vider vos caches ou à utiliser un autre navigateur internet. Vérifiez également que les fichiers PDF ne sont pas protégés.

Il est conseillé d’utiliser au minimum les versions de navigateur suivantes :

- Firefox version 45 et plus ;
- Opera version 11 et plus ;
- Safari version 5.1 pour Windows et version 6.0 et plus pour MacOs ;
- Internet Explorer version 9 et plus ;
- Microsoft Edge version 25 et plus ;
- Google Chrome version 48 et plus.

Dans le cas contraire, un message d’erreur s’affichera à l’écran vous informant que les pièces jointes  ne peuvent être ajoutées au dossier. 

### Une erreur technique est survenue au moment de joindre un document <!-- collapsable:close -->

Si une erreur technique survient lors du téléchargement d’un document, vérifiez son format et sa taille et vérifiez que les fichiers au format PDF avec des champs saisissables (formulaires) ne soient pas corrompus et qu'ils s'affichent correctement avec Acrobat Reader.

Vos pièces doivent obligatoirement être au format PDF et être d’une taille inférieure à 2Mo. 

Si l’erreur persiste, veuillez [contacter l’INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

**Bon à savoir**

Les modifications manuelles d’extensions peuvent causer une erreur technique lors du chargement de la pièce jointe (en modifiant un document ayant une extension .doc en .pdf sans conversion du fichier, par exemple).

### Je n'arrive pas à effectuer le paiement demandé <!-- collapsable:close -->

Si vous rencontrez un problème technique lors du paiement, veuillez [contacter l’INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### Je n'arrive pas à signer le formulaire <!-- collapsable:close -->

Le service de signature électronique peut être momentanément indisponible en raison d'un grand nombre de connexions simultanées.

Dans ce cas, nous vous invitons à réessayer ultérieurement.

## 9. Traitement des formalités <!-- collapsable:open -->

### Qui contacter pour le suivi mon dossier ? <!-- collapsable:close -->

Une fois votre dossier validé et envoyé, le Guichet Entreprises le transmet immédiatement au CFE compétent et, le cas échéant, au teneur de registre, pour traitement. 

Ce sont les seules autorités habilitées à étudier le dossier, vous devez donc les contacter pour toute information ou modification.

Les coordonnées des destinataires de votre dossier s’affichent une fois votre démarche terminée. Notez leurs coordonnées pour prendre contact avec eux directement si vous souhaitez, par exemple, connaître l’avancement de votre dossier. 

### Quel est le délai de traitement du dossier par les organismes compétents ? <!-- collapsable:close -->

Le site guichet-entreprises.fr vous permet de constituer votre dossier de création d’entreprise en ligne. Une fois complété et validé, ce dernier est transmis au centre de formalités des entreprises (CFE) compétent pour traitement.

Le site guichet-entreprises.fr ne peut donc s’engager sur les délais de traitement des CFE.

Si l’organisme a besoin de pièces complémentaires pour pouvoir traiter votre dossier, il prendra contact avec vous par courrier.

**Bon à savoir**

Pour toute question relative à votre dossier (suivi, traitement, etc.) une fois ce dernier validé et envoyé, veuillez contacter le CFE destinataire de votre dossier.

### Quand et comment recevrai-je les numéros SIREN et SIRET de mon entreprise ? <!-- collapsable:close -->

Les numéros SIREN et SIRET sont envoyés directement par l’[Insee](https://www.insee.fr/fr/accueil) (Institut national de la statistique et des études économiques) par courrier et courriel.

Rapprochez-vous de votre interlocuteur principal, le centre de formalités des entreprises (CFE) qui traite votre dossier pour plus d’informations. Il vous indiquera à quelle date la demande a été transmise à l’Insee.

### Quand recevrai-je mon extrait Kbis/mon extrait K ? <!-- collapsable:close -->

Si vous êtes concerné par l’immatriculation au registre du commerce et des sociétés (RCS), vous recevrez un extrait Kbis (pour toutes les personnes morales) ou K (pour les personnes physiques exerçant une activité commerciale ou ayant une double activité artisanale et commerciale), de la part du greffe du tribunal de commerce (GTC) territorialement compétent.

Les micro-entrepreneurs et les entrepreneurs individuels exerçant uniquement une activité libérale ne sont pas concernés par le Kbis et le K.

Le délai de traitement dépend uniquement du greffe et peut varier en fonction de la recevabilité du dossier transmis et du besoin éventuel de  pièces ou d’informations complémentaires.

### Pourquoi me dit-on ne pas avoir connaissance de mon dossier lorsque je contacte le centre de formalités des entreprises ou le teneur de registre dont je dépends ?

Si dans un délai de quinze jours ouvrés, l’organisme destinataire de la formalité n’a pas été en mesure d’accuser réception de la formalité, veuillez [contacter l’INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1) en indiquant le numéro de dossier commençant par « 2020- » ou « 2021- » ou le numéro de liasse commençant par « H10000 ».

### Je souhaite modifier, annuler ou supprimer mon dossier créé sur guichet-entreprises.fr <!-- collapsable:close -->

#### Vous n’avez pas encore validé votre dossier sur le site guichet-entreprises.fr

- Vous pouvez modifier votre dossier.

**Attention :** Certaines informations ne sont pas modifiables. Il s’agit de celles renseignées lors de l’initialisation du dossier (étape 1). En effet, les informations relatives à l’activité, au lieu d’exercice de l’activité, à la forme juridique et à quelques informations complémentaires (telles que le fait que vous ayez ou non déjà exercé une activité non salariée, si l’activité de la société débutera ou non dès la création de cette dernière) permettent de déterminer la formalité à compléter ainsi que les destinataires du dossier. En cas d’erreur lors de la complétion de cette étape il convient d’initialiser un nouveau dossier.

- Vous pouvez supprimer votre dossier.

**A savoir :** tout dossier démarré et non complété sera automatiquement supprimé de la plateforme dans un délai de 3 mois.

#### Vous avez validé votre dossier sur le site et celui-ci a été transmis au CFE compétent

- Plus aucune modification n’est autorisée sur le site guichet-entreprises.fr, et ce, conformément à la réglementation en vigueur. Il sera également impossible de le supprimer.
- Pour toute modification, vous devrez vous rapprocher des organismes destinataires du dossier :
  - soit le dossier n’a pas encore été traité par l’organisme, et dans ce cas, il convient de voir si la modification est possible sans qu’il soit nécessaire de refaire un dossier en ligne ;
  - soit le dossier a déjà été traité, et dans ce cas, il conviendra de procéder à une formalité de modification afin de rectifier le ou les élément(s) erroné(s).

**Attention :** Dans le premier cas, toute modification entraînant un changement de formulaire ou modifiant la compétence des organismes destinataires nécessitera de créer un nouveau dossier. Dans le deuxième cas, toute formalité de modification doit être effectuée après attribution du numéro SIREN, ce dernier étant nécessaire à cette formalité.

**A savoir :** Conformément à l’article R. 123-18 du Code de commerce, le Guichet Entreprises est dessaisi du dossier après transmission aux organismes compétents. Dans ce cadre, les dossiers finalisés seront supprimés après un délai de trois mois. Après traitement par les organismes compétents, les informations concernant les sociétés civiles et commerciales seront publiées au Registre national du commerce et des sociétés. Elles seront [mises à disposition du public](https://www.inpi.fr/fr/services-et-prestations-domaine/bases-de-donnees) en open data et feront l’objet d'une [diffusion à des fins de réutilisation](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041593491/), dans le cadre de licences de réutilisation homologuées.

Dès transmission du ou des dossier(s) les déclarants peuvent se rapprocher des autorités compétentes pour accéder à leur(s) dossier(s) ou le(s) consulter sur le Guichet Entreprises pendant les trois mois suivants la transmission.

La suppression d’un dossier finalisé, avant sa suppression automatique au bout de trois mois, doit en revanche être demandée à l’INPI via le [formulaire de contact](https://www.inpi.fr/fr/contactez-nous?about=299&under_about=883-702-b1). La modification d’un dossier finalisé fait l’objet d’une nouvelle formalité.