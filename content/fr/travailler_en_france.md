﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Travailler en France" -->
<!-- var(key)="fr-eugo" -->
<!-- var(translation)="None" -->
<!-- var(lang)="fr" -->

Travailler en France
====================

## Vous êtes ressortissant de l'Union européenne ou de l'Espace économique européen et vous souhaitez vous installer en France ? <!-- collapsable:off -->

Le service Guichet Entreprises de l'INPI encourage la création d'entreprise et la mobilité professionnelle en France. Les sites guichet-entreprises.fr et [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/) constituent à eux deux le guichet unique de la création d'entreprise reconnu par la Commission européenne. Ils vous permettent d'explorer de nouveaux débouchés et de développer une activité sur le territoire français.

### Activités réglementées <!-- collapsable:close -->

Informez-vous sur les conditions à respecter pour exercer une activité réglementée en France.

\> [Consultez les fiches d'information](../reference/fr/directive-services/index.md)

### Libre établissement <!-- collapsable:close -->

Vous pouvez désormais vous installer en France et exercer votre activité librement, et ce, de façon permanente.

\> [En savoir plus sur le libre établissement](libre_etablissement.md)

### Libre prestation de services <!-- collapsable:close -->

Délivrer une prestation de services temporaire ou occasionnelle en France n'a jamais été aussi facile ! Retrouvez tous les aspects pratiques avant de fournir votre prestation de services.

\> [En savoir plus sur la libre prestation de services](libre_prestation_services.md)

**Bon à savoir**

Pour obtenir la reconnaissance d'une qualification professionnelle, rendez-vous sur [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/) !

### S'implanter dans un autre État membre <!-- collapsable:close -->

Tous les pays membres de l'Union européenne ont un guichet unique ! Les guichets uniques sont des portails d'administration en ligne destinés aux entrepreneurs actifs dans le secteur des services. Ils font partie du réseau "EUGO". Retrouvez plus d'informations sur le site de la Commission européenne :

<p align="center">[Guichets uniques européens](https://ec.europa.eu/growth/single-market/services/services-directive/in-practice/contact_en) | [Guide pratique pour les entreprises](https://europa.eu/youreurope/business/index_en.htm)

<p align="center">![logo_eugo](./logo_eugo.png)