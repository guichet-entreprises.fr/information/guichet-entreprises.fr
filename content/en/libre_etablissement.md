﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Freedom of establishment" -->
<!-- var(keywords)="Freedom of establishment" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-libre_etablissement" -->
<!-- var(page:title)="Freedom of establishment" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

Freedom of establishment
=====================

## I am a European citizen and I would like to set up a business in France (freedom of establishment)

The **freedom of establishment** guarantees EU and EEA nationals who wish to start a business in France to carry out an activity on a permanent basis, under the same conditions as French citizens.

The freedom of establishment also enables European Union [[1]](#1) <a id="Retour1"></a> and European Economic Area [[2]](#2) <a id="Retour2"></a> nationals to start and run a business in France. Thus, EU and EEA nationals can start a business on guichet-entreprises.fr by completing the same formalities as French citizens.

## Regulated activities and/or professions

The freedom of establishment is a two-step procedure:

1. Obtaining the recognition of your professional qualifications if your profession or activity is regulated in France. You can read the fact sheets on regulated professions on [guichet-qualifications.fr](https://www.guichet-qualifications.fr/en/dqp/index.html),
2. Applying for an authorisation to practise your profession or carry out your activity. You may read our fact sheets about regulated activities in the [Regulated activities](../reference/en/directive-services/index.md) section.

In both cases, you need to contact the relevant authority to obtain the recognition of your activity or profession. The contact information of the relevant authority can be found in the fact sheets on [regulated professions](https://www.guichet-qualifications.fr/en/dqp/index.html) and [regulated activities](../reference/en/directive-services/index.md).

**Good to know**: If your profession or activity is not regulated, the freedom of establishment can be exercised unconditionally.

## Regulated activities and professions in France

It is necessary to comply with specific rules and formalities (declaration, authorisation, etc.) in order to start a business, as far as certain activities are concerned.

[> Read our fact sheets](../reference/en/directive-services/index.md)

In France, more than 100 activities sorted by activity categories are regulated under [Directive 2006/123/CE](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32006L0123&from=EN) of the European Parliament and of the Council of 12 December 2006 on services in the Single Market.

**Good to know:** In order to practise a profession in France on a permanent basis, you might need to obtain the recognition of your professional qualifications, depending on your profession. You can complete this procedure on [guichet-qualifications.fr](https://www.guichet-qualifications.fr/en/).

<a id="1"></a> [[1]](#Retour1) **Member states of the European Union (EU)** — [27 countries]: Germany, Austria, Belgium, Bulgaria, Cyprus, Croatia, Denmark, Spain, Estonie, Finland, France, Greece, Hungary, Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, Netherlands, Poland, Portugal, Romania, Slovakia, Slovenia, Sweden, Czechia.

<a id="2"></a> [[2]](#Retour2) **Countries of the European Economic Area (EEA)** — [30 countries]: it comprises the 27 member states of the European Union (EU), which are listed above, plus Norway, Iceland and Liechtenstein.

**See also**: [I am a European citizen and I would like to carry out an activity in France on an occasional basis (freedom to provide services)](libre_prestation_services.md)