﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| The Guichet Entreprises information repository is licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International licence.
| 
| Please visit the following address to obtain a copy of that licence:
| http://creativecommons.org/licenses/by-nd/4.0/
| or send a mail to: Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(site:package)="https://gitlab.com/|13561843|latest|reference_md|reference/configuration.yml" -->
<!-- var(site:home)="Accueil" -->
<!-- var(site:menu)="__menu__.md" -->
<!-- var(site:footer)="__footer__/__links__.md" -->
<!-- var(site:lang)="fr" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="business creation; start; business; procedures; cessation; modification; regulated activity; sole proprietorship; sole proprietor; France; company;" -->
<!-- var(page:font_sizer_off)="off" -->
<!-- var(page:breadcrumb_off)="off" -->
<!-- var(translation)="True" -->
<!-- var(last-update)="2020-12" -->
<!-- var(lang)="en" -->
<!-- var(key)="fr-index" -->

Guichet Entreprises <!-- section:banner --> <!-- color:dark -->
===================================================

Brexit: The new rules British citizens need to comply with to start a business in France

[Read the article <!-- link-model:box-trans -->](articles/brexit_creation_entreprise.md)

Deuxième bloc  <!-- section-information:-160px -->
===================================================

[Optimise your visit](articles/bienvenue_premiere_visite.md)
-------------------------------------------------------------
We recommend the use of the Mozilla Firefox or Google Chrome internet browers for a better browsing experience on guichet-entreprises.fr.

[Futur entrepreneur](futur_createur.md)
---------------------------------
Legal forms, business name protection, state aids, etc.,  find all relevant information before you get started!

[Online procedures](demarches_en_ligne/formalites.md)
------------------
Complete all the required procedures to register a business (whatever its legal form), modify the information about a business or cease a business activity.

[Processing of your application](demarches_en_ligne/traitement_dossiers.md)
---------------------------------------------------
Once your application has been completed and approved, it is sent to the relevant centre for business formalities (CFE) for processing: chamber of commerce and industry (CCI), chamber of trades and crafts (CMA), Urssaf, etc.

One address to start a business <!-- section-welcome: -->
===================================================

The guichet-entreprises.fr online service promotes business creation in France by enabling citizens to complete the required administrative procedures to start a business activity (registration, authorisation applications, etc.). Guichet-entreprises.fr is the government's official website for starting a business, modifying the information about a business or ceasing a business activity. This service is operated by the National institute of industrial property (INPI).

Simplifying administrative procedures is to promote entrepreneurial spirit!

148357
------
business creation applications were sent to centres for business formalities (CFEs) in 2020 via guichet-entreprises.fr (a 40,43% increase compared to 2019)

177436
------
applications were sent to centres for business formalities (CFEs) in 2020 via guichet-entreprises.fr (a 44,23% increase compared to 2019)

102
------
fact sheets on regulated activities in France are available online

2000
------
More than 2,000 state aids can help you start or grow your business

Regulated activities <!-- section:courses -->
===================================================

Construction tradesman, restaurant owner, nurse, baker, etc., prior to starting your business, find all information regarding the formalities you need to complete and the conditions of access that apply to a regulated activity in France.

Read our [<font style="text-transform: lowercase;">fact sheets</font>](../reference/en/directive-services/index.md) to know whether your activity is regulated or not.

Are you ready to start?  <!-- section:stories --><!-- color:dark -->
===================================================

The guichet-entreprises.fr service makes administrative procedures easy for you by providing you with clear information and a personalised pathway.  
The service records your formalities and sends them on to the relevant centre for business formalities (CCI, CMA, URSSAF, Clerks of the Commercial Courts, Chambers of Agriculture). All the authorities involved in business creation (business tax service, URSSAF, INSEE, etc.) will be informed by the centres for business formalities (CFEs).  Later on, you will receive a SIREN number (business identification number), an APE code (business sector identification code), a VAT number, etc.

Already registered? [<font color="#0092BC">Log in</font>](https://account.guichet-entreprises.fr/session/new)

[<font color="#0092BC">User guide</font>](articles/comment_creer_une_entreprise.md) [<font color="#0092BC">Create an account</font>](https://account.guichet-entreprises.fr/users/new)

Why choose guichet-entreprises.fr?  <!-- section:welcome -->
===================================================

This online service has been designed to make business creation formalities easy for you. All you need to do is answer the questions about your business project, attach the supporting documents and pay the fees that may be required for the procedure. Your registration application is then directly sent on to the relevant authority for processing.

<font color="#0092BC">My account</font>
------------------------------------------------------------------------------
Manage the application(s) you created on the website, postpone the completion of your application and save it, etc.

<font color="#0092BC">Secure service</font>
------------------------------------------------------------------------------
Complete your procedures online on a secure platform from home.

<font color="#0092BC">Online payment</font>
------------------------------------------------------------------------------
If you register in the RCS, the RM, the RSAC or the RSEIRL, you can pay the registration fees directly online.

<font color="#0092BC">Our partners</font>
------------------------------------------------------------------------------
Centre for business formalities (CFEs), INSEE, business tax services, URSSAF, etc.

Become an entrepreneur in France and in Europe  <!-- section-stories:actu.png -->
===================================================

Are you a European Union or a European Economic Area national?

We promote business creation and job mobility in France and throughout Europe.
Guichet-entreprises.fr is the French point of single contact for business creation. It is a member of the EUGO network which was created by the European Commission. 
The website provides the information you need on the conditions you need to fulfill in order to start a service activity.
And if you need the recognition of your professional qualifications, just visit the [<font style="text-transform: lowercase;">guichet-qualifications.fr</font><!-- link-model:normal -->](https://www.guichet-qualifications.fr/fr/) website!

[How can I work in France on a temporary or permanent basis?<!-- link-model:box-trans -->](travailler_en_france.md)