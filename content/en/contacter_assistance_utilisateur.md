﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright Â© Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Contacting the user support" -->
<!-- var(key)="en-contacter_assistance_utilisateur" -->
<!-- var(translation)="None" -->
<!-- var(lang)="en" -->

Contacting the user support
========================

Have you got any question? Did you encounter a problem using guichet-entreprises.fr?

## Question about the follow-up of your application

The guichet-entreprises.fr online service enables users to create an application online, attach the supporting documents and pay the fees that may be required for the procedure. Once your application is completed and approved, it is sent to the relevant authority for processing.

As a consequence, **we would like to infom you that the INPI is not able to provide you with information regarding the follow-up of your application once it has been approved and sent to the relevant authority.** In order to have a follow-up of your application, please contact the relevant authority. The contact details of the authority appear on your dashboard. Thank you for your understanding.

## Question about the use of the website or technical question

You may contact the INPI :
* By phone from Monday to Friday, 9 AM-6 PM : +33156658998
* By email via the [contact form](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1)