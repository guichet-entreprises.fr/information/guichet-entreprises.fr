﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="frequently asked questions faq france setting up business" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Frequently Asked Questions" -->
<!-- var(key)="en-faq" -->
<!-- var(translation)="Pro" -->
<!-- var(lang)="en" -->

Frequently Asked Questions
===================

## 1. Why completing business procedures? <!-- collapsable:open -->

### What is a business procedure? <!-- collapsable:close -->

Business procedures are administrative procedures related to the lifecycle of a business.

There are three main types of procedures:

- **business creation procedures** (registration or declaration of start of activity), which enable your business to have a legal existence,
- the **procedures to modify the information about a business** (change of activity, of address, of name, in te number of partners, etc.), which enable entrepreneurs to update the information about their business,
- **business closure procedures**, which terminate the legal existence of a business.

Procedures comprise:

- one or more **CERFA form(s)**. CERFA forms are regulated administrative forms, the models of which are laid down by Decree,
- **supporting documents**: ID document, certificate of no conviction, certificate of artisanal professional qualification, proof of address, etc.,
- possibly, a **payment**, to pay registration fees in most cases (RCS, RM, RSAC, etc.).

### Are business procedures mandatory? <!-- collapsable:close -->

Yes, business procedures are mandatory. 

Business procedures give a business a legal existence and inform relevant administrative authorities (business tax services, INSEE, URSSAF, etc.) that will interact with it.

### What is the role of a centre for business formalities (CFE)? <!-- collapsable:close -->

The role of the Guichet Entreprises is not to process applications. This task is in the remit of the centres for business formalities (CFEs). The role of the Guichet Entreprises is solely to simplify the sending of documents to the relevant CFE.

Centres for business formalities act as the interface between business entrepreneurs and administrative authorities.

Your CFE centralises the documents of your application, checks that it is properly completed, and sends them on to the various authorities concerned with the information about your business (business tax services, CPAM, URSSAF, etc.).

The CFE in charge of your business is determined based on two criteria: category and location.

The category criterion is based on your business's type of business activity and its legal form:

<table class="table table-striped table-bordered">
<thead style="text-align: center;border-collapse: collapse">
<tr>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Chambers of trades and crafts (*Chambres de métiers et de l’artisanat*) (CMA))</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Chambers of commerce and industry (*Chambres de commerce et d’industrie* (CCI))</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Chambers of agriculture (*Chambres d’agricultures* (CA))</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Clerks' office of the commercial court (*Greffes du Tribunal de Commerce* (GTC))</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">URSSAF (ACOSS)</th>
</tr>
</thead>
<tbody style="text-align: center;border-collapse: collapse">
<tr>
<td style="border: 1px solid #AAA;padding: 4px">Artisanal activities </td>
<td style="border: 1px solid #AAA;padding: 4px">Traders and commercial companies (exclusive of artisanal activities)</td>
<td style="border: 1px solid #AAA;padding: 4px">Farm operators</td>
<td style="border: 1px solid #AAA;padding: 4px">*Sociétés civiles* and commercial agents</td>
<td style="border: 1px solid #AAA;padding: 4px">Liberal activities carried out under a sole proprietorship (micro-entreprise or not)</td>
</tr>
</tbody>
</table>


CFEs have regional branches. Therefore, the location of your business also determines the relevant CFE.

### Which authorities have a role in the processing of business procedures (business tax services, SSI, INSEE, etc.)? <!-- collapsable:close -->

Other authorities besides CFEs will have a role in the lifecycle of your business to record your declaration of business activity, register your business, etc. 

- The **INSEE** (National institute of statistics and economic studies (*Institut National de la Statistique et des Etudes Economiques*):
  - registers businesses in the SIRENE directory (Electronic system of the national directory of businesses and establishments (*Système Informatisé du Répertoire National des Entreprises et des Établissements*)). This directory contains the civil status of businesses,
  - issues: 
    - A SIREN number (Identification system of the directory of businesses (*Système d’Identification du Répertoire des ENtreprises*)),
    - A SIRET number (Identification system of the directory of establishments (*Système d’Identification du Répertoire des ETablissements*)), which consists of a series of 14 digits: the SIREN number followed by the 5-digit NIC code (Internal number of classification (*Numéro Interne de Classement*)). The INSEE directly sends the SIREN and SIRETnumbers by mail or email,
  - issues APE codes (Activité Principale Exercée* or main business activity),
- The **business tax service** (*Service des Impôts des Entreprises* (SIE)) is the only point of contact of SMEs (sole proprietorships, companies), whatever their business activity (traders, artisans, farmers and liberal professions) for the submission of professional declarations (turnover declaration, VAT return, CFE return (business property tax), CVAE return (contribution on businesses' added value), etc.) and the payment of main professional taxes (corporate tax, VAT, payroll tax, etc.).
- The **Sécurité Sociale des Indépendants** (SSI or social security scheme for the self-employed) replaces the former *Régime Social des Indépendants* (RSI) for the collection and management of mandatory social security contributions of self-employed workers. These include self-employed workers, micro-entrepreneurs, limited liability sole proprietorships (EIRLs), but also majority shareholder managers (*gérants majoritaires*) of EURLs and minority shareholder managers (*gérants minoritaires*) of SARLs. Since 2020, the SSI is part of the general social security scheme.

Registration is made with registrars, i.e. the bodies in charge of keeping official registries. It is mandatory for companies to be registered in these registries. Relevant registries are determined based on your business activity: 

- The Clerk's office of the commercial court (*greffe du tribunal de commerce*) is competent to register businesses:
  - in the Registry of trade and companies (*Registre du Commerce et des Sociétés* (RCS)) for commercial business activities and the setting up of companies,
  - in the Special registry of commercial agents (*Registre Spécial des Agents Commerciaux* (RSAC)) for commercial agents,
  - in the Special registry of sole proprietors with limited liability (*Registre Spécial des Entrepreneurs Individuels à Responsabilité Limitée* (RSEIRL)) for traders and liberal professions,
- The Chamber of trades and crafts (*Chambre de Métiers et de l'Artisanat* (CMA)) is competent to register businesses in the Registry of trades (*Répertoire des Métiers* (RM)) for artisanal business activities.

It is not necessary to inform these bodies as the CFE will take care of that.

## 2. Why using the Guichet Entreprises? <!-- collapsable:open -->

### What is the guichet-entreprises.fr website? <!-- collapsable:close -->

The guichet-entreprises.fr website is an online service that enables entrepreneurs to complete administrative procedures to:

- modify the information about a business,
- close a business.

The use of the website is free-of-charge. However, the payment of fees can be required for certain procedures (such as registration fees for registration in the Registry of trades, for instance).

The payment service on the website is secure.

Note that the Guichet Entreprises does not assist users in the completion of administrative procedures. 

It cannot help them choose a legal form, for instance. Public and private organisations can advise business entrepreneurs in making this choice or in choosing a tax regime, etc.

**Good to know**

The Guichet Entreprises integrated system includes business rules. Thanks to a quick and simple set of questions, users are automatically guided towards the relevant CFE and registrar.

### What kind of business can I start on guichet-entreprises.fr? <!-- collapsable:close -->

All legal forms (sole proprietorships (*Entreprises Individuelles*), limited liability companies (*SARLs*), simplified limited companies (*SAS*) are available, and all the stages of a company's lifecycle (change of legal form, increase of share capital) are taken into account.

On guichet-entreprises.fr, you can complete procedures to:

- start a business, modify the information about a business, close a business for liberal professions, artisanal, commercial and agricultural businesses and commercial agents,
- start or modify the information about an SARL/SELARL, a trading company (exclusive of SARLs and non-trading companies),
- start and close an agricultural company.

### Is the use of guichet-entreprises.fr free of charge? <!-- collapsable:close -->

The completion of your application on guichet-entreprises.fr is completely free of charge.

However, the payment of fees can be required for certain procedures (such as registration fees for registration in the Registry of trades, for instance).

Payments on guichet-entreprises.fr are secure.

### Is the website secure? <!-- collapsable:close -->

The Guichet Entreprises of the INPI was issued a cybersecurity accreditationon on 1 June 2019 for a period of 30 months.

This accreditation guarantees that the service complies with the cybersecurity requirements of the ANSSI (French National Cybersecurity Agency) regarding access, keyboarding, storage, the processing and exchange of data.

## 3. I need information on business administrative procedures <!-- collapsable:open -->

### Can the Guichet Entreprises help me with my project? <!-- collapsable:close -->

The Guichet Entreprises does not assist users in the completion of administrative procedures. Public and private organisations can help business entrepreneurs in making this choice or in choosing a tax regime, etc.

### What is the difference between a natural person and a legal person?  <!-- collapsable:close -->

A natural person is an individual with a civil identity.

A legal person is a grouping of natural or legal persons working together towards a common goal, which has a legal existence. It can have one member only, however.

If you have a business project, you may use various legal forms for your business activity. 

There are two main categories, which are very distinct, in French law:

- The first category is that of natural persons, which refer to sole proprietorships (*Entreprises Individuelles* (EI)) and its variants (limited liability sole proprietorship (*Entreprise Individuelle à Responsabilité Limitée* (EIRL)), micro-enterprise scheme). Therefore, there is no legal distinction between the owner and the business entity in a sole proprietorship. 
- The second category is that of legal persons, i.e. companies. The most common types of legal person in France are limited liability companies (*Société A Responsabilité Limitée* (SARL)) and simplified limited companies (*Société par Actions Simplifiées* (SAS)). In a company, there is a legal distinction between the business entity and its members. Consequently, the senior managers of a company can change.

Natural and legal persons both have a legal personality, which means they have rights and obligations once the business has been created. In other words, these entities or individuals can apply to the courts or be subject to legal proceedings.

### What does "Have you ever carried out a self-employed activity?" mean? <!-- collapsable:close -->

A self-employed activity (*activité non salariée*) is a business activity carried out as an independent worker. This means that it comes under a different scheme than that of employees.

Are considered self-employed workers:

- micro-entrepreneurs,
- sole proprietors (*entrepreneurs individuels*),
- managers of SARLs (limited liability company) and EURLs,
- members of companies called *sociétés en nom collectif* (SNC).

### What is the single identification number? <!-- collapsable:close -->

The single identification number, which corresponds to the SIREN number, is issued to each business. This 9-digit number is unique and cannot change. 

On guichet-entreprises.fr, your SIREN number is required as part of the procedures to close or modify the information about a business, in order to find the information about your business in the INSEE's SIRENE directory. 

### I need information concerning the APE code <!-- collapsable:close -->

#### Definition of the APE code <!-- collapsable:close -->

The APE code (*Activité Principale Exercée* (main business activity)) is used to identify the main business activity of a business or self-employed worker.

This code comprises 4 digits and 1 letter and is based on the Nomenclature of French activities (*Nomenclature d'Activités Française* (NAF))

This code is issued by the INSEE at the moment of registering a business or declaring a business activity. The code is determined based on the declared and actually carried out business activity

This code is mainly used to compile statistics. The INSEE uses it to classify businesses by fields of activity.

#### Modification of the APE code <!-- collapsable:close -->

If your field of activity changed, your modification application will be sent to the INSEE by your centre for business formalities ([*centre de formalités des entreprises*](https://www.economie.gouv.fr/entreprises/cfe-centre-formalites-entreprises) (CFE)) or by the guichet-entreprises.fr website. The INSEE will or will not modify the APE code based on the information you provided.

If you think the APE code you were issued does not correspond to your main business activity, your modification application must be sent by mail or email to the regional branch of the INSEE in charge of the *département* in which your head office or establishment is located.

A printable [form to modify the APE code](https://www.insee.fr/fr/information/2015441#titre-bloc-6) is available on the INSEE's website.

#### Unkown APE code problem <!-- collapsable:close -->

If your APE code is unknown, it means your business activity is not part of the [Services Directive](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32006L0123&from=EN) of 12 December 2006. In this case, this business activity is not included in the scope of online procedures available on the guichet-entreprises.fr website. 

Indeed, the guichet-entreprises.fr website is not competent for the procedures to start a business, modify the information about a business or cease a business activity in the public or parapublic sectors.

This is the case for the following activities:

* pension funds,
* general public administration,
* public administration (administrative authority) in the sectors of health, vocation training, culture and social services, except security,
* public administration (administrative authority) of economic activities,
* foreign affairs,
* defence,
* justice,
* public order and security activities,
* fire and emergency services,
* general social security activities,
* management of supplementary pension schemes,
* social distribution of income,
* employer organisations' activities, chambers of commerce, industry and trade's activities,
* communication equipment repair,
* trade unions' activities,
* religious organisations' activities,
* political organisations' activities,
* other organisations working through voluntary membership,
* extraterritorial organisations and authorities' activities.

### My business activity is artisanal: Why is my centre for business formalities (CFE) the Chamber of commerce and industry (CCI)? <!-- collapsable:close -->

The centre for business formalities (*centre de formalités des entreprises* (CFE)) of an artisanal business is determined based on the number of employees:

- If a business employs 10 employees or less, its CFE is the Chamber of trades and crafts (*Chambre de Métiers et de l’Artisanat* (CMA)).
If a business employs more than 10 employees, its CFE is the Chamber of commerce and industry (*Chambre de Commerce et d’Industrie* (CCI)).

### I am an employee: Can I start a business? <!-- collapsable:close -->

Under certain conditions, you can start a business while remaining an employee. 

You must inform your employer and obtain his or her authorisation.

You must also respect the interests of your employer, particularly if you plan on starting a business in the same field of activity. Ensure that your employment contract does not include an exclusivity clause.  

### I am retired: Can I start a business? <!-- collapsable:close -->

Retirees can start a business.

The income stemming from their professional activity can be added to all or part of their pension. 

For more information on the consequences of starting a business on your pension, please contact your pension fund.

### Can I declare two main business activities? <!-- collapsable:close -->

A business can have several business activities, but it can declare only one main business activity.

In this case, you must declare one main and one secondary business activity. The main business activity corresponds to the activity that generates the most significant turnover or for which you spend most of your time.

Only certain business activities must be exclusive, such as regulated activities like certain security activities or personal services. 

#### The specificity of sole proprietorships

A natural person can only have one sole proprietorship. However, he or she can carry out several business activities, even though there is no relation between them.

Therefore, a sole proprietor can carry out all artisanal activities, most commercial activities and certain liberal activities.

**BE CAREFUL**

Liberal activities cannot be added to commercial and/or artisanal activities. In this case, you need to start a liberal activity and indicate that you already carry out a commercial actvity in the form's observation section.

### Is my business activity a commercial agent's activity? <!-- collapsable:close -->

A commercial agent is a person whose regular occupation is to independently negotiate and, if need be, sign purchase, sales, lease or service contracts on behalf and for other people.

It should not be confused with a service provider, whether natural or legal persons, including public organisations, who provides services (home help, IT or telecommunication services, consulting services, etc.).

### What does an itinerant trader activity correspond to? <!-- collapsable:close -->

An **itinerant business activity** is an activity that is carried out on the street, in markets, fairs or private places by way of doorstep selling with a view to selling chattel, or signing a lease or service contract.

This type of activity is regulated and requires the obtaining of an itinerant trader/artisan's card.

### What is the ACRE application? <!-- collapsable:close -->

The [Aid to start or take over a business](https://www.service-public.fr/particuliers/vosdroits/F11677) (*Aide aux Créateurs et Repreneurs d’Entreprises* (ACRE)) consists in a partial exemption from social security contributions known as *business start-up exemption*, and in support and counselling during the first years of activity. It also enables some beneficiaries to claim other types of aid.

Since 1 January 2020, the ACRE application and the business creation application must be completed separately. It is no longer allocated automatically and is subject to eligibility criteria. 

Micro-entrepreneurs must [apply for the ACRE](https://www.service-public.fr/particuliers/vosdroits/R55376) with the URSSAF 45 days after the submission of the business creation or takeover application at the latest.

The law states that in the absence of a response within one month following the submission of the application, the ACRE exemption is deemed to be accepted.

For other legal forms, a control is made afterwards.

## 4. Managing my account <!-- collapsable:open -->

### Why is it necessary to create an account? <!-- collapsable:close -->

You must create an accout in order to use the Guichet Entreprises services. Your account will allow you to manage your application(s) and your personal information. 

Thanks to guichet-entreprises.fr, you will be able to:

- complete an application online,
- attach required supporting documents
- pay the fees that may be required for the procedure.

Once your application is complete and validated, it will be sent to the relevant centre for business formalities (*Centre de Formalités des Entreprises* (CFE)) for processing.

Thus, you can complete your application from home. 

### How does the dashbaord work on my account? <!-- collapsable:close -->

After creating your account, you will access your dashboard. Your dashboard will display all your applications and will enable you to:

- start a new procedure,
- finish the procedures you started,
- download your application once it has been sent to the recipient authority. 

The dashboard is common to both:

- the guichet-entreprises.fr website, which enables users to complete the procedures to start a business, modify the information about a business and close a business,
- the [guichet-qualifications.fr](https://www.guichet-qualifications.fr/en/) website, which is intended for the nationals of EU member states and of the states party to the EEA agreement (except French nationals) who wish to obtain the recognition of their professional qualifications in order to carry out a business activity in France.

If you complete applications on one of these websites, they will be visible on your dashboard.

### I have a password problem <!-- collapsable:close -->

#### The confirmation email does not work <!-- collapsable:close -->

The link in the confirmation email is unique and valid for 24 hours. After 24 hours, you will need to create a new account with the same email address.

This link is a one-time link. If you already activated your account, click on [Connection](https://account.guichet-entreprises.fr/session/new) to access it.

#### Forgotten password <!-- collapsable:close -->

If you forgot your password, click on "[Reset my password](https://account.guichet-entreprises.fr/users/renew) on the connection page.

Enter the email you use on guichet-entreprises.fr and you will receive an email containing a link which will enable you to reset your password. This link is valid for 24 hours after the email has been sent.

#### Reset of password <!-- collapsable:close -->

If the link to reset your password does not work, it probably means that it is no longer valid. You will therefore have to make a [new request to reset your password](https://account.guichet-entreprises.fr/users/renew).

Moreover, for security purposes, you must change your password every 90 days.

### I wish to modify my personal details on my account <!-- collapsable:close -->

If you wish to modify your personal details, log in to your account and modify your personal details in the upper-right tab of the page. 

You will have the possibility to change your surname, first name, country, phone number and secret question/answer.

In order to change your password, please follow the [forgotten password procedure](https://account.guichet-entreprises.fr/users/renew).

However, it is not possible to change your account email. You need to create a second account with a different email. 

Moving the applications you started completing from your former to your new account is not possible either. 

### Can I delete my account? <!-- collapsable:close -->

If you wish to delete your account on guichet-entreprises.fr, please [contact the INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### Can I log in using my FranceConnect account? <!-- collapsable:close -->

The guichet-entreprises.fr website allows you to use the FranceConnect login details you created on the [Impots.gouv.fr](https://www.impots.gouv.fr/portail/), [LaPoste](https://lidentitenumerique.laposte.fr/) or [Ameli.fr](https://www.ameli.fr/) websites.

### I lost my France Connect login details <!-- collapsable:close -->

The Guichet Entreprises cannot provide you with your FranceConnect login details. 

You have to follow the procedure of the ([Impots.gouv.fr](https://www.impots.gouv.fr/portail/), [LaPoste.fr](https://lidentitenumerique.laposte.fr/) or [Ameli.fr](https://www.ameli.fr/)) website, on which you created your FranceConnect login details.

### I started an application on guichet-entreprises.fr by logging in with my FranceConnect account which has been deleted <!-- collapsable:close -->

If you started an application on guichet-entreprises.fr by using your [FranceConnect](https://franceconnect.gouv.fr/) account which has been deleted, you still have the possibility to "retrieve" your application by [contacting the INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

Guichet Entreprises applications are linked to a Guichet Entreprises account even though the user uses a FranceConnect account. 

### How long are my personal data stored? <!-- collapsable:close -->

Your personal data on guichet-entreprises.fr are stored for one year. If you wish to delete them before the one-year period, [please contact the INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

Applications compiled and sent to relevant authorities via guichet-entreprises.fr are stored for three months.

## 5. Completing my application <!-- collapsable:open -->

### Prior to starting an application <!-- collapsable:close -->

The use of the guichet-entreprises.fr requires an internet browser set to accept session cookies. For an optimal browsing experience, we recommend the use of the following web browser versions:

* Firefox version 45 and later
* Opera version 11 and later
* Safari version 5.1 for Windows and version 6.0 and later for MacOs
* Internet Explorer version 9 and later
* Microsoft Edge version 25 and later
* Google Chrome version 48 and later

Indeed, some browsers are likely not to support certain functionalities of the guichet-entreprises.fr website. 

We recommend that you use your browser's latest version and update it regularly to benefit from security patches and ensure optimum performances.

The guichet-entreprises.fr website is optimized for a 1024x768 screen resolution. 

### Can I save an application if it is not complete? <!-- collapsable:close -->

You can save an application at any moment and finish it within one month. To do this, just click on « Enregistrer et quitter » (Save and quit).

You will have the possibility to resume the completion of your applications without re-entering the information you already provided. 

The application is sent to the relevant authority for good once you clicked on « Envoyer mon dossier » (Send my application).

### Can I modify an application if it is not complete? <!-- collapsable:close -->

You can modify your application as long as it has not been sent to recipient authorities.

### Can I modify an application that has been sent on to recipient authorities? <!-- collapsable:close -->

In accordance with the regulations in force, you cannot modify or delete an application on guichet-entreprises.fr once it has been validated and sent to recipient authorities.

If you wish to modify or delete an application that has been sent to recipient authorities, please contact them directly.

### Can I delete an application? <!-- collapsable:close -->

It is possible to delete an application that has not been validated. In this case, [please contact the INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

Once your application has been validated and sent to relevant authorities, it can no longer be deleted. To do so, you need to contact the CFE to which it has been sent.

**Good to know**

The applications that have not been validated after three months will automatically be deleted from the digital platform.

### Why do I have to pay fees whereas my application is supposed to be free of charge?

The fees you might have to pay are calculated based on the information you provided on your business. If the payment(s) do not seem to concern you, please check the information you provided in step 1 and 2

For instance, registering as a *micro-entrepreneur* carrying out a liberal activity is free of charge, except if the limited liability sole proprietor status (EIRL) was chosen. A *micro-entrepreneur* carrying out a liberal activity who chose the limited liability sole proprietor (EIRL) status must be registered in the RSEIRL registry (*Registre Spécial des Entrepreneurs Individuels à Responsabilité Limitée* (Special registry of sole proprietors with limited liability)), which entails registration fees.

### I have a problem entering my social security number in the relevant form field <!-- collapsable:close -->

If the Social Security number you provided is not accepeted after several attempts, you should check whether the number you entered corresponds to your birthdate. We check whether the birthdate and the Social Security number match.

If your number is still not accepted, you can temporarily fill in the box with fifteen zeros. Then, in the last page of the form (« *Formalité* »), all you need to do is enter your Social Security number in the "Observations" box on the top of the page.

### I cannot find my place of birth in the drop-down menu <!-- collapsable:close -->

In the last years, some towns have been grouped together due to territorial reorganisations. As the former town no longer exists administratively, you need to select the new administrative town in the « Commune de naissance » (Town of birth) field. 

If need be, you may also indicate the name of the former town in the "Observations" box at the end of the form. The name of the town is irrelevant since the *code commune* does not change. It is the *code commune* (code of the town from the INSEE's reference system) that matters.

Examples in the Calvados department:

- If you were born in *Aunay-sur-Odon*, you must now select the town of *Les Monts d'Aunay*.
- If you were born in *Mézidon-Canon*, select *Mézidon Vallée d'Auge* in the drop-down menu.

### I cannot find my postal code <!-- collapsable:close -->

The « code postal » (postal code) only applies to French addresses. For foreign countries, all you need to enter are the country and town. The postal code can be entered in the « complément d’adresse » form field (additional address details).

## 6. Uploading my supporting documents <!-- collapsable:open -->

### What should be the format of my attachments? <!-- collapsable:close -->

In order to prevent any technical error during the forwarding of your application to the recipient authority, we recommend that you comply with certain standards as you upload supporting documents.

- your supporting documents should not exceed 2MB. If they do, an error message informing you that the attachments could not be uploaded to your application will appear on screen,
- your supporting documents must also comply with the PDF format,
- the PDF file must not be protected, or else it will not be attached to the application.

Check that the PDF files that include form fields are not corrupt and that they are displayed correctly in Acrobat Reader.

If you encounter a problem with an attachment, please [contact the INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1) prior to validating and sending your application.

### Can I download the attachments I uploaded? <!-- collapsable:close -->

It is possible to delete and/or change the documents you uploaded on guichet-entreprises.fr at any moment, as long as your application has not been validated and sent to the relevant centre for business formalities(CFE) for processing. 

At any moment, you can download your application from your dashboard, even though it has been sent to the relevant centre for business formalities (CFE).

### Why was I asked to provide documents that do not concern me? <!-- collapsable:close -->

The online service determines the required supporting documents based on the information you provided in your declaration.

If you think that the requested supporting documents do not concern you, please check the information you provided in the online form.

### I was asked to provide "the list of subscribers", what is it? <!-- collapsable:close -->

The list of subscribers is required in the applications to start or modify the information about a legal person (company), public limited companies (*Sociétés anonymes* (SA)) and simpified limited companies (*Sociétés par Actions Simplifiées* (SAS)) more particularly.

The list of subscribers comprises the list of the various shareholders, the number of subscribed shares and the sums each of them paid.

This list is provided separately from the documents provided by the bank that holds the funds comprising the company's share capital. This list must be drawn up on a letterhead of the company that is being set up.

### What kind of documents can I submit to prove my home address? <!-- collapsable:close -->

**For natural persons:**

You must submit a proof of address issued within the last three months comprising the family name and the first name of the business owner. 

The documents that can be submitted are:

- invoices (electricity, gas, water, landline or mobile phone, residence tax),
- the home purchase certificate or the residential lease agreement if you moved in recently.

If the home you live in is not in your name, you must submit a certificate of less than 3 months comprising the surname and first name of the homeowner and an accommodation letter (document written by the homeowner certifying that you live in his or her home).

If you live in a rented home, you must submit a copy of the residential lease agreement and a certificate by the landlord consenting to your business being located on his property.

**For legal persons:**

In addition to the proof of address of the head office of less than three months, the senior manager must write a domiciliation certificate, which should include the following elements:

- the identity of the applicant,
- the company name,
- the exact address of the head office as it written on the business's Kbis extract,
- the date and the signature of the senior manager.

### Why was I asked to provide a "copy of the diploma, title or any document proving my professional qualification"? <!-- collapsable:close -->

This document is requested when the business activity requires a professional qualification and when the entrepreneur did not undertake to hire a qualified employee for the activity in the declaration. 

Users are therefore asked to provide a proof of their qualification (diploma) or work experience:

- payslips or employment certificates **for employees**,
- registration certificate (SIREN), Kbis extract and proof of registration to the Registry of trades (*Répertoire des Métiers* (RM)) **for senior managers**.

### I received a mail asking me to modify the supporting documents I attached to my application. How should I proceed? <!-- collapsable:close -->

If you received an additional mail asking you to modify the supporting documents you uploaded, it means your application has been approved and sent the relevant authority. 

At this stage, reopening the application on guichet-entreprises.fr is no longer possible. 

Additional documents and/or information requested by the recipient(s) of the application must be brought to the authority directly, via mail, email or physical submission, according to the recommendations of the authority and with the references it requested.

## 7. Paying my application fees and sending my application <!-- collapsable:open -->

### Who are the recipients of my payment on guichet-entreprises.fr? <!-- collapsable:close -->

When you complete an application online, you have the possibility to pay the fees related to the online procedure. The guichet-entreprises.fr website forwards the payments made on the website directly to recipient authorities. All complaints related to the payment service should be addressed to the recipient authority in charge of your application.

### I made a payment on guichet-entreprises.fr: what should I do if the recipient authority asked me to pay the same fees again? <!-- collapsable:close -->

If the recipient authority in charge of your application asks you to pay administrative fees again, you may send a copy of your payment receipt on www.guichet-entreprises.fr. The authority will thus be able to identify your payment. Should this not be the case, please [contact the INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### How can I get an invoice? <!-- collapsable:close -->

If you made a payment related to your administrative procedure on guichet-entreprises.fr, you have the possibility to ask an invoice directly to the authority that collected the fees. You will have to attach the invoice you received via email after you made the payment to your request.

### When will my application be forwarded to the recipient authority? <!-- collapsable:close -->

Once the application you created on www.guichet-entreprises.fr is complete, it is forwarded to recipient authorities for processing.

In order to know if your application has been sent, log in to your account and check that the progress bar of the application in question reached 100% and turned green.

The applications are forwarded within a period of between 1 to 24 hours.

If you notice that the progress bar of your application has not reached 100% and turned green more than 48 working hours after its completion, please [contact the INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### Which centre for business formalities (CFE) is in charge of my application? <!-- collapsable:close -->

The remit of a centre for business formalities (*Centre de Formalités des Entreprises* (CFE)) depends on the nature of a business's legal form, its business activity and the location of its head office. 

- **business activity and legal form**:

<table class="table table-striped table-bordered">
<thead style="text-align: center;border-collapse: collapse">
<tr>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Your business activity</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Your CFE</th>
</tr>
</thead>
<tbody style="text-align: center;border-collapse: collapse">
<tr>
<td style="border: 1px solid #AAA;padding: 4px"><a href="https://www.economie.gouv.fr/entreprises/statut-entreprise-individuelle">Sole proprietor</a> or company (<a href="https://www.economie.gouv.fr/entreprises/entreprise-unipersonnelle-responsabilite-limitee-EURL">EURL</a>, <a href="https://www.economie.gouv.fr/entreprises/societe-responsabilite-limitee-sarl">SARL</a>, <a href="https://www.economie.gouv.fr/entreprises/societe-anonyme-SA">SA</a> - <a href="https://www.economie.gouv.fr/entreprises/societe-actions-simplifiee-SAS">SAS</a>, <a href="https://www.economie.gouv.fr/entreprises/societe-en-nom-collectif-snc">SNC</a>) c&rryong out a commercial business activity
</td>
<td style="border: 1px solid #AAA;padding: 4px">Chamber of commerce and industry (*Chambre de Commerce et d'Industrie* (CCI))</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">Sole proprietor or company carrying out an artisanal activity</td>
<td style="border: 1px solid #AAA;padding: 4px">Chamber of trades and crafts (Chambre de Métiers et de l'Artisanat* (CMA))</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px"> Inland waterway transport businesses or river cooperative companies</td>
<td style="border: 1px solid #AAA;padding: 4px">Chamber of trades and crafts (Chambre de Métiers et de l'Artisanat* (CMA))</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">Sole proprietor or company carrying out an agricultural business activity as their main activity</td>
<td style="border: 1px solid #AAA;padding: 4px"><a href="http://www.chambres-agriculture.fr/">Chamber of agriculture(*Chambre d'Agriculture* (CA))</a></td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">Sole proprietors practising a liberal profession<br>Artists/authors/composers/performers
</td>
<td style="border: 1px solid #AAA;padding: 4px">URSSAF </td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">Commercial agent (natural person)<br>Non-trading company (SCI, SCM, SCP, etc.)<br>*Société d'exercice libéral* (SELARL, SELAFA, SELCA)<br>*Société en participation*<br>*Établissement public et industriel* (EPIC)<br>Economic interest grouping (*Groupement d'intérêt économique* (GIE))<br>Associations subjected to commercial taxes<br>Lessor of furnished accommodations
</td>
<td style="border: 1px solid #AAA;padding: 4px">Court of first instance hearing commercial cases (*Greffe du tribunal de commerce ou du tribunal de grande instance (TGI) statuant commercialement*)</td>
</tr>
</tbody>
</table>

- **the location of the business activity**.

Once your procedure is over, write down the contact details of the CFE in order to contact it directly if need be. 

## 8. I have a technical problem <!-- collapsable:open -->

### I encounter display errors <!-- collapsable:close -->

The use of the guichet-entreprises.fr requires an internet browser set to accept session cookies. For an optimal browsing experience, we recommend the use of the following web browsers:

* Firefox version 45 and later
* Opera version 11 and later
* Safari version 5.1 for Windows and version 6.0 and later for MacOs
* Internet Explorer version 9 and later
* Microsoft Edge version 25 and later
* Google Chrome version 48 and later

Indeed, some browsers are likely not to support certain functionalities of the guichet-entreprises.fr website.

We recommend that you use your browser's latest version and update it regularly to benefit from security patches and ensure optimum performances.

The guichet-entreprises.fr website is optimized for a 1024x768 screen resolution. 

### A message says an error occurred <!-- collapsable:close -->

If an error occurred during the completion of your application, please [contact the INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### I have a problem with my account <!-- collapsable:close -->

#### You cannot log in to your account <!-- collapsable:close -->

If you cannot log in to your account, clear cookies and try to log in from Google Chrome or Mozilla Firefox web browsers using an incognito or private window.

**Good to know**

If this does not work, then your account may have been deactivated. In this case, click on [*Renouveler mon mot de passe*](https://account.guichet-entreprises.fr/users/renew) (Reset my password) and follow the procedure to reset your password in order to log in again.

#### You did not receive the account confirmation email or the password reset email <!-- collapsable:close -->

If you did not receive the account confirmation email or the password reset email, check the junk mail of your inbox. 

If there is no such email, create a new account using the same email address. If the problem persists, please [contact the INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

#### The confirmation email does not work <!-- collapsable:close -->

The link in the confirmation email is unique and valid for 24 hours. After 24 hours, you will need to create a new account with the same email address.

**Good to know**

This link is a one-time link. If you already activated your account, click on [Connection](https://account.guichet-entreprises.fr/session/new) to access it.

### I cannot reset my password <!-- collapsable:close -->

The link to reset your password is unique and valid for 24 hours. If the link does not work, it probably means that it is no longer valid. You therefore have to make a [new request to reset your password](https://account.guichet-entreprises.fr/users/renew).

For security purposes, you must change your password every 90 days.

### I cannot upload my supporting documents <!-- collapsable:close -->

Your supporting documents must be in PDF format and not exceed 2MB. 

If you have problems uploading documents, we recommend that you clear your browser's cache or use another web browser. Moreover, ensure that the PDF files are not protected.

We recommend the use of the following web browser versions:

* Firefox version 45 and later
* Opera version 11 and later
* Safari version 5.1 for Windows and version 6.0 and later for MacOs
* Internet Explorer version 9 and later
* Microsoft Edge version 25 and later
* Google Chrome version 48 and later

If your documents do not comply with these standards, an error message informing you that the attachments could not be uploaded to your application will appear on screen. 

### An error occurred when I tried to upload a document <!-- collapsable:close -->

If an error occurs while you upload a document, check its format and size and ensure that the PDF files that include form fields are not corrupt and that Acrobat Reader displays them correctly.

Your documents must be in PDF format and not exceed 2MB. 

If the problem persists, please [contact the INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

**Good to know**

Modifying extensions manually can cause a technical error when you upload an attachment (for instance, turning a .doc extension into a .pdf extension without converting the file).

### I cannot pay my application fees <!-- collapsable:close -->

If a technical error occurs during the payment step, please [contact the INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1).

### Je n'arrive pas à signer le formulaire <!-- collapsable:close -->

The electronic signature service can be unavailable due to many simultaneous connections.

In this case, please try again later.

## 9. Processing of applications <!-- collapsable:open -->

### Who should I contact concerning the follow-up of my application? <!-- collapsable:close -->

Once your application has been validated, it is immediately transferred to the relevant CFE and, when appropriate, to the registrar for processing. 

These authorities are the only authorities competent to process your application, you must therefore contact them for any information or modification request.

The contact details of the recipient authorities in charge of your application are displayed when your procedure is over. Write down their contact details to contact them directly if you wish to know if your application has been processed, for instance. 

### How long does it take for the relevant authorities to process an application? <!-- collapsable:close -->

The guichet-entreprises.fr website enables users to complete a business creation application online. Once you completed and validated it, your application is forwarded to the relevant centre for business formalities (*Centre de Formalités des Entreprises* (CFE)) for processing.

The guichet-entreprises.fr website therefore cannot provide you with information regarding the processing time of your application by CFEs.

If additional documents are necessary to process your application, you will be notified via mail by the CFE.

**Good to know**

If you have any question regarding your application (follow-up, processing, etc.) once you validated and sent it, you must contact the centre for business formalities in charge of your application.

### When and how will I receive my business's SIREN and SIRET numbers? <!-- collapsable:close -->

The [INSEE](https://www.insee.fr/fr/accueil)  (*Institut National de la Statistique et des Études Économiques* or National Institute of Statistics and Economic Studies) directly sends the SIREN and SIRET numbers via mail and email.

Contact the centre for business formalities (*Centre de Formalités des Entreprises* (CFE)) in charge of your application for more information. Your CFE will tell you when the request has been sent on to the INSEE.

### When will I receive my K-bis/K extract? <!-- collapsable:close -->

If you need to be registered in the Registry of trade and companies (*Registre du Commerce et des Sociétés (RCS)), the Clerk's office of the commercial court (*Greffe du Tribunal de Commerce* (GTC)) with territorial competence will send you a Kbis extract (for legal persons) or K extract (for natural persons carrying out a commercial business actvity or carrying out both a commercial and an artisanal business activity).

Micro-entrepreneurs and sole proprietors carrying out a liberal business activity are not provided with a Kbis or K extract.

The processing time depends on the Clerks office (*greffe*) and may vary depending on the admissibility of your application and the need to provide additional information or supporting documents.

### Why am I told that there is no trace of my application when I contact the centre for business formalities or registrar in charge of my application?

If, beyond fifteen working days, the recipient authority has not acknowledged receipt of the application, please [contact the INPI](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1). Do not forget to provide the reference of your application starting in "2020-" or "2021" or the liasse number starting in "H10000".

### I would like to modify, cancel or delete an application I completed on guichet-entreprises.fr <!-- collapsable:close -->

#### You have not validated your application on guichet-entreprises.fr yet

- You can modify yor application.

**Be careful:** Certain pieces of information cannot be modified: Those provided in the first step of the procedure. Indeed, the information about the activity, the location of your activity, the legal status and certain additional pieces of information (if you already carried on a self-employed activity or not, for instance, or if the business activity is supposed to start as soon as the business is created) determine the online form you need to complete, as well as the recipients of the application. If you notice that any information is incorrect after you validated step 1, you will have to create a new application.

- You can delete your application.

**Good to know:** The applications that have not been completed and validated within three months will automatically be deleted.

#### You validated your application which has been sent to the relevant CFE

In accordance with the regulations in force, it is no longer possible to modify or delete an application on guichet-entreprises.fr. 
Please contact the authorities in charge of your application if you need to make any modification:
  - If your application has not been processed by the recipent authority, you will have to check whether you can make modifications without creating a new application online.
  - If your application has been processed, you will have to complete a modification form in order to correct the incorrect piece(s) of information.

**Be careful:** In the first case, if the modifications entail completing a new form or determining a new recipient authority, you will have to start a new application. In the second case, you need to receive your SIREN number prior to making any modification, since it is necessary to complete this procedure.

**Good to know:** In accordance with Article R. 123-18 of the Commercial Code, the Guichet Entreprises is no longer in charge of the application once it has been transferred to relevant authorities. In this context, the applications that have been completed and validated will be deleted from the portal within three months. Once the applications have been processed by relevant authorities, the information concerning non-trading and commercial companies will be published in the National registry of trade and companies. They will be [made available to the public](https://www.inpi.fr/fr/services-et-prestations-domaine/bases-de-donnees) as open data and [disseminated for further use](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041593491/) as part of accredited licences for further use.

As soon as users' application(s) have been transferred to relevant authorities, users may contact them to access their application(s) or access them directly on Guichet Entreprises within three months.

However, the deletion of a validated application, prior to its automatic deletion within three months, must be requested to the INPI via the [contact form](https://www.inpi.fr/fr/contactez-nous?about=299&under_about=883-702-b1). The modification of a validated application must be done via the completion of a new application.