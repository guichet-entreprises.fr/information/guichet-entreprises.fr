﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Legal notice" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="True" -->
<!-- var(key)="en-__footer__-mentions_legales" -->
<!-- var(last-update)="2020-09" -->
<!-- var(lang)="en" -->

Legal notice
===========

Latest update: <!-- begin-var(last-update) -->2020-09-01<!-- end-var -->

## Identification of the publisher

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p>

The Guichet entreprises service is in charge of the editorial design, follow-up, maintenance and updating of the guichet-entreprises.fr website.

The Guichet Entreprises service is a department within the INPI, pursuant to Decree No. 2020-946 of 30 July 2020.

## Purpose of the website

The guichet-entreprises.fr website enables any entrepreneur to complete, from home, the necessary formalities and procedures to start a business, modify the information about a business and cease a business activity, either by sending the single application defined in Article R. 123-23 of the Commercial Code if it respects the provisions of Articles R. 123-24 of the Commercial Code, or by completing an application online and by sending it on.

Applications are signed electronically. The modalities of electronic signature are available in the electronic signature policy.

Besides, the guichet-entreprises.fr website provides entrepreneurs with information on formalities, procedures and requirements related to regulated activities, pursuant to [Article R. 123-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006255836&dateTexte=&categorieLien=cid) of the Commercial Code.

## Processing of personal data

Personal data included in the single application, in accordance with the General Data Protection Regulation, are subject to provisions available [here](./protection_donnees.md).

You have the right to access and rectify your personal data. However, the person in charge of your application must process it to comply with legal requirements. Therefore, you cannot oppose the processing of your application, pursuant to the provisions of Article 6.1 c) of Regulation (UE) 2016/679 of the European Parliament and of the Council of 27 April 2016, and to Article 56 of the consolidated version of Act No. 78-17 of 6 January 1978 on information technology, data files and civil liberties.

There are several ways you can exercise this right:

* By contacting the centre for business formalities (CFE) in charge of your application,
* By sending an email to the user support  via the [contact form](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1),
* By sending a mail to the following address:

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;À l'attention du délégué à la protection des données<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p>

## Reproduction rights

The content of this website comes under French and international legislation on copyright and intellectual property.

All the graphic elements of the website are the property of the INPI. Any reproduction or adaptation of the website's pages using its graphic elements is strictly prohibited.

Any commercial use of the website's contents is also prohibited.

Any quote or reuse of the website's content must have been authorised by the publishing director. The source (guichet-entreprises.fr), the date of copy and the INPI must be referred to.

## Links to the website's pages

Any public or private website is authorised to have links to the pages of the guichet-entreprises.fr website. No prior authorisation is required. However, the source of information must be referred to. For instance: "Business creation (source: guichet-entreprises.fr, a website operated by the INPI)". The pages of the guichet-entreprises.fr website cannot be embedded within the pages of another website. They must be posted in a new window or tab.

## Links to pages of external websites

The links published on guichet-entreprises.fr may direct users to external websites, the content of which is in no way the responsibility of the INPI.

## Technical environment

Some browsers can prevent windows from opening on this website by default. You must authorise the opening of windows when your browser asks you to by clicking on the banner that appears on top of the page in order for certain pages to open.

If no message appears in your browser, you must set it up in order for it to open windows of the guichet-entreprises.fr website.