﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="About us" -->
<!-- var(keywords)="About us" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-__footer__-qui_sommes_nous" -->
<!-- var(lang)="en" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(translation)="Pro" -->

About us
================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

**Guichet-entreprises.fr is the national website of the National institute of industrial property (INPI) that enables business creators and entrepreneurs to complete their formalities.**

**Creation, modification of information or closure, etc.: More than 2,000 formalities can be completed online.**

## Guichet-entreprises.fr: The INPI's website to simplify business creation

The guichet-entreprises.fr website is operated by the Guichet Entreprises service of the INPI, pursuant to Decree No. 2020-946 of 30 July 2020.

The Guichet Entreprises service operates both the guichet-entreprises.fr and [guichet-qualifications.fr](https://www.guichet-qualifications.fr/en/) websites. Both these websites form the digital point of single contact defined by European Directives [2006/123/CE](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32006L0123&from=EN) and [2005/36/CE](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32005L0036&from=FR).

On guichet-entreprises.fr, business creators and executives can complete online and securely the administrative formalities related to their business's lifecycle: Creation, modification of information (address or legal structure modification, etc.) or closure.

As far as business creation is concerned, the guichet-entreprises.fr website records business creation applications and sends them on to the relevant centres for business formalities (*centre de formalités des entreprises (CFE)*) from one of the following networks:

* The chambers of commerce and industry (CCI) – [cci.fr](https://www.cci.fr/)
* The chambers of agriculture (CA) – [chambres-agriculture.fr](https://chambres-agriculture.fr/)
* The chamber of trades and crafts (CMA) – [cma-france.fr](https://cma-france.fr/)
* The URSSAFs – [urssaf.fr](https://www.urssaf.fr/portail/home.html)
* The clerks of the commercial courts – [cngtc.fr](https://www.cngtc.fr/fr/)

## Guichet-entreprises.fr: Accessing regulated professions

In France, some activities (restaurant owner, estate agent, certified accountant, etc.) require an authorisation or approval in order to be carried out. 

Fact sheets on these regulated activities are available on guichet-entreprises.fr.

## Guichet-entreprises.fr: A European website

Guichet-entreprises.fr is also intended for EU and EEA nationals who wish to set up a business in France.

The website provides information on the conditions to carry out an activity on the long term (freedom of establishment) or temporarily (freedom to provide services) in France. It also provides an online service to complete all their formalities.

## Guichet-qualifications.fr: A website for the recognition of professional qualifications

[1] The [guichet-qualifications.fr](https://www.guichet-qualifications.fr/en/) website promotes professional mobility by providing citizens with as detailed information as possible on how to access and practise regulated professions in France.

The guichet-entreprises.fr and [guichet-qualifications.fr](https://www.guichet-qualifications.fr/en/) are fully secure and have been accreddited by the French National Cybersecurity Agency (ANSSI).

## Guichet-entreprises.fr: Facts and figures

**Guichet-entreprises.fr provides:**

* **\+ than 2,000 online procedures**
* **\+ than 100 fact sheets on regulated activities**
* **\+ 250 fact sheets on regulated professions**