﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Use of cookies on guichet-entreprises.fr" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Pro" -->
<!-- var(key)="en-__footer__-cookies" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="en" -->

Use of cookies on guichet-entreprises.fr
================================

## What are cookies and how does the INPI use them?

The INPI can use cookies when users browse the website. Cookies are files that are sent to browsers via a web server in order to record the activities of users while they browse the website. Cookies enable the service to identify users' web browser in order for them to browse the website more easily. Cookies are also used to measure the website's audience and produce browsing statistics.

The cookies used by the INPI do not provide the service with references that could enable it to infer users' personal data or personal information that could enabe it to identify a specific user. These cookies are temporary. Their only objective is to make further transfer more efficient. The cookies used on the website cannot be used for more than two years.

Users have the possibility to set their browser to be informed when they receive cookies and refuse their installation on their device. When users prevent cookies from being stored on their device or when they disable them, they may not access certain functionalities of the website.

If cookies are refused or disabled, users should restart the session.

## What kinds of cookies are used by Guichet Entreprises?

### Technical cookies

Technical cookies enable users to browse the website and use some of its functionalities.

### Analytical cookies

The INPI uses analytical cookies to analyse the website's audience and assess the number of users. These cookies enable the service to assess the number of users and analyse the way they browse the website.