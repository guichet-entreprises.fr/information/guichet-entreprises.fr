<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| The Guichet Entreprises information repository is licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International licence.
| 
| Please visit the following address to obtain a copy of that licence:
| http://creativecommons.org/licenses/by-nd/4.0/
| or send a mail to: Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Guichet-entreprises.fr: A secure website" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Pro" -->
<!-- var(key)="" -->
<!-- var(last-update)="2020-12" -->
<!-- var(lang)="en" -->

Guichet-entreprises.fr: A secure website
=====================================

The Guichet Entreprises service of the INPI was issued a cybersecurity accreditationon on 1 June 2019 for a period of 30 months.

This accreditation guarantees that the service complies with the cybersecurity requirements of the [ANSSI](https://www.ssi.gouv.fr/) (French National Cybersecurity Agency) regarding access, keyboarding, storage, the processing and exchange of data.

The approach to analyse security risks is based on the principles laid down in the ANSSI's EBIOS method (general security database) and on the ISO 27005 standard.