﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Our partners" -->
<!-- var(keywords)="Partenaires" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-__footer__-partenaires" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

# Our partners

Bringing the best experts on business creation together!

The guichet-entreprises.fr website was initiated by the French government in order to implement the [European Directive on services](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32006L0123&from=FR).

The centres for business formalities (*centre de formalités des entreprises* (CFEs)) are the main partners of guichet-entreprises.fr, since they are naturally close to the interests of entrepreneurs and their first point of contact. Their collaboration enables French and European entrepreneurs to start their business on a simple and complete website.

## Our partners <!-- collapsable:off -->

### The chambers of agriculture (CA) <!-- collapsable:close -->

The chambers of agriculture (CA) offer services to farmers in various domains: technical advice on plant and animal production, advice in business set up, transfer, planning, diversification, etc.

Since 1997, they have managed centres for business formalities (CFEs) by providing farm businesses with assistance to complete the procedures to start a business, modify the information about a business or cease a business activity.

Agricultural CFEs centralise and make the various procedures that need to be completed with different organisations (INSEE, tax servcies, MSA, department breeding establishments, vineyard register, Clerk's office of the commercial courts for companies) easy. The declarations concerning busines creation, the modification of information about a business or the cessation of a business activity that are required by the law and regulations in force can be done in one single place and on one single document. They ensure that the declarations and supporting documents comply with requirements and that they are sent to the recipient authorities. The confidentiality of collected information is guaranteed.

Since 2006, CFEs are in charge of both collecting agricultural assets declarations and keeping records of agricultural assets.

Since 2009, following the European Directive on services No. 2006/123/CE of December 12 2006, the CFEs of the chambers of agriculture are the point of single contact for equestrian centres. They provide information and complete all the procedures necessary to start a business for these service businesses.

You can make an appointment with your chamber of agriculture, in which the agricultural CFE's advisers will help you to complete procedures.

The chambers of agriculture are grouped together at regional level in the regional chambers of agriculture. The **Permanent assembly of the chambers of agriculture** (*Assemblée permanente des chambres d’agriculture (APCA)*) is the coordinator of the chambers of agriculture at national level. It is an associate member of the French agricultural council.

**Contact:** Assemblée permanente des chambres d’agriculture - Service Entreprises, 9, avenue George V, 75008 Paris

[> Visit the website of the chambers of agriculture](https://chambres-agriculture.fr/) [FR]

### The chambers of commerce and industry (CCI) <!-- collapsable:close -->

The chambers of commerce and industry are public establishments run by peer-elected business entrepreneurs. They are in charge of:

* **Representing business interests vis-à-vis public authorities**
* **Counselling project leaders and businesses** at each stage of their lifecycle
* **Training associates and business leaders** (apprenticeship, business schools, engeneering schools)
* **Managing equipment that is necessary to regions** (business incubators, harbours, airports, exhibition and convention centres, etc.).

The chambers of commerce and industry (CCIs) are part of the French CCI network, which is coordinated at national level by **CCI France**.

The chambers of commerce and industry use their strong and diverse competences to help you develop your business creation, takeover or development project and offer a tailored pathway for your project and training to develop your entrepreneurship skills.

Don't hesitate to contact your local CCI. The advisors of your local CCI will help you with your studies and its centre for business formalities will process your administrative formalities.

[> Visit the website of the chambers of commerce and industry](https://www.cci.fr/) [FR]

### The chambers of trades and crafts (CMA) <!-- collapsable:close -->

The chambers of trades and crafts are the first counselling network to artisanal businesses. They are managed by elected artisanal business entrepreneurs.

CMAs are present in mainland France and overseas territories. They are key partners for artisanal businesses as they help them develop their economic situation, competitiveness and stability: They offer, in close contact with businesses, services that suit the specific needs of each type of public, be they apprentices, business entrepreneurs or buyers, employees, jobseekers, people looking for career guidance or artisanal business assignors.

This offer covers all the stages of a business's lifecycle, from the creation or takeover to all the stages of its development, including the formation of assets.

Each year, project leaders benefit from a tailored pathway that includes post-creation counselling in one of the 372 reception points of the CMA network. CMAs can involve their network of partners (banks, notaries, certified accountants, lawyers and trade unions) to help entrepreneurs formalise their project and meet their financial, legal, fiscal and social needs.

**CMAs are also in charge of:**

* Keeping a registry of legal publicity (the registry of trades)
* Managing the centres for business formalities that are competent to deal with sole proprietorships and companies subject to registration in the registry of trades and with people carrying out an artisanal activity who are exempted from being registered in this registry
* Certifying the *artisan*, *artisan d'art* (handicraftsman) or *maître artisan* (master craftsman) statuses
* Organising apprenticeship in the artisanal sector in partnership with French regions.

In order to find your nearby chamber of trades and crafts, [visit the website of the chambers of trades and crafts](https://www.artisanat.fr/).

National phone number: +33825 36 36 36 (€0.15/min all taxes included)

### The clerks of the commercial courts <!-- collapsable:close -->

The clerks of the commercial courts are **an original and efficient model to help businesses and economic justice.**

At the crossroads of the legal and economic sectors, the clerks of the commercial courts fulfill **an efficient and modern public service remit.** They are involved in each important stage of businesses' lifecycle, from their creation to the cessation of their activity, and from the resolution of disputes to the solving of difficulties.

The clerks of the commercial courts are real civil registrars for businesses who are efficiently involved in legal security and the transparence of economic life.

Appointed by decision of the Ministry of Justice, the clerks of the commercial courts are entrusted with the state's public authority. They fulfill their assignments under the supervision of the public prosecutor.

Several duties come within the jurisdictional remit of the clerks of the commercial courts:

* **Duties towards litigants and the court:** assistance to judges, keeping of acts and archives, authentication and issuing of copies of court rulings
* **Duties of an economic nature towards businesses:** the clerks of the commercial courts are legal professionals and experts in the keeping of legal registries (control of formalities in the registry of trade and companies, keeping and publicity of security interests and dissemination of legal and financial information on businesses).

Their experience together with their mastery of technological innovations is a considerable asset to the benefit of commercial justice. The clerks innovate to the benefit of businesses and litigants, while fulfilling public service and community assignments within a regulatory framework established by the law.

The status of the clerks of the commercial courts and their assignments meet two requirements:

* **The satisfaction of the state**, on behalf of whom they exercise certain prerogatives and to whom they must be competent, loyal and ethical
* **The satisfaction of commercial court users**, of whom they are the direct points of contact.

**The clerks of the commercial courts are involved in commercial justice in the service of jurisdiction and litigants:**

The juridical assignments of clerks, as members of the commercial courts, relate to disputes between businesses, and to the prevention and solving of businesses' difficulties. Each year, they deal with more than a million court rulings.

**The clerks of the commercial courts are involved in economic life, in the service of businesses:**

By keeping legal registries, clerks are a unique point of view on economic life. By providing users with the information from these registries, i.e. from 60,000 to 80,000 acts a day, the clerks of the commercial courts enable anyone to obtain reliable information on businesses and their senior managers and to ensure that a commercial partner is reliable economically and financially.

The clerk's office of either the commercial courts or the courts of first instance hearing commercial cases are the relevant centres for business formalities regarding:

a) Non-trading companies and other non-commercial companies,
b) *Sociétés d'exercice libéral*,
c) Juridical persons subject to registration in the registry of trade and companies, other than:
  * Traders,
  * Commercial companies,
  * Natural persons and companies subject to registration in the registry of trades,
  * Natural persons who are exempted from registration, pursuant to the Act of 5 July 1996 on the development and promotion of trade and tradesmanship,
  d) Public establishments of an industrial and commercial nature
e) Commercial agents
f) Economic interest groupings and European economic interest groupings.

[> Visit the website of the National council of the clerks of the commercial courts](https://www.cngtc.fr/fr/) [FR]

[> Leaf through the virtual book introducing the clerks of the commercial courts](https://www.cngtc.fr/page-flip/les-greffiers-des-tribunaux-de-commerce/) [FR]

### The URSSAFs <!-- collapsable:close -->

**The URSSAF network** [[1]](#1) <a id="Retour1"></a> is the driving force of our social protection system. Its main duty is to collect social security contributions, which finance the general social security scheme. It is entrusted with collecting and controlling the payment of contributions by more than 800 partners. Thus, it collects **unemployment insurance and wage guarantee scheme contributions** [[2]](#2) <a id="Retour2"></a> on behalf of the UNEDIC. It also calculates contributions and issues requests for the payment of contributions to the Social protection scheme for the self-employed (*Régime social des indépendants (RSI)*).

The URSSAF network's strategy is based on the quality development of relations with and services to 9.5 million users. As a modern and reliable public service, the URSSAF network has developed specific service offers, for individuals employing domestic workers (CESU, Pajemploi), associations (CEA) and SMEs (TESE) more particularly.

The URSSAFs are also the centre for business formalities (*centre de formalités des entreprises (CFE)*) of liberal professions, associations employing staff, original artists, taxi drivers renting a driving licence, self-employed door-to-door salesmen and micro-entrepreneurs (liberal professions and traders) who complete their formalities on the [autoentrepreneur.Urssaf.fr](https://www.autoentrepreneur.urssaf.fr/portail/accueil.html) website [FR].

The **Central agency for social security bodies** (*Agence centrale des organismes de sécurité sociale (ACOSS)*) is the national fund of the URSSAF network. Aside from running its network, the ACOSS manages the funds of the various branches of the general scheme.

In order to offer consistent services while maintaining strong local presence, the ACOSS strengthened its network by opening 22 regional URSSAFs that complete its office in French departments.

Since 2011, the ACOSS is also the only organisation accredited to compile statistics on salaried employment on a quarterly basis.

[> Visit the website of the URSSAF network](https://www.urssaf.fr/portail/home.html) [FR]

**<a id="1"></a> [[1]](#Retour1) URSSAF:** Unions for the collection of social security contributions and family allowances (*Union de recouvrement des cotisations de sécurité sociale et d’allocations familiales*)

**<a id="2"></a> [[2]](#Retour2) AGS :** Association for the management of the employee wage guarantee scheme (*Association pour la gestion du régime de garantie des créances des salariés* (or *Régime de garantie des salaires*))

## Main users of guichet-entreprises.fr <!-- collapsable:off -->

Proxies and proxy associations are the main users of the guichet-entreprises.fr service.

Are you a proxy? Do you often use the guichet-entreprises.fr website for your activities? <a href="mailto:slelong@inpi.fr">Contact us</a> to improve your use of our website.