﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Personal data protection policy" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Pro" -->
<!-- var(key)="en-__footer__-protection_donnees" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="en" -->

Personal data protection policy
===============================================

Last update: <!-- begin-var(last-update) -->2021-06<!-- end-var -->

<table class="table table-striped table-bordered">
<thead style="text-align: center;border-collapse: collapse">
<tr>
<th style="border: 1px solid #AAA;padding: 4px">Version</th>
<th style="border: 1px solid #AAA;padding: 4px">Date</th>
<th style="border: 1px solid #AAA;padding: 4px">Object</th>
</tr>
</thead>
<tbody style="text-align: center;border-collapse: collapse">
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V1</td>
<td style="border: 1px solid #AAA;padding: 4px">25 May 2018</td>
<td style="border: 1px solid #AAA;padding: 4px">Original version</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V2</td>
<td style="border: 1px solid #AAA;padding: 4px">01 September 2018</td>
<td style="border: 1px solid #AAA;padding: 4px">Update of the information regarding the data protection officer</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V3</td>
<td style="border: 1px solid #AAA;padding: 4px">05 May 2019</td>
<td style="border: 1px solid #AAA;padding: 4px">Update of CERFA form references</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V4</td>
<td style="border: 1px solid #AAA;padding: 4px">1 September 2020</td>
<td style="border: 1px solid #AAA;padding: 4px">Update following the coming into force of Decree No. 2020-946 of 30 July 2020 designating the National institute of industrial property as the single body referred to in paragraph 9 of Article 1 of Act No. 2019-486 of 22 May 2019 on growth and the transformation of businesses, and entrusting this institute with the management of the computer services referred to in Articles R. 123-21 and R. 123-30-9 of the Commercial Code</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V5</td>
<td style="border: 1px solid #AAA;padding: 4px">3 June 2021</td>
<td style="border: 1px solid #AAA;padding: 4px">Update concerning the personal data retention period and the processing of applications</td>
</tr>
</tbody>
</table>

The personal data protection policy (hereinafter the "policy") provides you with information on the way your data are collected and processed by the Guichet Entreprises service of the INPI, on the security measures implemented to guarantee their integrity and confidentiality, and on your rights to control their use.

This document complements the terms of use and, where necessary, the legal notice. Consequently, the present document is deemed to be incorporated in the said documents.

As part of the development of our services and the implementation of new regulatory standards, we may modify the present document. We therefore recommend that you read it on a regular basis.

## About us

The INPI operates an online service that enables any entrepreneur to complete, from home, the necessary formalities and procedures to start a business, modify the information about a business and cease a business activity, either by sending the single application defined in Article R. 123-23 of the Commercial Code if it respects the provisions of Articles R. 123-24 of the Commercial Code, or by completing an application online and by sending it on.

The online Guichet Entreprises services fits into the legal framework of:

* [Directive 2006/123/CE](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32006L0123&from=EN) of the European Parliament and of the Council of 12 December 2006 on services in the European Single Market
* [Regulation No. 910/2014](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32014R0910&from=hr) of the European Parliament and of the Council of 23 July 2014 (eIDAS) on electronic identification and trust services for electronic transactions in the Single Market
* [Regulation No. 2016/679](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32016R0679&from=EN) of the European Parliament and of the Council of 27 April 2016 on the processing of personal data
* [Act No. 2019-486](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038496102&categorieLien=id) of 22 May 2019 on growth and the transformation of businesses (Article 1 in particular)
* [Act No. 2020-946](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000042182948&categorieLien=id) of 30 July 2020 designating the National institute of industrial property as the single body referred to in paragraph 9 of Article 1 of Act No. 2019-486 of 22 May 2019 on growth and the transformation of businesses, and entrusting this institute with the management of the computer services referred to in Articles R. 123-21 and R. 123-30-9 of the Commercial Code
* [Ordinance of 8 December 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) on electronic exchanges between users and administrative authorities, and between administrative authorities, and the [Ministerial Decree No. 2010-112](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id)of 2 February 2010 implementing Articles 9, 10 and 12 of this Ordinance
* The provisions of the amended [Act No. 78-17](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) of 6 January 1978 on information technologies, data files and civil liberties
* [Article 441-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) of the Penal Code
* [Articles R. 123-1](https://www.legifrance.gouv.fr/codes/id/LEGIARTI000021926763/2010-03-04/) to [R.123-30](https://www.legifrance.gouv.fr/codes/id/LEGIARTI000032944426/2017-01-01/) of the Commercial Code. 

## What measures do we implement to protect your data?

The data protection officer of the INPI service ensures that the provisions applicable to personal data protection are respected and implemented.

## How to contact us 

If you have any question about the use of your personal data by the INPI, please read the present document and contact our data protection officer:

- By mail. The mail should be addressed to:

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;À l’attention du délégué à la protection des données personnelles<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001 92677 Courbevoie Cedex</p>

- By email, with the following subject line: "Data protection" (*Protection des données*). The email should be addressed to [this address](mailto:guichet-entreprises-support@inpi.fr).

## What are personal data?

Personal data are pieces of information that can be used to identify you directly or indirectly. These can be your surname, first name, address, telephone number or email address, but also your computer's IP address, for instance, or the information related to the use of the online Guichet Entreprises service.

## When do we collect personal data?

The INPI collects personal data when you:

* create an account,
* complete a procedure via the online Guichet Entreprises service.

## What personal data do we process?

We process personal data that only concern business formalities in accordance with the regulatory provisions of Articles R. 123 to R. 123-27 of the Commercial Code.

The data that are collected by the service are used to compile the "single application" defined in Article R. 123-7 of the Commercial Code.

The data that are collected are used in particular to complete the Cerfa documents related to the necessary formalities and procedures to start a business, modify the information about a business and cease a business activity.

Below is a table which lists these Cerfa documents.

<table class="table table-striped table-bordered">
<thead>
<tr>
<th style="text-align:center;border: 1px solid #AAA;padding: 4px">Procedure</th>
<th style="text-align:center;border: 1px solid #AAA;padding: 4px">Legal form</th>
<th style="text-align:center;border: 1px solid #AAA;padding: 4px">Cerfa: Main form</th>
<th style="text-align:center;border: 1px solid #AAA;padding: 4px">Cerfa: Possible inserts</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Creation</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Micro-entrepreneur</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13821<br>(P0 PL micro-entrepreneur)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14214 (PEIRL Micro-entrepreneur)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">15253<br>(P0 CMB micro-entrepreneur)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14215 (PEIRL CMB)<br>11771 (P0’)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13821 (AC0)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14218 (PEIRL PL / AC)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Sole proprietor<br>(exclusive of the micro-entrepreneur scheme)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11768 (P0 PL)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14218 (PEIRL PL / AC)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11676<br>(P0 CMB exclusive of the micro-entrepreneur scheme)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14215 (PEIRL CMB)<br>11771 (P0’)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11922 (P0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11771 (P0’)<br>14216 (PEIRL agricole)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13821 (AC0)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14218 (PEIRL PL / AC)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SARL</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Exclusive of agricultural activities<br>11680 (M0 SARL, SELARL)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11772 (M0 ’ SARL, SELARL)<br>11686 (TNS)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Agricultural activities<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>14116 (Agri-com)<br>11925 (NSM agricole)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SELARL</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11680 (M0 SARL, SELARL)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11772 (M0 ’ SARL, SELARL)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SAS</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Exclusive of agricultural activities<br>13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Agricultural activities<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>14116 (Agri-com)<br>11925 (NSM agricole)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Exclusive of agricultural activities<br>13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Agricultural activities<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>14116 (Agri-com)<br>11925 (NSM agricole)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SNC</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Exclusive of agricultural activities<br>13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Agricultural activities<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>14116 (Agri-com)<br>11925 (NSM agricole)&lt;br&gt;11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCS</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>14077 (JQPA)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SELAFA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SELAS</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SELCA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13959 (M0 SAS, SA…)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14068 (M0’ SAS, SA…)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Non-trading company</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13958 (M0 société civile)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14067 (M0’ société civile)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCM</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13958 (M0 société civile)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14067 (M0’ société civile)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCP</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13958 (M0 société civile)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14067 (M0’ société civile)<br>11686 (TNS)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">GAEC</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Activité agricole<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>11925 (NSM agricole)<br>11926 (NSP)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">SCEA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Agricultural activities<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>11925 (NSM agricole)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">EARL</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Agricultural activities<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>Cerfa11925 (NSM agricole)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">GFA</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Agricultural activities<br>11927 (M0 agricole)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14117 (M0’ agricole)<br>Cerfa11925 (NSM agricole)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Regularisation</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Micro-entrepreneur</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">15260<br>(R CMB micro-entrepreneur)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14077 (JQPA)<br>11771 (P0’)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Modification of information</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Micro-entrepreneur</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13905 (P2P4)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14077 (JQPA)<br>14214 (PEIRL Micro-entrepreneur)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">11678 (P2 CMB)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14077 (JQPA)<br>14215 (PEIRL CMB)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Deregistration</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Micro-entrepreneur</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">13905 (P2P4)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">14214 (PEIRL Micro-entrepreneur)<br>11679 (P4 CMB)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Declaration of beneficial owners</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Companies (including non-trading companies and foreign trading companies)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16062 (M’BE)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16062 (M’BE) insert in M0, M2 and M3 forms)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Collective investments</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16063 (M’BE)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16063 (M’BE) insert in M0, M2 and M3 forms)</td>
</tr>
<tr>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px"></td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">Economic interest grouping or association goverened by the status of 1901</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16064 (M’BE)</td>
<td style="text-align:center;border: 1px solid #AAA;padding: 4px">16064 (M’BE)  insert in M0, M2 and M3 forms)</td>
</tr>
</tbody>
</table>

The following list sums up the personal data that we process.

To manage the access to their account on the service, users share the following information:

* Their user ID
* Their password
* Their email address
* Their mobile phone number
* Their IP address.

To use their personal storage account, users share the following information:

1° For natural persons:

* Surname, birth name
* First name, usual name
* Birthdate and place of birth
* Nationality
* Address, mail address
* Marital situation, matrimonial regime
* The spouse's surname, the spouse's birth name
* The spouse's first name, the spouse's usual name
* The spouse's address
* The spouse's date and place of birth
* The spouse's nationality
* Showman status, itinerant status
* Trader status in a Member State of the European Union other than France
* Declaration of non-seizability for the primary residence, place of publication in the Bureau of mortgages's registry
* Names of the previous owners, birth names of the previous owners
* First names of the previous owners, usual names of the previous owners
* Name of the goodwill's renter, birth name of the goodwill's renter
* First name of the goodwill's renter, usual name of the goodwill's renter
* Address of the goodwill's renter
* Social security number
* Healthcare insurance scheme
* Previous self-employed activity
* The social security number of the associate spouse or associate civil partner (*Pacs*)
* The surname of the authorised representative
* The birth name of the authorised representative
* The first name of the authorised representative
* The usual name of the authorised representative
* The address of the authorised representative
* The birthplace of the authorised representative
* The birthdate of the authorised representative
* The nationality of the authorised representative
* The assignee's surname
* The assignee's birth name
* The assignee's first name
* The assignee's usual name
* The assignee's social security number
* The assignee's birthplace
* The assignee's birthdate
* The assignee's nationality
* Family relationship
* The signatory's surname
* The signatory's birth name
* The signatory's first name
* The signatory's usual name
* The signatory's address.

2° For legal persons:

* Surname of the goodwill's renter
* Birth name of the goodwill's renter
* First name of the goodwill's renter
* Usual name of the goodwill's renter
* Address of the goodwill's renter
* Nationality of the senior manager of an associate company
* Surname of the senior manager of an associate company
* Birhtname of the senior manager of an associate company
* First name of the senior manager of an associate company
* Usual name of the senior manager of an associate company
* Address of the senior manager of an associate company
* Birthplace of the senior manager of an associate company
* Birthdate of the senior manager of an associate company
* Surname of the legal person's authorised representative
* Birth name of the legal person's authorised representative
* First name and usual name of the legal person's authorised representativey
* Address of the legal person's authorised representative
* Birthdate and birthplace of the legal person's authorised representative
* Nationality of the legal person's authorised representative
* The spouse's surname
* The spouse's birth name
* The spouse's first name
* The spouse's usual name
* The spouse's address
* The spouse's birthplace
* The spouse's birthdate
* The spouse's nationality
* Name of another person who can act on behalf of the company
* Birthname of another person who can act on behalf of the company
* First name of another person who can act on behalf of the company
* Usual name of another person who can act on behalf of the company
* Address of another person who can act on behalf of the company
* Birthplace of another person who can act on behalf of the company
* Birthdate of another person who can act on behalf of the company
* Nationality of another person who can act on behalf of the company
* The signatory's surname
* The signatory's birthname
* The signatory's first name
* The signatory's usual name
* The signatory's address
* The senior manager's social security number
* The senior manager's health insurance scheme
* The social security number of the associate spouse or associate civil partner (*Pacs*)
* The assignee's surname
* The assignee's birth name
* The assignee's first name
* The assignee's usual name
* The assignee's social security number
* The assignee's birthplace
* The assignee's birthdate
* The assignee's nationality
* Family relationship.

## What personal supporting documents do we process?

The personal supporting documents that we process only concern business formalities in accordance with the regulatory provisions of Articles R. 123-1 to R. 123-27 of the Commercial Code.

The supporting documents are used to compile the "single application" defined in Article R. 123-7 of the Commercial code.

The list of supporting documents is subject to approval by the authority specified in [Article 3 of Decree No. 98-1083](https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000208797&idArticle=LEGIARTI000006544269&dateTexte=&categorieLien=cid) of 2 december
1998 on administrative simplification.

The documents proving the user's identity and home address are part of the personal supporting documents. These supporting documents are stored according to the same modalities as personal data (see below).

They are stored on storage spaces that cannot be accessed from outside (Internet). Encryption enables a secure download and transfer in accordance with the provisions of the section entitled "How are your personal data protected?" below. 

## How do we manage cookies?

Our management of cookies is described [here](cookies.md).

All the cookies we manage are secure cookies.

Secure cookies are exclusively sent via encrypted connections (https).

Technical cookies contain the following information: surname, first name, email, telephone number.

Analytical cookies contain the connection IP address.

## What are your rights concerning the protection of your personal data?

You have the right to access and rectify your personal data. However, the person in charge of your application must process it to comply with legal requirements. Therefore, you cannot oppose the processing of your application, pursuant to the provisions of Article 6.1 c) of Regulation (UE) 2016/679 of the European Parliament and of the Council of 27 April 2016, and to Article 56 of the consolidated version of Act No. 78-17 of 6 January 1978 on information technology, data files and civil liberties.

If you think the use of your personal date goes against the regulation on the protection of personal data, you may file a complaint with the *Commission nationale de l'informatique et des libertés* (CNIL or National commission on information technologies and civil liberties).

You may also provide instructions concerning the processing of your personal data in the event of your death. Specific instructions can be recorded with the person in charge of data processing. General instructions can be recorded with a digital trusted third party certified by the CNIL. You may modify or delete these instructions at any moment.

If need be, you may also ask us to send you the personal data you provided in a readable format.

You may send your requests to the following address:

- By mail. The mail should be addressed to:

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;À l’attention du délégué à la protection des données personnelles<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001 92677 Courbevoie Cedex</p>

* By email, with the following subject line: "Data protection" (*Protection des données*). The email should be addressed to [this address](mailto:guichet-entreprises-support@inpi.fr).

## How are your personal data protected?

The INPI implements all the standard industry practices and security procedures to prevent any personal data breach.

All the information provided by users are stored on secure servers hosted by our web hosting providers.

When the transmission of data is necessary and authorised by users, the INPI ensures that these third parties offer enough guarantees to provide an adequate level of protection. Exchanges are encrypted and servers are authenticated by mutual recognition.

Pursuant to the General Data Protection Regulation, should a data breach occur, the INPI undertakes to inform the relevant supervisory authority of the data breach and, where required, the persons concerned.

## User account

All the data transferred to your account are encrypted and accessed securely via the use of a personal password. You must keep this password confidential and you must not disclose it to anyone.

## Personal data retention period and processing of applications

Collected personal data are necessary for the processing of applications.

These pieces of information are collected by the INPI in accordance with [Decree No. 2020-946 of 30 July 2020](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042182948?r=EJdJkGOj3Y) and regulatory provisions of Articles R. 123-1 to R. 123-27 of the Commercial Code. The data that are collected by the service are used to compile the "single application" defined in Article R. 123-7 of the Commercial Code. Personal data collected during the creation of an account and the completion of an application on guichet-entreprises.fr are stored for one year (Articles R. 123-18 to R. 123-27 of the Commercial Code).

In accordance with Article R. 123-18 of the Commercial Code, the Guichet Entreprises is no longer in charge of an application once it has been transferred to relevant authorities. In this context, the applications that have been completed and validated will be deleted from the portal within three months. Once the applications have been processed by relevant authorities, the information concerning non-trading and commercial companies will be published in the National registry of trade and companies. They will be [made available to the public](https://www.inpi.fr/fr/services-et-prestations-domaine/bases-de-donnees) as open data and [disseminated for further use](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041593491/) as part of accredited licences for further use.

As soon as users' applications have been transferred to relevant authorities, users may contact them to access their applications or access them directly on Guichet Entreprises within three months.

Users can modify or delete applications online as long as they have not been validated. However, the deletion of a validated application, prior to its automatic deletion within three months, must be requested to the INPI via the [contact form](https://www.inpi.fr/fr/contactez-nous?about=299&under_about=883-702-b1). The modification of a validated application must be done via the completion of a new application.

Any application that has been started but not validated will automatically be deleted from the portal within three months. 

If you have any question regarding the protection of personal data, please [contact the data protection officer of the INPI](https://www.inpi.fr/fr/contactez-nous?about=299&under_about=883-702-b1). You will need to prove your identity. You may contact the CNIL (*Commission Nationale de l'Informatique et des Libertés* or National commission on information technologies and civil liberties) as a means of redress.

## Credit card data 

The INPI does not store or keep the credit card data of users who need to make a payment as part of their online procedure. Our payment service provider manages transaction data on behalf of the INPI, in accordance with the strictest security rules applying to online payment via the use of encrypted processes.

## To whom are your personal data transferred?

The data that are collected are used to compile the "single application" defined in Article R. 123-7 of the Commercial Code.

Pursuant to Article R. 123-3 of the Commercial Code, the data collected by the service are transferred to recipient authorities in order to be processed. These authorities are:

"1° Subject to the provisions of 2° and 3°, the territorial chambers of commerce and industry create and manage the competent centres for business formalities for:

1. Traders,
2. Trading companies.

2° The chambers of trades and crafts create and manage the competent centres for natural persons and companies recorded in the registry of trades. 

3° The Clerks' offices of the Commercial Courts or the Clerks' offices of the Courts of First Instance hearing commercial cases create and manage the competent centres for:

1. Non-trading companies and other non-commercial companies,
2. *Sociétés d'exercice libéral*,
3. Legal persons subject to registration in the registry of trade and companies, other than those referred to in 1°and 2°,
4. Public establishments of an industrial and commercial nature,
5. Commercial agents,
6. Economic interest groupings and European economic interest groupings.

4° The Unions for the collection of social security contributions and family allowances (URSSAFs) or the General social security funds create and manage the competent centres for:

1. The persons whose regular occupation consists in carrying out a self-employed activity, be it regulated or not, if this activity is not a commercial, artisanal or agricultural one,
2. Employers whose businesses are not registered in the registry of trade and companies, the registry of trades or the registry of independent inland waterway transport businesses, and who are not concerned by the centres referred to in 6°

5° The chambers of agriculture create and manage the competent centres for natural and legal persons whose main occupation is to carry on agricultural activities[…]."

Pursuant to the provisions of Articles L. 411-1 and D. 411-1-3 of the Intellectual Property Code, collected data are to be made available on the [data.inpi.fr](https://data.inpi.fr/) portal of the National institute of industrial property via its accredited open data licences.