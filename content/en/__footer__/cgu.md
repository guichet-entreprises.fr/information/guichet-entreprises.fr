﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="Terms of use" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="Pro" -->
<!-- var(key)="en-__footer__-cgu" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(lang)="en" -->

Terms of use
======================================

Latest update: <!-- begin-var(last-update) -->2020-09-01<!-- end-var -->

## Introduction

This document provides the terms of use of the online Guichet Entreprises service provided by the National institute of industrial property (INPI) for users. It fits into the legal framework of:

* [Directive 2006/123/CE](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32006L0123&from=EN) of the European Parliament and of the Council of 12 December 2006 on services in the European Single Market<br/><br/>
* [Regulation No. 910/2014](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32014R0910&from=hr) of the European Parliament and of the Council of 23 July 2014 (eIDAS) on electronic identification and trust services for electronic transactions in the Single Market<br/><br/>
* [Regulation No. 2016/679](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32016R0679&from=EN) of the European Parliament and of the Council of 27 April 2016 on the processing of personal data<br/><br/>
* [Act No. 2019-486](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038496102&categorieLien=id) of 22 May 2019 on growth and the transformation of businesses (Article 1 in particular)
* [Act No. 2020-946](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000042182948&categorieLien=id) of 30 July 2020 designating the National institute of industrial property as the single body referred to in paragraph 9 of Article 1 of Act No. 2019-486 of 22 May 2019 on growth and the transformation of businesses, and entrusting this institute with the management of the computer services referred to in Articles R. 123-21 and R. 123-30-9 of the Commercial Code<br/><br/> 
* [Ordinance of 8 December 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) on electronic exchanges between users and administrative authorities, and between administrative authorities, and the [Ministerial Decree No. 2010-112](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021779444&categorieLien=id)of 2 February 2010 implementing Articles 9, 10 and 12 of this Ordinance<br/><br/>
* The provisions of the amended [Act No. 78-17](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) of 6 January 1978 on information technologies, data files and civil liberties<br/><br/>
* [Article 441-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) of the Penal Code<br/><br/>
* [Articles R. 123-1](https://www.legifrance.gouv.fr/codes/id/LEGIARTI000021926763/2010-03-04/) to [R.123-30](https://www.legifrance.gouv.fr/codes/id/LEGIARTI000032944426/2017-01-01/) of the Commercial Code.  

## Purpose of the document

The purpose of this document is to lay down the terms of use of the INPIs online Guichet Entreprises service, hereinafter the "Service", between the INPI and users.

This service is operated under the authority of the Director-General of the National Institute of Industrial Property (INPI).

## Definition and purpose of the service

The Service is an online service implemented by the INPI's Guichet Entreprises (hereinafter the "Guichet Entreprises service of the INPI") which contributes to facilitate the procedures to start a business, modify the information about a business and cease a business activity for French and European users.

This service obtained a security accreditation in accordance with the 901/SGDSN/ANSSI interministerial instruction of the ANSSI (NOR : PRMD1503279J) of 28 January 2015 and with the PGSSI (*General Security Policy of Information Systems*) of Finance Ministries (NOR : FCPP1622039A) of 1 August 2016.

The use of the Service is free and optional. However, you might be requested to pay administrative fees when you complete certain formalities on this portal, such as when you start a business, for instance. These payments are secure.

All the recipients of the applications forwarded by the Service are hereinafter called partner authorities.

Article R. 123-3 of the Commercial Code states that the data collected by the service are forwarded to partner authorities in order to be processed. These authorities are:

"1° Subject to the provisions of 2° and 3°, the territorial chambers of commerce and industry create and manage the competent centres for business formalities for:

a) Traders
b) Commercial companies.

2° The chambers of trades and crafts create and manage the competent centres for natural and legal persons recorded in the registry of trades.

3° The Clerk's offices of the Commercial Courts or the Clerk's offices of the Courts of First Instance hearing commercial cases create and manage the competent centres for:

a) Non-trading companies and other non-commercial companies<br/>
b) *Sociétés d'exercice libéral*<br/>
c) Legal persons subject to registration on the registry of trade and companies, other than those referred to in 1°and 2°<br/>
d) Public establishments of an industrial and commercial nature<br/>
e) Commercial agents<br/>
f) Economic interest groupings and European economic interest groupings.

4° The Unions for the collection of social security contributions and family allowances (URSSAFs) or the General social security funds create and manage the competent centres for:

a) The persons whose regular occupation consists in carrying out a self-employed activity, be it regulated or not, if this activity is not a commercial, artisanal or agricultural one
b) Employers whose businesses are not registered in the registry of trade and companies, the registry of trades or the registry of independent inland waterway transport businesses, and who are not concerned by the centres referred to in 6°.

5° The chambers of agriculture create and manage the competent centres for natural and legal persons whose main occupation is to carry out agricultural activities[…]."

Pursuant to the provisions of Articles L. 411-1 et D. 411-1-3 of the Intellectual Property Code, collected data are to be made available on the [data.inpi.fr](https://data.inpi.fr/) portal of the National institute of industrial property via its accredited open data licences.

## Functionalities

The Service enables users to:

* have access to the documentation specifying the duties of centres for business formalities (CFEs) as well as the constituent components of the declaration application and of the authorisation application,
* create a user account giving access to a personal storage space. This account enables users to manage and use their personal data, to retain the information that concerns them, as well as the documents and supporting documents that are necessary to complete administrative procedures.

From their account, users can:

* compile their application referred to in Article R. 123-1 of the Commercial Code, which includes the declaration application and, where necessary, the authorisation applications,
* send their declaration application and, where necessary, authorisation applications to the relevant centre for business formalities,
* have access to the follow-up of their declaration application and, where necessary, authorisation applications,
* enable the centre for business formalities to process the information sent by the legal person referred to in Article R. 123-21 of the Commercial Code: receipt of the application provided for in Article 2 of Act No. 94-126 of February 11th 1994 on individual initiative and enterprise and sent by the legal person referred to in Article R. 123-21 of the Commercial Code; receipt of follow-up information of these applications sent on by partner organisations and authorities; forwarding of follow-up information of applications to the legal person referred to in Article R. 121-21 of the Commercial Code.

## Registration procedure and use of the Service

The Service is free-of-charge and open to all users. The service is optional. Users can complete their formalities via other means.

To manage the access to their account on the Service, users share the following information:

* their email address,
* the login they chose.

Users are required to provide personal data to use the Service with a view to compile the single application.
The processing of personal data is described in the personal data protection policy, which is available [here](./protection_donnees.md).

An internet connection and an internet browser are necessary to use the Service. Your browser must be set to accept session cookies.

For an optimal browsing experience, we recommend the use of the following web browser versions:

* Firefox version 45 and later,
* Google Chrome version 48 and later.

Indeed, some browsers are likely not to support certain functionalities of the Guichet Entreprises website.

The Service is optimized for a 1024x768 screen resolution. We recommend that you use your browser's latest version and update it regularly to benefit from security patches and ensure optimum performances.

## Specific terms of use for the signature service

The electronic signature service can be accessed directly via the Service.

Article R. 123-24 of the Commercial Code, pursuant to Regulation No. 910/2014 of the European Parliament and of the Council of 23 July 2014 (eIDAS) are the references that can be applied to the electronic signature service of the Service.

## Roles and commitments

### Commitments of the Guichet Entreprises service of the INPI

1.   The Guichet Entreprises service of the INPI develops and operates the Service in accordance with the legal framework in force defined in the introduction.
2.   The Guichet Entreprises service of the INPI undertakes to take all the necessary measures to ensure the security and confidentiality of the information submitted by users.
3.   The Guichet Entreprises service of the INPI undertakes to protect the data collected by the Service. It is particularly committed to preventing the data from being distorted, damaged or accessible by third parties, pursuant to the measures provided for in the Ordinance of 8 december 2005 on electronic exchanges between users and administrative authorities and between administrative authorities, pursuant to the measures provided for in Decree No. 2010-112 of 2 February 2010 implementing Articles 9, 10, and 12 of the Ordinance, and pursuant to Regulation No. 2016/679 of the European Parliament and of the Council of 27 April 2016 on the processing of personal data.
4.   The Guichet Entreprises service of the INPI and partner authorities ensure that the users of the Service have the right to access, correct and oppose data, as provided for in Act No. 78-17 of 6 January 1978 on information technology, data files and civil liberties and in Regulation No. 2016/679 of the European Parliament and of the Council of 27 April 2016 on the processing of personal data. This right can be exercised in several ways:
	a) By contacting the centre for business formalities (CFE) in charge of your application,
	b) By sending an email to the user support via the [contact form](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1),
	c) By sending a mail to the following address:

<br>
<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;Pôle Guichet Entreprises<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p><br>

5.   The Guichet Entreprises service of the INPI and partner authorities are committed not to commercialise the information and documents submitted by users via the Service or to pass them on to third parties in circumstances not provided for by law.
6.   The Guichet Entreprises service of the INPI undertakes to ensure the traceability of all the actions carried out by all the users of the Service, including the actions carried out by partner authorities and individual users.
7.   The Guichet Entreprises service of the INPI offers support services in the event of an identified incident or security alert.

## User commitments

1.   Users complete and approve their application online and, if need be, attach the documents necessary for the processing of the dossier.
2.   Following the completion of the application by users, a summary of the elements provided by users appears in order for them to check and confirm these elements. After users confirm these elements, their application is forwarded to the relevant CFE. The validation and sending of the form by users is equivalent to a handwritten signature.
3.   The present terms of use apply to all the users of the Service.

## Availability and development of the Service

The Service is available on a 24 hour/7 day basis.

The Guichet Entreprises service of the INPI reserves the right to develop, modify or suspend the Service without notice for maintenance reasons or any other reason deemed necessary by the Guichet Entreprises service of the INPI. The unavailability of the Service shall not give rise to any compensation. Should the Service be unavailable, a page informing users that the Service is unavailable should appear. Users will be invited to complete their procedure later on.

The present terms of use can be amended at any moment, without notice, according to the changes of the Service, the legislative and regulative developments, or for any other reason deemed necessary by the Guichet Entreprises service of the INPI.

## Responsibilities

1.   The Guichet Entreprises service of the INPI cannot be held accountable in the event of an identity theft or any fraudulent use of the Service.
2.   The data that are sent on to the online services of partner authorities remain the exclusive responsibility of users, even though they are sent on via the technical means provided by the Service. Users can modify or delete them with partner authorities at any moment. They can choose to delete all the information on their account by deleting their data with the Service.
3.   Users are reminded that any person making a false declaration for themselves or someone else is subject to penalties provided for in Article 441-1 of the Penal Code, which provides penalties of up to three years' imprisonment and €45,000.