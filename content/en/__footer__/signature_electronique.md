﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Electronic signature policy" -->
<!-- var(keywords)="Electronic signature policy" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-__footer__-signature_electronique" -->
<!-- var(lang)="en" -->
<!-- var(last-update)="2020-09-01" -->
<!-- var(translation)="Pro" -->

<style type='text/css'>
.TabulTexte { margin-left: 2em; }
table {
    border-collapse: collapse;
}
th,td,tr {
    border: 1px solid #AAA;
    padding: 4px;
}
</style>

Electronic signature policy
===================================

<table class="table table-striped table-bordered">
<thead style="text-align: center;border-collapse: collapse">
<tr>
<th style="border: 1px solid #AAA;padding: 4px">Version</th>
<th style="border: 1px solid #AAA;padding: 4px">Date</th>
<th style="border: 1px solid #AAA;padding: 4px">Object</th>
</tr>
</thead>
<tbody style="text-align: center;border-collapse: collapse">
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V1</td>
<td style="border: 1px solid #AAA;padding: 4px">25 May 2018</td>
<td style="border: 1px solid #AAA;padding: 4px">Original version</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V2</td>
<td style="border: 1px solid #AAA;padding: 4px">1 September 2018</td>
<td style="border: 1px solid #AAA;padding: 4px">Update following the coming into force of Decree No. 2020-946 of 30 July 2020 designating the National institute of industrial property as the single body referred to in paragraph 9 of Article 1 of Act No. 2019-486 of 22 May 2019 on growth and the transformation of businesses, and entrusting this institute with the management of the computer services referred to in Articles R. 123-21 and R. 123-30-9 of the Commercial Code</td>
</tr>
<tr>
<td style="border: 1px solid #AAA;padding: 4px">V3</td>
<td style="border: 1px solid #AAA;padding: 4px">3 June 2021</td>
<td style="border: 1px solid #AAA;padding: 4px">Update concerning the storage period of personal data and the processing of applications</td>
</tr>
</tbody>
</table>

## Purpose of the document

The electronic signature that is appended on a set of data guarantees the integrity of transferred data. It also guarantees that the signed data will not be rejected and that the sender is authenticated.

The present electronic signature policy is a document that describes the eligibility requirements of a file in which an electronic signature is appended by recipient authorities, i.e. the centres for business formalities and registrars, as part of the electronic exchanges referred to in Article R. 123-24 of the Commercial Code.

Pursuant to this article, "when a signature is required, resorting to secure electronic signature is a requirement subject to the conditions set out in Articles 1316-4 of the Civil Code and in Decree No. 2017-1416 of 28 September 2017 on electronic signature. However, for the electronic transfer of business creation and declaration applications referred to in Article L. 526-7, including applications for registration to the registry of trade and companies, the use of electronic signature complying with the characteristics set out in the first sentence of the second paragraph of Article 1367 of the Civil Code (formerly Article 1316-4) is authorised."

Indeed, the regulatory provisions in force for the implementation of the electronic signature of the online Guichet Entreprise service come from the following legislative texts:

* Regulation No. 910/2014 of the European Parliament and of the Council of 23 July 23 2014 (eIDAS) on electronic identification and trust services for electronic transactions in the Single Market
* The Civil Code
* Decree No. 2017-1416 of 28 September 2017 on electronic signature.

If the electronic signature requires the provision of personal data, the regulatory provisions in force come from the following legislative texts:

* Regulation (EU) 2016/679 of the European Parliament and of the Council of 27 April 2016 on the protection of natural persons concerning the processing of personal data and the flow of these data repealing Directive 95/46/CE (General Data Protection Regulation)
* Act No. 78-17 of 6 January 1978 on information technology, data files and civil liberties.

The present document, "Electronic signature policy of the online Guichet Entreprises service", sets out all the rules and provisions defining the requirements that all the players involved in these digitised exchanges need to meet for the transmission and reception of data flow.

This document is intended for:

* centres for business formalities
* public registrars
* the service providers that could take part in these digitised exchanges on behalf of these recipient authorities.

In the following sections of this document:

* the aforesaid recipient authorities are referred to as "recipients"
* the aforesaid digitised exchanges are referred to as "applications"
* the National institute of industrial property is referred to as the "INPI".

## Scope of application

Electronic signature is required for any formality relating to business creation, the modification of information about a business or the cessation of a business activitity, or relating to the access to and to the carrying out of a regulated activity as referred to in Directive 2006/123/CE of the European Parliament and of the Council of December 12 2006 on services in the Single Market ("Services Directive") and referred to in Article R. 123-1 of the Commercial Code.

The characteristics of electronic signature vary depending on the nature of forwarded applications.

**When the application concerns a business creation procedure**, the required electronic signature meets the provisions of the first sentence of the second paragraph of Article 1316-4 of the Civil Code:

"When the signature is electronic, it consists in using a reliable identification process that guarantees it is related with the act related thereto."

**When the application concerns a procedure to modify the information about a business or cease a business activity**, the electronic signature meets the provisions of Article 1 of Decree No. 2017-1416 of 28 September 2017 (reference to the "eIDAS" Regulation No. 910/2014):

"An electronic signature process is deemed to be reliable, until proven otherwise, when this process implements a qualified electronic signature." 

A qualified electronic signature is an advanced electronic signature, which complies with Article 26 of the above-mentioned Regulation, and that is created via a qualified electronic signature creation device meeting the requirements of Article 29 of the Regulation, based on a qualified certificate for electronic signature meeting the requirements of Article 28 of the Regulation.

Article 26 of the "eIDAS" Regulation No. 910/2014:

"An advanced electronic signature meets the following requirements: 

1.	It is uniquely linked to the signatory
2.	It allows the identification of the signatory
3.	It was created using electronic signature creation data that the signatory can, with a high level of confidence, use under his sole control
4.	It is linked to the data signed therewith in such a way that any subsequent change in the data is detectable."

For your information, the "eIDAS" Regulation No. 910/2014 sets out three levels of guarantee:

* Low: The objective of this level of guarantee is simply to reduce the risk of misuse or alteration of identity.
* Substantial: The objective of this level of guarantee is to reduce substantially the risk of misuse or alteration of identity.
* High: The objective of this level of guarantee is to prevent any misuse or alteration of identity.

## Identification

The identification of this electronic signature policy is indicated by the presence of the certificate of the INPI used for the electronic signature of the document.

The certificate's serial number is:

4B-51-5A-35-00-00-00-00-01-EF.

## Publication of the document

Prior to its publication, this electronic signature policy was approved by the Chief information security officer (CISO) of the INPI.

## Update process

### Circumstances making an update necessary

The update of the present electronic signature policy can be caused by changes in the law in force (cf. "Purpose of the document"), the emergence of new threats and new security measures, the consideration of the various stakeholders' comments.

The present electronic signature policy is reexamined at least once every two years.

### Consideration of comments

All comments or requests for change concerning the present electronic signature policy should be addressed by email to the following address: [contact](mailto:guichet-entreprises-support@inpi.fr).

Comments and requests for change are examined by the CISO of the INPI, who updates the present electronic signature policy when necessary.

### Informing stakeholders

The information relating to the current and previous versions of this policy is available in the table of versions at the head of this document.

The publication of a new version of the electronic signature policy consists in:

1. 	publishing the electronic signature policy in HTML format
2. 	archiving the previous version after each page was flagged with the word "obsolete".

## Entry into force of the new version and period of validity

A new version of the electronic signature policy comes into force only one calendar month after it was published and remains valid until a new version comes into force.

The recipients should use the one-month time limit to take the changes to the electronic signature policy into account in their applications.

## Stakeholders

### Application signatories

Application signatories are the registrants or their proxy who complete(s) formalities to start a business, modify the information about a business or cease a business activity.

#### Role of signatories

The role of signatories is to append their electronic signature to their application which contains the form and the supporting documents relating to it.

In order to append an electronic signature to their application, signatories commit to use a signature tool that complies with the present electronic signature policy.

#### Obligations of signatories

These obligations are described in the sections below.

### Signature tool

Signatories must check the data they are about to sign prior to appending their electronic signature.

### Electronic signature procedure

The electronic signature procedure varies depending on type of administrative procedure (cf. "Scope of application").

**As far as business creation formalities are concerned**, signatories must tick a box at the end of the formality. The ticked box serves both as a sworn declaration that the information provided by signatories is correct and as a signature.

It is then indicated that the signature complies with paragraph 3 of Article A. 123-4 of the Commercial Code in the signature field of the CERFA form.

Paragraph 3 of Article A. 123-4 of the Commercial Code:

"3° By ticking the electronic box, registrants make a sworn declaration that the information they provided is correct. The declaration is formulated as follows: "I declare on my word of honour that the information provided in the formality is correct and sign the present declaration No...., made in ..., on ..."". 

**As far as the applications to modify the information about a business and cease a business activity are concerned**, signatories must attach a copy of an ID document certified as true to the original that complies with the provisions of paragraph 2 of Article A. 123-4 of the Commercial Code:

Paragraph 2 of Article A. 123-4 of the Commercial Code:

"2° The documents comprised in the declaration were digitised. The copy of the proof of identity is digitised after the signatory appended a hand-written statement certifying that the copy is true to the original, the date and a hand-written signature of the registrant.

The document to be signed electronically (the CERFA form and the copy of the piece of identification) is presented to the registrant, as well as the agreement of proof listing the conditions and consequences of signing the document electronically.

Signatories must tick a box to indicate that they read the agreement of proof and accept it without reservation.

A code to validate the electronic signature is sent to the mobile phone number that registrants provided to create their account.

When registrants enter this code, the document (the CERFA form and the copy of the piece of identification) is sealed — which means it is signed electronically based on a certificate — hashed, and all the elements are timestamped.

The sealed document is archived and sent to the relevant recipient(s).

The most frequent error cases are the following:

**Case 1 = Incorrect code** 

Functional instance:

*The user entered an incorrect code (letters instead of numbers, for instance).*

Exemple of message displayed:

*The code you entered is incorrect. Please check the code you received via SMS and enter it again.*

**Case 2 = Code no longer valid**

Functional instance:

*The user entered the code he/she received via SMS too late (a one-time password is valid for 20 minutes).*

Exemple of message displayed:

*Your code is no longer valid. You must make a new electronic signature request.*

**Case 3 = Blocked code**

Functional instance:

*The user made several unsuccessful attempts to enter the code. In order to prevent hacking attempts, the user's phone number is identified as blocked in the system.*

Exemple of message displayed:

*You made too many input errors. Consequently, you no longer can sign your formality electronically using your phone number (+33600000000). Please provide a new phone number in your user account or contact the Guichet Entreprises service of the INPI to unblock your current phone number.*

#### Protection and use of signed applications

This section only deals with the applications relating to the formalities to modify the information about a business or cease a business activity.

The documents that are signed electronically are stored in a digital vault and are accompanied by a readable file bearing all evidence that they were sealed for audit purposes.

The digital vault is only accessible by its administrator and the CISO of the Guichet Entreprises service.

The digital vault is only accessible via the use of a nominative certificate. All uses of this certificate are recorded.

The CISO of the INPI is authorised to access the certificate for audit purposes only, upon justified request (court applications, for instance).

Signed documents are forwarded to the relevant authorities in charge of processing the related applications.

Their recipients are in charge of the protection modalities of the forwarded applications that were signed electronically, which should comply with the provisions of the General Security Regulation and the General Data Protection Regulation.

The CISO of the INPI ensures that these provisions are respected.

#### Archiving and deletion of signed applications

In accordance with Article R. 123-18 of the Commercial Code, the Guichet Entreprises is no longer in charge of an application once it has been transferred to relevant authorities. In this context, the applications that have been completed and validated will be deleted from the portal within three months. Once the applications have been processed by relevant authorities, the information concerning non-trading and commercial companies will be published in the National registry of trade and companies. They will be [made available to the public](https://www.inpi.fr/fr/services-et-prestations-domaine/bases-de-donnees) as open data and [disseminated for further use](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041593491/) as part of accredited licences for further use.

As soon as users' applications have been transferred to relevant authorities, users may contact them to access their applications or access them directly on Guichet Entreprises within three months.

### Electronic signature solution providers

The solution must incorporate the requirements of the present electronic signature policy in the signature data structure.

### The INPI

The CISO of the INPI ensures that the provisions of the present electronic signature policy are respected concerning the online Guichet Entreprises service that is made available for users.

The CISO ensures that the source and recipient information systems that send and receive dataflow do not pose any risk concerning the integrity and confidentiality of stored and forwarded applications.

### Recipients

Recipients check the integrity of the application's electronic signature and ensure that the storage and deletion modalities of received applications comply with regulatory provisions.

### Verification data

To make verifications, the INPI uses the sealing information of the applications that were signed electronically (cf. "Protection and use of signed applications").

### Protection of technical means

The INPI ensures that the necessary means to protect the equipment providing validation services are implemented.

The measures taken concern:

* the protection of physical and logical accesses to equipment, which should only be accessible to authorised persons only,
* the availability of the service,
* the supervision and follow-up of the service.

### Assistance to signatories

Technical support to users for the electronic signature procedure is provided by [INPI Direct](../contacter_assistance_utilisateur.md).

## Electronic signature and validation

### Signed data

The signed data comprise the CERFA form and the copy of the piece of identification in the form of a single PDF file which includes a modifiable electronic signature field.

The entire PDF file is signed and therefore cannot be modified after it was sealed.

### Signature characteristics

The signature complies with the requirements of the "XML Signature Syntax and Processing (XMLDsig)" specification developed by the W3C ([World Wide Web Consortium](https://www.w3.org/)) and with the signature format extensions mentioned in the XML Advanced Electronic Signature (XADES) European standard of the [ETSI](https://www.etsi.org).

The deployed electronic signature implements a "substantial" (ISO/IEC 29115:2013 standard) level of identification, i.e. in accordance with the eIDAS regulation, an advanced electronic signature.

#### Signature type

The electronic signature is an enveloped signature.

#### Signature standard

The electronic signature must comply with the [XMLDsig](https://www.w3.org/TR/2002/REC-xmldsig-core-20020212) standard (version1.1 of February 2002).

The electronic signature must comply with the XAdES-EPES (Explicit Policy based Electronic Signature) standard, [ETSI TS 101 903 version v1.3.2](http://uri.etsi.org/01903/v1.3.2).

In accordance with the XadES standard, signed properties (SignedProperties/SignedSignatureProperties) must contain the following elements:

* The signatory's certificate (SigningCertificate) ;
* The date and time of the signature (SigningTime) in UTC format.

### Algorithms applicable to the signature

#### Hashing algorithm

Calculating the hashed message by iteration of the compression function on the series of blocks generated by hashing the message (iterative Merkle-Damgård construction).

This algorithm can be accessed [here](https://en.wikipedia.org/wiki/Merkle%E2%80%93Damg%C3%A5rd_construction).

#### Signature algorithmee

The signature algorithm is based on RSA/SHA256.

#### Canonicalisation algorithme

The canonicalisation algorithm is [c14N](https://www.w3.org/TR/xml-exc-c14n/).

### Conditions to declare signed files as valid*

The conditions to declare signed files as valid are the following:

* Signatories met their obligations: 
  * Uploading of a copy of their piece of identification certifying it is true to the original and approval of the agreement of proof
  * Entering of the code sent via SMS (one-time password method)
* The INPI met its obligations: 
  * Provision of a certificate generated by a competent authority certifying its origin (electronic signature service provider) with the following characteristics: Public key = RSA 2048 bits; signature algorithm = RSA SHA 256
* The electronic signature service provider met its obligations:> 
    * Signature and sealing of the PDF file according to the characteristics mentioned in the previous sections.

It is possible to ensure that the signature is valid via Adobe Acrobat.

## Legal provisions

### Nominative data

Registrants have the right to acess, modify, rectify and delete the data that concern them. They can exercise this right by sending an email to [this address](mailto:contact@guichet-entreprises.fr). However, the person in charge of your application must process it in order to meet legal requirements. Therefore, you cannot oppose the processing of your application, pursuant to the provisions of Article 6.1 c) of Regulation (UE) 2016/679 of the European Parliament and of the Council of 27 April 2016, and to Article 56 of the consolidated version of Act No. 78-17 of 6 January 1978 on information technology, data files and civil liberties.

### Applicable law – Dispute resolution

The present provisions are subject to French law.

Any dispute regarding the validity, interpretation and implementation of the present provisions will come under the jurisdiction of the Administrative Court of Paris.

Last update: June 2021