<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| The Guichet Entreprises information repository is licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International licence.
|
| Please visit the following address to obtain a copy of that licence:
| http://creativecommons.org/licenses/by-nd/4.0/
| or send a mail to: Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.| 
+-------------------------------------------------------------------------- -->
<!-- var(page:title)="About us" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(translation)="True" -->
<!-- var(key)="en-__footer__-qui_sommes_nous" -->
<!-- var(last-update)="2021-09-29" -->
<!-- var(lang)="en" -->

Accessibility: non-compliance with accessibility standards
========================

The Ministry for the Economy, Finance and Recovery undertakes to make its services accessible, pursuant to Article 47 of Act No. 2005-102 of 11 February 2005.

This declaration of accessibility applies to Guichet Entreprises

## Compliance status

Guichet Entreprises **does not comply with the RGAA 4.1**. The website has not been audited yet: Content is not accessible.

## Drawing up of the declaration of accessibility

This declaration was drawn up on **20 September 2021**.

## Improvement and contact details

If you cannot access content or services, you may contact the Guichet Entreprises to obtain an accessible, alternative link or content in an accessible form.

- via the [contact form](https://www.inpi.fr/fr/contactez-nous?about=301&under_about=1235-702-b1)
- by sending a mail to the following address:

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;Pôle Guichet Entreprises<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p>

## Remedies

This procedure applies if you reported non-accessible content or services to the Guichet Entreprises and are not satisfied with the answer.

You may:

- send a message to the [*Défenseur des droits*](https://formulaire.defenseurdesdroits.fr/code/afficher.php?ETAPE=accueil_2016) (Ombudsman's office)
- contact [the representative of the *Défenseur des droits* (Ombudsman) in your region](https://www.defenseurdesdroits.fr/saisir/delegues)
- send a mail (free-of-charge, do not stamp the envelope) :

<p>&nbsp;&nbsp;&nbsp;Défenseur des droits<br>
&nbsp;&nbsp;&nbsp;Libre réponse<br>
&nbsp;&nbsp;&nbsp;71120 75342 Paris CEDEX 07</p>

__________________________________________________________________

This declaration of accessibility was drawn up on 20 September 2021 thanks to [BetaGouv's generator of declaration of accessibility](https://betagouv.github.io/a11y-generateur-declaration/#create).