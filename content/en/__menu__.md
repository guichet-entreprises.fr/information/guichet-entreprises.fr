﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright Â© Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Home" -->
<!-- var(key)="en-__menu__" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

# [Home](index.md)
# Business creation
# <!--include(creation_entreprise/__menu__.md)-->
# Online procedures
# <!--include(demarches_en_ligne/__menu__.md)-->
# Regulated activities
# <!-- include(../reference/en/directive-services/_list_menu.md) -->
# [Working in France](travailler_en_france.md)
# [Frequently Asked Questions](faq.md)
# [Services @toggle.env](https://welcome.guichet-entreprises.fr/)
# [My applications @visibility.authenticated @toggle.env](https://dashboard.guichet-entreprises.fr/)