﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Working in France" -->
<!-- var(keywords)="Working in France" -->
<!-- var(collapsable)="close" -->
<!-- var(page:title)="Working in France" -->
<!-- var(key)="en-eugo" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

Working in France
====================

## You are an European Union or European Economic Area national and you would like to start a business in France? <!-- collapsable:off -->

The Guichet Entreprises service of the INPI promotes business creation and job mobility in France. The guichet-entreprises.fr and [guichet-qualifications.fr](https://www.guichet-qualifications.fr/en/) websites both form the online point of single contact for business creation recognised by the European Commission. Thanks to these websites, you will be able to explore new career prospects and start a business in France.

### Regulated activities <!-- collapsable:close -->

Find out about the requirements you need to meet in order to carry out a regulated activity in France.

\> [Read our fact sheets](../reference/en/directive-services/index.md)

### Freedom of establishment <!-- collapsable:close -->

You now have the possibility to set up in France to freely carry out an activity on a permanent basis.

\> [More on the freedom of establishment](libre_etablissement.md)

### Freedom to provide services <!-- collapsable:close -->

Providing services occasionally or temporarily in France has never been so easy! Find out about all the practical aspects of the freedom to provide services before you get started.

\> [More on the freedom to provide services](libre_prestation_services.md)

**Good to know**

In order to get your professional qualifications recognised, visit the [guichet-qualifications.fr](https://www.guichet-qualifications.fr/en/) website!

### Setting up a business in another member state <!-- collapsable:close -->

All the member states of the European Union have a point of single contact! The points of single contact are government portals intended for entrepreneurs working in the service sector. They are members of the EUGO network. More information on the European Commission's website:

<p align="center">[European points of single contact](https://ec.europa.eu/growth/single-market/services/services-directive/in-practice/contact_en) | [Practical guide to doing business in Europe](https://europa.eu/youreurope/business/index_en.htm)

<p align="center">![logo_eugo](./logo_eugo.png)