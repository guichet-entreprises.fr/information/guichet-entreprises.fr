﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Glossary" -->
<!-- var(keywords)="Glossary" -->
<!-- var(collapsable)="close" -->
<!-- var(page:title)="Glossary" -->
<!-- var(key)="en-creation_entreprise-glossaire" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

Glossary
=========

Please select an entry in the collapsible menu:

## A <!-- collapsable:close -->

### ACRE

Aid to start or take over a business (ACRE). The Aid to start or take over a business (ACRE) enables beneficiaries to benefit from exemption from social contributions for 12 months.

The ACRE application must be submitted on the [autoentrepreneur.urssaf.fr](https://www.autoentrepreneur.urssaf.fr/portail/accueil.html) website alongside the submission of the business creation application or 45 days after the submission of the business creation or takeover declaration at the latest.

The URSSAF is the sole competent authority to process this application and to notify applicants of the decision.

### ACOSS

[**Central agency for social security bodies**](https://www.Acoss.fr/home.html) (national fund of the URSSAF network). The ACOSS ensures the financial management of the general social security scheme (employee scheme) and coordinates the activities of the tax collection services of the French territory.

### Acte authentique

**Authentic act**: An authentic act is an act which has been executed before a notary.

### Acte sous seing privé (SSP)

**Private deed**: A deed that has been executed by the parties, without the intervention of a notary.

### Actif

**Asset**: Accounting concept referring to a business' balance sheet representing all of its property. There are two kinds of asset: fixed (buildings, machinery, etc.) and current assets (stock, trade receivables, etc.).

### Action

**Share**: Title issued by limited companies (public limited companies (SA), simplified limited companies (SAS), partnerships limited by shares (SCA)) conferring certain rights to its owner:

* Right to vote in general meetings
* Remuneration right (dividend and liquidation surplus). A share is said to be:
  * A registered share when the issuing company knows its owner
  * A bearer share when the issuing company does not know its owner.

"Bearer shares" are issued by listed companies.

### Actionnaire

**Shareholder**: Natural or legal person owning one or more shares from a limited company (public limited company (SA), simplified limited company (SAS), partnership limited by shares (SCA)).

### Activité agricole

**Agricultural activity**: Any act inherent in the professional exploitation of a plant or animal life cycle as well as any activity that is part of the continuation of this exploitation, such as the transformation of the products and their marketing.

### Activité artisanale

**Artisanal activity**: An activity is considered to be artisanal when it consists in making, transforming, repairing a product or in providing services that require manual labour. The list of artisanal activities was drawn up by the [Decree of 2 avril 1998](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=3BEF27CD0444BF1F02BA62962BEBCD73.tpdjo12v_1?cidTexte=JORFTEXT000000571009&dateTexte=20090202).

Any business carrying on an artisanal activity (whether main or secondary activity) must be registered with the centre for business formalities of the chambers of trades and crafts in order to be registered in the registry of trades (*répertoire des métiers* (RM)).

### Activité civile

**Civil activity**: This type of activity describes non-commercial activities. It mainly includes agricultural and liberal activities.

### Activité commerciale

**Commercial actvity**: A commercial activity is defined the Commercial Code. It mainly includes the purchase of movables and immovables with a view to selling them for profit, as well as the sale of certain services: hotels, restaurants, shows, transports, rentals, etc. Businesses carrying on a commercial actvity must be registered in the registry of trade and companies (*registre du commerce et des sociétés* (RCS)).

### ADELE

**Online French administration** (*Administration française en ligne*).

### ADIE

[**Association for the right to economic initiative**](https://www.adie.org/) (*Association pour le Droit à l'Initiative Économique*). The ADIE finances and assists entrepreneurs who cannot access bank credit, jobseekers and people living on income support more particularly. The ADIE's website offers online assistance and enables entrepreneurs to apply for funding directly.

### Administrateur

**Director**: Natural or legal person chosen among the shareholders of a public limited company (*société anonyme* (SA)) to manage a company on a collegiate basis within a board of directors. Within six months after his or her appointment, the director must hold a minimum number of shares, in accordance with the company's articles of association.

### Affiliation

**Affiliation**: The state, for an insured person, of being affiliated with a fund.

### AFNOR

[**French association for standardization**](https://www.afnor.org/) (*Association française de normalisation*). The objective of the AFNOR is to run and coordinate the elaboration of standards, accredit and promote them, and to simplify their use.

It represents France and defends its interests within the authorities of standardisation. It also develops the certification of products and services using the NF label.

### Agent commercial

**Commercial agent**: Independent agent entrusted with the power to negotiate on a permanent basis and, if need be, to sign purchase, sales, lease or service contracts on behalf and for other businesses.

Businesses do not have direct control over these activities, for, as an independent professional, a commercial agent is completely free to organise them.

### Agent d'affaires

**Private agent**: A person whose profession is to be in charge of private individuals' interests for a fee, by advising them and sometimes by acting on their behalf. Private agents carry on a commercial activity.

### AGESSA

[**Independent social security association for the artistic professions**](http://www.secu-artistes-auteurs.fr/).

### AGIRC

[**General association of pension institutions for executives**](http://www.agirc-arrco.fr/). The AGIRC manages the supplementary pension scheme of the executives from the private sector in the industry, trade, service and agriculture sectors.

### Allocataire

**Beneficiary**: A person who receives social welfare benefits.

### Année civile

**Calendar year**: A year which starts on 1 January and ends on 31 December.

### APCA

[**Permanent assembly of the chambers of agriculture**](https://chambres-agriculture.fr/).

### APCMA

[**Permanent assembly of the chambers of trades and crafts**](https://cma-france.fr/). The APCMA is the head of the chambers of trades and crafts's network.

### APE

**Main activity** (*Activité principale exercée (APE)*) (see the « Code APE » entry). 

### Apporteur d'affaires

**Business provider**: A person who liaises between a prospect and a business concerning a potential market for a fee. This activity is not a profession.

### Apports

**Capital contribution**: Assets and/or financial or technical resources made available to the company by partners with a view to exploiting them mutually, which entitles them to social rights in compensation (shares). All the capital contributions make up the company's share capital.

### Apports en industrie

**Contribution in the form of services or know-how**: Technical knowledge, work or services made available by a partner to his or her company. This contribution is not taken into account in the formation of the initial share capital. However, it can entitle the contributor to inalienable though paid advantages (profit sharing and participation at general meetings) if the company is a limited liability company (SARL) or a simplified limited company (SAS).

### Apports en nature

**Contribution in kind**: Contribution of assets other than money to the company's share capital. The company's articles of association must mention the value of each asset based on the report drawn by a valuer of contributions that will be appended to the articles of association. However, [Decree 2010-1669](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000023317402&categorieLien=id) authorises the partners of a limited liability company (SARL) and the partner of a limited liability sole proprietorship (EURL) not to solicit a valuer of contributions if the two following requirements are met with:

* no contribution in kind has a value of more than €30,000,
* the total value of the contributions in kind does not exceed half the share capital.

### Apports en numéraire

**Cash contribution**: Influx of money into a company's share capital. This contribution can be paid in instalments within a maximum period of 5 years. A minimum contribution must be paid during the formation of the company (1/5 for limited liability companies (SARL) and 50% for public limited companies (SA) and simplified limited companies (SAS)).

### Artisan

Qualification title which can be granted by the chamber of trades and crafts (CMA) to an artisanal business entrepreneur under certain conditions: diploma or/and duration of professional activity (6 years minimum).

### Assiette

**Tax base**: Calculation basis.

### Association

**Association**: Grouping of people with a common interest other than the sharing of profits. This legal structure, which was created by the 1901 law, is widely used in certain domains (sports, culture, arts, etc.). Beyond certain thresholds, profit-making activities come under the general company scheme (VAT and corporate tax). Associations are excluded from the area of competence of the Guichet Entreprises service.

### Partner

Natural or legal person who contributed in kind, in cash or in services to the company's share capital.

The term "partner" is mainly used:

* in non-trading company,
* in limited liability companies (SARL), general partnerships (SNC) and limited partnerships (SCS).

As for the term "shareholder", it is used in public limited companies (SA), in simplified limited companies (SAS) or in partnerships limited by shares (SCA).

### Auto-entrepreneur

See the « Micro-entrepreneur » entry.

### Autorité compétente

**Competent authority**: Authority or body that is specifically entitled by a member state to deliver or receive evidence of formal qualification and other documents or information. They can also receive requests and make decisions concerning authorisations to carry on an activity.

### Ayant droit

**Assignee**: Person who substitutes for another in the exercise of a right bestowed by the latter (for instance: an unemplyed person is the assignee of his or her spouse with regard to social security, an heir or heiress is the assignee of the deceased, etc.).

## B <!-- collapsable:close -->

### Bail

**Lease**: Contract whereby a person grants to another person the right to use something for a set period of time in exchange for a specified sum of money. More particularly, a lease is a contract that one signs when renting accomodation or a shop.

### Bail commercial

**Commercial lease**: Industrial, commercial, artisanal and self-employed activities require businesses to take out a commercial lease. Although they are usually issued for nine years, lessees are allowed to terminate commercial leases at the end of a three-year period (this is why they are often referred to as "3-6-9 leases"), provided they give six months’ notice. A commercial lease has a number of advantages when the lessee is registered with the registry of trade and companies or registry of trades at the lease renewal date. In particular, he or she enjoys relative stability with respect to:

* the right to renew the lease, also known as the "commercial property" (not available for micro-entrepreneurs),
* the cap on rent increases, which are calculated based on fluctuations in legal indexes that are stipulated when the original lease is signed.

### Bail professionnel

**Professional lease**: Rental contract given to liberal (self-employed) or artisanal activities in the absence of artisanal goodwill. These leases are issued for a minimum six-year period, which the lessee can terminate at any time provided hr or she gives six-months’ notice.

### Bailleur

**Lessor**: Owner of a good who grants a lease to a lessee.

### BpiFrance (public investment bank)

[BpiFrance](https://www.bpifrance.fr/) provides financial support instruments of all types to SMEs and mid to large-sized businesses in compliance with European regulations.

The bank offers supplementary, enhanced support in the areas of innovation and export, which is made available to businesses via the one-stop shops in every region of France.

### Bénéfices agricoles (BA)

**Agricultural profits**: Profits made by natural persons or companies that are subject to income tax and that come from the carrying on of an agricultural activity (farming and/or rearing).

Agriglutural profits are taxed based on the amount of income, under either the flat-rate regime, the simplified actual profit regime or the normal actual profit regime.

### Bénéfices industriels et commerciaux (BIC)

**Industrial and commercial profits**: Profits made by natural persons or companies that are subject to income tax and who carry on a commercial, industrial or artisanal actvity.

Industrial and commercial profits can be calculated:

* by applying a lump-sum allowance representative of business expenses: *micro-enterprise* tax regime,
* by deducting the business's actual expenses from its turnover: (normal or simplified) actual profit tax regime.

### Bénéfices non commerciaux (BNC)

**Non-commercial profits**: Profits that are subject to income tax for people carrying on a non-commercial activity (liberal professions) in their personal capacity or as partners of certain types of company. Non-commercial profits can be calculated:

* by applying a lump-sum allowance representative of business expenses: *micro-enterprise* tax regime,
* by deducting the business's actual expenses from its turnover: controlled return tax regime.

### BIC

**Industrial and commercial profits** (*bénéfices industriels et commerciaux*) (see this entry).

### Bilan

**Balance sheet**: A statement made at a particular point in time, detailing the situation of a business regarding the use of assets and liabilities.

Assets include everything the business owns: buildings, machines, material, patents, commodities, debts, money, etc. Liabilities include:

* the origin of resources: contributions, subsidies, etc.,
* the business's debts: loans, loan accounts, etc.

### BNC

**Non-commercial profits** (*bénéfices non commerciaux*) (see this entry).

### BODACC

[**Official bulletin of publication of non-commercial and commercial notices**](https://www.bodacc.fr/). The BODACC is a national publication, published daily, which includes official notices of business creations, sales and transfers of business goodwill, and the opening of insolvency proceedings.

### Boutique de gestion

**Small business consultancy**: Organisation that has the necessary skills to provide support and consulting services to people creating or taking over a business, from the search for an idea up until the two-year mark in the life of the business. Small business consultancies are organised into networks (*réseau des boutiques de gestion*).

### BTP

**Building and public works sector** (*Bâtiment et travaux publics*): Industry of construction.

### BTS

**Higher technician's certificate** (*Brevet de technicien supérieur*).

### But lucratif

**For-profit**: A for-profit business is a business whose primary objective is earning profits.

### But non lucratif

**Non-profit**: A non-profit business is a business that brings together individuals around a project where the ultimate goal is something other than sharing profits. This could include the promotion of a sporting activity, the discovery of one of France’s regions, saving money, the professional integration of the disadvantaged, local development, testing a new activity, etc. Profits may be earned, but this must not be the business’s primary objective.

## C <!-- collapsable:close -->

### CADA

[**Commission for the access to administrative documents**](https://www.cada.fr/) (*Commission d'accès aux documents administratifs*).

### Caisse des dépôts

**Deposits and Loans Fund**: Publicly-owned financial group that has provided support for France’s economic development since 1816.

As a long-term investor, particularly via its subsidiary CDC Entreprises, the group fosters French businesses’ access to innovation and sustainable growth thanks to its excellent knowledge of local issues (it has offices in all the regions of France) and its capacity to link up with the private sector.

[> Visit the website of the *Caisse des dépôts*](https://www.caissedesdepots.fr/)

### CAP

**Occupational proficiency certificate** (*Certificat d'aptitude professionnelle*).

### Capital social

**Share capital**: Total amount of cash and in-kind contributions provided by a business’s partners or shareholders when the business is being set up (or later, when the business is raising additional capital).

For each type of company, the law sets the minimum amount of share capital.

### Capitaux propres

**Shareholders’ equity**: The difference between the value of a business’s assets and its accounts payable.

Their carrying amount is entered as a liability in the business’s balance sheet. When a business is being set up, the shareholders’ equity is the capital contributed by those starting a business (partners’ capital and current accounts), which is different from borrowed capital that is loaned to the business (for example by banks).

### Carte de résident

**Resident card**: A card issued to foreign nationals who have lived in France uninterruptedly for at least three years. It is valid for a period of 10 years and is automatically renewed at the end of this period.

### Carte de séjour

**Residence permit**: Document that must possessed by every foreign national who wishes to live in France. The period of validity of a residence permit cannot exceed one year. However, it is renewable.

### Casier judiciaire

**Criminal record**: A list of criminal convictions handed down with respect to a natural or legal person, which also includes commercial, civil and administrative decisions that prevent individuals from exercising certain rights.

The « *Casier judiciaire national* » (**National criminal record**) is a service provided by the Ministry of Justice. A document entitled *bulletin n° 3*, which may be issued to individuals who request it, includes non-suspended sentences of imprisonment of more than two years, along with certain current disqualifications, prohibitions and incapacities.

### CCI

**Chamber of commerce and industry** (*Chambre de commerce et d'industrie*) (see this entry).

### CCI France

[CCI France](https://www.cci.fr/) is the head of the chambers of commerce and industry's network. Since September 2012, CCI France has replaced the Assembly of French chambers of commerce and industry (ACFCI).

### CCMSA

**Mutual insurance for farmers** ([*Mutualité sociale agricole*](https://www.msa.fr/lfy/accueil)).

### CDC

**Deposits and Loans Fund** (*Caisse des dépôts et consignations*), now called « *Caisse des dépôts* » (see this entry).

### CDD

**Fixed-term (employment) contract** (*Contrat à durée déterminée*).

### CDI

**Permanent (employment) contract** (*Contrat à durée indéterminée*).

### Centre de formalités des entreprises (CFE)

**Centre for business formalities**: A CFE is an obligatory one-stop shop where entrepreneurs submit, one single time and using a single form (the « *liasse unique* »), all mandatory declarations concerning the creation, modification of information about a business or cessation of activity.

### Centre de gestion agréé

**Approved management centre**: Organisation whose goal is to supply commercial, industrial, artisanal and agricultural businesses with technical support in terms of management and taxation, and to encourage them to expand their use of accounting. Members of an approved management centre benefit from certain tax advantages, and in particular a deduction on their taxable profits.

### Cerfa

**Registration and revision centre of administrative forms** (*Centre d'enregistrement et de révision des formulaires administratifs*). By extension: standardised administrative forms.

### Cession

**Assignment**: Sale or donation of a good or right.

### Chambre de métiers

See the « *Chambres de métiers et de l'artisanat* » (CMA) entry.

### Chambres d'agriculture

**Chambers of agriculture**: Professional public agencies under the suppervision of the Minister of Agriculture that is mandated to represent the interests of agriculture and rural areas with regard to public authorities. They also play a key role in providing services to farmers.  

### Chambres de commerce et d'industrie (CCI)

**Chambers of commerce and industry**: Public agencies run by peer-elected retailers and manufacturers that represent, at a local level, commercial and industrial professions. They are also responsible for defending the general interests of commerce and industry, and act as centres for business formalities (CFEs) for traders and commercial companies. They provide training and support for businesses.  [CCI France](https://www.cci.fr/) is the head of their network

### Chambres de métiers et de l'artisanat (CMA)

**Chambers of trades and crafts**: Public agencies run by peer-elected artisans (craftsmen/tradesmen). They are present in each of France’s regions and represent the general interests of artisan businesses. They provide training for apprentices, and offer support to artisanal businesses.

CMAs act as centres for business formalities (CFE) for artisanal businesses, and maintain the registry of trades (*répertoire des métiers* (RM)) of all artisanal businesses.

They issue artisanal qualifications to entrepreneurs (*artisan*, *maître artisan*) under conditions defined by decree. 

### Chiffre d'affaires

**Turnover**: Total amount of invoices that a business issues to third parties. This represents the total sales of goods or services carried out in a given period.

### CHR

**Cafés, hotels and restaurants** (*Cafés, hôtels et restaurants*): Refers to businesses and establishments working in the cafe, hotel and restaurant industry.

### CIPAV

**CIPAV pension fund scheme (pension fund for the self-employed professions)** (*Caisse interprofessionnelle de prévoyance et d'assurance vieillesse*).

### CNIL

**National commission for information technology and civil liberties** (*Commission nationale de l'informatique et des libertés*): The CNIL is the French Data Protection Authority.

### Code APE

**APE code (main activity code)**: The APE code is a code issued by the INSEE that corresponds to a business's main activity. It consists of four digits and one letter and is based on the *nomenclature d'activités françaises* (NAF, **nomenclature of French activities**). It is sometimes called "NAF code".

[> Read the article on the APE code](../articles/code_ape.md)

### Code NAF

**NAF code**: The "NAF code" is a misnomer. It is often used to refer to the APE code (see the « Code APE » entry).

### Collectivité territoriale

**Local authority**: Municipalities, department, region, overseas departments/territories and groupings of such entities (*communautés de communes* (communities of communes).

### COM

**Overseas territorial authority** (*Collectivités d'outre-mer*).

### Commerçant

**Trader**: A person who carries on commercial transactions as a regular occupation ([Article L.121-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006219167&cidTexte=LEGITEXT000005634379&dateTexte=20000921) of the Commercial Code). Traders are registered in the registry of trade and companies (*registre du commerce et des sociétés* (RCS)). They operate in their own name and on their own behalf.

### Commissaire aux apports

**Valuer of contributions**: A professional in charge of assessing the contributions in kind to a company's capital.ca The valuer of contributions is appointed by the partners or the president of the commercial court, depending on the company's structure.

### Compte de résultat

**Income statement for the year**: The income statement for the year is one of the tables appended in the annual financial statements. It summarises and compares the profits (the turnover) and the expenditures of the financial year. It shows a difference that forms the profits or the losses of the financial year (called the "accounting results" of the year). This table shows whether the business is profitable (it earns profits) or not.

### Comptes annuels

**Annual financial statements**: The annual financial statements are composed of the balance sheet, of the income statement for the year and of the appendix. They must be prepared at the end of each financial year by businesses that are subject to an actual tax regime in the BIC category (industrial and commercial profits).

Commercial companies (SARL, SA, SAS, etc.) must file them with the clerk's office of the commercial court within one month following the annual general meeting at which they were approved.

### Concubin

**Partner**: The person who lives with one without being married.

### Conformité

**Compliance**: The fact of complying with certain rules.

### Conseil d'administration

**Board of directors**: A collegial body that manages public limited companies; it consists of no less than three and no more than twenty-four members. The board of directors is invested with the broadest possible powers to be able to act in any circumstances in the name of the company, within the limits set out in the articles of association.

### Conseil de surveillance

**Supervisory board**: A body within a public limited company with a two-tier structure (management board/supervisory board), mandated to oversee the management of the company by the management board.

It consists of no less than three and no more than eighteen members, all of whom are shareholders. No member of the supervisory board may be part of the management board.

### Contrat d'assurances

**Insurance policy**: The reciprocal commitments made by insurer and insured are set out in the insurance policy. The policy has two parts: general terms and specific terms.

The general terms are applicable to all individuals insured by the same company under a given type of policy. They explain how the policy operates and spell out the full set of guarantees.

The specific terms customise the contract by adapting it to the situation of each insured person: contact details, chosen guarantees, policy fees, etc. These are the only documents that are signed by both insurer and insured.

### Contrat de travail

**Employment contract**: Contract under which an individual agrees to work under the supervision of another individual or legal entity in exchange for remuneration.

### Conventionnel

**Contractual**: Established by agreement between several individuals.

### Cotisation foncière des entreprises (CFE)

**Business premises contribution**: The CFE replaces the local business tax (*taxe professionnelle*).

### Couverture sociale

**Social security cover**: Protection provided to insured persons (reimbursement for certain types of care, drugs, equipment, etc.).

### CPAM

**Primary health insurance fund** ([*Caisse primaire d'assurance maladie*](https://www.ameli.fr/)).

### CRDS

**Social security debt repayment contribution** (*Contribution pour le remboursement de la dette sociale*).

### CSG

**General social security contribution** (*Contribution sociale généralisée*): A tax paid by all taxpayers according to their income for the purpose of financing social insurance schemes.

## D <!-- collapsable:close -->

### Dividendes

**Dividends**: Portion of a business’s profits distributed to each partner.

### DOM

**Overseas department** (*Département(s) d'Outre-mer*).

### Domicile

**Home**: Official and regular place of residence.

### Domiciliation

**Domiciliation**: Use of a business’s legal representative’s personal residence as the firm’s registered office.

### Droit au bail

**Leasehold right**: Compensation paid to a trader holding a commercial lease to the person who takes over the rented premises he or she occupied.

### Droit d'auteur

**Copyright**: Right held by the author of an original intellectual work. This right is granted by the simple creation of the work.

"Intellectual works" include:

* Literary works: theses, novels, plays, etc.
* Works of art: paintings, sculptures, creations, architects drawings, photographs
* Musical or multimedia works
* Software.

Copyright combines a moral right (right of disclosure, right of authorship, right of integrity, right to reconsider and right of withdrawal) and a right of ownership (compensation). The right of ownership bestows upon the author the exclusive right to either authorise or forbid any use of his or her works, and to receive compensation, particularly in the form of reproduction and performing rights.

## E <!-- collapsable:close -->

### EARL

**Limited liability agricultural holding** (*Exploitation agricole à responsabilité limitée*): Non-trading agricultural holding with up to ten partners, who are liable up to the limit of their contributions.

### EEE

**European Economic Area** (*Espace économique européen*): The European Economic Area comprises the 27 member states of the European Union (EU) and the three countries belonging to the European Free Trade Association (EFTA): Iceland, Liechtenstein and Norway.

### EIRL

**Limited liability sole proprietor** (*Entrepreneur individuel à responsabilité limitée*): This status is available to individual entrepreneurs (sole proprietors) who wish to limit the scope of their liability. It offers them the possibility of assigning assets to their business without needing to set up a company. It is not a new legal status.

The EIRL is similar to a sole proprietorship, but differs in two aspects: the scope of the owner’s liability and the possibility, in certain cases, to opt for corporate tax.

### Enseigne

**Commercial sign**: A commercial sign is a board (luminous or not) displayed for information, advertising or decoration purposes (or all three), usually to the public.

A commercial sign is also an intangible asset of the goodwill. It is an extension of the trade name. Except for the name, it is through the commercial sign that the business is known by the public. The trader displays it on his or her window, delivery vans, etc.

### Entreprise

**Business**: An individual carrying on a self-employed business activity, or a legal entity set up for producing goods or providing services that is financially and legally autonomous. Most businesses have only one single establishment, but the largest ones have several.

### Entreprise individuelle

**Sole proprietorship**: A sole proprietorship is a business in which the entrepreneur (or sole proprietor) conducts his or her professional activity in his or her own name. The entrepreneur is fully liable for all professional debts from his or her personal assets. The choice of matrimonial regime can thus be critical. A sole proprietorship can have employees.

### Établissement

**Establishment**: An economic unit dependent on a business that may be an entire business or a fraction thereof. This could be an office, a workshop, a plant, etc.

### État membre

**Member state**: A member state of the European Union (EU).

### EURL

**Limited liability sole proprietorship** (*Entreprise unipersonnelle à responsabilité limitée*): Single-member limited liability company (SARL).

### Exercice social

**Financial year**: The length of a business’s accounting period.

In principle, the accounting period is 12-month long.

Nevertheless, this may vary for the first year:

* If it is a business subject to corporate tax, the first financial year may extend to 31 December of the year following the year in which the business was set up.
* If it is a sole proprietorship or a partnership subject to personal income tax, the first financial year may not, in principle, extend past 31 December of the year in which the business was set up.

### Exonération

**Exemption**: When payment is waived.

### Expert-comptable

**Certified accountant**: An independent, self-employed professional who, at the request of the head of a business, intervenes in the following areas:

* Drafting of annual accounts
* Management of a business
* Compliance with the legal obligations of a business
* Audit of a business
* Computerisation of a business.

To be able to practice his or her profession, a certified accountant must, following a lengthy theoretical and practical training (doctorate level), swear an oath and register with the Order of certified accountants (*Ordre des experts-comptables*). Certified accountants follow a strict code of ethics with respect to their clients’ interests.

### Exploitation agricole à responsabilité limitée (EARL)

**Limited liability agricultural holding**: Non-trading agricultural company with up to ten partners, who are liable up to the level of their contribution.

## F <!-- collapsable:close -->

### Fonds de commerce

**Goodwill**: The full set of tangible (machinery, equipment, furniture, fixtures, etc.) and intangible (clients, trade name, commercial name, leashold right, patents, trademarks, licences, etc.) elements owned by a commercial or industrial business.

### Fonds propres

**Own funds**: Generic term used to refer to a business’s stable financial resources that it "owns" either directly or indirectly. These include capital, reserves, profits, profits carry forward, investment subsidies and statutory provisions, as opposed to external financing from third parties.

When setting up a business, own funds refer to shareholders’ or partners’ personal contributions to the business’s share capital.

### Franchise

**Franchise**: A system for marketing products, services or technologies based on a close collaboration between two legally and financially distinct companies. In exchange for a financial contribution, a business (the franchisee) acquires from another (the franchisor) the right to use that company’s commercial name and/or brand along with its know-how, and the right to sell its products or services in accordance with directives stipulated in the contract. The franchisee also receives marketing and technical assistance.

## G <!-- collapsable:close -->

### Gérant

**Manager**: The director of a partnership or of an SARL/EURL.

### Grande entreprise

**Large business**: According to the European Commission, a "large business" is one with at least 5000 staff (employees, executives, partners, etc.), and which generates more than €1.5 billion in turnover or which has more than €2 billion in total assets.

### Greffe

**Clerk's office**: Office that keeps record of case files, keeps registries and records declarations.

### Greffe du tribunal de commerce

**Clerk's office of the commercial court**: An office that provides administrative services to the court, such as maintaining registries, updating files, filing minutes, reception services, etc. The clerk's office is also in charge of maintaining the registry of trade and companies (*registre du commerce et des sociétés* (RCS)), of managing insolvency proceedings and of keeping the securities file (pledges and preferential liens). It also acts as a centre for business formalities (*centre de formalités des entreprises* (CFE)) for commercial agents, non-trading companies, economic interest groupings and public establishments of an industrial or commercial nature (*établissements publics industriels et commerciaux* (EPIC)).

The clerk's office of the commercial court is a mandated position, similar to bailiffs or notaries.

### Groupement d'intérêt économique (GIE)

**Economic interst grouping**: A group of several pre-existing businesses, either sole proprietorships or companies, that come together to facilitate or develop their economic activity while maintaining their independence. An economic interest grouping must be registered in the registry of trade and companies (*registre du commerce et des sociétés* (RCS)).

### Groupement européen d'intérêt économique (GEIE)

**European economic interst grouping (EEIG)**: An EEIG is a European legal entity of cooperation allowing several businesses from different countries to do business together. It must be registered with the competent business registry in the member state in which its head office is located.

## I <!-- collapsable:close -->

### Immobilisation

**Fixed assets**: A good or security purchased by a business (or that it produces for itself) with the idea it will be used for a period greater than one year.

Fixed assets are entered into the company’s balance sheet. 

There are several categories of fixed assets:

* Tangible: machines, furniture, vehicles, computers, land, buildings
* Intangible: business registration costs, costs of initial installation, patents, trademarks, goodwill, licenses
* Long-term investments: shares, bonds, deposits and securities.

### Impôt sur le revenu (IR)

**Income tax**: This is one of the two ways of taxing company earnings, the other being corporate tax.

Income tax mainly concerns sole proprietorships, single-member limited liability companies (EURLs), partnerships and other types of companies if they choose that option.

### Impôt sur les sociétés (IS)

**Corporate tax**: This tax is levied on:

* Limited liability companies (SARL, SA, SAS, SCA and SCS)
* Profit-making associations
* Other companies, as an option (EURL, SNC, etc.)

### INPI

**National institute of industrial property** (*Institut national de la propriété industrielle*): The [INPI](https://www.inpi.fr/fr) is a French public institution in charge of issuing patents, licensing trademarks and designs, and providing information on industrial property and businesses. It also plays a part in the fight against counterfeiting.

### INSEE

**National institute of statistics and economic studies** (*Institut national de la statistique et des études économiques*). The INSEE is a Directorate-General of the Ministry for the Economy and Finance. This INSEE is in charge of collecting, analysing and disseminating information on theFrench economy and society.

### Investissement

**Investment**: The use of capital to bolster a business’s potential. Investments appear on a business’s balance sheet as fixed assets.

For example, purchasing a computer or a commercial vehicle, building a warehouse, or acquiring a stake in a supplier’s company are all investments.

### IR

**Income tax** (*Impôt sur le revenu*) (see this entry).

### IS

**Corporate tax** (*Impôt sur les sociétés*) (see this entry).

## J <!-- collapsable:close -->

### Journal d'annonces légales

**Bulletin of legal notices**: Daily or weekly publication authorised to publish announcements by companies concerning their creation, amendments to their articles of association, and their close-down, in the department in which their head office is located. A list of authorised publications, along with the rate per line, is established for each department by order of the Prefect.

## K <!-- collapsable:close -->

### KBIS (extrait)

**KBIS extract**: Document issued by the clerk's office of the commercial court to any person requesting legal and financial information about a company registered in the registry of trade and companies (*registre du commerce et des sociétés* (RCS)). For sole proprietorships, the equivalent of the KBIS extract is the K extract.

## L <!-- collapsable:close -->

### Légataire

**Legatee**: Individual or institution receiving the assets or fortune of a deceased person.

### Libérale (profession)

See the « Profession libérale » entry.

### Libre prestation de services (LPS)

**Freedom to provide services**: Refers to the free circulation of services between member states, giving service providers the legal certainty needed to effectively exercise this fundamental freedom guaranteed by the Treaty.

[> More on the freedom to provide services](../libre_prestation_de_services.md)

### Licence

**Licence**: Administrative authorisation allowing an individual to exercise a trade or a regulated profession.

Authorisation to use a patent.

## M <!-- collapsable:close -->

### Mandataire

**Proxy**: Individual authorised to act on behalf of another individual.

### Mensualité

**Monthly payment**: Amount paid each month.

### Micro-entrepreneur (formerly autoentrepreneur)

**Micro-entrepreneur**: This scheme is intended for anyone who wishes to carry on a commercial, artisanal or liberal activity as a main or secondary activity. This status comes under the micro-enterprise tax regime. The method of calculation of social security contributions is simplified for micro-entrepreneurs.

[> More on the micro-entrepreneur scheme](./micro_entrepreneur/le_regime_micro_entrepreneur.md)

### Micro-entreprise

**Micro-enterprise**: This concept is primarily used for statistical and economic purposes to identify businesses that employ less than ten people (including executives) and whose annual turnover or total assets do not exceed €2 million.

It is also used to refer to an extremely simplified tax regime used for the assessment of sole proprietorships whose turnover is beneath a certain level. This is called the micro-enterprise tax regime

### MSA

**Mutual insurance for farmers** ([*Mutualité sociale agricole*](https://www.msa.fr/lfy/accueil)).

## N <!-- collapsable:close -->

### NAF

**Nomenclature of French activities** (*Nomenclature d'activités française*) (see the « Code APE » entry).

[> Read the article on the APE code](https://www.guichet-entreprises.fr/fr/aide/code-ape/)

### Nom commercial

**Trade name**: Name used to identify a retail trade business.

### Nom de domaine

**Domain name**: Name given to a website. Inclusion in a domain name of a name belonging to a third party may have legal consequences.

### Non lucratif

See the « But non lucratif » entry.

### Notification

**Notice**: Document that informs an individual about a decision that concerns him or her. As a general rule, notices are used to indicate the beginning of a procedural time limit.

### Numéro Siren

See the « Siren » entry.

### Numéro Siret

See the « Siret » entry.

## O <!-- collapsable:close -->

### Obligation solidaire

**Joint and several liability**: Obligation for each member of a group of debtors to pay the full amount of a debt.

## P <!-- collapsable:close -->

### PACS

**Civil solidarity pact** (*Pacte civil de solidarité*).

### Parts sociales

**Partner's share**: A partner's share is a title of ownership in the share capital of a company.

A partner's share is held by either:

* a partner in a commercial company that is not a joint-stock company (such as an SARL in France),
* or a member of a cooperative or mutual society.

The bearer of a partner's share is entitled to:

* The right to vote at the general assembly ("one share, one vote" for commercial companies, "one person, one vote" for cooperatives)
* A monetary share of the profits (dividends for companies, interests for cooperatives).

### Patrimoine

**Asset base**: The full set of a person’s assets, receivables and debts.

### Pépinière d'entreprises

**Business incubator**: A business incubator is an organisation that provides significant support for startups, including:

* Offices in modern and practical premises that are adapted to their needs in exchange for a reasonable rent; they are allowed to stay in these premises for up to 23 months.
* A range of on-site services, including assistance (legal and management consulting, facilitation), training, leadership, pooled secretarial services, a meeting room and various equipment (photocopy facilities, video projectors, etc.).

Each incubator has its own criteria for selecting the businesses it accommodates.

Premises in a business incubator keeps fixed costs down and allows startups to transform certain fixed costs into variable costs (e.g. paying for photocopies by the page rather than purchasing a photocopier).

### Personne morale

**Legal person**: A group that has a legal existence, and therefore has rights and obligations (institutions, organisations, companies, associations, public establishments, etc.).

### Personne physique

**Natural person**: As opposed to a legal person, which defines a legal entity (a business, a group, etc.), a natural person is an individual (one single person).

### Plan d'affaires

**Business plan**: A written dossier presenting a project for starting a business. The plan includes all aspects of the project: the creators, the product or service, the market (customers), the technical means that will be employed, human resources, the cost of these means and resources, financial forecasts, the chosen legal status for the undertaking, a calendar and any other useful information to allow the reader to understand the project. A business plan is vital to explain a project to third parties and for the purposes of discussion with various partners (investors, banks, government offices, etc.). It is also very useful to create a shared vision of a project between partners. On the financial level, a business plan should include, at the very least:

* A forecast income statement for the year
* A financing plan
* A cash flow plan
* A calculation of the breakeven point.

A business plan should not be a huge, thick document that discourages readers right from the start. It should be no longer than 30 to 50 pages (appendices such as technical documents, supporting documentation, extracts from quoted sources, market research, correspondence, etc. should be included in a separate file). A good business plan should be complete, concise, detailed, clear and well thought out, and it should promote the project in question.

### Plus-value

**Capital gain**: Increase in the value of a good between its purchase and its resale.

### PME

**SME**: (*petite ou moyenne entreprise*): Abbreviation of Small and Medium-sized Enterprise. This concept is used for statistical and economic purposes to designate a business that employs fewer than 250 people, whose annual turnover is less than €50 million, and whose total assets do not exceed €43 million.

### PMI

**Small or Medium-sized Industry** (*Petite ou moyenne industrie*).

### Préjudice

**Damage**: Material or moral damage.

### Prestataire de services

**Service provider**: Person who provides services.

### Profession libérale

**Liberal professions (self-employed professions)**: In French, the term « professions libérales » does not refer to a particular status, but rather an "intellectual" occupation based on the personal practice of a science or an art.

One must distinguish between "regulated" and "unregulated" professions.

The "regulated" self-employed professions are the most well-known and are defined by French legislation.  Members of these professions include architects, lawyers, certified accountants, land surveyors, physicians, bailiffs, notaries, general insurance agents, etc.

They require membership in a specific order or organisation and, when they are carried on as part of a professional practice, they must have specific structures (SCP, SEL, etc.).

The "unregulated" self-employed professions include all economic activities that have nothing to do with trade, craft, industry or agriculture, and which are not regulated professions. Members of these professions include consultants, trainers, experts, translators, etc.

[> Read the article on the liberal professions operating under the micro-entrepreneur status](./micro_entrepreneur/micro_entrepreneur_profession_liberale_ou_activite_independante.md)

### Profession réglementée

**Regulated profession**: A professional activity for which, in order for an individual to join or practice it, specific conditions must be met.

[> Read the "regulated profession" fact sheets on guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/professions-reglementees/)

### Propriété commerciale

**Commercial property**: The right for a trader to require the owner of the premises to renew a commercial lease.

### Propriété industrielle

**Industrial property**: The rights relating to inventions and creations.

### Propriété littéraire et artistique

**Literary and artistic property**: The set of pecuniary and moral rights enjoyed by the author of a literary or artistic creation.

## Q <!-- collapsable:close -->

### Quote-part

**Share**: The part that each individual either receives or pays.

## R <!-- collapsable:close -->

### Raison sociale

**Company name**: Name given to certain partnerships (mainly to SCPs (professional partnerships for regulated professions)), with one or more partner(s) followed by « et compagnie » ("& Co.")

### RCS

**Registry of trade and companies** (*registre du commerce et des sociétés*).

### Récépissé

**Receipt**: A document by which an individual or entity acknowledges having received sums of money, goods or files from another.

### Recouvrement

**Debt recovery/collection**: Action carried out to recover sums due.

### Régime matrimonial

**Matrimonial regime**: Rules that determine the distribution of assets between spouses and the way in which they are managed.

### Régime social des indépendants (RSI)

See the « Sécurité sociale des indépendants » entry.

### Registre du commerce et des sociétés (RCS)

**Registry of trade and companies**: The *registre du commerce et des sociétés* is a file maintained by each commercial court or *tribunal de grande instance* (Court of first instance) hearing commercial cases, used to register declarations by traders, businesses and economic interest groupings.

### Registre spécial des agents commerciaux (RSAC)

**Special registry of commercial agents**: The *registre d'inscription des agents commerciaux* is registry for commercial agents.

### Répertoire des métiers (RM)

**Registry of trades**: The *répertoire des métiers* is a registration file for artisans. It is kept by the chamber of trades and crafts.

### Répertoire Sirene

**SIRENE registry**: The SIRENE registry is a national registry managed by the National institute of statistics and economic studies (INSEE) that is updated on a daily basis with declarations by businesses transmitted by the centres for business formalities. It issues an ID number to each business, which is then made official by registration in the court registries.

[> Consult the SIRENE registry](https://avis-situation-sirene.insee.fr/)

### Rescrit

**Ruling**: Official ruling on a piece of legislation issued by the government at the request of a citizen.

### Résiliation

**Cancellation**: Act or ruling that brings a contract to an end.

### Responsabilité civile

**Civil liability**: Obligation to make good damage caused either by oneself, or by a person or animal in one’s charge.

### Résultat d'exploitation

**Operating income**: Subtotal of the income statement for the year giving the profits (or losses) from a business’s current activity, but which does not take into account the depreciation and amortisation and the operating provision. The operating income does not take into account either "financial expenses and revenue" or "extraordinary expenses and revenue".

### Résultat financier

**Financial profit/loss**: Income statement subcategory showing the difference between financial products and financial expenses for the financial year.

### Résultat net

**Net income**: Final total in the income statement for the year.

## S <!-- collapsable:close -->

### SA

**Public limited company** (*société anonyme*) (see this entry).

### SARL

**Limited liability company** (*société à responsabilité limitée*) (see this entry).

### SAS

**Simplified limited company** (*société par actions simplifiée*) (see this entry).

### SASU

**Single member simplified limited company** (*société par actions simplifiée à associé unique*).

### SCIC

**Collective interest cooperative** (*société coopérative d'intérêt collectif*): The goal of a SCIC is to produce or supply goods and services in the collective interest that are of social utility. It allows those who wish to work together in a common local development project – employees, beneficiaries, volunteers, local authorities or any other partners – to participate in the company’s capital. A SCIC is an open-ended company which may take the form of an public limited company (SA) or a limited liability company (SARL), and which is governed by a law that was issued in July 2001.

### SCM

**Professional partnership** (*société civile de moyens*) (see this entry).

### SCOP

**Cooperative company** (*Société coopérative de production*): A SCOP is a commercial company (limited liability company (SARL) or public limited company (SA)) whose partners have expressed the wish to enforce strict equality under the "one partner, one vote" principle. In return, specificities concerning the way a SCOP operates, particularly the obligation to set up an asset base (indivisible financial reserves), mean that this form of business offers certain tax advantages.

A SCOP could be a good solution for transferring a business to its employees.

### SCP

**Professional partnership (for regulated professions)** (*Société civile professionnelle*).

### SCS

**Limited partnership** (*Société en commandite simple*) (see this entry).

### SEL

***Société d'exercice libéral*** (see this entry)

### Sécurité sociale des indépendants (SSI)

The [Sécurité sociale des indépendants](https://www.secu-independants.fr/) (social security scheme for the self-employed) took over the activities of the RSI in January 2020.

Self-employed workers are now part of the general social security scheme.

### Siège social

**Head office**: The place where a business is effectively managed, which determines its legal domicile, its nationality and the jurisdiction under which it falls.

### SIREN

**SIREN number**: Nine-digit number assigned by the National institute of statistics and economic studies (INSEE) when a company is registered in the SIRENE national registry, which is used to identify the business.

### SIRET

**SIRET number**: Fourteen-digit number assigned by the National institute of statistics and economic studies (INSEE) which identifies a business establishment. It consists of a SIREN number followed by a five-digit NIC number.

### SNC

**General partnership** (*Société en nom collectif*) (see this entry).

### Société

**Company**: Contract by which two or more individuals pool something for the purpose of generating profits.

### Société à responsabilité limitée (SARL)

**Limited liability company**: Commercial company whose share capital is divided into shares and whose partners are liable up to the level of their contribution.

### Société anonyme (SA)

**Public limited company**: Commercial company whose share capital is divided into shares, and whose partners, known as shareholders, are liable up to the level of their contribution. An SA must have a minimum share capital of €37,000; 50% of the cash contributions must be paid at the time of incorporation.

### Société civile de moyens (SCM)

**Professional partnership**: An SCM is a company founded by the members of a liberal profession (whether regulated or not) who wish to share premises, equipment (such as medical or IT equipment) and administrative structures with an eye to keeping costs down.

Members of this type of business are fully independent with regard to their clients and their professional practice. Professional partnerships are non-profit companies: they are simply a means to share costs, such as between two partners who may have a common bank account.

### Société de portage salarial

**Umbrella company**: A company that exists to invoice services provided by individuals who do not wish to become self-employed. Umbrella companies pay out the sums received in the form of wages.

**Please note**

The legitimacy of this practice, which has expanded in recent years, is questioned by some labour law specialists.

### Société d'exercice libéral (SEL)

A specific type of limited company in which partners are individuals practising a liberal (self-employed) profession.

### Société en commandite simple (SCS)

**Limited partnership**: An SEL is a partnership in which there are two types of partners. The first, known as general partners, have the status of partners in a general partnership (and therefore must have the status of traders), and have unlimited liability for the company’s debts.. The other partners, known as limited partners, are liable only up to the level of their contribution.

### Société en cours d'immatriculation

**Company in the process of registration**: Company that filed its registration application form with the centre for business formalities but which has not been registered yet.

### Société en formation

**Company in the process of being set up**: Company whose partners or shareholders decided to set up the company but that has not been registered with the registry of trade and companies (*registre du commerce et des sociétés*) yet. During this period, operations required to set up the company are carried out by the partners or shareholders "in the name of and on behalf of the company being set up".

### Société en nom collectif (SNC)

**General partnership**: An SCN is a general partnership in which the partners have unlimited joint and several liability for the company’s debts.

Important decisions must, as a rule, be taken unanimously.

### Société en participation

**Joint venture**: An entity without legal personality that is not registered with the registry of trade and companies (*registre du commerce et des sociétés*), which is set up by several natural or legal persons in a spirit of collaboration.

### Société par actions simplifiée (SAS)

**Simplified limited company**: An SAS is a company structure very similar to a public limited company (*société anonyme* (SA)), whose shareholders are either natural or legal persons. It is more flexible to operate than an SA. The shareholders are free to set the minimum capital amount.

### SSI

See the « *Sécurité sociale des indépendants* » entry.

### SSII

**Computer engineering company** (*Sociétés de services en ingénierie informatique*).

### Statutaire

**Statutory**: In compliance with a company or association’s rules and articles of association that detail its aims, resources and rules of operation.

### Statuts

**Articles of association**: Articles of incorporation for a company or association. The articles of association contain specific mandatory information concerning the operation and the purpose of the entity.

### Stipulations

**Provisions**: Terms and conditions set out in a contract.

### Subvention

**Subsidy**: Non-reimbursable financial assistance provided to a business, most often by the government or a local authority. There are:

* "Investment subsidies", the goal of which is to help businesses to purchase certain types of equipment
* "Operational subsidies", which are used to top up a business’s turnover when its activity has generated abnormal additional costs, or when the activity is not such that it can be made profitable, but it is deemed to be of use to the community
* "Balancing subsidies", which are used to make up some or all of the losses that a business would have suffered without such support.

### Succession

**Inheritance**: Transmission to heirs of the assets and debts of a deceased person.

### Succursale

**Branch**: Commercial or financial establishment that is dependent on another establishment, but which enjoys some autonomy, particularly with respect to management.

## T <!-- collapsable:close -->

### Tacite reconduction

**Automatic renewal**: Automatic renewal of a contract at its term, based on an automatic renewal clause or on the continuation of a contractual relationship.

### Titre de formation

**Evidence of formal training**: Diplomas, certificates and other titles issued by an authority of a member state recognising vocational training.

### Titre de séjour

**Residence permit**: Document providing acknowledgment by a public authority of the right of an adult foreign national to reside within a country’s territory. A residence permit is defined by its legal status, the reason for admission and the term of validity.

### Très petite entreprise (TPE)

**Very small business (VSB)**: This non-legal term usually refers to a micro-enterprise (business employing less than ten employees).

### Tuteur

**Guardian**: A person in charge of protecting and representing a child or a dependent adult.

### TVA

**VAT**: VAT (value-added tax) is an indirect tax on consumption that is levied on nearly all goods and services consumed and used in France. The final consumer is responsible for paying VAT, rather than the business that produced the good or service. The business invoices the customer for VAT and later pays it over to the Public Treasury (*Trésor public*), deducting any VAT it has paid on purchases included in its cost price.

## U <!-- collapsable:close -->

### UCANSS

**Union of national social security funds** ([*Union des caisses nationales de sécurité sociale*](http://extranet.ucanss.fr/portail)).

### UE

**EU (European Union)**: The EU is made up of 27 Member States, including Austria, Belgium, Bulgaria, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hungary, Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, Netherlands, Poland, Portugal, Romania, Slovakia, Slovenia, Spain and Sweden.

### URSSAF

**Union for the collection of social security contributions and family allowances** ([*Union de recouvrement des cotisations de sécurité sociale et d'allocations familiales*](https://www.urssaf.fr/portail/home.html)).

### Usufruit

**Usufruct**: Right of an individual to use a good and receive the income that derive from this good, ithout possessing it.

## V <!-- collapsable:close -->

### Valeur locative

**Rental value**: Amount that a building or premises may provide if rented.

### Valeur mobilière

**Transferable security**: Certificate issued by a public limited company (*société anonyme* (SA)) that attest to the rights of a shareholder or a medium- or long-term lender (bond).

### Valeur nette comptable

**Net book value**: The residual value of a company’s asset after deduction of its purchase price, depreciation and any provisions for depreciation.

### VRP

**Sales representative **.