﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Aids to businesses" -->
<!-- var(keywords)="Aids to businesses" -->
<!-- var(collapsable)="open" -->
<!-- var(key)="en-creation_entreprise-prealables-1-aides_publiques_aux_entreprises" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

Aids to businesses
===================================

**Are you about to start a business? Providing funding for your business will be crucial for your project.**

**Beyond bank and personal loans, numerous aids and subsidies can help you. You just need to find those which will help you start your business.**

## Aides-entreprises.fr, a website to finance your business

The [aides-entreprises.fr](https://aides-entreprises.fr) website lists more than 1,800 aids for project leaders and business owners. 

Aides-entreprises.fr provides users with detailed and up-to-date information at local, national and European level. It guides applicants towards the right point of contact for each aid scheme.

The website also offers a calendar of competitions, some of which reward prizewinners with endowments.

## The aid to start or take over a business (ACRE): A simplified aid

Since 1 January 2020, the ACRE is no longer allocated automatically. It is subject to eligibility criteria anew.

To benefit from the ACRE, applicants must be in one of the following situations:

* Receive unemployment benefits,
* Be unemployed, not receive unemployment benefits and have been registered with Pôle Emploi (French unemployment agency) for more than 6 months in the course of the last 18 months,
* Receive the specific solidarity allowance (ASS) or the active solidarity income (RSA),
* Be aged 18 to 26,
* Be aged below 30 and officialy recognised as disabled,
* Have signed a contract of support to a business project (CAPE),
* Have started or taken over a business located in a priority urban neighbourhood (QPV),
* Receive the shared allowance for the education of a child (PreParE),
* Be self-employed and not come under the micro-social scheme.

As far as companies are concerned, you must have control over it, which means you must be in one of the following situations:

* You — personally or with your spouse, PACS partner, partner or ancestors and descendants — own more than 50% of the company's share capital, of which at least 35% are owned by you,
* You run the company and you — personally or with your spouse, PACS partner, partner or ancestors and descendants — own at least 1/3 of the company's share capital, of which at least 25% are owned by you, provided that no other shareholder owns more than 50% of the company's share capital,
* You and other applicants own more than 50% of the company's share capital together provided that one or more of them is an executive and that each applicant has a share that is equal to at least 10% of the majority shareholder's share.

Applicants must not have benefited from the ACRE in the course of the last three years.

The ACRE application must be submitted on the [autoentrepreneur.urssaf.fr](https://www.autoentrepreneur.urssaf.fr/portail/accueil.html) website alongside the submission of the business creation application or 45 days after the submission of the business creation or takeover declaration at the latest.

At present, only micro-entrepreneurs must file an ACRE application with the URSSAF. Entrepreneurs starting another business legal structure do not need to file an ACRE application.

The URSSAF remains the only competent authority to process applications and notify applicants of its decision. 

This scheme is now available to every person who starts or takes over a business. It enables its beneficiaries to be exempted from social security contributions temporarily for a period of 12 months.