﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Availability of a business name" -->
<!-- var(key)="en-creation_entreprise-prealables-5-disponibilite_du_nom_de_lentreprise" -->
<!-- var(translation)="Pro" -->
<!-- var(lang)="en" -->

Availability of a business name
===========================================

A business can be identified by different names. In any case, **it is necessary to ensure of their availability** prior to using them. This can be done via a prior art search taking various categories of name in databases into account.  The objective is to ensure that the chosen name is not already used by a rival registered company either as a trade name or company name, or as a trademark to label products or services related to the future activity, or even as the address (domain name) of an existing website.

## Ensuring that your name is not already used as a company or trade name

You have the possibility to carry out a **first-level** search on the [data.inpi.fr](https://data.inpi.fr/) database of the National institute of industrial property (*Institut national de la propriété industrielle* (INPI)) for free to ensure that your name is not already used as a company or trade name. This database contains the data from the National registry of trade and companies and from the INSEE's SIRENE database.

This is a first step. **It is recommended to carry out an in-depth search** (considering all the orthographic, phonetic and intellectual similarities) on trade and company names. This service may be purchased online on the [INPI's website](https://www.inpi.fr/fr/disponibilite-marque-et-societe-en-france).

## Ensuring that your name is not already used as a trademark

You may carry out a **first-level** search by searching the [data.inpi.fr](https://data.inpi.fr/) database on the INPI's website for free.

This is a first step. **It is recommended to carry out an in-depth search** (considering all the orthographic, phonetic and intellectual similarities) on trade and company names. This service may be purchased online on the [INPI's website](https://www.inpi.fr/fr/disponibilite-marque-et-societe-en-france).

## Ensuring that your name is not already used as a domain name

You have the possibility to search the [AFNIC's database](https://www.afnic.fr/noms-de-domaine/tout-savoir/whois-trouver-un-nom-de-domaine/) (French association for cooperative internet naming) to ensure that your name is not already used as a domain name (.fr domain names).

This is a first step. **It is recommended** that you complete this first-level search with an **in-depth search**, which you can purchase on the INPI's website. This search consists in [a package](https://www.inpi.fr/fr/disponibilite-marque-societe-et-domaine-en-france) comprising a similarity search among trademarks and company names and an identical search among .fr, .eu and generic top-level domain names (.com, .org, etc.).

 You may also purchase [additional services](https://www.inpi.fr/fr/services-et-prestations/2704) from the INPI.

## Other procedures

If you wish to register a trademark in addition to starting your business, you will find all the information on trademark registration on [inpi.fr](https://www.inpi.fr/fr/proteger-vos-creations/proteger-votre-marque/les-etapes-cles-du-depot-de-marque).

If you wish to register a domain name in addition to starting a business, you need to contact a registrar accredited by the AFNIC. This registrar will act as a compulsory intermediary between you and the AFNIC for any request to register a domain name. Find out about domain name registration on the [AFNIC's website](https://www.afnic.fr/en/your-domain-name/how-to-choose-and-create-your-domain-name/).