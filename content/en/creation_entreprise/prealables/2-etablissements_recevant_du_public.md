﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright Â© Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Establishments open to the public" -->
<!-- var(keywords)="Establishments open to the public" -->
<!-- var(page:title)="Establishments open to the public" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-creation_entreprise-prealables-2-etablissements_recevant_du_public" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

Establishments open to the public
============================

**Establishments open to the public are premises open to people other than employees.**

**When a business that has to be open to the public is being set up, such as a trade shop for instance, it is necessary to meet legal standards concerning security and accessibility.**

## What is an ERP?

Establishments open to the public (ERP) are "buildings or premises people can access either freely or for a consideration, and where meetings are open to anyone or at the invitation of someone, for free or for a consideration" ([Article R. 123-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000039041081&cidTexte=LEGITEXT000006074096&dateTexte=20190901) of the Construction and Housing Code).

This concerns numerous types of establishment: trade shops, restaurants, hotels, theatres, etc.

ERPs are [classified by type](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32351) (represented by a letter), depending on their activity or the nature of their exploitation, and by category, depending on the building's capacity. 

The classification of an establishment is validated by the safety commission based on the information provided by the operator in the safety documentation filed at the town or city council.

## ERP: enhanced safety obligations

The opening of an ERP is subject to [safety obligations](https://www.service-public.fr/professionnels-entreprises/vosdroits/F31684), regarding fire hazards particularly. These obligations must be met during the conception phase of the building and while it is operated.

The applicable regulations depends on the classification of the building.

## ERP accessibility

The accessibility of an ERP is a [legal requirement](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32873).

Since the [Act of 11 February 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000809647&dateTexte=) on equal rights and opportunities, participation and citizenship of disabled people came into force, establishments open to the public must be accessible to all types of disabilities (motor, visual, hearing, mental disabilities, etc.).

When ERPs are not accessible, requests for authorisations to carry out works or for building permits to ensure compliance with ERP regulations must be filed with local authorities.

\> [Fact sheets on the definition of an ERP, the procedures to obtain an authorisation to carry out works, safety and accessibility rules are available on the service public.fr website](https://www.service-public.fr/professionnels-entreprises/vosdroits/N31782)