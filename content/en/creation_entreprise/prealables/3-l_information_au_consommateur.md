﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright Â© Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="consumer regulations information consuption counterfeiting fraud" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Informing consumers" -->
<!-- var(key)="en-creation_entreprise-prealables-3-l_information_au_consommateur" -->
<!-- var(translation)="Pro" -->
<!-- var(lang)="en" -->

# Informing consumers

**As a business manager providing products or services, you have to inform your clients.**

**The Consumer Code and Civil Code regulate the obligation to inform consumers.**

## Businesses' obligations towards their clients

As a professional, you must inform consumers on the goods and services you offer.

[Article L. 111-1 of the Consumer Code](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000020625532&cidTexte=LEGITEXT000006069565) clearly sets the obligation to inform consumers:

"Prior to the signing of a contract of sales of goods or services between the consumer and the professional, the latter legibly and comprehensibly provides the former with the following information:

1. The essential characteristics of the good or service [...],
2. The price of the good or service [...],
3. [...] the date at which the professional commits himself or herself to delivering the product or providing the service,
4. The information regarding his or her identity, postal address, phone number and mail address, and regarding his or her activities [...]".

Specific rules regarding information apply to certain types of goods or services (foodstuffs, mortgage loans, etc.).

In some cases, the seller must provide the client with specific information documents (quote, technical features, date of availability) prior to signing the contract.

## The DGCCRF, the office ensuring that consumers are informed

The [Directorate general for competition, consumption and the repression of frauds](https://www.economie.gouv.fr/dgccrf) (*Direction générale de la concurrence, de la consommation et de la répression des fraudes (DGCCRF)*) ensures that markets function smoothly, for the benefit of consumers and businesses. The DGCCRF operates:

* on all aspects of consumption (food and non-food products, services),
* at all the stages of economic activity (production, transformation, importation, distribution),
* whatever the type of business (stores, e-commerce websites or websites related to collaborative economy, etc).

The [DGCCRF's fact sheets](https://www.economie.gouv.fr/dgccrf/Publications/Vie-pratique/Fiches-pratiques-de-la-concurrence-et-de-la-consom) provide you with information on competition and consumption in a summarised form. The fact sheets are informative and regularly updated according to the evolutions of regulations.