﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="business name trade name commercial sign domain" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Business names" -->
<!-- var(key)="en-creation_entreprise-prealables-4-les_noms_de_l_entreprise" -->
<!-- var(translation)="Pro" -->
<!-- var(lang)="en" -->

# Business names

**Company name, domain name, brand name, etc., are used to identify your business.**

**Ensuring their availability and protecting them are essential steps in the creation of your business.**

## Business creation, finding a company name

Trade name, commercial sign, company name, etc., are different names that refer to different legal aspects of your business. 

### Trade name

The trade name is the name the public and the business's clients know. 

It applies to sole proprietorships or goodwill and can be chosen after the owner/founder's name.

### Company name

Company names (*dénomination sociale*) refer to trade companies and professional civil companies (*sociétés civiles professionnelles* (SCP)).

The partners of a company can freely choose a company name. It can be purely creative or refer to the company's activity. It can be used for every type of company (SARL, EURL, SA, SAS, SNC, etc.). For example: "Parfumerie DURAND".

### Non-trading company name 

A non-trading company name (*raison sociale*) is the name given to a business. It can only be made of either all or some of the partners' names. 

Unlike the company name, non-trading companies can have a company name that is not only made of (all or some of) the partners' names.

At the moment of registering the company in the [Registry of trade and companies](https://www.infogreffe.fr/formalites-entreprise/immatriculation-entreprise.html) (RCS), it is mandatory to provide the company or non-trading company name followed by, when applicable, the commercial sign ([Article R123-53 of the Commercial Code](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006256506&cidTexte=LEGITEXT000005634379&dateTexte=20070327)).

The non-trading company and company names can be used as trade names.

### Commercial sign

A commercial sign enables the general public to know the location of a commercial establishment or goodwill, which are generallly considered outlets. The commercial sign is usually put up on the establishment's front facade and enables clients to identify it. The commercial sign can be mentioned in the Registry of trade and companies (*registre du commerce et de sociétés* (RCS))

<!--
https://www.guichet-entreprises.fr/fr/creation-dentreprise/les-prealables-a-la-creation-dentreprise/les-noms-de-lentreprise-et-leur-protection/disponibilite-et-protection-des-noms-de-societes/
-->

## Business creation: Protecting a trademark

Registering a trademark is a procedure that enables any natural or legal person to acquire and protect the exploitation rights regarding a trademark. 

This procedure enables you to verify the availability of the chosen trademark and to protect you from counterfeiting and from competitors using it fraudulently. 

In France, trademarks are registered at the [National Institute of Industrial Property](https://www.inpi.fr/fr/comprendre-la-propriete-intellectuelle/la-marque) (*Institut National de la Propriété Industrielle* (INPI)).

It is necessary to choose the classes corresponding to your products and/or services following the International (Nice) Classification of Goods and Services when registering your trademark. Once the trademark has been validated and registered, its use is guaranteed on the chosen territory (at national, European or international level).

## Business creation: Choosing a domain name

If you decide to open a website for your business, you must choose and register its domain name. 

A domain name is the equivalent of a postal address on the internet. Domain names are preceded by the prefix "www." and followed by an extension such as, ".fr", ".com", ".eu", ".org", etc.

Easily memorable, representative of your activity and available, a domain name must respect the potential prior rights of third parties on this name. 

After ensuring that the desired domain name is available, you must register it with one of the registrars accredited by the [ICANN](https://www.icann.org/) (Internet Corporation for Assigned Names and Numbers).

Various organisations offer these services: AFNIC, GANDI, OVHcloud, etc.

A domain name can also be registered in the Registry of trade and companies (RCS) during the business or company registration via a procedure of modification. The administrative document needed to declare a domain name is the NDI CERFA form.