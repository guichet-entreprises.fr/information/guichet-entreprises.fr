﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Legal forms" -->
<!-- var(keywords)="Legal forms" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-creation_entreprise-prealables-0-0-les_formes_juridiques_de_l_entreprise" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

Business legal forms
====================================

**When you create your business, you must choose the legal form under which you will carry out your activity.**

**Micro-entrepreneur, EIRL (limited liability sole proprietor), EURL (limited liability sole proprietorship), SA (public limited company), SARL (limited liability company), etc.: The choice of a legal structure has consequences at tax and social level.**

**Your type of activity, the fact that you have business partners, the responsibilities you want to assume or the protection of your assets are elements that you need to take into account when you choose the legal structure of your business.**

## Starting a business on your own

You can be a **micro-entrepreneur**, a trader in your own name (sole proprietor), the single partner of a limited liability sole proprietorship (EURL) or of a single-member simplified limited company (SASU).

### Sole proprietorships (Micro-entrepreneurs and sole proprietors)

A micro-entrepreneur is an entrepreneur who comes under a [simplified scheme](../micro_entrepreneur/le_regime_micro_entrepreneur.md).

The sole proprietorship is exclusively owned by a natural person who is fully liable. The business and the entrepreneur form one and the same entity.

The business's identity corresponds to the owner's identity. The owner may add a [trade name](4-les_noms_de_l_entreprise.md) to his or her name (e.g. Marion Durand, Atelier des étoiles). 

Be careful though: The entrepreneur is fully liable for all professional debts arising from his or her personal assets.  However, the entrepreneur may secure primary residence as well as the realty that is not assigned to his or her professional activity (second home or land) by filing a declaration of non-seizability before a notary public. This declaration must be published officially.

Tradesmen, traders, industrialists and liberal professions may start their business as sole proprietors. 

Sole proprietors do not earn any salary. The entrepreneur's profits (the turnover minus costs) are his or her income. They are subjected to income tax in the category that is related to the entrepreneur's activity: industrial and commercial profits (BIC) for traders and tradesmen, or non-commercial profits (BNC) for liberal professions.

Sole proprietors come under the [*Sécurité Sociale – Indépendants*](https://www.secu-independants.fr/) scheme (SSI).

### Limited liability sole proprietorships (EIRL)

The EIRL status (*Entrepreneur Individuel à Responsabilité Limitée*) enables entrepreneurs to secure their personal assets, by allocating specific assets to their professional activity. Legally, the EIRL is a sole proprietorship. 

### Single-member simplified limited companies (SASU)

A single-member simplified limited company (*Société par Actions Simplifiées Unipersonnelle* or SASU) is a single-member company. 

A SASU, just as any other company, must have a share capital deposited into a bank account in the company's name. The share capital must be of at least €1.

A president (either a natural or a legal person) must be appointed. 

For more information on the various legal structures, read the [article on the service-public.fr website](https://www.service-public.fr/professionnels-entreprises/vosdroits/F23844).

### Limited liability sole proprietorships (EURL)

EURL limited liability sole proprietorships (*Entreprise Unipersonnelle à Responsabilité Limitée* or EURL) only have one partner, but they obey the same rules as limited liability companies (*Société A Responsabilité Limitée* or SARL, see below).

The single partner of an EURL cannot be another EURL.

An EURL has more legal and accounting obligations than an EIRL.

As soon as the EURL is set up, the single partner must have a share capital, the amount of which is set freely in the articles of association. 

If the EURL's single partner is a natural person and the manager, the business can come under the micro-entrepreneur scheme.

The share capital can be paid up in the same conditions as in the case of an SARL. 

## Starting a business with partners

### Starting an SARL (limited liability company)

A *Société A Responsabilité Limitée* (SARL) is the most popular company structure in France.

This legal structure is very simple as it only requires two partners who are liable up to the limit of their capital contribution.

#### The SARL's share capital <!-- collapsable:close -->

No minimum amount of share capital is required. An SARL can be set up for a symbolic euro.

The amount of share capital is set by the partners according to the nature of the project and to the company's capital requirements. 

Any person that makes a contribution to the company's capital automatically becomes a partner. He or she receives a number of shares in proportion to the amount of his or her contribution.

Choosing the SARL structure enables partners to secure their personal assets. In the event of financial difficulties, they are liable up to the limit of their contribution. Creditors only seize the company's assets and cannot have access to the partners' personal assets.

#### The partners of an SARL <!-- collapsable:close -->

An SARL must have between 2 to 100 partners. 

The partners of an SARL may be natural persons (individuals) of legal persons (legal structures such as companies or associations coming under the law of 1901).

There is no minimum age requirement to contribute to the capital of an SARL.

An SARL can have only one member. It is therefore referred to as a single-member SARL or limited liability sole proprietorship (Entreprise Unipersonnelle à Responsabilité Limitée or EURL).

#### The contribution of the SARL's partners <!-- collapsable:close -->

The contributions can be in cash (sum of money), in kind (premises, equipment, etc.) or in services, which means that a person makes his or her technical knowledge, know-how or services available to the company. 

In the case of a contribution in kind or in services, the procedure is cumbersome (a valuer of contributions in charge of appraising the contribution must be appointed).

Contributors are entitled to capital shares, can vote during general meetings and earn dividends. 

Voting and profit rights are calculated in proportion to each partner's number of shares.

In order to prevent deadlocks, it is recommended to sign a partnership agreement clarifying certain operating rules.

#### The SARL's manager <!-- collapsable:close -->

An SARL is managed by one or several manager(s) appointed by the partners. 

The manager performs all the acts of management in the company's interest: signing contracts, hiring employees, legal actions, etc. As the company's legal representative, the manager acts in its name and on its behalf. 

The manager is also in charge of holding general meetings. The partners can limit the manager's authority. Indeed, the performance of certain acts can be dependent on the general meeting's prior authorisation.

At tax and social level, the manager's status varies if he or she is a majority or minority partner.

His or her liability is limited in proportion to the amount of his or her contribution. However, in the event of mismanagement, the manager's property can be seized.

#### SARL and taxation <!-- collapsable:close -->

At tax level, an SARL is theoratically subject to corporate tax. 

Under certain conditions, the partners of an SARL may decide unanimously to choose the partnership (*société de personnes*) tax regime, which consists in subjecting the profits to the income tax. All the profits of the SARL are then paid to the partners in proportion to their contribution. The partners are then subject to the income tax in the BIC/BNC category (industrial and commercial profits/non-commercial profits). The income tax option applies during the first five years of the SARL and is valid for five financial years. The company is then automatically subject to the corporate tax.

#### Selling the shares of an SARL <!-- collapsable:close -->

Any partner wishing to sell his or her shares of an SARL must obtain the approval of the other partners beforehand. This procedure is called the approval procedure. 

There is an exception however: The shares can be freely sold to another partner or member of his or her family (spouse, parents or children) unless otherwise provided in the articles of association.

### Starting a *société d'exercice libéral* <!-- collapsable:close -->

A *société d’exercice libéral (SEL)* enables members of liberal professions to carry out their activity as a limited company.

The activity is carried out via existing legal structures, and it is possible to team up with other professionals from the same profession.

A *société d'exercice libéral* cannot be used to carry out various types of activity and must be registered in the registry of trade.

#### SELARL, SELAFA, SELAS et SELCA: Various kinds of *société d'exercice libéral* <!-- collapsable:close -->

SELs are regulated by the various regulations that apply to trade companies.

There are four kinds of *société d'exercice libéral*:

* The *société d'exercice libéral à responsabilité limitée* (SELARL) which comprises at least 2 partners or one single partner for a single-member SELARL,
* The *société d'exercice libéral à forme anonyme* (SELAFA) which comprises at least 3 partners,
* The *société d'exercice libéral par actions simplifiée* (SELAS) which comprises at least 1 partner,
* The *société d'exercice libéral en commandite par actions* (SELCA) which comprises at least 4 partners.

#### The benefits of a *société d'exercice libéral* <!-- collapsable:close -->

A *société d'exercice libéral* (SEL) has certain advantages with regard to taxation and the payment of social security contributions:

* BNCs (non-commercial profits) are taken into account to determine the net taxable amount of the worker and his or her base for social security contributions,
* The taxation of non-deductible expenses is made through corporate tax rather than through the taxation of the natural person's income,
* Professionals can profit from limited liability up to the level of their contribution while maintaining the independence bestowed by their status of members of a liberal profession,
* They also profit from specific regulations with regard to the transfer of their shares.

#### Social security schemes of the partners and executives of a *société d'exercice libéral* <!-- collapsable:close -->

The social security scheme that applies depends on the kind of company and the position of the partner or executive within the company.

The minority partners of an SELARL and the executives of a SELAFA or SELAS come under the social security scheme of workers whose status is equivalent to employees. 

The majority partners of an SELARL, the executives of a single-member SELARL and of a SELCA are subject to the social security scheme of self-employed workers.

## Other legal strucures

\> [More information on other business legal structures](https://www.service-public.fr/professionnels-entreprises/vosdroits/F23844) (service-public.fr)