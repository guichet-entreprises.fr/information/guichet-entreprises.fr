<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| The Guichet Entreprises information repository is licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International licence.
|
| Please visit the following address to obtain a copy of that licence:
| http://creativecommons.org/licenses/by-nd/4.0/
| or send a mail to: Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="micro-entrepreneur; micro-entreprise; services" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur: Service activities" -->
<!-- var(key)="en-creation_entreprise-micro_entrepreneur-micro_entrepreneur_activites_service" -->
<!-- var(translation)="None" -->
<!-- var(lang)="en" -->

# Micro-entrepreneur: Service activities

For some years now, service activities have developed in part due to the creation of the micro-entrepreneur scheme.

This concerns numerous activities: this article focuses on three of them.

## Personal services <!-- collapsable:open -->

Personal services comprise 26 activities carried out at the customer's home: childcare, support to vulnerable people, housekeeping, academic support, DIY tasks, etc.

Only two of these activities cannot be carried out by micro-entrepreneurs: childminder services, which can only be carried out as an employee, and gardening services, which come under the mutual insurance for farmers scheme (*Mutualité sociale agricole (MSA)*).

Calling on a service provider offers an important advantage as it gives the possibility to benefit from a 50% tax credit. The micro-entrepreneur must have an accreditation or an authorisation, or complete an optional declaration depending on the personal service activity that is carried out.

Personal service activities that require a declaration are listed in section II of [Article D. 7231-1](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=9F2A07A5DF68DA4DD211F8DF632EC683.tplgfr29s_1?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000018521280&dateTexte=20180803&categorieLien=cid#LEGIARTI000018521280) of the Labour Code.

**Activities that require a declaration:**

- Housekeeping and household chores,
- Small gardening tasks,
- Small DIY tasks,
- In-home childcare of children above 3,
- In-home academic support,
- In-home beauty treatment for vulnerable people,
- In-home preparation of meals,
- In-home meal delivery,
- In-home grocery delivery,
- In-home collection and delivery of ironed laundry,
- In-home IT support,
- Grooming of pets and pet-sitting for vulnerable people,
- In-home maintenance, housekeeping and temporary surveillance,
- In-home administrative assistance,
- Transport of children above 3,
- Remote assistance,
- Sign language interpretation,
- Assistance to people needing temporary assistance at home,
- Driving of temporarily disabled people's vehicle,
- Transport of temporarily disabled people,
- Coordination and provision of personal services.

**Activities that require an accreditation:**

- In-home care of children under 3 and of disabled children under 18,
- Transport of children under 3 and of disabled children under 18.

**The following activities require an accreditation if they are carried out in proxy mode (*mode mandataire*) or an authorisation if they are carried out in service provider mode (*mode prestataire*):**

- Assistance to disabled people and the elderly,
- Driving of the vehicle of people with mobility difficulties,
- Transport of people out of their home.

In order to obtain these accreditations, micro-entrepreneurs must comply with certain conditions: 

- Having a clean criminal record,
- Possessing sufficient resources, i.e. the material, human, financial resources necessary to carry out the activity,
- Complying with a condition of exclusivity, i.e. being dedicated exclusively and entirely to the provision of personal services.

**Good to know**

The proxy mode (*mode mandataire*) refers to the situation in which an individual entrusts a personal services organisation with a task via a mandate agreement: searching for candidates, recruiting and hiring staff, calculating and editing payslips and even the deduction of tax at source, etc.

The service provider mode (*mode prestataire*) applies when the person who provides services at the customer's home is employed by the personal services organisation.

For more information, please visit the [servicesalapersonne.gouv.fr](https://www.servicesalapersonne.gouv.fr) website.

## Private hire vehicle driver <!-- collapsable:open -->

The profession of private hire vehicle driver (*conducteur VTC*) is regulated: micro-entrepreneurs must complete certain procedures prior to starting their business activity.

**Obtaining a professional card**

PHV drivers must hold a professional card prior to starting their business activity as micro-entrepreneurs. 

The application for a PHV driver professional card must be completed by mail and addressed to the *préfecture*. 

This card is issued within a maximum period of 3 months and is valid for 5 years. The cost of the card is of €57.60 VAT-exclusive, plus the VAT and the sending by mail of the card, for a total of €62.09.

**Registration in the Registry of PHVs**

Once the professional card has been obtained and the micro-enterprise has been started, there is one more obligation: the registration in the Registry of PHVs (*Registre des VTC*).

Registration in the Registry of PHVs is mandatory. The registration application can be completed [online](https://registre-vtc.din.developpement-durable.gouv.fr/public/accueil.action).

The supporting documents necessary for the registration are: 

- A certificate of professional liability insurance,
- A proof of registration of the business,
- A copy of the professional card,
- A vehicle registration certificate (*carte grise*),
- A D1 registration extract,
- A proof of financial guarantee for each vehicle used on a regular basis (except for exceptional circumstances such as exhibitions, etc.), the amount of which is of €1,500 for each vehicle. The financial guarantee is not necessary if the operator owns the vehicle used for the provision of the VTC service or if the vehicle is rented on a long-term basis (more than 6 months). In this case, any supporting document proving that the operator owns the vehicle or that he or she rents it on a long-term basis must be submitted.

Registration fees are of €170.

## Delivery driver <!-- collapsable:open -->

The public health situation of the past few months has caused an increase in the home delivery of both meals and parcels.

Specialised internet platforms work with self-employed delivery drivers who are paid on a commission basis for the most part.

More often than not, people who wish to carry out this business activity choose the micro-entrepreneur scheme, whether they make deliveries with a bicycle or a motor vehicle (motorcycle, car, etc.).

**How to become a delivery driver**

A self-employed delivery driver carries out a commercial business actvity, which requires being of age and living in France.

Foreigners of non-EU countries living in France must hold a residence permit authorising them to carry out a self-employed business activity in France.

It is possible to be both a delivery driver working as a micro-entrepreneur and a student, jobseeker, employee, etc.

**What are the prerequisites to become a delivery driver?**

There are prerequisites to become a delivery driver. First and foremost, it is necessary to own a means of transport that suits the type of parcel to be delivered: bicycle, motorcycle, car or van.

A smartphone is also necessary. Internet platforms contact delivery drivers in a specific area via a mobile application to deliver a parcel.

**Bicycle delivery driver**

No professional qualification or training is necessary to work as a bicycle delivery driver.

**Motor vehicle delivery driver**

Delivery drivers working with motor vehicles must be registered in the National registry of road hauliers (*Registre National des Transporteurs*) in order to obtain a transport licence: licence to transport light goods (below 3.5 tons).

This licence is issued following a paid training programme of about 100 hours concluded with a written exam.

The authorisation application and the application for registration in the Registry of road hauliers must be submitted to the *Direction régionale de l’environnement, de l’aménagement et du logement (DREAL)* (or to the [*Direction régionale et interdépartementale de l’équipement et de l’aménagement (DRIEA)*](http://www.driea.ile-de-france.developpement-durable.gouv.fr/contactez-nous-a4575.html?lang=fr&forcer_lang=true) if you are located in the Île-de-France region).

For more information on the procedures to complete, please visit the [service-public.fr](https://www.service-public.fr/professionnels-entreprises/vosdroits/F31849) website.

**Good to know**

All internet platforms do not request the certificate of professional capacity. Please note that in the absence of this certificate, you could face charges in the event of a control or accident.

**Taking out insurance**

In addition to the mandatory car insurance, it is recommended to take out professional liability insurance (*RC Pro*) to cover you in the event of third-party damage (exemple: knocking over a pedestrian).

This insurance is optional for bicycle delivery drivers, but it is mandatory for motor vehicle delivery drivers.