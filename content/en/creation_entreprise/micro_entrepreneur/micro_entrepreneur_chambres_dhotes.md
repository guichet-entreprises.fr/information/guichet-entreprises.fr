<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| The Guichet Entreprises information repository is licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International licence.
|
| Please visit the following address to obtain a copy of that licence:
| http://creativecommons.org/licenses/by-nd/4.0/
| or send a mail to: Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="micro-entrepreneur; micro-entreprise; bed; breakfast" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur: Operating a bed and breakfast" -->
<!-- var(key)="en-creation_entreprise-micro_entrepreneur-micro_entrepreneur_chambres_dhotes" -->
<!-- var(translation)="None" -->
<!-- var(lang)="en" -->

# Micro-entrepreneur: Operating a bed and breakfast

In 2020, bed and breakfasts accounted for 10% of commercial tourist accomodation in France. Around 21,500 individuals rented out a part of their home  (source: [Accueillir Magazine](https://www.accueillir-magazine.com/pour-proprietaires-chambres-hotes/secteur-chambres-hotes.html)).

This business activity, which can be the main activity or a secondary activity, is governed by Articles [L. 324-3](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006813153/) and [D. 324-13](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006813408) et seq. of the Tourism Code.

Bed and breakfast rooms are defined as "furnished rooms located in private homes used to accomodate tourists for a fee, for one or more night(s), and offering services".

This activity can be carried out as a micro-entrepreneur but must comply with certain obligations.

## Obligations of the bed and breakfast operator

Renting out bed and breakfast accommodation comes with several obligations:

- This activity is subject to a prior declaration to the mayor.
- The renter must use his or her home to accommodate guests.
- The maximum capacity is of 5 rooms and 15 people.
- The service comprises accomodation in a furnished room, the provision of breakfast and linen, and the cleaning of the room which cannot be charged as a supplement.
- A minimum level of comfort (room of at least 9m2, access to bathroom and toilets) must be guaranteed. 

## Applicable scheme

The renting of bed and breakfast accommodation is a commercial business activity. Therefore, the relevant center for business formalities (*Centre de Formalités des Entreprises (CFE)*) is the Chamber of Commerce and Industry (*Chambre de Commerce et d’Industrie (CCI)*) and the operator must be registered in the Registry of Trade and Companies (*Registre du Commerce et des Sociétés (RCS)*).

**Amount of tax and social security contributions:**

- The turnover must not exceed €176,200.
- The contribution rate on the turnover is of 12.8% (6.4% until the end of the third calendar quarter that follows the activity start date for entrepreneurs benefitting from the Aid to start or take over a business (ACRE).
- Since the renting of bed and breakfast rooms is subject to industrial and commercial profits (*BIC*), it is subject to a rate of payment of the income tax in discharge of 1%.
- The renter must pay an additional contribution of 0.1% of the turnover for the funding of vocational training.
- The threshold of the basic exemption from VAT that is applicable is of €85,800 the first year (a tolerance threshold of €94,300 applies the following years if the VAT threshold was not exceeded the year before).