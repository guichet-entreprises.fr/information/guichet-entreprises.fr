﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Micro-entrepreneur: Commercial agent" -->
<!-- var(keywords)="Micro-entrepreneur: Commercial agent" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-creation_entreprise-micro_entrepreneur-micro_entrepreneur_agent_commercial" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

Micro-entrepreneur: Commercial agent
==============================================

A commercial agent is an agent whose regular occupation is to independently negotiate and, if need be, sign purchase, sales, lease or service contracts on behalf and for other people. Commercial agents are real entrepreneurs who must not be subordinate to their principal, which distinguishes them from salaried salesmen (sales representatives in particular).

## Prerequisites

Commercial agents can choose to operate under various legal forms (sole proprietorship operating or not under either the [micro-entrepreneur](./le_regime_micro_entrepreneur.md) or EIRL (limited liability sole proprietor) schemes, non-trading or commercial company).

For more information, please read the article on [businesses' legal forms](../prealables/0-les_formes_juridiques_de_l_entreprise.md).

## The activity of commercial agents

The activity of commercial agents, which is defined in [Article L. 134-1](http://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=B1D7B5F3637A8F91E5735137A590921D.tpdjo08v_3?idArticle=LEGIARTI000006220397&cidTexte=LEGITEXT000005634379&dateTexte=20101125) of the Commercial Code, is a civil activity that consists in selling goods, items, processed or non-processed food, or in providing customers with accomodations such as hotel or bed and breakfast rooms, for profit.

Commercial agents permanently and independently negotiate or sign purchase, sales, lease or service contracts on behalf and for other businesses.

## Registering a commercial agent activity

You must be registered in the Special registry of commercial agents (RSAC) with the Clerks office of the commercial court (or with the court of first instance hearing commercial cases in the Bas-Rhin, Haut-Rhin and Moselle departments) of your domicile ([a registration fee is charged](https://www.service-public.fr/professionnels-entreprises/vosdroits/F23282)). You may complete this procedure online on guichet-entreprises.fr.

## Micro-social scheme and micro-enterprise tax regime

Commercial agents operating under the micro-social scheme are natural persons who come under the micro-enterprise tax regime (special non-commercial profits scheme ([BNC](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32105)) for the taxation of profits and [basic exemption from VAT scheme](https://www.service-public.fr/professionnels-entreprises/vosdroits/F21746)) to whom the micro-social scheme defined in [Article L. 133-6-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000019285616&dateTexte=&categorieLien=cid) of the Social Security Code automatically applies.

For more information, read our article on the [micro-entrepreneur scheme](./le_regime_micro_entrepreneur.md).