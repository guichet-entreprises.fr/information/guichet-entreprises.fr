﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Micro-entrepreneur: Artisan, tradesman" -->
<!-- var(keywords)="Micro-entrepreneur: Artisan, tradesman" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-creation_entreprise-micro_entrepreneur-micro_entrepreneur_artisan" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

Micro-entrepreneur: Artisan, tradesman
=========================================

Artisans (craftsmen, tradesmen or artisans) are business owners who carry out all the stages of the production process of their products, from their production to their marketing. This activity can be carried out as a main or secondary activity.

An artisanal activity is defined as the production of products or the provision of services. The activity that consists in producing, processing and fixing products and in providing services from the [BIC category](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32919) (industrial and commercial profits) can be carried out thanks to specific, practical knowledge. The *Artisanat* industry comprises three main categories: construction trades, food trades and production and service trades. Registration formalities vary depending on whether the profession is regulated or not.

An artisanal activity becomes a commercial one when a business employs more than 10 employees (or when it uses purely industrial processes in the Alsace-Moselle department).

Commercial agents operating under the micro-social scheme are natural persons who come under the micro-enterprise tax regime (special non-commercial profits scheme ([BNC](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32105)) for the taxation of profits and [basic exemption from VAT scheme](https://www.service-public.fr/professionnels-entreprises/vosdroits/F21746) to whom the micro-social scheme defined in [Article L. 133-6-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000019285616&dateTexte=&categorieLien=cid) of the Social Security Code automatically applies.

For more information, read our article on the [micro-entrepreneur scheme](./le_regime_micro_entrepreneur.md).

Some activities require **professional qualifications**. The professions concerned are listed on [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/dqp/artisanat/index.html). Artisanal activities are mentioned in the Registry of trades ([appendix to Decree No. 98-247](http://legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0F6D69D7A7595283840D4B6E1396FF9D.tpdila09v_2?idArticle=LEGIARTI000030665874&cidTexte=LEGITEXT000005625550&dateTexte=20150824)) of 2 April 1998 on artisanal qualification and the Registry of trades.

## Areas of activity subject to professional qualifications <!-- collapsable:close -->

The following activities can be carried out by a person with the proper professional qualifications only, or by a person operating under the control of a qualified superviser:

* Maintenance and repairing of vehicles and machines,
* Construction, maintenance and repairing of buildings,
* Installation, maintenance and repairing of the networks and equipment using fluids and of the equipment used for gas supply, electrical installations and the heating of buildings,
* Chimney sweeping,
* (Non-medical or paramedical) Cosmetic procedures on individuals and beauty massage,
* Preparation and production of bakery, pastry, meat, pork meat and fish products, as well as preparation and production of artisanal ice creams,
* Farrying.

There are requirements (diplomas, certificates, etc.) to carry out a regulated artisanal activity (non-exhaustive list, more information on [guichet-qualifications.fr](https://www.guichet-qualifications.fr/en/dqp/index.html)).
<87/85/84% >

Qualified artisans (the entrepreneur or the spouse if he or she is involved in the activity, or even an employee) must either hold:

* an occupational proficiency certificate,
* or a certificate of professional studies,
* or an accredited diploma or title of equivalent or superieur standing ([Act No. 96-603](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000193678&categorieLien=cid) of July 5 1996 on the development and promotion of trade and tradesmanship). In order to know the level of your diploma, you can check the National register of professional certifications (*Répertoire national des certifications professionnelles ([RNCP](http://www.rncp.cncp.gouv.fr/))*).

In the absence of a level-5 diploma or title for the activity, artisans must have an actual work experience of three years in the European Union (EU) or in a state party to the Agreement on the European Economic Area (EEA). The experience must have been obtained as a business executive, a self-employed worker or an employee in one of the trades from the list of regulated activities.

### Specific cases

#### Obligation to take out an insurance policy for the activities from the construction industry

Tradesmen working in the construction industry must refer to the professional liability insurance they took out for their activity if it is mandatory, but also the insurance company and the policy's geographic coverage on their quotes and invoices.

#### Using the *Artisan* status

In order to use the word *artisan* or its derivatives, you must hold a level-5 diploma or an equivalent in your profession or have three years of work experience. At the moment of registration, the chamber of trades and crafts (*chambre de métiers et de l'artisanat (CMA)*) will ask you to prove your eligibility in order for you to mention your status in the registry of trades and on the professional card you will be issued with.

#### Using the *Artisan d'art* (handicraftsman) status

The handicraftsman status (*artisan d'art*) is a recognition of a professional qualification in a craft trade from [the list laid down by Decree](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031941949). The handicraftsman status is granted in the same conditions as the artisan status: Holding a level-5 diploma, having a work experience of three years and specifically requesting the status.

If you have any question regarding business formalities, you need to contact the local chamber of trades and crafts of your main establishment.

#### Using the *Maître artisan* (master craftsman) or *maître artisan en métier d'art* (master craftsman in handicrafts) status

The status of master craftsman (*maître artisan*) or master craftsman in handicrafts (*maître artisan en métier d’art*) is granted to sole proprietors holding a master's qualification (*brevet de maîtrise* (BM)) in the relevant profession and having a work experience of at least two years. It is also granted to people who have been registered in the registry of trades for at least ten years and whose skills, in the absence of a diploma, are recognised via the promotion of tradesmanship (*artisanat*) or via vocational trainings.

More information on the [*Artisanat* web portal](https://www.artisanat.fr/artisan/valoriser-mon-activite/obtenir-un-titre-de-maitre-artisan).

#### Hairdressing

Someone working as a hairdresser must be permanently supervised by a tradesman with professional qualifications. Likewise, working as a mobile hairdresser requires professional qualifications.

Mobile hairdressers (the entrepreneur, one of his employees or the associate spouse) must either hold:

* a professional diploma in hairdressing,
* or a master's qualification in hairdressing,
* or a diploma or title that is or was mentioned in the [National registry of professional certifications](http://www.rncp.cncp.gouv.fr/) (RNCP) in the same domain as the professional diploma in hairdressing, with equivalent or superior standing.

For more information, read our article on the [micro-entrepreneur scheme](./le_regime_du_micro_entrepreneur.md).

## References <!-- collapsable:close -->

* [Decree No. 98-247](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000571009) of 2 April 1998 on the qualification of artisans and the registry of trades
* [Decree No. 98-246](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=B5391EAD59BB6DBBE4DF181DD395A375.tpdjo08v_2?cidTexte=JORFTEXT000000388449&dateTexte=20090831) of 2 April 1998 on the professional qualifications required for these activities laid down in Article 16 of Act No. 96-603 of 5 July 1996 on the development and promotion of trade and tradesmanship
* Articles 16 and 19 of [Act No. 96-603](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000193678&categorieLien=cid) of 5 July 1996 on the development and promotion of trade and tradesmanship
* Article 1 of [Decree No. 97-558](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000383103&categorieLien=cid) of 29 May 1997 on the preconditions to work as a hairdresser
* Article 3 of [Act No. 46-1173](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006068010&dateTexte=20091214) of 23 May 1946 regulating the preconditions to work as a hairdresser