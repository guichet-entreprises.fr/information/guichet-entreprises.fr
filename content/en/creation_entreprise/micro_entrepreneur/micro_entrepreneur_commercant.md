﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Micro-entrepreneur: Trader" -->
<!-- var(keywords)="Micro-entrepreneur: Trader" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-creation_entreprise-micro_entrepreneur-micro_entrepreneur_commercant" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

Micro-entrepreneur: Trader
==========================================

A trader is a person who carries out commercial transactions as a regular occupation ([Article L. 121-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006219167&cidTexte=LEGITEXT000005634379&dateTexte=20000921) of the Commercial Code).

The following operations are considered acts of trade by the law (Articles L. 110-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006219125) and [L. 110-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006219126&cidTexte=LEGITEXT000005634379&dateTexte=20000921) of the Commercial Code) :

* any purchase of movables for resale, whether as such or after works were performed on them
* any purchase of immovables for resale, unless the purchaser acted in order to build one or several buildings and then sell them as a whole or as individual premises
* any operation performed by an intermediary for the purchase, subscription, or sale of buildings, goodwill, property company shares or stocks
* any rental of furniture
* any operation of manufacturing, commission, or transport by land or water
* any supplying business, agency, business office, auction sale or public show business
* any foreign exchange, bank, broking operation or any issuing or managing operation of electronic money or any payment service
* any public bank operation
* any obligation between dealers, traders and bankers
* bills of exchange
* any construction operation and any purchase, sale or resale of ships for the inland or high sea navigation
* any maritime expeditions
* any purchase or sale of apparatus, devices or victuals
* any chartering or bottomry
* any insurance policy or contract regarding the sea trade
* any agreement about the salaries and rents of a crew
* any hiring of seafarers to serve in a trade ship

## Carrying out an activity in an office or a shop

Retail trade in outlets is ill-suited to the [micro-entrepreneur scheme](./le_regime_micro_entrepreneur.md) since rents and related charges cannot be deducted from the reported turnover that is taken into account for the thresholds of the micro-enterprise.

## Micro-social scheme and micro-enterprise tax regime

Micro-entrepreneurs are natural persons who come under the micro-enterprise tax regime (industrial and commercial profits scheme ([BIC](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32919)) for the taxation of profits and [basic exemption from VAT scheme](https://www.service-public.fr/professionnels-entreprises/vosdroits/F21746) to whom the micro-social scheme defined in [Article L. 133-6-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000019285616&dateTexte=&categorieLien=cid) of the Social Security Code automatically applies.

The share of social security contributions for micro-entrepreneurs carrying out a commercial activity is 12.8% of the quarterly or monthly turnover, depending on the chosen payment frequency.

Micro-entrepreneurs are entitled to statutory sick pay under certain conditions. The share of the mandatory contribution to vocational training for micro-entrepreneurs carrying out a commercial activity is 0.10% of their turnover.

For more information, read our article on the [micro-entrepreneur scheme](le_regime_micro_entrepreneur.md).

## Specificities

Micro-entrepreneurs must pay the tax for chambers of commerce and industry costs if they are not exempted from the business premises contribution, i.e. when their turnover is equal to or below €5,000.

If you have any question regarding business formalities, please contact the local chamber of commerce and industry of your main establishment.