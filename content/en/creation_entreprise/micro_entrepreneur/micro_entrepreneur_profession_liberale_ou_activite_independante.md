﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Micro-entrepreneurs practising a liberal profession or carrying out a freelance activity" -->
<!-- var(keywords)="Micro-entrepreneurs practising a liberal profession or carrying out a freelance activity" -->
<!-- var(collapsable)="close" -->
<!-- var(page:title)="Micro-entrepreneur: Liberal professions, freelance" -->
<!-- var(key)="en-creation_entreprise-micro_entrepreneur-micro_entrepreneur_profession_liberale_ou_activite_independante" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

Micro-entrepreneur: Liberal professions, freelance
==============================================================

**Liberal activities** encompass all the activities that are not salaried, agricultural, commercial or artisanal — the provision of services from the [BNC category](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32105) (non-commercial profits) in particular. These activities are carried out by self-employed service providers. These service providers have specific qualifications that they sell to third parties under their own responsibility.

**Liberal professions** refer to self-employed individuals whose usual activity is to provide services under their own responsibility, in the interest of their client or in the general interest. These services may be intellectual, technical or care services. Specific professional qualifications are necessary to carry out these activities, and ethical principles or a code of conduct must be respected. 

There are two categories of liberal professions: [regulated](https://www.guichet-qualifications.fr/en/dqp/index.html) and non-regulated professions.

**Regulated liberal professions** are classified in the liberal sector by the law. These professions must obey strict ethical rules and are supervised by their professional authority (professional association, chamber or union). The most well-known regulated liberal professions are lawyers, certified accountants and doctors. Their title is protected.

**Non-regulated liberal professions** encompass the professions that cannot be classified in another category, which means they are neither commercial, artisanal or agricultural professions and cannot come under the general employee scheme (journalists and stringers, composers, part-time workers from the entertainment industry, models, sports referees and umpires, etc.). These are generally intellectual or artistic professions. Although they belong to the category of non-regulated liberal professions, since they are not supervised by any professional authority, they can nonetheless be subject to specific regulations.

Our fact sheets on regulated activities are available in the [Regulated activities](../../../reference/en/directive-services/index.md) section.

The professions that are subject to specific regulations are listed on [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/dqp/index.html).

## Prerequisites <!-- collapsable:close -->

A micro-entrepreneur is an individual who comes under the micro-enterprise tax regime ([micro BNC tax regime](https://www.service-public.fr/professionnels-entreprises/vosdroits/F32919) (non-commercial profits) and the [basic exemption from VAT scheme](https://www.service-public.fr/professionnels-entreprises/vosdroits/F21746)) to whom the micro-social scheme defined in [Article L. 133-6-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000019285616&dateTexte=&categorieLien=cid) of the Social Security Code automatically applies.

For more information, read our article on the [micro-entrepreneur scheme](le_regime_micro_entrepreneur.md).

## Registering a liberal or freelance activity <!-- collapsable:close -->

You may register your activity on [guichet-entreprises.fr](https://forms.guichet-entreprises.fr/profilEntreprisesV3). Your registration application will then be sent on to the URSSAF for processing.

In order to benefit from the micro-entrepreneur status, your annual turnover must not exceed the threshold of the micro-enterprise tax regime, which was set at €72,600 (VAT-exclusive) for the provision of liberal services in 2020.

For more information, read our article on the [micro-entrepreneur scheme](./le_regime_micro_entrepreneur.md).

**Please note**

Liberal activities must be registered with the relevant professional association, union or chamber. The declaration of activity must be submitted to the [URSSAF](https://www.urssaf.fr/portail/home.html), which plays the role of a centre for business formalities (CFE) in this case.

The [*Sécurité sociale – Indépendants*](https://www.secu-independants.fr/) is the mandatory social protection scheme in charge of managing the health insurance of self-employed workers.

The activities that come under the [National Fund for the French Bar Associations](https://www.cnbf.fr/fr/accueil-2) (*Caisse nationale des barreaux français (CNBF)*) or under the branches of the [National pension fund for liberal professions](https://www.cnavpl.fr/) (*Caisse nationale d'assurance vieillesse des professions libérales (CNAVPL)*) other than the CIPAV cannot be carried out as a micro-entrepreneur.

### Useful links

* Declaring and paying social contributions: [The official website for the declaration of social security contributions](http://www.net-entreprises.fr/)
* Social protection: [*Sécurité sociale – Indépendants*](http://www.secu-independants.fr/)
* Pensions: [Interprofessional fund of liberal professions](https://www.lacipav.fr/)

### References

* Article 43 of [European Directive No. 2005/36/CE](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2005:255:0022:0142:fr:PDF) on the recognition of professional qualifications.
* Article 29 of [Act 2012-387](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025553296&categorieLien=id#JORFARTI000025553621) of 22 March 2012 on the simplification of law and the reduction of red tape.