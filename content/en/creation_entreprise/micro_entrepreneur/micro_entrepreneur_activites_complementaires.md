<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| The Guichet Entreprises information repository is licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International licence.
|
| Please visit the following address to obtain a copy of that licence:
| http://creativecommons.org/licenses/by-nd/4.0/
| or send a mail to: Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="micro-entrepreneur; micro-enterprise; employee; retired" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur: Secondary activities" -->
<!-- var(key)="en-creation_entreprise-micro_entrepreneur-micro_entrepreneur_activites_complementaires" -->
<!-- var(translation)="None" -->
<!-- var(lang)="en" -->

# Micro-entrepreneur: Secondary activities

Starting a business is much easier since the creation of the auto-entrepreneur status in 2009, which has become the micro-entrepreneur status since.

Designed as a way to earn supplementary income or a way to test a business idea, the micro-entrepreneur status can be used by employees, students, retirees and jobseekers to carry out a secondary activity.

## Students <!-- collapsable:open -->

In order to gain work experience, develop their network or pay their studies, students have the possibility to start an activity under the micro-entrepreneur status.

However, some activities cannot be carried out below a certain age.

### Underage students <!-- collapsable:close -->

#### Non-emancipated minors <!-- collapsable:close -->

Certain agricultural and artisanal activities or liberal professions can be carried out under the EIRL, EURL and SASU legal form by non-emancipated minors, subject to the agreement of their parents.

De facto, they cannot carry out the activities that require diplomas or specific professional qualifications (private hire vehicle driver, security guard, etc.).

As far as commercial actvities are concerned, non-emancipated minors can carry them out under the EURL or SASU legal forms only (subject to the agreement of their parents).

#### Emancipated minors <!-- collapsable:close -->

Since emancipated minors are regarded as adults, they may carry out all types of activity under any legal form (subject to the regulations in force).

If an emancipated minor wishes to be a trader, run, be member or partner of an SNC (*Société en Nom Collectif*), general partner of an SCS (*Société en Commandite Simple*) or SCA (*Société en Commandite par Actions*), he or she needs to obtain the authorisation of:

- either the guardianship judge at the time of grating emancipation,
- or the President of the Court if the minor makes the request after his of her emancipation.

### Adult students <!-- collapsable:close -->

Subject to compliance with the conditions of access to certain activities, adult students may carry out a work activity under the micro-entrepreneur status.

If a student is provided with CROUS (*Centre Régional des Œuvres Universitaires et Scolaires*) accommodation and wants to use the address as a registered office for his or her micro-enterprise, the student should make sure that it is possible.

### Tax, social and financial consequences <!-- collapsable:close -->

Carrying out an activity as a micro-entrepreneur has different consequences if the student is included in the tax household of his or her parents or not:

- If the student is included in the tax household of his or her parents and is under 25 years old, the income stemming from the micro-enterprise must be reported in the income tax return of the household.
- If the student is not included in the tax houshold of his or her parents, the student must file his or her own tax return.

Micro-entrepreneur students must pay social security contributions even if they are covered by the student social security scheme.

Financial aid to students granted by the State, such as APL housing support or grants based on eligibility criteria, are calculated based on the income reported two years before. Therefore, the micro-enterprise has no direct consequences on APL housing support or grants.

## Retirees <!-- collapsable:open -->

All retirees may start an activity as micro-entrepreneurs. This scheme is particularly well-suited for retirees who wish to keep working while receiving their pension, since the two can be cumulated under certain conditions.

The retirees who come under the general social security scheme, the social security scheme for the self-employed, the pension scheme of liberal professions and the agricultural scheme can cumulate the totality of the income stemming from their micro-enterprise and their retirement pension, provided that they acquired full pension rights and requested all of their pensions.

In other cases, the cumulation of the income stemming from the micro-enterprise and of the retirement pension is capped.

The retirees who carry out a business activity as micro-entrepreneurs must pay pension contributions as any micro-entrepreneur even though they by definition already receive a pension.

For more information on the consequences of starting a business on your pension, please contact your pension fund.

## Jobseekers <!-- collapsable:open -->

From the beginning, the micro-entrepreneur scheme has been conceived as a tool to help jobseekers return to employment.

Micro-entrepreneur and jobseeker statuses are therefore compatible, and the income that stem from them can be cumulated in full or in part.

But first and foremost, inform Pôle Emploi that you start a micro-enterprise. You will also have to declare you turnover to update the monthly amount of your unemployment benefit:

- If the turnover of your micro-enterprise is of €0, your unemployment benefit (*Allocation de Retour à l'Emploi (ARE)*) does not decrease.
- If your micro-enterprise generates profits, your *ARE* unemployment benefit decreases partially.

Pôle Emploi also converts your turnover into additional ARE days, which extends your unemployment benefit period.

## Employees <!-- collapsable:open -->

Subject to certain restrictions, you may start a micro-enterprise as an employee. These restrictions mainly have to do with your relationship with your employer (exclusivity or non-compete clause).

All employees are bound by a duty of loyalty towards their employer. You must therefore inform your employer of your intention to start a micro-entreprise (and obtain his or her authorisation).

Beyond the normal rules of correctness, an employee must not develop his or her project during working hours, use the business's resources for his or her profit or criticise the business and must respect its interests, particularly if the micro-entrepreneur wishes to carry out an activity in the same domain as his or her employer.

Breaching duty of loyalty is a cause for dismissal.