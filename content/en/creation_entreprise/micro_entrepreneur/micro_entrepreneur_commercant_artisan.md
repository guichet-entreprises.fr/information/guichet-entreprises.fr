﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur: Artisan and trader" -->
<!-- var(description)="Micro-entrepreneurs carrying out both a commercial and an artisanal activity" -->
<!-- var(keywords)="Micro-entrepreneurs carrying out both a commercial and an artisanal activity" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-creation_entreprise-micro_entrepreneur-micro_entrepreneur_commercant_artisan" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

Micro-entrepreneur: Artisan and trader
=====================================================

People who carry out both a commercial and an artisanal activity must be registered in both the [registry of trades](https://www.artisanat.fr/porteur-de-projet/lancer-mon-activite/immatriculation-au-repertoire-des-metiers) (RM) and the registry of trade and companies (RCS) ([Act of 5 July 1996](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000193678&categorieLien=cid)). The centre for business formalities (CFE) of the chamber of trades and crafts is the only competent authority to receive the registration application. These formalities can be completed on guichet-entreprises.fr.

As a consequence, an entrepreneur may carry out both [a commercial activity](micro_entrepreneur_agent_commercial.md) of purchase and sale of goods and [an artisanal activity](micro_entrepreneur_artisan.md)) of production, transformation, repairing and provision of services. They may very well be separate or related activities (such as the transformation of products or raw materials for resale that is inherent to certain retail activities).

When micro-entrepreneurs register a business with mixed activities, the main activity is the activity that generates the largest profits. 

**When micro-entrepreneurs working in the construction industry or in tradesmanship sell the raw materials coming from their works, they carry out mixed activities.**

## The micro-entrepreneur scheme with mixed activities

When micro-entrepreneurs carry out mixed activities, their turnover does not increase, but a different threshold applies to each activity.

The threshold remains at:

* €176,200 for the activities that consist in selling goods, items, materials, food for on-site or takeaway consumption, or that consist in providing accommodations,
* €72,600 for the provision of services.

If the purshase-resale activity is the main activity, the overall turnover must not exceed €176,200. Within this amount, the turnover relating to the provision of services must not exceed €72,600.

For more information, read our article on the [micro-entrepreneur scheme](./le_regime_micro_entrepreneur.md).

If you have any question regarding business formalities, you need to contact the local chamber of trades and crafts of your main establishment.