﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="The micro-entrepreneur scheme" -->
<!-- var(keywords)="The micro-entrepreneur scheme" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-creation_entreprise-micro_entrepreneur-le_regime_micro_entrepreneur" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

The micro-entrepreneur scheme <!-- collapsable:close -->
==============================

**The micro-entrepreneur scheme simplifies business creation as it requires less administrative formalities, less tax returns and less social declarations.**

**This scheme is particularly suitable for people who wish to either start a secondary activity or test a new activity.**

**However, it cannot apply to certain activities and requires not to exceed certain turnover thresholds.**

## Micro-entrepreneur: A scheme with many benefits

Formerly called auto-entrepreneur status, the micro-entrepreneur scheme was created on 4 August 2008 by the Modernisation of the Economy Act. It is the simplest scheme to start a self-employed activity. 

A micro-entrepreneur is a self-employed sole proprietor who opted in to the micro-entreprise scheme and who benefits from a specific micro-social scheme. It is not a legal status as such.

It is possible to opt in to this scheme during the declaration of activity with the relevant centre for business formalities (CFE).

Registration is mandatory and free-of-charge:

* in the registry of trades (*registre des métiers* (RM)) for artisans,
* in the registry of trade and companies (*registre du commerce et des sociétés* (RCS)) for traders.

Registration is mandatory and a registration fee applies:

* in the Special registry of commercial agents (*Registre spécial des agents commerciaux* (RSAC)) for commercial agents,
* in the Special registry of sole proprietors with limited liability (*Registre spécial des entrepreneurs individuels à responsabilité limitée* (RSEIRL)) for the members of the liberal professions who chose the EIRL legal form.

The advantages of the micro-entrepreneur status are:

* social contributions are calculated in proportion to the turnover. In the event of a €0 turnover, no social contribution is to be paid, whereas a flat rate applies in the case of the ordinary scheme,
* social contributions are collected after the turnover has been achieved,
* the business does not charge VAT, at least as long as the turnover remains below the thresholds of the basic exemption from VAT (€85,500 for purchase/sale activities and €36,500 for the provision of services).

## Micro-entrepreneur: A capped turnover

Commercial, artisanal and liberal activities can be carried out under the micro-entrepreneur scheme provided that the annual turnover does not exceed: 

* €176,200 for the sale of goods, restaurant trades or the accomodation of guests,
* €72,600 for the provision of services and liberal professions.

The turnover must be declared on [net-entreprises.fr](https://www.net-entreprises.fr/), even if it is a €0 turnover.

If the turnover exceeds the legal threshold for 2 consecutive years, the entrepreneur automatically leaves the micro-entrepreneur scheme and comes under:

* the ordinary social security scheme,
* the simplified real tax regime.

## A specific tax scheme

### The income tax

Every micro-entrepreneur is subject to the income tax. 

The profits of the micro-enterprise are calculated by taking an allowance for business expenses into account. The amount of the allowance depends on the nature of the activity, and is of: 

* 71% for the sale of goods, restaurant trades or the accomodation of guests,
* 50% for the provision of services,
* 34% for liberal professions.

In any case, a minimum tax allowance of €305 applies.

To determine the amount of the income tax scale, the allowance is taken into account in the total taxable income together with the incomes of the tax household and is subjected to the progressive income tax scale.

### The payment in discharge

The [income tax payment in discharge](https://www.impots.gouv.fr/portail/professionnel/le-versement-liberatoire) is a simplified tax regime exclusively intended for micro-entrepreneurs.

In 2020, micro-entrepreneurs for whom the tax household income of the penultimate year did not exceed €27,519 can opt in to this payment method.

This threshold applies to one share of family quotient. A 50% surcharge applies for each additional half-share, or a 25% surcharge applies for each additional quarter of share.

The amount of the payment in discharge is calculated based on the turnover. The income tax must be paid together with social contributions each month or quarter depending on the payment frequency you chose. The applicable rates are as follows:

* 1% for the sale of goods, restaurant trades or the accomodation of guests,
* 1.7% for the provision of services,
* 2.2% for liberal professions.

### The business premises contribution

Micro-entrepreneurs are subject to the business premises contribution (*cotisation foncière des entreprises (CFE)*) according to a progressive tax scale. However, during their first year of activity, micro-entrepreneurs are exempted from paying the business premisess contribution. 

Moreover, the entrepreneurs whose turnover is equal to or below €5,000 are exempted from paying the minimum rate of the business premises contribution.

### The tax for chambers of trades and crafts costs for traders and artisans

When micro-entrepreneurs are exempted from business premises contribution (turnover equal to or below €5,000), they are also exempted from tax for chambers of trades and crafts costs 

The micro-entrepreneurs carrying out a commercial or artisanal activity who are not exempted from this tax must pay a percentage of their turnover. The tax is collected together with  social security contributions.

## Opening a dedicated bank account

Since the enactment of the PACTE Act of 22 May 2019, the obligation to have a dedicated bank account for the professional activity of micro-entrepreneurs only applies when their turnover is above €10,000 for two consecutive years. 

## Change in status

Micro-entrepreneurs may modify the information about their business (trade name, new establishment or home address, change of activity, etc.) or opt in to another scheme if they remain sole proprietors. 

## Temporary or permanent cessation of activity

Micro-entrepreneurs may cease their activity at any moment, on a temporary or permanent basis. 

In order to cease their activity temporarily, micro-entrepreneurs just need to declare a €0 turnover in their monthly or quarterly declaration during a maximum period of 24 months.