<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| The Guichet Entreprises information repository is licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International licence.
|
| Please visit the following address to obtain a copy of that licence:
| http://creativecommons.org/licenses/by-nd/4.0/
| or send a mail to: Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="micro-entrepreneur; micro-entreprise; rental: goods; equipment" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur: Renting goods and equipment" -->
<!-- var(key)="en-creation_entreprise-micro_entrepreneur-micro_entrepreneur_location_biens_materiels" -->
<!-- var(translation)="None" -->
<!-- var(lang)="en" -->

# Micro-entrepreneur: Renting goods and equipment

Wedding outfits, drills, canoes, works of art, etc.: everything can be rented these days.  Using equipment temporarily, the desire for change or economic logic increasingly prompt individuals to rent all sorts of goods and equipment rather than purchase them.

All consumer durables can be rented, which can be a reason for starting a dedicated business. The micro-entrepreneur scheme applies to this kind of business. 

## What kind of goods can be rented?

The goods that can be made available for rent are called consumer durables, which means that they can be used several times and that their life cycle is quite long.

Tese products are often made available for rent:

- Sports and leisure goods,
- Furniture for various events (seminars, receptions, weddings, etc.),
- Audiovisual and IT equipment,
- DIY and gardening equipment,
- Home appliance,
- Motor and non-motor vehicles,
- Works of art,
- Luxury clothing and accessories.

## Modalities for the renting of goods and equipment

No training, certificate or diploma is necessary to rent out consumer durables.

As the renting of consumer durables is considered a provision of services, it comes under the simplified micro-social scheme and must comply with the following thresholds and contribution rates:

- The annual turnover must not exceed €72,600,
- A 50% abatement rate on the turnover must be complied with to determine the taxable profits,
- The rate of payment in discharge is of 22% of the turnover,
- The rate of the payment of the income tax in discharge is of 1.7%,
- The rate of the payment in discharge of the contribution to vocational training is of 0.2% of the turnover.


