<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| The Guichet Entreprises information repository is licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International licence.
|
| Please visit the following address to obtain a copy of that licence:
| http://creativecommons.org/licenses/by-nd/4.0/
| or send a mail to: Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.| 
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="micro-entrepreneur; micro-entreprise; services" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Micro-entrepreneur: Real estate professions" -->
<!-- var(key)="en-creation_entreprise-micro_entrepreneur-micro_entrepreneur_professions_immobilieres" -->
<!-- var(translation)="None" -->
<!-- var(lang)="en" -->

# Micro-entrepreneur: Real estate professions

Real estate is a popular sector that appeals to a lot of people. A professional card is necessary to carry out certain real estate activities, since this sector is regulated.

Estate agents cannot legally carry out this activity as micro-entrepreneurs, but commercial agents can. 

## What is the difference between estate and commercial agents?

Although estate agent and commercial agent are similar professions, they are not subject to the same regulations.

An estate agent must hold a professional card (T card) that enables its holder to perform legal acts regarding property.

The estate agent profession is de facto excluded from the micro-BIC and basic exemption from VAT schemes, and as a consequence cannot be practised by micro-entrepreneurs. 

A commercial agent working in real estate is a self-employed worker commissioned by an estate agency. Since commercial agents do not hold a professional card, they cannot perform legal acts but negotiate, advise customers and follow up real estate transactions until the end.

Consequently, commercial agents are not part of the exclusions from the basic exemption from VAT scheme provided for in Article 293 C of the General Tax Code. Therefore, they have the possibility to choose the micro-enterprise tax regime.