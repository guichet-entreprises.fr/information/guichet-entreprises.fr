<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(collapsable)="close" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:title)="Procedures to modify the information about a business" -->
<!-- var(key)="en-formalites_modification" -->
<!-- var(translation)="None" -->
<!-- var(lang)="en" -->

# The procedures to modify the information about a business

## Why completing procedures to modify the information about a business?

The information you declare when you start a business is likely to change over time.

If the information you provided when you registered your business changed, then this modification has to be declared within one month (Article R. 123-45 of the Commercial Code).

This is the case when a change occurs concerning the company name, the type of activity, the legal form, the share capital, the manager, the location of the head office, etc.

All these modifications must be declared via a procedure to modify the information about the business.

## The procedures to modify the information about a company

The modifications may concern:

- the information regarding the legal person (company name, acronym, legal form, share capital, duration, closing date of the financial year, dissolution, deregistration, etc.),
- the information regarding the senior managers (birth or usage name, nationality, domicile, first name, the spouse's status (associate or salaried spouse), company name, legal form, head office, permanent representative),
- the information regarding the establishment (transfer, opening or closure of a secondary establishment, modification of the activity, modification of the trade name or commercial sign, registration of a dormant company, modification of the proxy, etc.).

Modifying the information about a company can be subject to conditions, such as a joint decision between partners or shareholders during a general meeting. For certain types of modification, it is necessary to reach a unanimous decision. For other types of modification, a majority quorum of 2/3 or 3/4 of the attending partners' shares is enough.

The modification must also be published within one month in a bulletin of legal notices from the *département* in which the company's head office is located.

Any modification concerning registration, such as the company's type of activity, the company name, share capital, etc., must be published in a bulletin of legal notices.

If a change in the articles of association is not published, any person concerned has three years to undertake an action to compel the company to regularise this situation.

[\> More information on the procedures to modify the information about a business](https://www.service-public.fr/professionnels-entreprises/vosdroits/N31143) (service-public.fr [FR])

However, it is not necessary to complete a modification procedure concerning the filing of a company's annual accounts, the transfer of shares (except if it has consequences on the senior managers), taxes (modification of the tax regime, such as a transition from a simplified actual tax regime to a normal regime that is not due to a change of threshold), etc. 

## Modifying the information about a sole proprietorship

A sole proprietorship is not a company. If you wish your sole proprietorship to evolve, be it a simple or limited liability sole proprietorship (EIRL), and whether it comes under the micro-enterprise scheme or not, the modification procedures will concern: 

- the information regarding the entrepreneur (birth or usage name, nationality, domicile, first name, the spouse's status (associate or salaried spouse),
- the information regarding an establishment (transfer, opening or closure of an establishment, modification of the activity, modification of the trade name or commercial sign, registration of a dormant company, modification of the proxy, etc.),
- the allocation of assets (opting for the EIRL, closure of an EIRL or modification of the information regarding an EIRL, modification of the declaration of allocation of assets),
- deregistration (permanent cessation of activity).

Be careful not to forget to declare the cessation of activity of your sole proprietorship, as it could result in your having to continue to pay the business premises contribution (*cotisation foncière des entreprises* (CFE)) and social security contributions.

## Specificities for micro-entrepreneurs

A micro-entrepreneur is a sole proprietor benefitting from the sole proprietorship's simplified scheme.

The same principles apply to the business and the entrepreneur in the case of a modification. Whether the modification concerns his or her information (change of name, of home address, etc.), his or her assets, the activity itself or any aspect of the activity, the micro-entrepreneur must complete the same procedures.

Be careful though: If you wish to opt out of the micro-enterprise scheme, you will need to opt for the actual tax regime for your profits. Since this modification is purely tax-related, you will need to contact tax services directly. It is therefore not necessary to complete a procedure.