﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Online procedures" -->
<!-- var(keywords)="Online procedures" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-formalites" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

# Online procedures

Complete all the required formalities to register, modify the information about a business or cease a business activity.

**The formalities you can complete online:**

* **Registering a business**
* **Modifying the information about a business or a business activity**
* **Closing a business**

## Registering a business

Guichet-entreprises.fr enables you to complete your business creation application online. The application will be forwarded to the relevant authority for processing. In France, the centres for business formalities (*centres de formalités des entreprises (CFE)*) are in charge of registration applications. CFEs are businesses' first-level contacts. They act as an interface between French administrations and businesses.

**List of business creation procedures available on guichet-entreprises.fr:**

- Declaration of business activity for commercial agents (AC0 form)
- Declaration of liberal business activity for natural persons (except micro-entrepreneurs) (P0PL form)
- Declaration of liberal business activity for natural persons working under the micro-entrepreneur status (P0PL micro-entrepreneur form)
- Declaration of creation of an agricultural business for natural persons (P0 *agricole* form)
- Declaration of a commercial or artisanal business activity for natural persons (except micro-entrepreneurs) (P0CM form)
- Declaration of a commercial or artisanal business activity for natural persons working under the micro-entrepreneur status (P0CM micro-entrepreneur form)
- Declaration of creation of an SAS, SA, SNC, SELAFA, SELAS, *société en commandite* of foreign commercial company (M0 form)
- Declaration of setting-up of an SARL or SELARL (M0 SARL/SELARL form)
- Declaration of registration of a non-trading company (*société civile*) (M0 *société civile* form)

\> [Complete the procedure to register your business](https://welcome.guichet-entreprises.fr/)

**See also:** [User guide to starting a business on guichet-entreprises.fr](../articles/comment_creer_une_entreprise.md)

## Modifying the information about a business activity

At any moment, you can complete the procedure to modify the information about your business on guichet-entreprises.fr. The update will be taken into account by all the relevant authorities (tax services, URSSAF, INSEE, public registers, etc.). Be careful though, you must declare these modifications one month after they occurred at the latest.

\> [More information on the procedures to modify a business activity](formalites_modification.md)

**List of modification procedures on guichet-entreprises.fr:**

- Declaration of modification of a commercial agent (AC2 form)
- Declaration of modification of a liberal business for natural persons (micro-entrepreneur or not) (P2PL form)
- Declaration of modification of a commercial or artisanal business (micro-entrepreneur or not) (P2CM form)
- Declaration of modification of an agricultural business or of a lessor of rural property (P2 *agricole* form)
- Declaration of modification of an agricultural company (M2 *agricole* form)
- Declaration of modification of a legal person (except legal persons carrying out an agricultural activity as their main business activity (M2 form)

\> [Complete the procedure to modify the information about your busines](https://welcome.guichet-entreprises.fr/)

## Closing a business

You have the possibility to close and deregister your business by informing all the relevant authorities (tax services, URSSAF, INSEE, public registers, etc.) of this modification. Be careful though, this procedure must be completed within one month.

**List of modification procedures on guichet-entreprises.fr:**

- Declaration of deregistration for commercial agents (AC4 form)
- Declaration of cessation of liberal business activity for natural persons (micro-entrepreneur or not) (P4PL form)
- Declaration of closure of an agricultural business for natural persons (P4 *agricole* form)
- Declaration of deregistration of a commercial or artisanal business for natural persons (micro-entrepreneur or not) (P4CM form)
- Declaration of deregistration of a company carrying out an agricultural activity as its main business activity (M4 *agricole* form)
- Declaration of deregistration of a legal person (except legal persons carrying out an agricultural activity as their main business activity (M4 form)

\> [Complete the procedure to close your business](https://welcome.guichet-entreprises.fr/)

## For further information

\> [Closing your business (company, sole proprietor or self-employed)](https://www.service-public.fr/professionnels-entreprises/vosdroits/F23744) (service-public.fr).