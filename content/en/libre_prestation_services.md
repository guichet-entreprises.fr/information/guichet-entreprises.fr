﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Freedom to provide services" -->
<!-- var(keywords)="Freedom to provide services" -->
<!-- var(collapsable)="close" -->
<!-- var(page:title)="Freedom to provide services" -->
<!-- var(key)="en-libre_prestation_services" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

Freedom to provide services
======================

## I am a European citizen and I would like to carry out an activity on an occasional basis

The **freedom to provide services** applies to occasional and temporary services provided by nationals from the member states of the European Union [[1]](/#1) <a id="Retour1"></a> or European Economic Area <a id="Retour2"></a> [[2]](#2). In this context, obtaining the recognition of your professional qualifications is not necessary.

However, some prerequisites are necessary to perform certain professional acts. Service providers must meet certain conditions:

* They must hold diplomas, certificates or titles obtained in one of these states
* They must be based and practise their profession in a member state other than France.

For instance, this is the case when an architect who works in France provides services to a customer based in Germany. In the context of a cross-border provision of services, service providers may have to move in the destination country temporarily or provide services remotely.

The **freedom to provide services is temporary**: An entrepreneur who moves abroad for a long period of time in order to carry out an activity on a permanent basis must comply with the rules on the freedom of establishment to set up a business in the destination country.

## Conditions

Do you intend to provide services in another country than the country you work in, a member state of the EU, the EEA or Switzerland? Or do you want to make tests in another market with a view to set up a business there or fulfill an order out of your country? You are therefore providing temporary services and need to obtain an authorisation from the relevant authorities.

It is not necessary to get your professional qualifications recognised in order to provide temporary services, but you must be established in your home country. In some cases, you will have to apply for an authorisation with relevant authorities prior to providing services. The fact sheets on regulated professions are available on [guichet-qualifications.fr](https://www.guichet-qualifications.fr/en/dqp/index.html).

## Procedures

More often than not, you must contact the relevant authority in the region where you want to carry out an activity.

A number of supporting documents are necessary, some of which require a certified translation.

Thus, the professions that can go through the [European Professional Card](https://www.guichet-qualifications.fr/en/comprendre/carte_professionnelle_europeenne.html) procedure can depart from the usual procedure.

**Good to know**: If your profession is regulated in your home country but not in your destination country (France), you can practise it in the same conditions as the nationals from your destination country. The freedom to provide services only concerns natural and legal persons established in a member state of the EU or the EEA.

As far as certain activities are concerned, a prior declaration containing supporting documents must be addressed to the relevant authority prior to providing any services. Please contact the authority that supervises your profession.

Furthermore, service providers must prove by any means that they have sufficient knowledge of French to provide their services.

<a id="1"></a> [[1]](#Retour1) **Member states of the European Union (EU)** — [27 countries]: Germany, Austria, Belgium, Bulgaria, Cyprus, Croatia, Denmark, Spain, Estonie, Finland, France, Greece, Hungary, Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, Netherlands, Poland, Portugal, Romania, Slovakia, Slovenia, Sweden, Czechia.

<a id="2"></a> [[2]](#Retour2) **Member states of the European Economic Area (EEA)** — [30 countries]: it comprises the 27 member states of the European Union (EU), which are listed above, plus Norway, Iceland and Liechtenstein.

**See also**: [I am a European citizen and I would like to set up a business in France (freedom of establishment)](libre_etablissement.md).