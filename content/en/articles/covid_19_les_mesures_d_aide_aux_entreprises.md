﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright Â© Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:font_sizer_off)="off" -->
<!-- var(page:breadcrumb_off)="off" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="coronavirus covid19 covid aid" -->
<!-- var(page:title)="COVID-19: Relief measures for businesses" -->
<!-- var(key)="en-articles-covid_19_les_mesures_d_aide_aux_entreprises" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

COVID-19: Relief measures for businesses
=============================================

In response to the COVID-19 epidemic, the French government implemented a series of relief measures. For more information on the implementation of the set of measures, on the useful contact details to help you complete procedures, etc., visit the following address:

[https://www.economie.gouv.fr/coronavirus-soutien-entreprises](https://www.economie.gouv.fr/coronavirus-soutien-entreprises)

If you have any question regarding the consequences of the COVID-19 epidemic on your business, you can send an email to the following address: [**covid.dge[@]finances.gouv.fr**](mailto:covid.dge[@]finances.gouv.fr)