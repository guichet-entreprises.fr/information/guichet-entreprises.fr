﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="User guide to starting a business on guichet-entreprises.fr" -->
<!-- var(keywords)="User guide to starting a business on guichet-entreprises.fr" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-articles-comment_creer_une_entreprise" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

User guide to starting a business on guichet-entreprises.fr
=========================================================

Whatever the sector or [legal form](../creation_d_entreprise/les_prealables_a_la_creation_d_entreprise/les_formes_juridiques/les_formes_juridiques_de_l_entreprise.md) of the business you want to start, guichet-entreprises.fr enables you to complete all the formalities related to business creation: registration, declaration and payment of the fees that may be required.

You can now do everything from home on one single, secure interface, even if you practise a [regulated profession](https://www.guichet-qualifications.fr/en/index.html).

## Business creation <!-- collapsable:close -->

In order to start a business and give it a legal existence, you must register your business.

Guichet-entreprises.fr enables you to complete your business creation application online. The application will be forwarded to the relevant authority for processing. In France, the centres for business formalities (*centres de formalités des entreprises (CFE)*) are in charge of registration applications. CFEs are businesses' first-level contacts. They act as an interface between French administrations and businesses.

The registration of your business is a four-step process on guichet-entreprises.fr:

1. Creation of your personal account (or connection using your [FranceConnect](https://franceconnect.gouv.fr/) login details) ;
2. Creation of the business itself (be it a sole proprietorship or a company) ;
3. Authorisation request or prior declaration (which can be performed for certain [regulated activities](../../reference/en/directive-services/index.md)) ;
4. general validation of the application.

## Creating a user account <!-- collapsable:close -->

Click on *My account* > [Registration](https://account.guichet-entreprises.fr/users/new).

You must then fill in the form that appears on screen (the form fields followed by an asterisk are required):

- « Courriel » (email): This address will be your default user ID. All the notifications will be sent to the email address you provided.
- « Nom » (surname): You should enter your family name.
- « Prénom » (first name): You should enter your usual first name(s).
- « Mot de passe » (password): Your password must be at least eight characters, including:
  - at least one figure,
  - at least one upper-case letter,
  - at least one lower-case letter,
  - at least one special character,
- « Confirmer le mot de passe » (confirm your password): Enter the above password one more time.

Other form fields must be filled in: recovery email address, phone number, country, secret question and answer.

When you filled in all the fields, click on the « *Valider* » button (Approve) to finalise the creation of your account.

## Registering a business: Compile your application on guichet-entreprises.fr <!-- collapsable:close -->

Once your account is activated, you may start creating your business strictly speaking by clicking on the « [Connexion](https://account.guichet-entreprises.fr/) » button (connection).

You must fill in the required form fields concerning:

1. your activity
2. its location
3. its legal status

Once you filled in these fields, you will not be able to modify the information you provided.

Then, the business creation process strictly speaking can start. You will have to enter various pieces of information concerning the activities of your business, fill in the forms and upload the required supporting documents (for instance, a copy of an ID).

**Good to know**: In order to upload the supporting documents, digital copies of the documents are necessary (PDF format is authorised if it does not exceed 2MB).

You will have the possibility to record your data at any moment and come back to complete your application by logging in to your account. Your centre for business formalities (CFE) will be indicated throughout the whole step. You will be able to contact it once your application has been validated and sent via guichet-entreprises.fr if you have any question regarding its follow-up or processing.

### Regulated activities

It is necessary to require an authorisation or prior declaration before starting certain [regulated activities](../../reference/en/directive-services/index.md) ([hairdresser](../../reference/en/directive-services/autres-services/coiffeur-en-salon.md), [butcher](../../reference/en/directive-services/alimentation/boucher.md), etc.) . The guichet-entreprises.fr website allows you to apply for authorisations or make prior declarations for a number of regulated activities. This possibility will be extended to other regulated activities at a later stage.

If you carry out a regulated activity and if the required authorisation application or prior declaration is not available on the website at the moment, you will only be able to start your business strictly speaking. In this case, you will have to contact the relevant authority to apply for the required authorisation or make the required prior declaration.

### Professional qualification

If you declare an artisanal activity that requires to be registered in the registry of trades (*répertoire des métiers (RM)*), you will have to prove your professional qualifications. You can prove your artisanal professional qualifications on guichet-entreprises.fr when you compile your business creation application.

## Validation of the application and payment of the fees <!-- collapsable:close -->

Once you created your business and, if need be, once you applied for an authorisation or made your prior declaration when your activity is regulated, you will have a year to finalise the validation of your application and pay the fees online.

Prior to this final stage, you will have the possibility to read over and/or print the various elements of the application.

## What happens once you validated your application on guichet-entreprises.fr? <!-- collapsable:close -->

When your validated your application on guichet-entreprises.fr, it is automatically forwarded to your centre for business formalities (CFE).

This CFE will process your application. If additional documents are necessary to process your application, you will be notified via mail by the CFE.

**Good to know**: If you have any question regarding the follow-up or processing of your application, you will need to contact the CFE in charge of processing your application.