﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Guichet Entreprises joins the European project START EASY" -->
<!-- var(keywords)="interreg START EASY " -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-articles-guichet_entreprises_s_associe_au_projet_europeen_start_easy" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

Guichet Entreprises joins the European project START EASY
===========================================================

The Guichet Entreprises service is a partner of the START EASY project to foster the competitiveness of start-ups.

The START EASY project was initiated by the government of Catalonia with the support of the [Interreg Europe](https://www.interregeurope.eu/)programme. The project brings together the competent authorities from several European countries (Belgium, Spain, France, Italy, Lithuania, Poland) to create a conducive environment for growth.

The project aims at creating a conducive environment for business creation and the competitiveness of start-ups via the development of tools and the elaboration of specific policies.

The development of the one-stop shop model, administrative simplification and the generalisation of the "once-only" principle are part of the work of the project's partner.

## What is the Interreg Europe programme?

Interreg Europe helps national, regional and local authorities across Europe to deliver and implement better policies. By creating an environment and opportunities to share solutions, the objective of Interreg Europe is to ensure that investments, innovation and implementation efforts all facilitate durable integration for the citizens and regions concerned.

In order to achieve this objective, Interreg Europe enables regional and local public authorities to share their ideas regarding and experience of the implementation of public policies to improve the strategies for citizens and regions.

**Links:**

* [Website of the START EASY project](https://www.interregeurope.eu/starteasy/)
* [START EASY on Twitter](https://twitter.com/StartEasyEU)
* [START EASY on Facebook](https://www.facebook.com/StartEasyEU/)
* [Website of the Interreg Europe programme](https://www.interregeurope.eu/about-us/what-is-interreg-europe/)