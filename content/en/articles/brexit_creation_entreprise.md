<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| ____ _  _ _ ____ _  _ ____ ___    ____ _  _ ___ ____ ____ ___  ____ _ ____ ____ ____ 
| | __ |  | | |    |__| |___  |     |___ |\ |  |  |__/ |___ |__] |__/ | [__  |___ [__  
| |__] |__| | |___ |  | |___  |     |___ | \|  |  |  \ |___ |    |  \ | ___] |___ ___] 
|                                                                                      
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| The Guichet Entreprises information repository is licensed under the Attribution-NonCommercial-NoDerivatives 4.0 International licence.
|
| Please visit the following address to obtain a copy of that licence:
| http://creativecommons.org/licenses/by-nd/4.0/
| or send a mail to: Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.| 
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="Brexit: Starting a business in France when you are British" -->
<!-- var(key)="en-articles-brexit_creation_entreprise" -->
<!-- var(translation)="Pro" -->
<!-- var(lang)="en" -->

# Brexit: The new rules British citizens need to comply with to start a business in France

Since 1 January 2021 and the entry into force of the Brexit agreement, British citizens who wish to start a business in France must comply with new rules.

## You lived in France before 1 January 2021

Until **30 September 2021**, British citizens who lived in France before 1 January 2021 do not need to obtain a residence permit to live in France or a work permit to carry out a professional activity.

Consequently, you may start a business in France without a residence permit until this date.

From 1 October 2021, holding this permit will be mandatory. [A specific "Brexit" residence permit](https://contacts-demarches.interieur.gouv.fr/brexit/brexit-demande-titre-sejour/) must be requested before 1 July 2021.

## You did not live in France before 1 January 2021

British citizens who did not live in France before 1 January 2021 must obtain a residence permit to start a business in France. Residence permit requests must be made in the relevant prefecture.