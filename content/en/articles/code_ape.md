﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="APE code" -->
<!-- var(keywords)="Code APE NAF" -->
<!-- var(collapsable)="close" -->
<!-- var(key)="en-articles-code_ape" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

# APE code

## What is the APE code? <!-- collapsable:open -->

The INSEE (*Institut national de la statistique et des études économiques* or National institute of statistics and economic studies) issues a code to each business and each of its establishments. This code describes its main activity based on the French nomenclature of activities (*nomenclature d'activités française* (NAF)) when it is registered in the [SIRENE directory](http://www.insee.fr/fr/methodes/default.asp?page=definitions/sys-inf-rep-nat-ent-etab.htm).

The APE code (*activité principale exercée* or main activity) consists of five characters: four digits and one letter. This piece of information is essential for business statistics as it forms the basis of the classification of businesses by fields of activity.

## How can I find my APE code?? <!-- collapsable:open -->

If you forgot your code, you can contact the INSEE. Do not forget to provide your SIREN number. You can also find it [on the INSEE's website](https://avis-situation-sirene.insee.fr/) if you accepted your information to be public.

This code is mainly used to compile statistics. The INSEE uses it to classify businesses by fields of activity.

## Modifying my APE code <!-- collapsable:open -->

If your activity changed, your modification request must be addressed to your centre for business formalities (CFE) for it to perform the necessary procedures. If you are a micro-entrepreneur, this form can be completed directly via the guichet-entreprises.fr website.

If your activity did not change and if you think the APE code issued by the INSEE is incorrect, your modification request must be sent via mail to the [INSEE's regional directorate](http://www.insee.fr/fr/service/default.asp?page=entreprises/sirene/sirene_dr.htm) responsible for the department in which your head office or establishment is located. A printable [APE modification form](http://www.insee.fr/fr/information/2015441) is available for you to complete on the [INSEE's website](http://www.insee.fr).

## Why is my APE code not recognised by the guichet-entreprises.fr website? <!-- collapsable:open -->

It is important that you comply with the APE code's exact syntax because the code will not be accepted in the event of an error (space, missing letter or digit, etc.).

If, after verification, your code is still not accepted, it means that the corresponding activity is not included in Directive 2006/123/CE of the European Parliament and of the Council of December 12 2006 on services in the Single Market. In this case, this activity is not included in the scope of online procedures available on the guichet-entreprises.fr website. Indeed, the guichet-entreprises.fr website is not competent for the procedures to start a business, modify the information about a business or cease a business activity in the public or parapublic sectors.

This is the case for the following activities:

* pension funds,
* general public administration,
* public administration (administrative authority) in the sectors of health, vocation training, culture and social services, except security,
* public order and security activities,
* fire and emergency services,
* general social security activities,
* management of supplementary pension schemes,
* social distribution of income,
* employer organisations' activities, chambers of commerce, industry and trade's activities,
* communication equipment repair,
* trade unions' activities,
* religious organisations' activities,
* political organisations' activities,
* other organisations working through voluntary membership,
* extraterritorial organisations and authorities' activities.