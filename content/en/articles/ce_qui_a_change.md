﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="What changed on January 2018" -->
<!-- var(keywords)="What changed on January 2018" -->
<!-- var(collapsable)="open" -->
<!-- var(key)="en-articles-ce_qui_a_change" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

What changed on January 2018
===================================

Here is a selection of changes that occurred on January  2018.

## Micro-enterprise: The turnover thresholds change

Since 1 January 2018, the turnover thresholds for micro-enterprises increased. Henceforward, the thresholds are set at:

* €170,000 for the sale of goods,
* €70,000 for the provision of services.

*More information to come, subject to the publication of regulatory decrees.*

## Self-employed workers: the RSI becomes the *Sécurité Sociale – Indépendants*

The Social protection scheme for the self-employed (*Régime social des indépendants* (RSI)) was transformed to become the *Sécurité Sociale – Indépendants* (Social security scheme for the self-employed). Changes will occur in 2018 and 2019 according to an official schedule set in the 2018 Social Security Financing draft law and after the probable publication of regulatory decrees.

[> Keep up with the transformations of the scheme in 2018 and 2019](http://secu-independants.fr/)

## Affiliation to the AGIRC and ARRCO supplementary pension scheme

Since 1 January 2018, any newly created business is exempted from the obligation to sign up with a supplementary pension fund. A business must only sign up with a supplementary pension fund when it hires its first employee.  

In the past, a business had to become affiliated with an AGIRC and ARCCO supplementary pension fund as soon at it was created.  

This simplification measure affects 80% of businesses, including SMEs and *micro-entrepreneurs*.

[> More information](https://www.agirc-arrco.fr/)

*Non-exhaustive list*