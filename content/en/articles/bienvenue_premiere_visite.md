﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="First steps on guichet-entreprises.fr" -->
<!-- var(keywords)="First steps on guichet-entreprises.fr" -->
<!-- var(collapsable)="open" -->
<!-- var(key)="en-articles-bienvenue_premiere_visite" -->
<!-- var(lang)="en" -->
<!-- var(translation)="Pro" -->

First steps on guichet-entreprises.fr
======================================

**Optimise your visit!** We recommend the use of the Mozilla Firefox or Google Chrome internet browers in order for your visit on guichet-entreprises.fr to be more pleasant. Indeed, some browsers are likely not to support certain functionalities of the Guichet Entreprises website.

Simple, quick and secure, the guichet-entreprises.fr service enables you to complete your administrative procedures online. Guichet-entreprises.fr is the point of single contact for business creation. Free and accessible to all users, this online service enables citizens to compile an application from home.

## Start your business <!-- collapsable:close -->

Did you come up with an idea? Did you find the fundings to get started? Did you choose your legal status? Then your business is ready to be registered!

The business creation formalities are collected by the centre for business formalities (*centre de formalités des entreprises (CFE)*). Thanks to guichet-entreprises.fr, you can complete these formalities [online](../formalites.md) without actually going to the relevant CFE. Indeed, depending on the nature of your professional activity (artisanal, commercial, agricultural activity, liberal profession, etc.), the centre for business formalities varies.

**See also** :

[> User guide to starting a business on guichet-entreprises.fr](./comment_creer_une_entreprise.md)
<br>
[>List of centres for business formalities (CFE) depending on the nature of a professional activity](https://www.insee.fr/fr/information/1972060)

## Modifying the information about a business and ceasing a business activity <!-- collapsable:close -->

On guichet-entreprises.fr, you can also update the information about your business with all the relevant authorities (tax services, URSSAF, INSEE, public registers, etc.).

Finally, you can cease your micro-entrepreneur activity and deregister your sole proprietorship (the formality to cease a business activity is not available online for companies yet). The update will be effective with all the relevant authorities (tax services, URSSAF, INSEE, public registers, etc.).

## Create an account and log in <!-- collapsable:close -->

In order to use the online service, please [create an account](https://account.guichet-entreprises.fr/user/create) on guichet-entreprises.fr. Thanks to this account, you will be able to create and manage your application(s) and modify your details. If you already have an account on [Impots.gouv.fr](https://www.impots.gouv.fr/portail/), [Ameli](https://www.ameli.fr/) or the LaPoste website, you can use the [FranceConnect](https://franceconnect.gouv.fr/) authentication to log in to guichet-entreprises.fr thanks to one of these accounts.

## Compile and validate your application online <!-- collapsable:close -->

The guichet-entreprises.fr website allows you to create an application online, attach the supporting documents and pay the fees that may be required for the procedure. Once your application is completed and approved, it is sent to the relevant authority for processing. To follow up your application, you will have the possibility to contact its recipient at any moment.

## Searching information about a regulated activity <!-- collapsable:close -->

Some regulated activities ([hairdresser](../../reference/en/directive-services/other-services/hairdresser-in-a-salon.md), [butcher](../../reference/en/directive-qualification-professionnelle/skilled-trades/butcher.md), [estate agent](../../reference/en/directive-qualification-professionnelle/consultancy-and-expertise/estate-agent-commonhold-association.md), [lawyer](../../reference/en/directive-qualification-professionnelle/consultancy-and-expertise/lawyeradvocate.md), etc.) require an authorisation or prior declaration before starting an activity. Guichet-entreprises.fr provides factsheets about regulated activities in order for citizens to know which conditions they must meet and which procedures they must follow to start a business when the activity is regulated.

**Did you know?** In France, more than 100 activities are regulated, the list of which is available in the [Regulated activities](../../reference/en/directive-services/index.md) section.